/**
 * 
 */
package com.meccabingo.desktop.page;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class HomePage {

	// By logoBingoHeader = By.xpath("//div[contains(@id,'header')]/header/a/img");
	// ////a[@class='logo' and @title='Mecca Bingo']/img[@alt='Mecca Bingo']
	// //*[@id=\"header\"]/header/a/img
	// By footer_UsefulLink = By.xpath("//ul[@class='useful-links']");
	// By footer_partnersLogo = By.xpath("//ul[@class='partners']");
	// By footer_privacyAndSecurityBlock = By.xpath("//div[@class='footer-row'] ");
	// By footer_PaymentProvidersBlock = By.xpath("//*[contains(text(),'Secure
	// Payments')]"); //div[@class='footer-payments']
	// By slider = By.xpath("(//div[@class='hero-carousel-slide-text'])[1]");
	// //(//div[@class='hero-carousel-slide-text'])[1]//span[contains(text(), 'Play
	// Book of Ra and Win2')]");
	// By loginButton = By.xpath("//button[contains(text(),'Login')]");
	// By myAccounticon = By.xpath("//i[@class='my-account']");

	By logoBingoHeader = By.xpath("//div[contains(@id,'header')]/header/a/img");
	By footer_Social = By.xpath("//ul[@class='footer-social']"); // div[@class='footer-social']
	By MyAccountButton = By.xpath("//a[contains(@class,'open-myaccount')]");
	By BalanceText = By.xpath("//div/apollo-balance-block/span[contains(@class,'balance-label')]");
	By BalanceAmount = By.xpath("//div/apollo-balance-block/strong");
	By BalanceToggle = By.xpath("//div[contains(@class,'balance-toggle')]");
	By BalanceDots = By.xpath("//div/apollo-balance-block/strong");
	By searchedGame_PlayNow_btn = By.xpath("//*[@id=\"overlay-level-1\"]/section/div/section/div/div/div/div/button");
	By bagroundimage = By.xpath("//*[contains(@class,'game-window-image')]"); // *[contains(@id,'Desktop')]/body/canvas
	By search_loupe = By.xpath("//button[contains(@class,'search-open-btn')]");
	By DepositButton = By.xpath("//button[contains(text(),'Deposit')]");
	By Message = By.xpath("//span[contains(@class,'messages')]");
	By NavigationMenu = By.xpath("//ul[contains(@class,'top-navigation')]");
	By CookiesText = By.xpath("//p[contains(text(),'cookie')]");
	By CookiesButton = By.xpath("//button[contains(.,'Continue')]");
	By TopArrowAnchor = By.xpath("//apollo-scroll-top");

	private WebActions objWebActions;
	private LogReporter logReporter;

	public HomePage(WebActions webActions, LogReporter logReporter) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
	}

	public void verifyHeaderLogo() {

		logReporter.log("Check mecca logo displayed", objWebActions.checkElementDisplayed(logoBingoHeader));

	}

	public void scrollToFooterSocial() {
		objWebActions.scrollToElement(footer_Social);
	}

	public void setBrowserSize(String width, String height) {
		getElementSizeBeforeResize();
		objWebActions.setBrowserWindowSize(width, height);
		// logReporter.log("Set browser window > >",
		// objWebActions.setBrowserWindowSize(width, height));
		// objUtilities.assertEquals("Set browser window > >", true,
		// objWebActions.setBrowserWindowSize(width, height));
	}

	public void verifySocialMediaBlockSize() {

		if (this.socialMediaBlock_width > objWebActions.getWidth(footer_Social)) {
			logReporter.log("Check social media block size after window resize: ", true);
		} else
			logReporter.log("Check social media block size after window resize: ", false);
	}

	private int socialMediaBlock_width = 0;

	private void getElementSizeBeforeResize() {
		socialMediaBlock_width = objWebActions.getWidth(footer_Social);
	}

	public void scrollToFooterUsefullLink() {
		By footer_UsefulLink = By.xpath("//ul[@class='useful-links']");
		objWebActions.scrollToElement(footer_UsefulLink);
	}

	public void scrollToPartnersLogo() {
		By footer_partnersLogo = By.xpath("//ul[contains(@class,'partners')]");
		objWebActions.scrollToElement(footer_partnersLogo);
	}

	public void scrollToprivacyAndSecurityBlock() {
		By footer_privacyAndSecurityBlock = By.xpath("//div[@class='footer-row']");
		objWebActions.scrollToElement(footer_privacyAndSecurityBlock);
	}

	public void scrollToPaymentProvidersBlock() {
		By footer_PaymentProvidersBlock = By.xpath("//*[contains(text(),'Secure Payments')]");
		objWebActions.scrollToElement(footer_PaymentProvidersBlock);
	}

	public void clickOnSlider() {
		By slider = By.xpath("(//div[@class='hero-carousel-slide-text'])[1]");
		logReporter.log("click on 'slider' > >", objWebActions.click(slider));
	}

	public void clickOnLogInButton() {
		By loginButton = By.xpath("//button[contains(text(),'Login')]");
		logReporter.log("click on 'LogIn button' > >", objWebActions.click(loginButton));
	}

	public void verifyMyAccount() {

		logReporter.log("Verify my acccount button displayed' > >",
				objWebActions.checkElementDisplayed(MyAccountButton));
	}

	public void windowRefresh() {
		objWebActions.pageRefresh();

	}

	public void verifyRegisterNowLink() {
		By registernowlink = By.xpath("//a[contains(text(),'Not a member')]");
		logReporter.log("Verify register now link", objWebActions.checkElementDisplayed(registernowlink));
	}

	public void verifyHeaderJoinNowbtn() {
		By joinnowbtn = By.xpath("//a[contains(text(),'Join Now')]");
		logReporter.log("Verify join now button", objWebActions.checkElementDisplayed(joinnowbtn));
	}

	public void verifybattenberg() {
		By battenberg = By.xpath("//apollo-battenberg");
		logReporter.log("verify batternberg block", objWebActions.checkElementDisplayed(battenberg));
	}

	public void verifyBlockInOneRow() {
		By onerowblock = By.xpath("/html/body/main/section[4]");
		logReporter.log("Verify block", objWebActions.checkElementDisplayed(onerowblock));
	}

	public void verifyThreeBlock() {
		By blockone = By.xpath(
				"//section[contains(@class,'three-columns')]/apollo-card-block[contains(@class,'boxed card-block')][1]");
		By blocktwo = By.xpath(
				"//section[contains(@class,'three-columns')]/apollo-card-block[contains(@class,'boxed card-block')][2]");
		By blockthree = By.xpath(
				"//section[contains(@class,'three-columns')]/apollo-card-block[contains(@class,'boxed card-block')][3]");
		logReporter.log("Verify block one", objWebActions.checkElementDisplayed(blockone));
		logReporter.log("Verify block two", objWebActions.checkElementDisplayed(blocktwo));
		logReporter.log("Verify block three", objWebActions.checkElementDisplayed(blockthree));

	}

	public void verifyUserBalanceSection() {
		logReporter.log("verify my account button", objWebActions.checkElementDisplayed(MyAccountButton));
	}

	public void verifyUserBalanceTitle() {
		logReporter.log("verify balance text", objWebActions.checkElementDisplayed(BalanceText));
	}

	public void verifyUserBalanceAmount() {
		logReporter.log("verify balance amount", objWebActions.checkElementDisplayed(BalanceAmount));
	}

	public void clickOnUserBalanceTitle() {
		if (!objWebActions.click(BalanceText))
			logReporter.log("verify balance text", true);
	}

	public void clickOnUserBalanceToggle() {
		logReporter.log("verify balance toggle", objWebActions.click(BalanceToggle));
	}

	public void clickOnUserBalanceDots() {
		logReporter.log("verify balance toggle", objWebActions.click(BalanceDots));
	}

	public void clickOnPlayNowButton() {
		logReporter.log("click on play now button", objWebActions.click(searchedGame_PlayNow_btn));
	}

	public void verifyBackroungimageInGame() {

		logReporter.log("check element > >", objWebActions.checkElementDisplayed(bagroundimage));
	}

	public void verifyHeaderComponents(String text) {
		switch (text) {

		case "Mecca Logo": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(logoBingoHeader));
			break;
		}

		case "Navigation menu": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(NavigationMenu));
			break;
		}

		case "Search loupe icon": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(search_loupe));
			break;
		}

		case "Deposit button": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(DepositButton));
			break;
		}
		case "Balance amount/dots": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(BalanceAmount));
			break;
		}
		case "Balance text": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(BalanceText));
			break;
		}
		case "Hide/Unhide balance toggle": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(BalanceToggle));
			break;
		}
		case "My account icon": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(MyAccountButton));
			break;
		}
		case "number of unread messages": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(Message));
			break;
		}

		}

	}

	public void NavigateToInvalidUrl() {
		logReporter.log("navigate to invalid url", objWebActions.navigateToInvalidUrl());
	}

	public void verifyErrorTitle() {
		By error = By.xpath("//h1[contains(text(),'404 Error')]");
		logReporter.log("verify 404 error title", objWebActions.checkElementDisplayed(error));
	}

	public void verifyCookiesText() {
		logReporter.log("verify cookies text", objWebActions.checkElementDisplayed(CookiesText));
	}

	public void verifyCookiesButton() {
		logReporter.log("verify cookies text", objWebActions.checkElementDisplayed(CookiesButton));
	}

	public void clickOnTopArrowAnchor() {
		logReporter.log("click on top arrow link", objWebActions.click(TopArrowAnchor));
	}
	
	public void scrollTowardsDownOfPage() {
		By locator = By.xpath("//span[text()='Watch us on']");
		objWebActions.scrollToElement(locator);
	}
}
