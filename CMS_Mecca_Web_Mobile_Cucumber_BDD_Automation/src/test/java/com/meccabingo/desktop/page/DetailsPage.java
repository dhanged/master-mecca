package com.meccabingo.desktop.page;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;

public class DetailsPage {

	private WebActions webActions;
	private LogReporter logReporter;

	public DetailsPage(WebActions webActions, LogReporter logReporter) {
		this.webActions = webActions;
		this.logReporter = logReporter;

	}

	By briefdescription = By.xpath("//div[contains(@class,'game-info')]/p");
	By backroundimageofgame = By.xpath("//img[contains(@class,'game-info-background')]");
	By mainimageofgame = By.xpath("//div[contains(@class,'game-info-image')]/img");
	By informationboxofgame = By.xpath("//div[contains(@class,'game-info-content-inner')]");
	By joinnow = By.xpath(
			"/html/body/main/div/section[1]/div/div[2]/div/apollo-bingo-info-actions/apollo-play-bingo-cta/button");

	By joinnowinbox = By
			.xpath("/html/body/main/div/section[2]/section/div/apollo-bingo-info-actions/apollo-play-bingo-cta/button");
	By casino = By.xpath("//a[contains(@class,'top-navigation-link') and contains(@href,'online-casino')]");

	public void navigateToLiveCasinoPgae() {
		logReporter.log("click on casino option from header", webActions.click(casino));
	}

	public void verifyLoginDetailsPage(String nameofgame) {
		String pageurl = "https://qa01-mecc-cms2.rankgrouptech.net/games/" + nameofgame;
		logReporter.log("verify login details page of game", webActions.getUrl().equals(pageurl));
	}

	public void verifyTitleOfGame(String nameofgame) {
		By titleofgame = By.xpath("//div[contains(@class,'game-info')]/h1[contains(text(),'" + nameofgame + "')]");
		logReporter.log("verify title of game", webActions.checkElementDisplayed(titleofgame));
	}

	public void verifyBriefDescription() {
		logReporter.log("verify brief description", webActions.checkElementDisplayed(briefdescription));
	}

	public void verifyBackroundImageOfGame() {
		logReporter.log("verify background image of game", webActions.checkElementDisplayed(backroundimageofgame));
	}

	public void verifyMainImageOfGame() {
		logReporter.log("verify main image of game", webActions.checkElementDisplayed(mainimageofgame));
	}

	public void verifyInformationBoxOfGame() {
		webActions.scrollToElement(informationboxofgame);
		logReporter.log("verify information box", webActions.checkElementDisplayed(informationboxofgame));
	}

	public void scrollUpTheBox() {
		logReporter.log("scroll up the box", webActions.scrollToElement(mainimageofgame));
	}

	public void scrollDownTheBox() {
		logReporter.log("scroll up the box", webActions.scrollToElement(joinnowinbox));

	}

	public void clickOnJoinNowfromDetailsPage() {
		logReporter.log("", webActions.click(joinnow));
	}

	public void verifyGameLaunchesInNewWindow() {
		String windowTitle = "sta.bingo.meccabingo.com";
		logReporter.log("verify game launches in new window", webActions.switchToWindowUsingTitle(windowTitle));
	}

	public void verifyJoinNowButtonInInformationBox() {
		logReporter.log("", webActions.checkElementDisplayed(joinnowinbox));
	}

	public void verifyTextInInformationBox(String text) {
		By texts = By.xpath("//p/span[contains(text(),'" + text + "')]");
		logReporter.log("", webActions.checkElementDisplayed(texts));
	}

	public void verifyNextGameStartsAtText() {
		By locator = By.xpath("//h4/span[contains(text(),'Next game starts at')]");
		logReporter.log("", webActions.checkElementDisplayed(locator));
	}

	public void verifyAVAILABLEONText() {
		By availableon = By.xpath("//h6[contains(text(),'Available on')]");
		logReporter.log("", webActions.checkElementDisplayed(availableon));
	}

	public void verifyHelpText(String helptext, String type) {
		By helptexts = By.xpath("//p/span[contains(text(),'" + type
				+ "')]/parent::p/parent::li/button/div/p[contains(text(),'" + helptext + "')]");
		logReporter.log("verify help text", webActions.checkElementDisplayed(helptexts));

	}

	public void hoverOnIButton(String type) {
		By ibuttton = By.xpath("//p/span[contains(text(),'" + type + "')]/parent::p/parent::li/button/i");
		logReporter.log("hover on i button", webActions.mouseHover(ibuttton));
	}
}
