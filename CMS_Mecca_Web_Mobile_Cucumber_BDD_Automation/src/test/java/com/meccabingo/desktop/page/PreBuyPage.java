package com.meccabingo.desktop.page;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.webdriver.DriverProvider;

public class PreBuyPage {
	WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;

	public PreBuyPage(DriverProvider driverProvider, WebActions webActions,
			LogReporter logReporter) {
		this.objDriverProvider = driverProvider;
		this.objWebActions = webActions;
		this.logReporter = logReporter;
	}

	public void selectTicket(String str) {
		By locator = By.xpath("//div[contains(@class,'pre-buy-popup-buttons')]/button[text()='"+str+"']");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click tickets> >", objWebActions.click(locator));
	}

	public void clickPreBuyButton() {
		By locator = By.xpath("(//button[text()='Pre-Buy'])[1]");
		logReporter.log("click pre-buy button > >", objWebActions.clickUsingJS(locator));
	}
	
	public void verifyPreBuyPopupDisplayed() {
		By locator = By.xpath("//section[contains(@class,'pre-buy-popup')]");
		logReporter.log("check pre-buy popup displayed > >", objWebActions.checkElementDisplayedWithMidWait(locator));
	}

}
