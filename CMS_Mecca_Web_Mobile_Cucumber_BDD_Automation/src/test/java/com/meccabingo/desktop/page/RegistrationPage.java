/**
 * 
 */
package com.meccabingo.desktop.page;

import org.openqa.selenium.By;
import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.mifmif.common.regex.Generex;

public class RegistrationPage {
	private WebActions webActions;
	private LogReporter logReporter;
	private Utilities utilities;
	private WaitMethods wait;

	public RegistrationPage(WebActions webActions, LogReporter logReporter, WaitMethods wait, Utilities utilities) {
		this.webActions = webActions;
		this.logReporter = logReporter;
		this.utilities = utilities;
		this.wait = wait;

	}

	String randomEmail;

	String greencolor = "#009D7A";
	String greycolor = "#5ECCF3";
	String redcolor = "#B41D68";

	By emailcheckbox = By.xpath("//input[contains(@id,'gdpr-email')]");
	By yesbutton = By.xpath("//button[contains(@name,'join-with-email')]");
	By selectall = By.xpath("//button[contains(@class,'colour-button-green')]");
	By SMScheckbox = By.xpath("//input[contains(@id,'gdpr-sms')]");
	By phonecheckbox = By.xpath("//input[contains(@id,'gdpr-phone')]");
	By postcheckbox = By.xpath("//input[contains(@id,'gdpr-post')]");
	By username = By.xpath("//input[contains(@id,'username')]");
	By password = By.xpath("//input[contains(@id,'password')]");

	By title = By.xpath("//h4[contains(text(),'Title')]");
	By firstname = By.xpath("//input[contains(@id,'firstname')]");
	By surname = By.xpath("//input[contains(@id,'surname')]");
	By dateofbirth = By.xpath("//div[contains(@class,'ip-dob-inner')]");
	By day = By.xpath("//input[contains(@id,'dob-day')]");
	By month = By.xpath("//input[contains(@id,'dob-month')]");
	By year = By.xpath("//input[contains(@id,'dob-year')]");
	By notyou = By.xpath("//a[contains(text(),'Not You')]");

	By Ms = By.xpath("//button[contains(.,'Ms')]");
	By Mr = By.xpath("//button[contains(.,'Mr')]");
	By Miss = By.xpath("//button[contains(.,'Miss')]");
	By Mrs = By.xpath("//button[contains(.,'Mrs')]");
	By Dr = By.xpath("//button[contains(.,'Dr')]");
	By Mx = By.xpath("//button[contains(.,'Mx')]");

	By emailtextbox = By.xpath("//div[contains(@class,'join-now')]/form/fieldset[2]/div/div/input");
	By agecheckbox = By.xpath("//input[contains(@id,'id-agreed')]");
	By next = By.xpath("//div[contains(@class,'join-now')]/form/fieldset[3]/button");
	By membership = By.xpath("//input[contains(@type,'number')]");
	By secondemail = By.xpath("//input[contains(@type,'email')]");
	By mobilenumber = By.xpath("//input[contains(@id,'mobile')]");

	By EnterAddressManuallyBtn = By.xpath("//form[contains(@class,'join-now')]/fieldset[3]/button");
	By firstAddress = By.xpath("//input[contains(@id,'input-address-line1')]");
	By secondAddress = By.xpath("//input[contains(@id,'input-address-line2')]");
	By Town_City = By.xpath("//input[contains(@id,'town-city')]");
	By country = By.xpath("//input[contains(@id,'county')]");
	By postcode = By.xpath("//input[contains(@id,'address-lookup-search')]"); // input[contains(@id,'postcode')]
	By uk = By.xpath("//select[contains(@id,'input-country')]/option[contains(text(),'United Kingdom')]");
	By countrydropdown = By.xpath("//select[contains(@id,'input-country')]");
	By addressFromPostcodeSearch = By.xpath("//ul[contains(@class,'postcode-search-results')]/li[1]");

	public void clickJoindNowbtn() {
		By JoinNow = By.xpath("//a[contains(text(),'Join Now')]");
		logReporter.log("click on join now button > >", webActions.click(JoinNow));
	}

	public String enterEmailaddress() {
		String randomemail = utilities.getEmail();
		logReporter.log("enter email address", webActions.setText(emailtextbox, randomemail));
		return randomemail;
	}
//	public String getEmail() {
//		String regex = "[a-zA-Z0-9]{5}\\@mailinator\\.com";
//		this.randomEmail = new Generex(regex).random();
//		return randomEmail;
//	}

	public void enterEmailaddressOnSecondPage() {
		logReporter.log("enter email address", webActions.setText(secondemail, "6abd32@gmail.com"));
	}

	public void verifyEmailaddressOnSecondPageTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(secondemail, "border-bottom-color", greencolor));
	}

	public void enterRegisteredEmailaddress(String email) {
		logReporter.log("enter email address", webActions.setText(emailtextbox, email));
	}

	public void clickAgecheckbox() {

		logReporter.log("click on age checkbox > >", webActions.clickUsingJS(agecheckbox));
	}

	public void clickOnNextbtn() {

		logReporter.log("click on next button > >", webActions.click(next));
	}

	public void clickOnEmailCheckbox() {

		logReporter.log("click on email checkbox > >", webActions.click(emailcheckbox));
		logReporter.log("email checkbox is selected > >", webActions.isCheckBoxSelected(emailcheckbox));
	}

	public void clickOnSMSCheckbox() {

		logReporter.log("click on SMS checkbox > >", webActions.click(SMScheckbox));
		logReporter.log("sms checkbox is selected > >", webActions.isCheckBoxSelected(SMScheckbox));
	}

	public void clickOnPhoneCheckbox() {
		logReporter.log("click on Phone checkbox > >", webActions.click(phonecheckbox));
		logReporter.log("phone checkbox is selected > >", webActions.isCheckBoxSelected(phonecheckbox));
	}

	public void clickOnPostCheckbox() {

		logReporter.log("click on post checkbox > >", webActions.click(postcheckbox));
		logReporter.log("post checkbox is selected > >", webActions.isCheckBoxSelected(postcheckbox));
	}

	public void clickOnDisableRegisterCTA() {
		By register = By.xpath("//button[contains(.,'Register')]");
		logReporter.log("click on post checkbox > >", webActions.clickOnDisabledElement(register));
	}
	
	public void clickRegisterCTA() {
		By register = By.xpath("//button[@type='submit']/span[text()='Register']");
		logReporter.log("click on post checkbox > >", webActions.click(register));
	}
	public void clickOnSelectAllbtn() {

		logReporter.log("click on join now button > >", webActions.click(selectall));
	}

	public void clickNoIhaveaMembershipCardbtn() {
		By membershipbtn = By.xpath("//button[contains(@name,'join-with-membership')]");
		logReporter.log("click on No (I have a membership card) button > >", webActions.clickUsingJS(membershipbtn));
	}

	public void clickOnNotYouLink() {
		logReporter.log("click on Not You link > >", webActions.click(notyou));
	}

	public void verifyNotYouLink() {
		logReporter.log("", webActions.checkElementDisplayed(notyou));
	}

	public void verifyAllSelectedCheckboxes(String checkboxes) {
		switch (checkboxes) {
		case "email":
			logReporter.log("email checkbox is selected > >", webActions.isCheckBoxSelected(emailcheckbox));
			break;
		case "SMS":
			logReporter.log("sms checkbox is selected > >", webActions.isCheckBoxSelected(SMScheckbox));
			break;
		case "phone":
			logReporter.log("phone checkbox is selected > >", webActions.isCheckBoxSelected(phonecheckbox));
			break;
		case "post":
			logReporter.log("post checkbox is selected > >", webActions.isCheckBoxSelected(postcheckbox));
			break;
		}

	}

	public void verifyMembershipScreenOptions(String options) {
		switch (options) {
		case "membershipnumberbox":
			logReporter.log(" > >", webActions.checkElementDisplayed(membership));
			break;
		case "DOBbox":
			logReporter.log(" > >", webActions.isCheckBoxSelected(dateofbirth));
			break;
		case "agecheckbox":
			logReporter.log("p > >", webActions.isCheckBoxSelected(agecheckbox));
			break;
		case "nextbtn":
			logReporter.log(" > >", webActions.isCheckBoxSelected(next));
			break;
		}

	}

	public void verifyColorOfErrorMessage() {
		By errormessage = By.xpath("//li[contains(.,'contact preference')]");
		String errorcolor = "#e22c2c";// "rgba(226, 44, 44, 1)";
		logReporter.log("Check color of error message > >",
				webActions.checkCssValue(errormessage, "color", errorcolor));
	}

	public String enterUsername() {
		String randomUserName = "opst_" + utilities.getUsername();
		logReporter.log("enter email address", webActions.setText(username, randomUserName));
		return randomUserName;
	}

	public void enterInvalidUsername() {
		String regex = "[a-zA-Z]{4}";
		String randomusername = new Generex(regex).random();
		logReporter.log("enter email address", webActions.setText(username, randomusername));
	}

	public void verifyUsernameTextboxColor() {

		logReporter.log("Check color of error message > >",
				webActions.checkCssValue(username, "border-bottom-color", greencolor));
	}

	public void enterExistingUsername(String existingusername) {
		logReporter.log("enter username", webActions.setText(username, existingusername));
	}

	public void verifyErrorMessages(String errormessage) {
		By error = By.xpath("//li[contains(text(),'" + errormessage + "')]");
		logReporter.log("Check error message", webActions.checkElementDisplayed(error));
	}

	public void verifyAllFieldsOnPage(String textboxes) {
		switch (textboxes) {
		case "Title":
			logReporter.log("Check title text ", webActions.checkElementDisplayed(title));
			break;
		case "First name":
			logReporter.log("Check firstname field", webActions.checkElementDisplayed(firstname));
			break;
		case "Surname":
			logReporter.log(" Check surname field", webActions.checkElementDisplayed(surname));
			break;
		case "Date of birth":
			logReporter.log("Check dateofbirth field", webActions.checkElementDisplayed(dateofbirth));
			break;
		case "Username":
			logReporter.log(" Check username field", webActions.checkElementDisplayed(username));
			break;
		case "Password":
			logReporter.log(" Check password field", webActions.checkElementDisplayed(password));
			break;
		}
	}

	public void verifyAllTitlesOnPage(String titles) {
		switch (titles) {
		case "Ms":
			logReporter.log("Check title 'Ms' is displayed", webActions.checkElementDisplayed(Ms));
			break;
		case "Mr":
			logReporter.log("Check title 'Mr' is displayed", webActions.checkElementDisplayed(Mr));
			break;
		case "Miss":
			logReporter.log("Check title 'Miss' is displayed", webActions.checkElementDisplayed(Miss));
			break;
		case "Mrs":
			logReporter.log("Check title 'Mrs' is displayed", webActions.checkElementDisplayed(Mrs));
			break;

		}

	}

	public void enterFirstName() {
		String regex = "[a-zA-Z]{4}";
		String randomfirstname = new Generex(regex).random();
		logReporter.log("enter firstname", webActions.setText(firstname, randomfirstname));
	}

	public void verifyFirstNameTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(firstname, "border-bottom-color", greencolor));
	}

	public void enterSurName() {
		String regex = "[a-zA-Z]{4}";
		String randomsurname = new Generex(regex).random();
		logReporter.log("enter surname ", webActions.setText(surname, randomsurname));
	}

	public void verifySurNameTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(surname, "border-bottom-color", greencolor));
	}

	public void enterDateOfBirth(String Day, String Month, String Year) {
		logReporter.log("Enetr day", webActions.setText(day, Day));
		logReporter.log("Enetr day", webActions.setText(month, Month));
		logReporter.log("Enetr day", webActions.setText(year, Year));
	}

	public void verifyDateOfBirthTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(dateofbirth, "border-bottom-color", greencolor));
	}

	public String enterPassword() {
		//String randomPassword = utilities.getPassword();
		String randomPassword = "Test@1234";
		logReporter.log("enter password", webActions.setText(password, randomPassword));
		return randomPassword;
	}

	public void enterInvalidPassword() {
		String regex = "[a-zA-Z]{8}";
		String randompassword = new Generex(regex).random();
		logReporter.log("enter password", webActions.setText(password, randompassword));
	}

	public void verifyPasswordTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(password, "border-bottom-color", greencolor));
	}

	public void verifyPasswordTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(password, "border-bottom-color", greycolor));
	}
	public void verifyRedColorBelowPassword() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(password, "border-bottom-color", redcolor));
	}
	public void verifyUsernameTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(username, "border-bottom-color", greycolor));
	}
	public void verifyRedColorBelowUsername() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(username, "border-bottom-color", redcolor));
	}
	public void verifyEmailTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(secondemail, "border-bottom-color", greycolor));
	}

	public void verifyMobileNumberTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color", greycolor));
	}

	public void verifyHelpTextBelowInput(String hinttext) {
		By helptextbelowinput = By.xpath("//span[contains(text(),'" + hinttext + "')]");
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkElementDisplayed(helptextbelowinput));
	}

	public void verifyYesBtnIsSelected() {
		String color = "#d35b94";
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(yesbutton, "background-color", color));

	}

	public void verifyEmailTextboxColorOnFirstPage() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(emailtextbox, "border-bottom-color", greencolor));
	}

	public void verifySelectAllbtn() {
		logReporter.log("Verify select all button", webActions.checkElementDisplayed(selectall));
	}

	public void clickOnDisabledNextbtn() {
		By next = By.xpath("//div[contains(@class,'join-now')]/form/fieldset[3]/button");
		logReporter.log("click on next button > >", webActions.clickOnDisabledElement(next));
	}

	public void verifyNextbtnIsEnabled() {

		logReporter.log("Verify next button is enabled", webActions.click(next));
	}

//	public void verifyHelpbtnDisappear() {
//		By helpbtn = By.xpath("//a[contains(@class,'livechat')]");
//		if (!webActions.checkElementDisplayed(helpbtn)) {
//			logReporter.log("check help button dissappears", true);
//		}
//
//	}

	public void verifyErrorOnFirstRegPage(String message) {
		By errormessage = By.xpath("//ul[contains(@class,'errors')]/li[contains(text()," + message + ")]");
		logReporter.log("click on next button > >", webActions.checkElementDisplayed(errormessage));
	}

	public void enterInvalidEmailaddress() {
		String regex = "[a-zA-Z0-9]{5}\\@gmail\\com";
		String randomInvalidEmail = new Generex(regex).random();
		By textbox = By.xpath("(//div[contains(@class,'join-now')]/form/fieldset[2]/div/div/input)[1]");
		logReporter.log("enter email address", webActions.setText(textbox, randomInvalidEmail));
	}

	public void enterMembershipNumber(String number) {
		logReporter.log("enter Membership Number", webActions.setText(membership, number));
	}

	public void clickOnDrtitle() {
		logReporter.log("click on title mx", webActions.click(Mrs));
	}

	public void verifyMrstitleIsSelected() {
		String color = "#A94976";//"#d35b94";
		By locator = By.xpath("//button[@value='Mrs' and contains(@class,'button-pink')]");
		logReporter.log("Mrs highlighted",
				webActions.checkElementDisplayed(locator));

	}

	public void verifyFirstRegPage() {
		logReporter.log("verify first registration page", webActions.checkElementDisplayed(next));
	}

	public void enterMobileNumber() {
		String regex = "[1-9]{9}";
		String randomnumber = new Generex(regex).random();
		logReporter.log("enter mobile number", webActions.setText(mobilenumber, randomnumber));
	}

	public void enterInvalidMobileNumber() {
		String regex = "[a-zA-Z]{8}";
		String randomnumber = new Generex(regex).random();
		logReporter.log("enter mobile number", webActions.setText(mobilenumber, randomnumber));
	}

	public void verifyMobileNumberTextboxColor() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color", greencolor));
	}

	public void verifyUKasDefaultValue() {

		logReporter.log("verify default value as UK", webActions.checkElementDisplayed(uk));
	}

	public void selecyUKInDropdown() {
		By locator = By.xpath("//option[text()='United Kingdom']");
		logReporter.log("select second option from dropdown", webActions.click(locator));
	}

	public void selectRepublicOptionInCountry() {
		By locator = By.xpath("//option[text()='Republic Of Ireland']");
		logReporter.log("select second option from dropdown", webActions.click(locator));
	}

	public void verifyEnterAddressManuallyBtn() {
		logReporter.log("check Enter Manually button", webActions.checkElementDisplayed(EnterAddressManuallyBtn));
	}

	public void clickOnEnterAddressManuallyBtn() {
		logReporter.log("click on enter address manually button", webActions.click(EnterAddressManuallyBtn));
	}

	public void verifyEnterAddressManuallyBtnDisappears() {
		if (!webActions.checkElementExists(EnterAddressManuallyBtn)) {
			logReporter.log("check Enter Manually button dissappears", true);
		}
	}

	public void verifyTownCityField() {
		logReporter.log("verify town and city field", webActions.checkElementDisplayed(Town_City));
	}

	public void enterValidInputInTownCityField() {
		logReporter.log("enter valid input in town/city field", webActions.setText(Town_City, "United Kingdom"));
	}

	public void verifyTownCityFieldDisappears() {
		if (!webActions.checkElementExists(Town_City)) {
			logReporter.log("check Enter Manually button dissappears", true);
		}
	}

	public void verifyGreenColorBelowTownCityField() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(Town_City, "border-bottom-color", greencolor));
	}

	public void verifyCountryField() {
		logReporter.log("verify countrys field", webActions.checkElementDisplayed(country));
	}

	public void verifyCountryFieldDisappears() {
		if (!webActions.checkElementExists(country)) {
			logReporter.log("check Enter Manually button dissappears", true);
		}
	}
    
	public void verifyPostcodeField() {
		logReporter.log("verify countrys field", webActions.checkElementDisplayed(postcode));
	}

	public void clickOnPostcodeField() {
		logReporter.log("click on postcode field", webActions.click(postcode));
	}

	public void enterValidPostcode(String Postcode) {
		logReporter.log("enter valid postcode", webActions.setText(postcode, Postcode));
	}

	public void enterInvalidPostcode() {
		logReporter.log("enter invalid postcode", webActions.setText(postcode, "11234PO167GZ"));
		try {
		wait.sleep(30);
		}
		
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void verifyRedColorBelowPostcode() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(postcode, "border-bottom-color", redcolor));
	}

	public void verifyGreyColorBelowPostcode() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(postcode, "border-bottom-color", greycolor));
	}

	public void verifyGreenColorBelowPostcode() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(postcode, "border-bottom-color", greencolor));
	}

	public void verifyHelpTextDisappearsBelowPostcode() {
		By helptext = By.xpath("//li[contains(text(),'Please enter a postcode')]");
		if (!webActions.checkElementExists(helptext)) {
			logReporter.log("help text below postcode disappear", true);
		}
	}

	public void clickOnMobileNumberField() {
		logReporter.log("click on mobile number field", webActions.click(mobilenumber));
	}

	public void verifyGreyColorBelowMobileNumber() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color", greycolor));
	}

	public void verifyGreenColorBelowMobileNumber() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color", greencolor));
	}

	public void verifyRedColorBelowMobileNumber() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color", "#E22C2C"));
	}

	public void verifyFirstAddressField() {
		logReporter.log("verify address line 1", webActions.checkElementDisplayed(firstAddress));
	}

	public void enterValidInputInFirstAddress() {
		logReporter.log("enter first address", webActions.setText(firstAddress, "washington"));
	}

	public void verifyFirstAddressFieldDissappears() {
		if (!webActions.checkElementExists(firstAddress))
			logReporter.log("", true);
	}

	public void verifyGreenColorBelowFirstAddress() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(firstAddress, "border-bottom-color", greencolor));
	}

	public void verifyRedColorBelowFirstAddress() {
		try {
			wait.sleep(30);
			}
			
			catch(Exception e) {
				e.printStackTrace();
			}
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(firstAddress, "border-bottom-color", "#E22C2C"));
	}

	public void verifySecondAddressField() {
		logReporter.log("verify address line 1", webActions.checkElementDisplayed(secondAddress));
	}

	public void enterValidInputInSecondAddress() {
		logReporter.log("enter second address", webActions.setText(secondAddress, "DC"));
	}

	public void verifySecondAddressFieldDissappears() {
		if (!webActions.checkElementExists(secondAddress))
			logReporter.log("", true);
	}

	public void verifyGreenColorBelowSecondAddress() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(secondAddress, "border-bottom-color", greencolor));
	}

	public void verifyRedColorBelowSecondAddress() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(secondAddress, "border-bottom-color", "#B3B3B3"));
	}

	public void selectAddressFromPostcodeSearch() {
		if(webActions.checkElementDisplayedWithMidWait(addressFromPostcodeSearch))
			logReporter.log("select address from postcode search", webActions.click(addressFromPostcodeSearch));
	}

	public void EnterInvalidInputInFirstAddress() {
		logReporter.log("", webActions.click(firstAddress));
		logReporter.log("", webActions.pressKeybordKeys(firstAddress, "tab"));
	}

	public void EnterInvalidInputInSecondAddress() {
		logReporter.log("", webActions.click(secondAddress));
		logReporter.log("", webActions.pressKeybordKeys(firstAddress, "tab"));
	}
	public void clearUsername() {
		logReporter.log("", webActions.clearText(username));
	}
	
	public void verifyH4HeadingDisplayed(String strText) {
		By locator = By.xpath("//h4[text()='" + strText + "']");
		if (webActions.checkElementExists(locator))
			logReporter.log("text displayed'" + strText + "' displayed >>", true);
	}
	
	public void clickCheckbox(String chkboxName) {
		By locator = By.xpath("//input[@type='checkbox' and @id='gdpr-" + chkboxName + "']");
		logReporter.log("check checkbox > >", webActions.click(locator));
	}

	public void verifyCheckboxIsChecked(String chkboxName) {
		By locator = By.xpath("//input[@id='gdpr-" + chkboxName + "']");
		String checkValue = webActions.getAttribute(locator, "value");
		if (checkValue.equals("true"))
			logReporter.log(chkboxName + "checkbox is checked :", true);
		else
			logReporter.log(chkboxName + "checkbox is checked :", false);

	}
	
	public void verifyCheckboxIsUnChecked(String chkboxName) {
		By locator = By.xpath("//input[@id='gdpr-" + chkboxName + "']");
		String checkValue = webActions.getAttribute(locator, "value");
		if (checkValue.equals("false"))
			logReporter.log(chkboxName + "checkbox is unchecked :", true);
		else
			logReporter.log(chkboxName + "checkbox is unchecked :", false);

	}
	
	public void verifyAllCheckboxesChecked() {
		By chkboxEmail = By.xpath("//input[@id='gdpr-email']");
		By chkboxSMS = By.xpath("//input[@id='gdpr-sms']");
		By chkboxPhone = By.xpath("//input[@id='gdpr-phone']");
		By chkboxPost = By.xpath("//input[@id='gdpr-post']");

		String valueEmail = webActions.getAttribute(chkboxEmail, "value");
		String valueSMS = webActions.getAttribute(chkboxSMS, "value");
		String valuePhone = webActions.getAttribute(chkboxPhone, "value");
		String valuePost = webActions.getAttribute(chkboxPost, "value");
		if (valueEmail.equals("true") == true && valueSMS.equals("true") == true && valuePhone.equals("true") == true
				&& valuePost.equals("true") == true)
			logReporter.log("All the checkboxes are checked", true);
		else
			logReporter.log("All the checkboxes are checked", false);

	}
	
	public void verifyAllCheckboxesUnChecked() {
		By chkboxEmail = By.xpath("//input[@id='gdpr-email']");
		By chkboxSMS = By.xpath("//input[@id='gdpr-sms']");
		By chkboxPhone = By.xpath("//input[@id='gdpr-phone']");
		By chkboxPost = By.xpath("//input[@id='gdpr-post']");

		String valueEmail = webActions.getAttribute(chkboxEmail, "value");
		String valueSMS = webActions.getAttribute(chkboxSMS, "value");
		String valuePhone = webActions.getAttribute(chkboxPhone, "value");
		String valuePost = webActions.getAttribute(chkboxPost, "value");
		if (valueEmail.equals("true") == false && valueSMS.equals("true") == false && valuePhone.equals("true") == false
				&& valuePost.equals("true") == false)
			logReporter.log("All the checkboxes are unchecked", true);
		else
			logReporter.log("All the checkboxes are unchecked", false);

	}
	
	public void verifyPasswordGuildelineDisplayed(String strGuideline) {

		By locator = By.xpath("//li[text()='" + strGuideline + "']");
		logReporter.log("Password guideline '" + strGuideline + "' displayed >>",
				webActions.checkElementDisplayed(locator));
	}
	
	public void verifyDrTitleDoesNotAppear() {
		By locator = By.xpath("//span[text()='Dr']");
		if(!webActions.checkElementExists(locator))
			logReporter.log("Title Dr check not displayed",true);
		else
			logReporter.log("Title Dr check not displayed",false);
		
	}

}