/**
 * 
 */
package com.meccabingo.desktop.stepDefinition;
import com.meccabingo.desktop.page.PromotionPage;
import com.generic.utils.Utilities;
import io.cucumber.datatable.DataTable;
import io.cucumber.java8.En;
/**
 * @author Datta Mehtre (Expleo)
 *
 */

public class PromotionPageStep implements En
{

	private Utilities objUtilities;
	private PromotionPage objPromotionPage;

	public PromotionPageStep(PromotionPage promotionPage,Utilities utilities) 
	{
		this.objPromotionPage = promotionPage;		
		this.objUtilities = utilities;
	
		
	When("^User click on Promotion tab$",() ->this.objPromotionPage.clickOnPramotionTab()); 
	
	Then("^Verify \"([^\"]*)\" card is available$",(String bonusCard)->this.objPromotionPage.VerifyBonusCardIsPresent(bonusCard));
	
	Then("^User verify \"([^\"]*)\" is Not claimed$", (String bonusName) -> this.objPromotionPage.VerifyBonusIsNotClaimed(bonusName));
	
	Then ("^Verify \"([^\"]*)\" display following components Before claim or OptIn :$",(String bonusName,DataTable table)->{
		for (int i = 0; i < objUtilities.getListDataFromDataTable(table).size(); i++)	{			
			this.objPromotionPage.verifyBonusCardComponents(bonusName,this.objUtilities.getListDataFromDataTable(table).get(i));
		}
	});
	
	Then ("^Verify \"([^\"]*)\" display following components After claim or OptIn :$",(String bonusName,DataTable table)->{
		for (int i = 0; i < objUtilities.getListDataFromDataTable(table).size(); i++)	{			
			this.objPromotionPage.verifyBonusCardComponents(bonusName,this.objUtilities.getListDataFromDataTable(table).get(i));
		}
	});
	
	Then("Verify \"([^\"]*)\" status on promotion details page$", (String status) -> this.objPromotionPage.VerifyStausOnPromotion(status));
	
	Then ("^Verify featured games section dispalyed on page$",()->this.objPromotionPage.VerifyFeaturedGameSectionIsDisplayed());
	
	Then ("^User Click on \"([^\"]*)\" button from \"([^\"]*)\" bonus card$",(String buttonName, String bonusCard)->this.objPromotionPage.ClickOnButtonFromBonusCard(buttonName, bonusCard));
	
	Then ("^Click on \"([^\"]*)\" button from promotion details page$",(String buttonName) ->this.objPromotionPage.ClickOnButtonsFromPromotionDetailPage(buttonName));
	
	}
}
