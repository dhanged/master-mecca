package com.meccabingo.desktop.stepDefinition;

import org.openqa.selenium.By;

import com.generic.utils.Utilities;
import com.meccabingo.desktop.page.PreBuyPage;

import io.cucumber.java.en.Then;

public class PreBuyPageStep {

	private PreBuyPage objPreBuyPage;
	Utilities utilities;

	public PreBuyPageStep(PreBuyPage preBuyPage) {

		this.objPreBuyPage = preBuyPage;
	}

	@Then("Verify pre-buy popup is displayed")
	public void verify_pre_buy_popup_is_displayed() {
		objPreBuyPage.verifyPreBuyPopupDisplayed();
	}
	
	@Then("Click pre-buy from bingo tile")
	public void click_pre_buy_from_bingo_tile() {
		objPreBuyPage.clickPreBuyButton();
	}
	
	@Then("Select ticket {string}")
	public void select_ticket(String string) {
		objPreBuyPage.selectTicket(string);
	}
	
	
}
