
package com.meccabingo.desktop.stepDefinition;

import com.generic.utils.Utilities;
import com.generic.webdriver.DriverProvider;
import com.meccabingo.desktop.page.FooterPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class FooterPageStep {

	private Utilities objUtilities;
	private DriverProvider objDriverProvider;
	private FooterPage objFooterPage;
	

	public FooterPageStep(Utilities utilities, DriverProvider driverProvider, FooterPage footerPage) {
		this.objUtilities = utilities;
		this.objDriverProvider = driverProvider;	
		this.objFooterPage = footerPage;
		
	}

	
	@Then("^Verify system displays following Social media components:$")
	public void verify_system_displays_following_Social_media_components(DataTable dt) {

		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
			objFooterPage.verifySocialMediaComponents(objUtilities.getListDataFromDataTable(dt).get(i));

		}
	}
	
	@Then("^Verify system displays Icon, Text and Name for each social media block$")
	public void verify_system_displays_Icon_Text_and_Name_for_each_social_media_block() {
		objFooterPage.verifyImageAndTextOfSocial();
	}
	
	@Then("^Click on Facebook block from footer$")
	public void click_on_Facebook_block_from_footer(){
	    objFooterPage.clickFacebookBlock();
	    
	}
	
	@Then("^Switch to child window$")
	public void Switch_to_child_window(){
		objFooterPage.switchToChild();
	}
	
	@Then("^Click on Instagram block from footer$")
	public void click_on_Instagram_block_from_footer() {
		objFooterPage.clickInstagramBlock();
	}

	@Then("^Click on twitter block from footer$")
	public void click_on_twitter_block_from_footer() {
		objFooterPage.clickTwitterBlock();
	}

	@Then("^Click on youtube block from footer$")
	public void click_on_youtube_block_from_footer() {
		objFooterPage.clickYouTubeBlock();
	}

	@Then("^Verify system navigate user to \"([^\"]*)\" url$")
	public void verify_system_navigate_user_to_url(String url){
		objFooterPage.verifyUrl(url);
	}
	
	@Then("^User mouse hover on Facebook block from footer$")
	public void user_mouse_hover_on_Facebook_block_from_footer() {
	   
		objFooterPage.mouseHoverOnFacebook();
	}

	@Then("^Verify system highlight \"([^\"]*)\" block in \"([^\"]*)\" color$")
	public void verify_system_highlight_block_in_color(String area, String colorCode) {
		objFooterPage.verifyColorCode(area, colorCode);
	}
	
	@Then("^User mouse hover on Instagram block from footer$")
	public void user_mouse_hover_on_Instagram_block_from_footer() {
		objFooterPage.mouseHoverOnInstagram();
	}

	@Then("^User mouse hover on Twitter block from footer$")
	public void user_mouse_hover_on_Twitter_block_from_footer() {
		objFooterPage.mouseHoverOnTwitter();
	}

	@Then("^User mouse hover on Youtube block from footer$")
	public void user_mouse_hover_on_Youtube_block_from_footer() {
		objFooterPage.mouseHoverOnYouTube();
	}
	
	
	@Then("^Verify following fonts are diminish for name, description, logo$")
	public void verify_following_fonts_are_diminish_for_name_description_logo(DataTable dt){
	    
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
			objFooterPage.verifyFontSizeOfElements(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}
	
	@Then("^Verify following$")
	public void verify_following(DataTable dt){
	    
	}
		
	@Then("^Verify following URLs are displaying under Useful Links block:$")
	public void verify_following_URLs_are_displaying_under_Useful_Links_block(DataTable dt){	    	    
	    for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
			objFooterPage.verifyUsefulLinksInFooter(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}
	
	@Then("^Click on Privacy Policy Link$")
	public void click_on_Privacy_Policy_Link() {
	    objFooterPage.clickPrivacyPolicyLink();
	}

	@Then("^Verify system navigates user to \"([^\"]*)\"$")
	public void verify_system_navigates_user_to(String url) {
	    objFooterPage.verifyUsefulLink(url);
	}
	
	
	@Then("^Click on Terms and Conditions Link$")
	public void click_on_Terms_and_Conditions_Link(){
	   objFooterPage.clickTermsAndConditionsLink();
	}

	@Then("^Click on Our Mecca promise Link$")
	public void click_on_Our_Mecca_promise_Link() {
		objFooterPage.clickOurMeccaPromiseLink();
	}

	@Then("^Click on Affiliates Link$")
	public void click_on_Affiliates_Link() {
	    objFooterPage.clickAffiliatesLink();
	}
	
	@Then("^Click on Mecca Club Terms Link$")
	public void click_on_Mecca_Club_Terms_Link() {
	    objFooterPage.clickMeccaClubTermsLink();
	}

	@Then("^Click on Play Online Casino Link$")
	public void click_on_Play_Online_Casino_Link(){
	    objFooterPage.clickPlayOnlineCasinoLink();
	}

	@Then("^Click on Mecca Blog Link$")
	public void click_on_Mecca_Blog_Link() throws Throwable {
	    objFooterPage.clickmeccaBlogLink();
	}
	
	@Then("^Verify user able to view following logos at Footer in partners block:$")
	public void verify_user_able_to_view_following_logos_at_Footer_in_partners_block(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
			objFooterPage.verifyPartnersLogoInFooter(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}
	
	@Then("^User clicks on essa logo$")
	public void user_clicks_on_essa_logo() {
	   objFooterPage.clickLogoEssa();
	}

	@Then("^User clicks on IBSA logo$")
	public void user_clicks_on_IBSA_logo() {
		objFooterPage.clickLogoIbsa();
	}

	@Then("^User clicks on Gambling Control logo$")
	public void user_clicks_on_Gambling_Control_logo(){
		objFooterPage.clickLogoGamblingControl();
	}

	@Then("^User clicks on GamCare logo$")
	public void user_clicks_on_GamCare_logo(){
		objFooterPage.clickLogoGamCare();
	}

	@Then("^User clicks on GamStop logo$")
	public void user_clicks_on_GamStop_logo() {
		objFooterPage.clickLogoGamStop();
	}

	@Then("^User clicks on Gambling Commission logo$")
	public void user_clicks_on_Gambling_Commission_logo(){
		objFooterPage.clickLogoGamblingCommission();
	}
	
	@Then("^Verify user should be able to view Verisign Secured logo at Footer section$")
	public void verify_user_should_be_able_to_view_Verisign_Secured_logo_at_Footer_section() {
		objFooterPage.verifyVeriSignLogo();
	}
	
	@Then("^Verify system displays following section for payment providers components:$")
	public void verify_system_displays_following_section_for_payment_providers_components(DataTable dt){
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
			objFooterPage.verifyPaymentProvidersBlockInFooter(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}
	
	@Then("^User clicks on Alderney Gambling Control Commission link$")
	public void user_clicks_on_Alderney_Gambling_Control_Commission_link(){
	    objFooterPage.click_Alderney_Gambling_Control_Commission_link();
	}
	
	@Then("^User clicks on UK Gambling Commission link$")
	public void user_clicks_on_UK_Gambling_Commission_link() {
		objFooterPage.click_UK_Gambling_Commission_link();
	}
	
	@Then("^User click on BeGambleAware link$")
	public void user_click_on_BeGambleAware_link() {
	    objFooterPage.click_BeGambleAware_link();
	}
	
	@Then("^User clicks on Rank Group link$")
	public void user_clicks_on_Rank_Group_link()  {
	    objFooterPage.click_Rank_Group_link();
	}
	
	@Then("^Navigate back to window$")
	public void navigate_back_to_window() {
	    objDriverProvider.getWebDriver().navigate().back();
	}
	@Then("Verify essa logo$")
	public void Verify_essa_logo() {
		objFooterPage.Verifyessalogo();
	}
}
