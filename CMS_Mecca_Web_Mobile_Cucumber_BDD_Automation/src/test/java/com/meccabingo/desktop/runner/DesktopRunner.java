
package com.meccabingo.desktop.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
//import io.cucumber.junit.CucumberOptions;

@CucumberOptions(features = ".", strict = true, plugin = {"summary","json:target/JSON/Desktop.json",
		"rerun:target/SyncFails.txt", /*"com.generic.cucumberReporting.CustomFormatter"*/ }, 
		tags = "@Desktop",  
		dryRun = false, monochrome = false,
		glue = { "com.hooks", "com.meccabingo.desktop.stepDefinition" }
)

public class DesktopRunner extends AbstractTestNGCucumberTests {
 
}	



