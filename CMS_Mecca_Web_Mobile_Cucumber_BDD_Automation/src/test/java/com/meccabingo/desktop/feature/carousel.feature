Feature: Carousel

#EMC-294
@Desktop
Scenario: Check whether system navigate user to configured link when user clicks anywhere on the slider/carousel
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click anywhere on slider/carousel
Then Verify user navigate to respective page

#EMC-95
@Desktop
Scenario: Check whether user can see navigation arrows and pagination dots for Carousel when more than one slide configure
Then Verify navigation arrows on carousel
Then Verify pagination dots on carousel

#@Desktop
Scenario: Check whether slider moves automatically one slide to left after after X seconds
Then Verify slider moves automatically after time inerval

@Desktop
Scenario: Check whether slider moves one slide to left when user clicks on Left Arrow
Then Click on left arrow on carousel

@Desktop
Scenario: Check whether slider moves one slide to left when user clicks on Right Arrow
Then Click on right arrow on carousel
 
#EMC-303
@Desktop
Scenario: Check whether system displays feefo carousel as configured 
Then Verify feefo carousel slide
Then Verify left button on feefo carousel
Then Verfi right button on feefo carousel