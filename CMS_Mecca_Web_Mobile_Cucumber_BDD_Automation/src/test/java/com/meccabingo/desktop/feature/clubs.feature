Feature: Clubs

@DesktopRegression
Scenario: Check whether System displays confirmation message pop up when user clicks on “Favourite“ CTA from Club details page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Rochadale"
Then Select first club that appears in list
Then Click on more Info
Then Mark it as favorite
Then Verify text "Next time you click on the club section, Mecca Rochdale will appear as your homepage. Happy to go ahead and favourite it?" displayed

@DesktopRegression
Scenario: Check whether System displays confirmation message pop up after successful login when user clicks on “Favourite“ CTA in logged out mode
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Rochadale"
Then Select first club that appears in list
Then Click on more Info
Then Mark it as favorite
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Verify text "Next time you click on the club section, Mecca Rochdale will appear as your homepage. Happy to go ahead and favourite it?" displayed
Then click button having label as "Yes"
Then Verify club marked as favorite

@DesktopRegression
Scenario: Check whether System displays unfavourite“ the club message pop up when user clicks on “Favourite“ CTA from Club details page which is already added in favourities
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Rochadale"
Then Select first club that appears in list
Then Click on more Info
Then Unfavorite it
Then Verify text "This club will be removed from your favourites and will no longer appear as your club homepage. Want to carry on?" displayed
Then click button having label as "Yes"
Then Verify club favorite tag is removed

@DesktopRegression
Scenario: Check whether User able to join favourite club using Join Club Cta available on club details page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Oldham"
Then Select first club that appears in list
Then Click on more Info
Then Click link having text as "Join Club"
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Enter join club email "automecca2020@mailinator.com"
Then Enter Home phone "023446789"
Then Enter valid mobile number
Then Click on select all in marketing preference section
Then Click on age checkbox
Then click button having label as "Join your local club"
Then Verify user is joined club successfully