Feature: Registration Page (Digital)

@DesktopRegression
Scenario: Check whether  system displays "Your account is suspended. Please contact customer support. " error when user tries to register user with details who is already suspended/ blocked
Then Click on Join Now button
Then Enter join club email "testman012@mailinator.com"
Then Click on age checkbox
Then Click on Next button
Then Verify error message "Your account is suspended. Please contact customer support."

#EMC-59
@Desktop
Scenario: Check whether all marketing preference checkboxes (Email, SMS, Phone and Post) are checked when user select 'Select All' CTA from marketing preference section
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on select all in marketing preference section
Then Verify following checkboxes are selected
|Email|
|SMS|
|Phone|
|Post|

@Desktop
Scenario: Check whether user able to select single marketing preference checkboxes (Email, SMS, Phone and Post) from marketing preference section
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on email checkbox
Then Click on SMS checkbox
Then Click on phone checkbox
Then Click on post checkbox

#EMC-871
@Desktop
Scenario: Check whether Marketing preference checkboxes are displayed in red color and displays err below fields when user do not select any of the option from Marketing preference section and click on Register button
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on disable Register CTA
Then Verify error message "Please select contact preference" is displayed in red color 

#EMC-863
@Desktop
Scenario: Check whether system displays green line below the username field when user enters username in valid format
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Verify green line displays below username field

@Desktop
Scenario: Check whether system displays error message when user enters the existing username in username field
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the existing username "automecca2020" in username field
Then Verify error message "The supplied username is already associated with a different user"

#EMC-61
@Desktop
Scenario: Check whether below fields are displayed on second step of registration journey:
- Title
- First name
- Surname
- Date of birth
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify fields are displayed on page
|Header|
|Title|
|First name|
|Surname|
|Date of birth| 
|Username|
|Password|

@Desktop
Scenario: Check whether all titles are displayed on second step of registration journey:
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify all titles are displayed on page
|Ms|
|Mr|
|Miss|
|Mrs|
|Mx|

@Desktop
Scenario: Check whether user able to select any option from Title section
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Select any option from title section
Then Verify selected option gets highlighted

@Desktop
Scenario: Check whether green line is displayed when user enter the first name in correct format
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the firstname in valid format
Then Verify green line displays below firstname field

@Desktop
Scenario: Check whether green line is displayed when user enter the surname in correct format
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the surname in valid format
Then Verify green line displays below surname field

@Desktop
Scenario: Check whether green line is displayed when user enter the DOB in correct format
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter Date of birth "11" "08" "1996"
Then Verify green line displays below DOB field

@Desktop
Scenario: Check whether system displays green line below the password field when user enters the password in correct format
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the password in valid format
Then Verify green line displays below password field

@Desktop
Scenario: Check whether system displays green line below the username field when user enters username in valid format (non existing user)
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Verify green line displays below username field

@Desktop
Scenario: Check whether system displays grey line along with password guidance below the password field:
- Please use at least one capital letter
- Please use at least one number
- Please use at least one lower case
- Minimum 8 characters
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the password in valid format
Then Verify registration page displays following password guidelines
|Please use at least one capital letter|
|Please use at least one number|
|Please use at least one lower case|
|Minimum 8 characters|

#EMC-63
@Desktop
Scenario: Check whether Yes option is selected by default on first step registration page
Then Click on Join Now button
Then Verify Yes option is selected

@Desktop
Scenario: Check whether green line is displayed below email address field when user enter the email address in correct format
Then Click on Join Now button
Then Enter email address
Then Verify green line displays below email field

@Desktop
Scenario: Check whether system displays error message and displays Login screen when user enters existing Email address and click on Next button
Then Click on Join Now button
Then Enter registered email address "emc685test@mailinator.com"
Then Click on disabled Next button
Then Verify error message "You must agree to the terms and conditions in order to continue"

@Desktop
Scenario: Check whether system displays Registration Page 2 when user enters valid email address n tick TnC check box and click on Next button
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify select all button

#EMC-810
@Desktop
Scenario: Check whether error message displayed under respective fields when user clicks on next button on first step registration page
Then Click on Join Now button
Then Click on disabled Next button
Then Verify error message "You must enter a valid email address"  
Then Verify error message "You must agree to the terms and conditions in order to continue" 

@Desktop
Scenario: Check whether help text disappear and error message display when user enters invalid email address on first step registration screen and leave field
Then Click on Join Now button
Then Enter invalid email address
Then Click on disabled Next button
Then Verify error message "You must enter a valid email address"

#EMC-60
@Desktop
Scenario: Check whether system displays correct validation when user selects Country field on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Verify UK is default value in country field
Then Select republic of ireland 
Then Verify enter address manually button gets disappear
Then Verify first address line field
Then Verify second address line field
Then Verify town/city field
Then Verify country field
Then Select united kingdom
Then Verify enter address manually button
Then Verify first address line field gets disappear
Then Verify second address line field gets disappear
Then Verify town/city field gets disappear
Then Verify country field gets disappear

@Desktop
Scenario: Check whether system displays correct validation when user selects Postcode field on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on postcode field
Then Verify grey line displays below postcode field
Then Verify help text appears below postcode field
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Verify first address line field
Then Verify second address line field
Then Verify town/city field

@Desktop
Scenario: Check whether system displays correct validation when user selected Enter address manually option for entering Address on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on enter address manually button
Then Enter valid input in address first line
Then Enter valid input in town/city field
Then Verify green line displays below first address field
Then Verify green line displays below town/city field

@Desktop
Scenario: Check whether system displays correct validation for Mobiel Number field on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on mobile number field
Then Verify grey line displays below mobile number field
Then Enter valid mobile number
Then Verify green line displays below mobile number field

#EMC-868
@Desktop
Scenario: Check whether system displays correct validations for postcode when user enters incomplete or invalid postcode on Digital Registration page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter invalid postcode
Then Verify red line displays below postcode field
Then Verify error message "Too many results, please enter more characters"

@Desktop
Scenario: Check whether system displays correct validations for Address fields when user select enter address manually option on Digital Registration page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on enter address manually button
Then Enter invalid input in first address line 
Then Verify red line displays below first address line field
Then Enter invalid input in second address line 
Then Verify red line displays below second address line field

@Desktop
Scenario: Check whether system displays correct validations for Mobiel number fields when entered incorrect or incomplete information on Digital Registration page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter invalid mobile number
Then Click on enter address manually button
Then Verify red line displays below mobile number field
Then Verify error message "Please enter a valid phone number between 8 and 11 digits long"

#EMC-1401
@Desktop
Scenario: Check whether system do not display Dr and Mx fro title while regsitration
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify title Dr does not appear