Feature: Recently Played

@DesktopRegression
Scenario: Check whether system displays Image, Title and Close button for bingo game tile available in recently played section
Given User clicks on Login Button from header of the Page
And User enters username "automecca2020"
And User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Verify below options in recently played section of bingo
|Header|
|Image|
|Title|
|Close button|

@DesktopRegression
Scenario: Check whether system displays Image, Title and Close button for slot game tile available in recently played section
Given User clicks on Login Button from header of the Page
And User enters username "automecca2020"
And User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Verify below options in recently played section of bingo
|Header|
|Image|
|Title|
|Close button|

@DesktopRegression
Scenario: Check whether System displays Other also played section under recently played tab with list configured in CMS for slots
Given User clicks on Login Button from header of the Page
And User enters username "testmeccam06"
And User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Verify that others also played displayed for "slot" section

@DesktopRegression
Scenario: Check whether System displays Other also played section under recently played tab with list configured in CMS for bingo
Given User clicks on Login Button from header of the Page
And User enters username "testmeccam06"
And User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Verify that others also played displayed for "bingo" section

@DesktopRegression
Scenario: Check whether System displays recently played tab evnthough user do not have any recently played games for Slots Game Panel
Given User clicks on Login Button from header of the Page
And User enters username "testmeccam06"
And User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Verify You dont have any recently played games message displayed for slot section

@DesktopRegression
Scenario: Check whether System displays recently played tab evnthough user do not have any recently played games for Bingo Game Panel
Given User clicks on Login Button from header of the Page
And User enters username "testmeccam06"
And User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Verify You dont have any recently played games message displayed for bingo section