Feature: Header 

#EMC-823
@Desktop
Scenario: Check whether system displays Join Now button when user access site first time
Then Verify header displays join now button

@Desktop @DesktopRegression
Scenario: Check whether system displays Not a member yet? Register here instead Join Now button
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on logout button
Then Verify header displays Not a member yet? Register here link

#EMC-310
#@Desktop
Scenario: Check whether card  components displays alternate the position of the image in battenberg block
Then Verify card is displayed in battenberg block

#EMC-312
#@Desktop
Scenario: Check whether in configured Three coulumns blcok section all 3 blocks are displaying in one row under Three Coulumn block
Then Verify blocks contain one row
Then Verify all three blocks

#EMC-35
@Desktop @DesktopRegression
Scenario: Check whether user able to view Playble balance in terms of Title and Amount when balance is shown
When User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button
Then Verify system displays user balance section at header
Then Verify system displays balance Title and correct amount under Balance section

#@Desktop
Scenario: Check whether no action triggered  when user clicks on Balance title  when balance is shown and not shown also when user click on 5 dots
When User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button
Then Click on user balance title from header
Then Click on balance toggle
Then Click on Balance title when balance not shown
Then Click on five dots when balance not shown

#EMC-236
#@Desktop
Scenario: Check whether game is loaded successfully when user log in to the site after invoking game from Search overlay in logged out mode
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "extra" in Search field from header
Then Click on play now button from searched list
Then Verify Login Page displayed
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button
Then Verify game launches successfully

#EMC-210
#@Desktop
Scenario: Check whether selecte game gets loads when user clicks on Play now button from search results
When User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button
When User clicks on search filed
When Search Game "chilli" in Search field from header
Then Click on play now button from searched list
Then Verify game launches successfully

#EMC-326
#@Desktop
Scenario: Check whether system displays Mecca logo, Nvaigation Menu, Search icon, Deposit CTA, balance block, My Account Icon in logged state at Header
When User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button
Then Verify header displays following components :
|Headaer|
|Mecca Logo|
|Navigation menu|
|Search loupe icon|
|Deposit CTA|
|Balance amount/dots|
|Balance text|
|Hide/Unhide balance switch|
|My account icon|
|number of unread messages|

#EMC-1069
#@Desktop
Scenario: Check whether system displays 404 error page to user when user tries to access page which is not configured
When User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button
Then Navigate to invalid url
Then Verify error title

#EMC-1070
@Desktop
Scenario: Check whether system displays cokkies banner when user access site without any stored cookies fro mecca site
Then Verify cookies text 
Then Verify continue button

#EMC-1218
@Desktop
Scenario: Check whether system allow user to Add Anchor Links from CMS
When Navigate to footer section
Then Click on top arrow anchor
Then Verify header displays join now button

#EMC-314
#@Desktop
Scenario: Check whether system displays configured video block with embeded video and cover image
When User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify video block
Then Click on play button
Then Verify video opens in frame