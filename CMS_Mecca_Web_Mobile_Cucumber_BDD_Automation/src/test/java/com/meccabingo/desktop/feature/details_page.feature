Feature: Details Page

#EMC-1106
@Desktop
Scenario: Check whether system displays top section on Live casino page without any issue
Then Navigate through hamburger to "slot" menu
Then User clicks on i button of game "Pirates Free Spins Edition"
Then Verify login details page of game "Pirates Free Spins Edition"
Then Verify title of the game of game "Pirates Free Spins Edition"
Then Verify brief description
Then Verify backround image of game
Then Verify main image of game

@Desktop
Scenario: Check whether system displays Information on Live casino page
Then Navigate through hamburger to "slot" menu
Then Click on info button of first game of slot section
Then Verify information box of game

#EMC-876
@Desktop
Scenario: Check whether system displays Top section on Bingo details page
Then Navigate through hamburger to "bingo" menu
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on info button of first game of bingo section
Then Verify brief description
Then Verify backround image of game
Then Verify main image of game

@Desktop
Scenario: Check whether system launch bingo lobby on click of Join Now button from Bingo details page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify game launches in new window

@Desktop
Scenario: Check whether system displays Information box on bingo details page all information coming form Feeds
Given Invoke the Mecca site on Desktop 
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then User clicks on i button of game "Pirates Free Spins Edition"
Then Verify Next game starts at in information box
Then Verify "Type" in information box
Then Verify "Tickets from" in information box
Then Verify "Open" in information box
Then Verify "Prizes up to" in information box
Then Verify AVAILABLE ON in information box
Then Verify join now button in information box

@Desktop
Scenario: Check whether system displays help text for each title and value fields from information box
Given Invoke the Mecca site on Desktop 
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then User clicks on i button of game "Pirates Free Spins Edition"
Then Verify information box of game
Then Hove on i button of "type"
Then Verify help text "The kind of bingo being played in this room." for "type"
Then Hove on i button of "tickets from"
Then Verify help text "The price tickets start at." for "tickets from"
Then Hove on i button of "open"
Then Verify help text "The times this room is available." for "open"
Then Hove on i button of "prizes"
Then Verify help text "The highest amount you can win in this game." for "prizes"