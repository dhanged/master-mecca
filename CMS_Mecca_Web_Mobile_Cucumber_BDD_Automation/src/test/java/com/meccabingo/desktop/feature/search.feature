Feature: Search functionality, Search title

@DesktopOptimized @DesktopRegression
Scenario: Check whether system displays Search results for Clubs for Valid Text and Include clubs in search result checkbox is ON
When Search Game "Mecc" in Search field from header
Then Check checkbox of include clubs in search result
Then Verify system displays search results to user 


@DesktopOptimized @DesktopRegression
Scenario: Check whether system displays Search results for Slots Games for Valid Text
When Search Game "chi" in Search field from header
Then Verify system displays search results to user 

@DesktopOptimized @DesktopRegression
Scenario: Check whether system displays Search results for bingo Games for Valid Text
When Search Game "Emoji" in Search field from header
Then Verify system displays search results to user 

@DesktopOptimized
Scenario: Check whether system displays Search overlay with all required fields
When User clicks on search filed
Then Verify Search overlay opens
Then User enters three characters in search filed
Then Verify system displays search result section
Then Close search
When User clicks on search filed
Then Click button having text "Clear"
Then Verify user able to view 'Quick Links' and 'What others are playing' section in search overlay
Then Close search
When Search Game "chilli con" in Search field from header

#EMC-217 : Search title
@Desktop @DesktopRegression
Scenario: Check whether system displays following options for search title:
-Image
-Name
-""Play Now"" CTA
-”Info"" CTA"
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "chilli con" in Search field from header
Then Verify system displays search results to user 
Then Verify system displays following options for search title:
|Image|
|Name|
|Play Now CTA|
|Info CTA|

#EMC-30 : Header - Search functionality
@Desktop
Scenario: Check whether search trigger when user enters three or more characters in search field
When User clicks on search filed
Then Verify Search overlay opens
Then User enters three characters in search filed
Then Verify system displays search result section

#EMC-563 : Search functionality - Quick links/ what others are playing
@Desktop 
Scenario: Check Whether user able to view "Quick Links" and "What others are playing" section in search overlay till user enters 3 or more characters in search field
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "b" in Search field from header
Then Verify user able to view 'Quick Links' and 'What others are playing' section in search overlay
Then Search Game "i" in Search field from header
Then Verify user able to view 'Quick Links' and 'What others are playing' section in search overlay
Then Search Game "ngo" in Search field from header
Then Verify 'Quick Links' and 'Whats others are playing' section dissapear
And Verify system displays search results to user 

#EMC-212 
@Desktop
Scenario: Check whether system displays Game / bingo details Page when user clicks on i button from Search overlay
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "chilli con" in Search field from header
Then Click on i button from search 
Then Verify user navigate to game details page

#EMC-238
@Desktop
Scenario: Check whether user able Navigate back to Page from where registration Journey started without deposit after clicking on play button in non logged in mode
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "chilli con" in Search field from header
Then Click on play now button from search
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Click on deposit close button
Then Verify balance section is displayed
Then Verify game launches successfully

#EMC-237
@Desktop
Scenario: Check whether system launch searched game after completing registration journey with deposit
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "cHILLI cON" in Search field from header
Then Click on play now button from search
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "5"
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Verify balance section is displayed
Then Verify game launches successfully