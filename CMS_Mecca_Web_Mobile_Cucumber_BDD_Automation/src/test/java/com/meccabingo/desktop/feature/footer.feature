Feature: Footer - Social Media, Useful links, Payments, Partener section etc

#EMC-22 Social media TCs
@Desktop @DesktopRegression
Scenario: Check whether system displays following Social media components:
-Facebook
-Twitter
-Instagram
-Youtube
Then Verify system displays following Social media components:
|Header|
|Twitter|
|Facebook|
|Youtube|
|Instagram|
And Verify system displays Icon, Text and Name for each social media block
Then Click on Facebook block from footer
Then Switch to child window
Then Verify system navigate user to "https://www.facebook.com/MeccaBingo" url
Then Switch to parent window
Then Click on Instagram block from footer
Then Switch to child window
Then Verify system navigate user to "https://www.instagram.com/meccabingo/" url
Then Switch to parent window
Then Click on twitter block from footer
Then Switch to child window
Then Verify system navigate user to "https://twitter.com/MeccaBingo" url
Then Switch to parent window
Then Click on youtube block from footer
Then Switch to child window
Then Verify system navigate user to "https://www.youtube.com/user/MeccaBingoClubs" url

#EMC-27 test Cases for useful links
@Desktop @DesktopRegression
Scenario: Check whether system displays folllowing URL under Useful Links block:
-Privacy Policy
-Terms and Conditions
-Affiliates
-Mecca Club Terms
-Play Online Casino
-Mecca Blog

Then Verify following URLs are displaying under Useful Links block:
|Headaer|
|Privacy Policy|
|Terms and Conditions|
|Affiliates|
|Mecca Club Terms|
|Play Online Casino|
|Mecca Blog|

@Desktop
Scenario: Check whether system navigate to appropriate URL when user clicks on any Link
Then Click on Privacy Policy Link
Then Switch to child window
Then Verify system navigates user to "https://qa01-mecc-cms2.rankgrouptech.net/privacy-policy"  
Then Switch to parent window
Then Click on Terms and Conditions Link
Then Switch to child window
Then Verify system navigates user to "https://qa01-mecc-cms2.rankgrouptech.net/terms-and-conditions"
Then Switch to parent window
Then Click on Affiliates Link
Then Switch to child window
Then Verify system navigates user to "https://www.rankaffiliates.com/"
Then Switch to parent window
Then Click on Mecca Club Terms Link
Then Switch to child window
Then Verify system navigates user to "https://qa01-mecc-cms2.rankgrouptech.net/club-terms-and-conditions"
Then Switch to parent window
Then Click on Play Online Casino Link
Then Switch to child window
Then Verify system navigates user to "https://www.grosvenorcasinos.com/"
Then Switch to parent window
Then Click on Mecca Blog Link
Then Switch to child window
Then Verify device navigate user to containing url "https://blog.meccabingo.com/"

#Sprint 7
#EMC-36 : Footer - GamCare, 18, GamStop, Gambling commision, Gambling control Logos
@Desktop  @DesktopRegression
Scenario: Check whether user able to view following logos at Footer section:
-GamCare
-18
-GamStop
-Gambling commision
-Gambling control
-IBAS
-ESSA
Then Verify user able to view following logos at Footer in partners block:
|ESSA|
|IBAS|
|18|
|Gambling Control|
|GamCare|
|GamStop|
|Gambling Commision|

@Desktop 
Scenario: Check whether user able to navigate to configured URL when user clicks on any of the Logo
Then User clicks on essa logo
Then Switch to child window
Then Verify system navigates user to "https://www.essa.uk.com/"
Then Verify essa logo
Then Switch to parent window
Then User clicks on IBSA logo
Then Switch to child window
Then Verify system navigates user to "https://www.ibas-uk.com/"
Then Switch to parent window
Then User clicks on Gambling Control logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamblingcontrol.org/"
Then Switch to parent window
Then User clicks on GamCare logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamcare.org.uk/"
Then Switch to parent window
Then User clicks on GamStop logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamstop.co.uk/"
Then Switch to parent window
Then User clicks on Gambling Commission logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamblingcommission.gov.uk/Home.aspx"

#Sprint 7
#EMC-272 : Footer - Verisign Secured
@Desktop 
Scenario: Check whether user able to view Verisign Secured logo at Footer section
Then Verify user should be able to view Verisign Secured logo at Footer section

#EMC-23 : Footer - Trusted Partners - Payment methods
@Desktop 
Scenario: Check whether system displays following section for Trusted partners components:
-Title
-Description 
-Logos
Then Verify system displays following section for payment providers components:
|Title|
|Description|
|Logos|

#EMC-10 : Footer - Rank group
@Desktop
Scenario: Check whether user navigate to Configured link on click of hyperlinked text
Then User clicks on Alderney Gambling Control Commission link
Then Switch to child window
Then Verify system navigates user to "https://www.gamblingcontrol.org/"
Then Switch to parent window
Then User clicks on UK Gambling Commission link
Then Switch to child window
Then Verify device navigate user to containing url "gamblingcommission.gov.uk"
Then Switch to parent window
Then User click on BeGambleAware link
Then Switch to child window
Then Verify system navigates user to "https://www.begambleaware.org/"
Then Switch to parent window
Then User clicks on Rank Group link
Then Switch to child window
Then Verify system navigates user to "https://www.rank.com/en/index.html"