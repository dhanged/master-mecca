Feature: My Account

@DesktopRegression
Scenario:Check whether system displays Active bonuses details on page when user having some bonuses which are in Bonus status = Active, Rewarded and PartQualified
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Bonuses"
Then Verify "Bonuses" as header displayed

@DesktopRegression
Scenario: Check whether system displays failed message when unabel to claim promotion
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Bonuses"
Then Verify "Bonuses" as header displayed
Then User enter bonus code "FIRST_DE_28SEP"
Then click button having label as "Submit"
Then Verify "Enter promotional code" as header displayed
Then Verify button having label as "Next" displayed
Then Verify button having label as "Cancel" displayed

@DesktopRegression
Scenario: Check whether System navigate user to Screen 2 when enters correct code in field and click on Submit button
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Bonuses"
Then Verify "Bonuses" as header displayed
Then User enter bonus code "FIRST_DE_28SEP"
Then click button having label as "Submit"
Then Verify "Enter promotional code" as header displayed
Then Verify button having label as "Next" displayed
Then Verify button having label as "Cancel" displayed

@DesktopRegression
Scenario: Check whether User able to claim Deposit Bonus using promocode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Bonuses"
Then Verify "Bonuses" as header displayed
Then User enter bonus code "FIRST_DE_28SEP"
Then click button having label as "Submit"
Then Verify "Enter promotional code" as header displayed
Then Verify button having label as "Next" displayed
Then Verify button having label as "Cancel" displayed

@DesktopRegression
Scenario: Check whether system displays Documents required pop up when “KYC check not possible“ response received while registration
Then Click on Join Now button
Then Enter random email id with suffix ".kycrejected@mailinator.com"
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify button having label as "Upload Documents" displayed
Then Verify button having label as "Live Help" displayed

@DesktopOptimized
Scenario: Check whether  system displays error when user tries to register user with details who is already suspended/ blocked
Then Click on Join Now button
Then Enter registered email address "testman012@mailinator.com"
Then Click on age checkbox
Then Click on Next button
Then Verify message "An error has occurred and has been logged, please contact system administrator"

@DesktopRegression
Scenario: Check whether digital user gets registered successfully on Click of Register button on entering all valid details on Page 1 and Page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button

@DesktopRegression
Scenario: Check whether user gets self excluded and loghed out from site when user clicks on Self Exclude and Log out cta from confirmation pop up
Then User clicks on Login Button from header of the Page
Then User enters username "testmeccam03"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "Yes"
Then click button having label as "6 Months"
Then Enter password "Password123" for take a break
Then click button having label as "Yes, I want to Self Exclude"
Then Verify text "Are you sure?" displayed
Then Verify button having label as "Self exclude & Log out" displayed
Then click button having label as "Cancel"

@DesktopOptimized
Scenario: Check whether user gets logged out from Site and unabel to login back till break time completed on click of “Take a break & Log Out” CTA from Confirmation Pop up
Then User clicks on Login Button from header of the Page
Then User enters username "testmeccam03"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then Verify "Take a break" as header displayed
Then Verify button having label as "1 Day" displayed
Then Verify button having label as "2 Days" displayed
Then Verify button having label as "6 Weeks" displayed
Then click button having label as "1 Day"
Then Verify message "Locked until: "
Then Verify button having label as "Take A Break" displayed
Then click button having label as "1 Day"
Then Enter password "Password123" for take a break
Then click button having label as "Take A Break"
Then Verify message " you are choosing to start your break. Your account will be locked until "
Then Verify button having label as "Take a break & Log Out" displayed
Then click button having label as "Take a break & Log Out"
Then Verify header displays Not a member yet? Register here link

@DesktopRegression
Scenario: Check whether system changes deposit limits immidiatly when user decrese limits
Then User clicks on Login Button from header of the Page
Then User enters username "testmeccam01"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Click deposit limit button "Monthly"
Then Reduce deposit limit by one
Then click button having label as "Set Limit"
Then Verify message "Your limits have been updated successfully"
Then click button having label as "Close"

@DesktopRegression
Scenario: Check whether system do not apply changes deposit limits when user increases limits
Then User clicks on Login Button from header of the Page
Then User enters username "testman006"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Click deposit limit button "Weekly"
Then Increase deposit limit by one
Then click button having label as "Set Limit"
Then Verify message "After 24 hours you will be able to activate your new limit."
Then click button having label as "Close"

@DesktopOptimized
Scenario: Check whether system displays error message when user enters  limit equal or smaller than next higher limits
Then User clicks on Login Button from header of the Page
Then User enters username "testmeccam01"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Verify button having label as "£" displayed 
Then Verify button having label as "Set Limit" displayed 
Then Verify button having label as "Monthly" displayed 
Then Verify button having label as "Weekly" displayed 
Then Verify button having label as "Daily" displayed 
Then Click deposit limit button "Monthly"
Then Click button having text "Reset limit"
Then Enter limit "400"
Then Verify error message "Your monthly limit must be greater than your weekly limit"
Then Click deposit limit button "Weekly"
Then Click button having text "Reset limit"
Then Enter limit "40"
Then Verify error message "Your weekly limit must be greater than your daily limit"
Then Click deposit limit button "Daily"
Then Click button having text "Reset limit"
Then Enter limit "1"
Then Verify error message "Please enter an amount greater than 5"

@DesktopOptimized
Scenario: Check whether user able to set new relaity check by selecting any of the options from list  on My Account Responsible gaming - Reality Checks page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then click button having label as "3 mins"
Then click button having label as "Save changes"
Then Click close 'X' icon
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then Verify "1 mins" reminder button is highlighted

@DesktopOptimized
Scenario: Check whether user able to update options selected for marketting preferences from my account 
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then Click menu option "Marketing Preferences"
Then Verify "Marketing Preferences" as header displayed
Then Click on "post" checkbox
Then Verify "post" checkbox is checked
Then Click on "post" checkbox
Then Verify "post" checkbox is unchecked
Then Click on "all" checkbox
Then contact preferences checkboxes are checked
Then Click on "all" checkbox
Then contact preferences checkboxes are unchecked
Then click button having label as "Update"
Then Verify text "Account details updated" displayed

@DesktopOptimized
Scenario: Check whether  user able to edit details from My Account > Edit Details Page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then click button having label as "Edit"
Then Verify "Edit Details" as header displayed
Then click button having label as "Clear"
Then click button having label as "Enter address manually"
Then Enter address line1 "1 Owen Close"
Then Enter city "Fareham"
Then Enter county "Hampshire"
Then Enter postal code "HP21 9NU"
Then Enter paypal password "Password123"
Then click button having label as "Update"
Then Verify text "Account details updated" displayed

@DesktopOptimized
Scenario: Check whether system displays account details page contains users information on Account Details Page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
And Verify close 'X' icon
Then Verify following my account menu options:
| Header |
| Change Password |
| Marketing Preferences |

@DesktopRegression
Scenario: Check whether user abet o delete message using Delete button from Message overlay
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then Open first message
Then Verify message in detail
Then Click delete button

@DesktopRegression
Scenario: Check whether system updates message count from Title section, Message sub menu icon and My account icon from header when user read / delete unread message
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then Read the message count
Then Open first message
Then Click delete button
Then Verify that message count is reduced
Then Click on back button
Then Read the message count
Then Open first message
Then Verify that message count is reduced

@DesktopRegression
Scenario: Check whether system displays Body section for message single view
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then Verify delete icon displayed
Then Open first message
Then Verify message in detail
Then Verify delete icon displayed
And Verify close 'X' icon
Then Verify Live help link


@DesktopOptimized 
Scenario: Check whether system displays header and body section for Messages overlay
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
And Verify back arrow '<'
And Verify close 'X' icon
Then Verify Live help link
Then Verify delete icon displayed
Then Verify messages table displayed

@DesktopOptimized
Scenario: Check whether system displays Collapsed and uncollapsed version for Transactions
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Deposits" filter activity
Then Click button Filter
Then Collapse one record
Then Verify the collapsed record

@DesktopOptimized
Scenario: Check whether system displays new drop down list when user selects Net Deposits from filter option
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Net deposits" filter activity
Then Verify option "Last 24 Hours" displayed
Then Verify option "6 Months" displayed
Then Verify option "Last Week" displayed
Then Verify option "All Time" displayed
Then Select SortBy "Last 30 Days"

@DesktopRegression
Scenario: Check whether system displays Transactions History according to option selected from list 
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Deposits" filter activity
Then Click button Filter
Then Verify records are found of "Deposit"
Then Select "Withdrawals" filter activity
Then Click button Filter
Then Verify records are found of "Withdrawal"
Then Select "Withdrawals" filter activity
Then Click button Filter
Then Select "Net deposits" filter activity
Then Select "Bonuses" filter activity
Then Click button Filter

@DesktopOptimized
Scenario: Check whether system displays Header and Body section on My Account Transaction 
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
And Verify back arrow '<'
And Verify close 'X' icon
Then Verify Live help link
Then Verify transaction filter box displayed
Then Verify dates are populated with today date
Then Verify transaction history box

@DesktopOptimized
Scenario: Check whether user navigate to respective section on click of links from My Account > Balance overlay
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Balance"
Then Verify "Balance" as header displayed
Then Verify link "deposit_limits" of sub menu page
Then Verify 'Live help' link
And Verify back arrow '<'
And Verify close 'X' icon
Then Verify bold text "Playable Balance" displayed
Then Verify message "Cash"
Then Verify message "All game Bonuses"
Then Verify button having label as "Deposit" displayed
Then Verify link "BonusHistory" of sub menu page

@DesktopOptimized
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paysafe and click on Deposit button
Then User clicks on Login Button from header of the Page
Then User enters username "testman009"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Click show more
Then Click saved card "paysafe"
Then Enter amount to deposit "10"
Then Click on Deposit button from Deposit page
Then Switch to child window
Then Enter paysafe account "0000000009903207"
Then Click paysafe agree
Then Click paysafe pay
#Then Switch to parent window
Then Verify deposit is successful
Then Click close button from deposit successful popup

@DesktopRegression
Scenario: Check whether system navigate user to safecharge frame when user accept TnC For PoPF 
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Verify message "We hold your balance in a designated bank account so that, in the event of insolvency, sufficient funds are always available for you to withdraw at any time. This represents the medium level of protection, based on the categories provided by the UK Gambling Commission"
Then Verify button having label as "Next" displayed
Then Click on age checkbox
Then click button having label as "Next"
Then Click payment method as "Card"
Then Enter Card number "4026201382933139"
Then Enter Expiry "12/22"
Then Enter CVV "432"
Then Enter amount to deposit "5"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@DesktopRegression
Scenario: Check whether system displays success message when clicks on Withdraw button from Safecharge iFrame  when user do not have active bonuses for account
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
Then Enter withdrawal amount "10"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then Verify text "Are you sure" displayed
Then click button having label as "Continue"
Then Click withdraw
Then Verify withdraw is successful 

@DesktopRegression
Scenario: Check whether system displays success message when clicks on Withdraw button from Safecharge iFrame when user have active bonuses for account
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
Then Enter withdrawal amount "10"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then Verify text "Are you sure" displayed
Then click button having label as "Continue"
Then Click withdraw
Then Verify withdraw is successful 

@DesktopOptimized
Scenario: Check whether system displays correct amount under Cash, Reward and Bonus Amount section in uncollapsed state
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on playable balance icon
Then Verify following options are displayed on playable balance section:
|playable Balance|
|playable balance amount|
|playable balance minus icon|
|cash text|
|cash amount|
|bonuses text|
|bonuses amount|
|detailed view button|
|deposit button|

#EMC-1031
@Desktop @DesktopRegression
Scenario: Check whether system displays all required sections on My Account Overlay page when accesssed
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Verify following options are displayed on my account page: 
|Username|
|Close button|
|Playable Balance|
|Cashier|
|Bonuses|
|Messages|
|Account details|
|Responsible gambling|
|Log out icon|
|Log out link|
|Recently played section|
|Email|
|Phone number|
|Live chat|

#EMC-1100
@Desktop @DesktopRegression
Scenario: System displays Balance block in Collapsed state when user access My Account Page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Verify playable balance text
Then Verify playable balance amount
Then Verify playable balance icon

@Desktop
Scenario: System displays Balance block in expanded state when user access My Account Page and click on + button from Balance section
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Click on playable balance icon
Then Verify following options are displayed on playable balance section:
|playable Balance|
|playable balance amount|
|playable balance minus icon|
|cash text|
|cash amount|
|bonuses text|
|bonuses amount|
|detailed view button|
|deposit button|

@Desktop
Scenario: System collapse balance block when user clicks on any part of the TOP SECTION of the Balance Box
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Click on playable balance icon
Then Verify playable balance minus icon
Then Click on playable balance minus icon
Then Verify on playable balance icon

@Desktop
Scenario: Check whether no action trigger whn user click on Cash and Bonuses title or amount from Balance section
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Click on playable balance icon
Then Click on following options:
|cash text|
|cash amount|
|bonuses text|
|bonuses amount|

@Desktop
Scenario: Check whether system navigate user to respective page when user clicks on Detailed View CTA and Deposit CTA
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Click on playable balance icon
Then Click on detailed view button
Then Verify button navigates to balance page
Then Click on back button
Then Click on playable balance icon
Then click button having label as "Deposit"
Then Verify "Deposit" as header displayed

#EMC-1031
@Desktop
Scenario: Check whether system naviate user to repective pages when user clicks on Links / Menu availabel on My Account Overlay
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click on back button
Then Click menu option "Bonuses"
Then Verify "Bonuses" as header displayed
Then Click on back button
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then Click on back button
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click on back button
Then Verify message count on message text
Then Click on username from top
Then Click on Live chat link
Then Switch to child window
Then Verify device navigate user to containing url "https://rank.secure.force.com/chat/" 

#EMC-1032
@Desktop
Scenario: Check whether system naviate user to repective pages when user clicks on Links / Menu availabel on My Account > Cashier Overlay
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Cashier" option
Then Verify following option on cashier page:
|Close button|
|Back button|
|Deposit|
|Withdrawal|
|Transaction History|
|Balance|
|Email|
|Phone number|
|Live chat|
Then Click menu option "Deposit"
Then Verify "Deposit" as header displayed
Then Click on back button
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
Then Click on back button
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Click on back button
Then Click menu option "Balance"
Then Verify "Balance" as header displayed
Then Click on back button

#EMC-1030
@Desktop
Scenario: Check whether system displays Header and body on Change Password page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Click on change password link
Then Verify following option on cashier page:
|Close button|
|Back button|
|Change password title|
|Live help link|
|Current password field|
|New password field|
|Update button|

@Desktop
Scenario: Check whether Update CTA is active when user enters Current passsword and New password in valid format
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Click on change password link
Then Verify update button is disabled
Then Enter valid input in current password
Then Enter valid input in new password
Then Verify update button is enabled

@Desktop
Scenario: Check whether system allow user to enter current password and new password in respective fields
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Click on change password link
Then User enters current password "Password123" 
Then Verify green line displays below current password field
Then User enters new password "Password123" 
Then Verify green line displays below new password field

#EMC-1034
@Desktop
Scenario: Check whether system dsiplays Header Body and Footer section on My Account Responsible Gambling section
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Verify following option on responsible gambling page:
|Close button|
|Back button|
|Responsible Gambling Title|
|Reality Check|
|Deposit Limits|
|Take a break|
|Self Exclude|
|Gamstop|
|Email|
|Phone number|
|Live chat|

@Desktop
Scenario: Check whether system Navigate to respective Pages when click on Links available on Responsible Gambling page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Reality Check" option
Then Verify text "Reality Check" displayed
Then Click on back button
Then Click on "Deposit Limits" option
Then Verify text "Deposit Limits" displayed
Then Click on back button
Then Click on "Take a break" option
Then Verify text "Take a break" displayed
Then Click on back button
Then Click on "Self Exclude" option
Then Verify text "Self Exclude" displayed
Then Click on back button
Then Click on "GamStop" option
Then Verify text "GamStop" displayed
Then Click on back button

#EMC-164
@Desktop
Scenario: Check whether system displays Header , Body on Deposit page for user have never made deposit
Then User clicks on Login Button from header of the Page
Then User enters username "nevermadedepsit"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Cashier" option
Then Click on "Deposit" option
Then Verify following option on deposit page:
|Close button|
|Back button|
|Deposit Title|
|Live help link|
|Deposit limit link|
|Information text box|

@Desktop
Scenario: Check whether system displays Header , Body and Safecharge iFrame is on Deposit page for user have already made a FTD (First time deposit) 
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Cashier" option
Then Click on "Deposit" option
Then Verify following option on deposit page:
|Close button|
|Back button|
|Deposit Title|
|Live help link|
|Deposit limit link|


@DesktopR @DesktopRegression
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Credit Card and click on Deposit button
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Cashier" option
Then Click on "Deposit" option
Then Click saved card "3139"
Then Enter security code
Then Enter the amount "10"
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Verify login window gets closed

@Desktop @DesktopRegression
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paypal and click on Deposit button
Then User clicks on Login Button from header of the Page
Then User enters username "paypalaccount21"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Cashier" option
Then Click on "Deposit" option
Then Enter the amount "10"
Then Click on Deposit button from Deposit page
Then Switch to child window
Then Enter paypal id "davidh@safecharge.com"
Then Click paypal next button
Then Enter paypal password ",nhfv911$"
Then Click paypal login button
Then Click the button pay now
Then Switch to parent window
Then Verify success message displays on screen
Then Click close button from deposit successful popup
Then Verify login window gets closed

@Desktop @DesktopRegression
Scenario: Check whether system displays success message for deposit  when user enters all correct details for PaySafe Card and click on Deposit button
Then User clicks on Login Button from header of the Page
Then User enters username "paysafeaccount"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Cashier" option
Then Click on "Deposit" option
Then Enter the amount "10"
Then Click on Deposit button from Deposit page
Then Switch to paysafe child window
Then Enter paysafe account no "0000000009903207"
Then Click paysafe agree checkbox
Then Click on pay button
Then Switch to deposit window
Then Verify success message displays on screen
Then Close success message
Then Verify login window gets closed

#EMC-167
@Desktop
Scenario: Check whether system displays Header , Body on My Account ACCOUNT DETAILS page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Verify following option on account details page:
|Close button|
|Back button|
|account details Title|
|account details window|
|edit button|
|change password|
|marketing preference|
|Email|
|Phone number|
|Live chat|

#EMC-172
@Desktop
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Take a break page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Take a break" option
Then Verify following option on take a break page:
|Header|
|Close button|
|Back button|
|Take a break Title|
|Live help link|
|Break collapsible box|
|Break time question|
Then click button having label as "1 Day"
Then Verify message "Locked until: "
Then Verify button having label as "Take A Break" displayed
Then Verify password field displayed for take a break page

@Desktop
Scenario: Check whether system displays Text box along with “Why to take a break“ Box + a collapse/uncollapse icon on My Account Responsible gaming - Take a break page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Take a break" option
Then Verify break period text
Then Click on plus sign of take a break page
Then Verify message "If you wish to take a break for a different length of time, please contact Customer Support and a member of our team will be able to help you. By choosing to take a break, you will not be able to access your account and you will be prevented from gambling until your break period has finished."
Then Verify message "You may extend your break period at any time by contacting our Customer Support Team."
Then Verify message "Your break will end automatically once the selected time period has passed and your account will be re-opened at this time."

@Desktop
Scenario: Check whether system displays Take a break confirmation pop up when user selects any of the option for Take a break and Password on My Account Responsible gaming - Take a break page and click on Take a break CTA
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Take a break" option
Then Verify "Take a break" as header displayed
Then click button having label as "1 Day"
Then Enter password "Password123" for take a break
Then click button having label as "Take A Break"
Then Verify message " you are choosing to start your break. Your account will be locked until "
Then Verify button having label as "Take a break & Log Out" displayed
Then Verify button having label as "Cancel" displayed

@Desktop
Scenario: Check whether user gets logged out from Site and unabel to login back till break time completed on click of “Take a break & Log Out” CTA from Confirmation Pop up
Then User clicks on Login Button from header of the Page
Then User enters username "testmeccam05"
Then User enters password "Test@1234"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Take a break" option
Then click button having label as "Take a break"
Then Verify text "Take a break" displayed
Then click button having label as "1 Day"
Then Enter generated password
Then click button having label as "Take A Break"
Then click button having label as "Take a break & Log Out"
Then Verify header displays Not a member yet? Register here link
Then User clicks on Login Button from header of the Page
Then User enters username "testmeccam05"
Then User enters password "Test@1234"
Then User clicks on login button from login window
Then Verify text "You have chosen to take a break from Mecca Bingo. You can login again from" displayed

#EMC-175
@Desktop
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Reality Checks page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Reality Check" option
Then Verify "Reality Check" as header displayed
Then Verify 'Live help' link
And Verify close 'X' icon
And Verify back arrow '<'
Then Verify message "Set your reminder"
Then Verify button having label as "1 mins" displayed
Then Verify button having label as "1 hour" displayed
Then Click on plus sign of reality check
Then Verify button having label as "Save changes" displayed


@Desktop
Scenario: Check whether system displays Text box on My Account Responsible gaming - Reality Checks page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Reality Check" option
And Verify text in text box
Then Click on uncollapsible icon
And Verify system displays text

@Desktop
Scenario: Check whether user able to set new relaity check by selecting any of the options from list on My Account Responsible gaming - Reality Checks page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Reality Check" option
Then click button having label as "1 mins"
Then click button having label as "Save changes"
And Click on back button
Then Click on "Reality Check" option
Then Verify "1 mins" reminder button is highlighted

#EMC-176
@Desktop
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Gamstop page  
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "GamStop" option
Then Verify following option on gamstop page:
|Header|
|Close button|
|Back button|
|Gamstop Title|

@Desktop @DesktopRegression
Scenario: Check whether system displays Text box on My Account Responsible gaming - Gamstop page 
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "GamStop"
Then Verify "GamStop" as header displayed
Then click link "http://www.gamstop.co.uk/"
Then Switch to child window
Then Verify system navigate user to "https://www.gamstop.co.uk/" url

#EMC-223
@Desktop
Scenario: Check whether user able to access my account from Game window
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on Play Now button from Game "Pirates Free Spins Edition"
Then Click on my account button
Then Click close 'X' icon
Then Verify my account window gets closed
And Verify system loads game window for selected game in real mode

@Desktop @DesktopRegression
Scenario: Check whether user able to continue game play after completing any actions from My Account Overlay
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on Play Now button from Game "Pirates Free Spins Edition"
Then Click on my account from game
Then Click on "Cashier" option
Then click button having label as "Deposit"
Then Click saved card "davidh"
Then Enter the amount "5"
Then Click on Deposit button from Deposit page
Then Switch to child window
Then Enter paypal id "davidh@safecharge.com"
Then Click paypal next button
Then Enter paypal password ",nhfv911$"
Then Click paypal login button
Then Click the button pay now
Then Switch to parent window
Then Verify success message displays on screen
Then Click close button from deposit successful popup
Then Verify game launches successfully

@Desktop
Scenario: Check whether user gets logged out from game window when user Take a break
Then Click on Play Now button from Game "Pirates Free Spins Edition"
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
Then Click on my account from game
Then Click on "Responsible Gambling" option
Then Verify text "Responsible Gambling" displayed
Then click button having label as "Take a break"
Then Verify text "Take a break" displayed
Then click button having label as "1 Day"
Then Enter generated password
Then click button having label as "Take A Break"
Then click button having label as "Take a break & Log Out"
Then Verify header displays Not a member yet? Register here link

#EMC-1027
@Desktop
Scenario: Check whether System displays Header and Body on My Account > Enter bonus code [Screen 1] page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Click on "Enter code here" option
Then Verify following option on enter bonus code page:
|Close button|
|Back button|
|Enter bonus code Title|
|Live help link|
|Enter code field|
|Clear button|
|Submit button|

@Desktop
Scenario: Check whether System displays Submit CTA in active state when user enters Bonus code in Bonus code field
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Click on "Enter code here" option
Then Verify submit button is disabled
When User enter bonus code "Test"
Then Verify submit button is enabled

#EMC-1028
@Desktop
Scenario: Check whether  System displays Header and Body on My Account > Edit Details Page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Click on edit button
Then Verify following option on edit details page:
|Close button|
|Back button|
|Edit Details Title|
|account details window|
|Live help link|
|Not editable fields box|
|Email field|
|clear toggle|
|Phone field|
|clear toggle|
|Clear CTA|
|Address field|
|Town/City field|
|County field|
|Country dropdown|
|Postcode field|
|Enter your password to update text|
|Password field|
|Update CTA|
Then Click on back button
Then Verify text "Account Details" displayed

@Desktop
Scenario: Check whether user able select populated address for address fields from My Account > Edit Details Page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Click on edit button
Then Enter postcode "wr53da"
And Select one address
Then Verify address is selected

@DesktopY @DesktopRegression
Scenario: Check whether changed details gets saved for customer account on click of Update CTA
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then click button having label as "Edit"

#EMC-1033
@Desktop
Scenario: Check whether  System displays Header and Body on My Account > Bonuses
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Verify title "Bonuses"
Then Verify following options are displayed on bonuses page: 
|Close button|
|Back button|
|Enter bonus code|
|Active bonuses|
|Bonus history|
|Email|
|Phone number|
|Live chat|

@Desktop
Scenario: Check whether System Navigates to approprite pages on My Account > Bonuses
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Verify text "Bonuses" displayed
Then Click on "Active bonuses" option
Then Verify text "Active bonuses" displayed
Then Click on back button
Then Click on "Bonus History" option
Then Verify text "Bonus History" displayed

#EMC-173
@Desktop
Scenario: Check whether System displays Header and Body for My Account - Responsible gaming - Self Exclusion screen 1
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Self Exclude" option
Then Verify following options are displayed on bonuses page: 
|Close button|
|Back button|
|Self Exclude Title|
|Question|
|Yes option|
|No option|

@Desktop
Scenario: Check whether System displays different options for My Account - Responsible gaming - Self Exclusion screen 2(NO)
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Self Exclude" option
And Click on "No" option
Then Verify "Reality Check" option
Then Verify "Deposit Limits" option
Then Verify "Take a Break" option
Then Verify information text "Alternatively, please get in touch if you want to Self Exclude for different reasons on 0800 083 1990" displayed 

@Desktop @DesktopRegression
Scenario: Check whether System displays different options for My Account - Responsible gaming - Self Exclusion screen 2(Yes)
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Self Exclude" option
And Click on "Yes" option
Then Verify information box
And Verify bold text "Why Self Exclude?" displayed
And Verify bold text "How long do you want to lock your account for?" displayed
Then Verify button "6 months"
Then Verify button "1 year"
Then Verify button "2 years"
Then Verify button "5 years"
Then Click on button "6 months"
And Verify date is visible
And Verify password field
Then Verify button "Yes, I want to Self Exclude"

@Desktop 
Scenario: Check whether System navigat eon respective pages on click of options for My Account - Responsible gaming - Self Exclusion screen 2(NO)
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Self Exclude" option
And Click on "No" option
Then Click on "Reality Check" option
Then Verify text "Reality Check" displayed
Then Click on back button
Then Click on "Deposit Limits" option
Then Verify text "Deposit Limits" displayed
Then Click on back button
Then Click on "Take a Break" option
Then Verify text "Take a break" displayed
Then Click on back button

@Desktop
Scenario: Check whether  System displays text box on Self Exclusion Screen 2(yes)
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Self Exclude" option
And Click on "Yes" option
Then Verify message "You can block yourself from playing with Mecca for a chosen period of time. This can only be reversed by written request after that period has ended."
Then Click on plus sign of take a break page
Then Verify message "If you believe you might have a problem with gambling, it is advisable to stop gambling altogether, or for a prolonged period. We would also recommend that you seek guidance from the support agencies listed on our keepitfun website."

@Desktop @DesktopRegression
Scenario: Check whether  System displays Self Exclude Confirmation pop up when user selects Self Exclude CTA from overly page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Self Exclude" option
And Click on "Yes" option
Then click button having label as "6 Months"
Then Enter password "Password321" for take a break
And Click on self exclude button
Then Verify error message 
Then Enter password "Password123" for take a break
And Click on self exclude button
Then Verify confirmation message

#EMC-270
@Desktop
Scenario: Check whether system displays header and body for My Account > Balance overlay 
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Cashier" option
Then Click on "Balance" option
Then Verify following options are displayed on balance page: 
|Close button|
|Back button|
|Balance title|
|Balance box|
|Email|
|Phone number|
|Live chat|