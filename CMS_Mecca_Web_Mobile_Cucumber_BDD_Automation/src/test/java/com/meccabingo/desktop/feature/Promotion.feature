Feature: Promotion

#EMC-14
@Desktop
Scenario: Check whether user able to claim instant Type promotion from Promotion Tile on Promotion Listing Page
Given Invoke the Mecca site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "testmecca0503"
And User enters password "Test@1234"
Then User clicks on login button
When User click on Promotion tab
Then Verify "Instant bonus" card is available
Then Verify "Instant bonus" display following components Before claim or OptIn :
|Claim|
|Learn more|
|T&Cs apply|
Then Click on Claim button from Instant bonus bonus card
Then Verify Instant bonus display following components After claim or optIn:
|Claimed|
|Learn more|
|T&Cs apply|

#EMC-14
@Desktop
Scenario: Check whether user able to claim instant Type promotion from Promotion details page using Claim CTA
Given Invoke the Mecca site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "testmecca0503"
And User enters password "Test@1234"
Then User clicks on login button
When User click on Promotion tab
Then User Click on "Learn more" button from "Instant bonus" bonus card
Then Click on "Claim" button from promotion details page
Then Verify "Claimed" status on promotion details page
Then Verify featured games section displayed on page

#EMC-14
@Desktop
Scenario: Check whether user able to claim instant Type promotion from Promotion Tile on Promotion Listing Page after successful Registration
Given Invoke the Mecca site on Desktop
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
When User click on Promotion tab
Then Verify "Instant bonus" card is available
Then Verify "Instant bonus" card display following components After claim or optIn:
|Options|
|Claimed|

#EMC-137
@Desktop
Scenario: Check whether user able to Opt-In for Opt-In Type promotion from Promotion Tile on Promotion Listing Page
Given Invoke the Mecca site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "testmecca0503"
And User enters password "Test@1234"
Then User clicks on login button
When User click on Promotion tab
Then Verify "Cashback opt in bonus" card is available 
Then User verify "Cashback opt in bonus" is Not claimed
Then Verify "Cashback opt in bonus" display following components Before claim or OptIn :
|Options|
|Opt in|
|Learn more|
|T&Cs apply|
Then User Click on "Opt in" button from "Cashback opt in " bonus card
Then Verify "Cashback opt in bonus" display following components After claim or optIn:
|Options|
|Opted in|
|Learn more|
|T&Cs apply|

#EMC-137
@Desktop
Scenario: Check whether user able to Opt-In for Opt-In Type promotion from Promotion details page using Opt-In CTA
Given Invoke the Mecca site on Desktop
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "testmecca0503"
And User enters password "Test@1234"
Then User clicks on login button
When User click on Promotion tab
Then User Click on "Learn more" button from "Cashback opt in" bonus card
Then Verify "Opted in" status on promotion details page
Then Verify featured games section displayed on page

#EMC-137
@Desktop
Scenario: Check whether user able to Opt-In for Opt-In Type promotion from Promotion Tile on Promotion Listing Page after successful Login
Given Invoke the Mecca site on Desktop
When User click on Promotion tab
Then Verify "Cashback opt in" is available
Then User Click on "Opt in" button from "Cashback opt in" bonus card
Then Verify Login Page displayed
Then User enters username "automecca2020"
And User enters password "Password123"
Then User clicks on login button
Then Verify "Cashback opt in bonus" display following components After claim or optIn:
|Options|
|Opted in|