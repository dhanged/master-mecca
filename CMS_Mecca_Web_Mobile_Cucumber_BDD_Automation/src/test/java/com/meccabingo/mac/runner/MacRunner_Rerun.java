
package com.meccabingo.mac.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "@target/SyncFails.txt", 
				strict = true, 
				plugin = { "json:target/JSON/MacRerun.json", /*"com.generic.cucumberReporting.CustomFormatter"*/}, 		
				monochrome = true,
				glue = { "com.hooks",	"com.meccabingo.mac.stepDefinition" }
		)

public class MacRunner_Rerun extends AbstractTestNGCucumberTests {
}