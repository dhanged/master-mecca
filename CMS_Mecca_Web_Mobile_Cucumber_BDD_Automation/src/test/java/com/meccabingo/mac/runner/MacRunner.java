
package com.meccabingo.mac.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
//import io.cucumber.junit.CucumberOptions;

@CucumberOptions(features = ".", strict = true, plugin = {"summary","json:target/JSON/Mac.json",
		"rerun:target/SyncFails.txt", /*"com.generic.cucumberReporting.CustomFormatter"*/ }, 
		tags = "@MACRegression", 
		dryRun = true, monochrome = false,
		glue = { "com.hooks", "com.meccabingo.mac.stepDefinition" }
)

public class MacRunner extends AbstractTestNGCucumberTests {
 
}	



