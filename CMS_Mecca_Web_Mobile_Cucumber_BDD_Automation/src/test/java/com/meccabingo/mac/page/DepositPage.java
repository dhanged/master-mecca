package com.meccabingo.mac.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.webdriver.DriverProvider;

import io.cucumber.java.en.Then;

public class DepositPage {
	WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;

	private String iFrameId = "payment-process";

	public DepositPage(DriverProvider driverProvider, WebActions webActions,
			LogReporter logReporter) {
		this.objDriverProvider = driverProvider;
		this.objWebActions = webActions;
		this.logReporter = logReporter;
	}

	public void clickOnDepositBesidesMyAcct() {
		By btnDeposit = By.xpath("//*[@id='logged-in-bar']/button");
		if (objWebActions.checkElementDisplayedWithMidWait(btnDeposit))
			logReporter.log("Click deposit besides my acct button> >", objWebActions.click(btnDeposit));
	}

	public void clickShowMore() {
		By showMore = By.xpath("//span[contains(text(),'Show More')]");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(showMore))
			logReporter.log("Click Show more button> >", objWebActions.click(showMore));
		objWebActions.switchToDefaultContent();
	}

	public void selectPaymentMethod(String strPaymtMethod) {
		By pMethod = By.xpath("//div[@title='" + strPaymtMethod + "']");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(pMethod))
			logReporter.log("Click payment method> >", objWebActions.click(pMethod));
		objWebActions.switchToDefaultContent();
	}

	public void enterCardNumber(String strNo) {
		By cardNo = By.xpath("//input[@id='cc_card_number']");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(cardNo))
			logReporter.log("enter card no> >", objWebActions.setText(cardNo, strNo));
		objWebActions.switchToDefaultContent();
	}

	public void enterExpiry(String strExpiry) {
		By expiry = By.id("cc-exp-date");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(expiry))
			logReporter.log("enter exp date> >", objWebActions.setText(expiry, strExpiry));
		objWebActions.switchToDefaultContent();

	}

	public void enterCVV(String strCVV) {
		By objCVV = By.xpath("//input[@id='cc_cvv2']");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(objCVV))
			logReporter.log("enter card no> >", objWebActions.setText(objCVV, strCVV));
		objWebActions.switchToDefaultContent();
	}

	public void enterDepositAmt(String strAmt) {
		By amt = By.xpath("//input[contains(@placeholder,'amount here')]");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(amt))
			logReporter.log("enter amount> >", objWebActions.setText(amt, strAmt));
		objWebActions.switchToDefaultContent();
	}

	public void clickDepositButton() {
		By locator = By.id("continueButton");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click Deposit button> >", objWebActions.clickUsingJS(locator));
		objWebActions.switchToDefaultContent();
	}

	public void verifyDepositSuccessful() {
		By locator = By.xpath("//h2[contains(text(),'Deposit successful')]");
		
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		logReporter.log("Verify Deposit Successful > >", objWebActions.checkElementDisplayedWithMidWait(locator));
		objWebActions.switchToDefaultContent();
	}

	public void clickCloseFromDepositSuccessfulPopup() {
		By locator = By.xpath("//button[contains(@class,'modal_close')]");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click close > >", objWebActions.click(locator));
		objWebActions.switchToDefaultContent();
	}

	public void clickWithdraw() {
		By locator = By.xpath("//button[contains(text(),'Withdraw')]");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click withdraw > >", objWebActions.click(locator));
		objWebActions.switchToDefaultContent();
	}

	public void verifyWithdrawSuccessful() {
		By locator = By.xpath("//h2[contains(text(),'Withdrawal request received')]");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		logReporter.log("Verify Withdraw Successful > >", objWebActions.checkElementDisplayedWithMidWait(locator));
		objWebActions.switchToDefaultContent();
	}

	public void selectSavedCard(String strCard) {
		By locator;
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (strCard.equals("paysafe") == true)
			locator = By.xpath("//div[contains(@class,'paysafe')]");
		else
			locator = By.xpath("//div[contains(@title,'" + strCard + "')]");

		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click saved card > >", objWebActions.click(locator));
		objWebActions.switchToDefaultContent();
	}

	public void clickPayPal() {
		By locator = By.xpath("//img[contains(@title,'PayPal')]");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click PayPal > >", objWebActions.click(locator));
		objWebActions.switchToDefaultContent();
	}

	public void clickPaySafe() {
		By locator = By.xpath("//img[contains(@title,'paysafe')]");
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click spaysafe > >", objWebActions.click(locator));
		objWebActions.switchToDefaultContent();
		objWebActions.switchToParentWindow();
	}
}
