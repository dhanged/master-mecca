package com.meccabingo.mac.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;

public class Carousel {

	private WebActions webActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;

	public Carousel(WebActions webActions, LogReporter logReporter,WaitMethods waitMethods) {
		this.webActions = webActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
	}

	By previousArrow = By.xpath("//*[@id=\"promoCarousel\"]/apollo-carousel/section/div[3]");
	By nextArrow = By.xpath("//*[@id=\"promoCarousel\"]/apollo-carousel/section/div[4]");
	By paginationDots = By.xpath("//div[contains(@class,'swiper-pagination-bullets')]");
	By image = By
			.xpath("//span[contains(@aria-label,'slide 1')]");
    By feefo = By.xpath("//apollo-carousel/section");
    By feefoRightButton = By.xpath("//apollo-carousel/section/div[2]");
    By feefoLeftButton = By.xpath("//apollo-carousel/section/div[3]");
    By closePopup = By.xpath("//img[contains(@class,'walkthrough-close-icon')]");
    
	public void clickOnSlide() {		
		if(webActions.checkElementDisplayed(image))
		logReporter.log("click on image", webActions.click(image));
	}

	public void verifyNavigateToPage() {
       String url = "https://qa01-mecc-cms2.rankgrouptech.net";
       logReporter.log("verify it navigate to game page", webActions.getUrl().contains(url));
	}

	public void verifyNavigationArrow() {
		logReporter.log("verify left arrow", webActions.checkElementDisplayed(previousArrow));
		logReporter.log("verify right arrow", webActions.checkElementDisplayed(nextArrow));
	}

	public void verifyPaginationDots() {
		logReporter.log("verify pagination dots on carousel", webActions.checkElementDisplayed(paginationDots));
	}

	public void clickOnLeftArrow() {
		logReporter.log("click on left arrow", webActions.click(previousArrow));
	}

	public void clickOnRightArrow() {
		logReporter.log("click on right arrow", webActions.click(nextArrow));
	}
	public void verifyFeefoCarousel() {
		logReporter.log("verify feefo carousel", webActions.checkElementDisplayed(feefo));
	}
    public void verifyRightButton() {
    	logReporter.log("verify right arrow", webActions.checkElementDisplayed(feefoRightButton));
    }
    public void verifyLeftButton() {
    	logReporter.log("verify left arrow", webActions.checkElementDisplayed(feefoLeftButton));
    	
    }
    public void closePopup() {
    	logReporter.log("close the welcome popup", webActions.click(closePopup));
    }
}
