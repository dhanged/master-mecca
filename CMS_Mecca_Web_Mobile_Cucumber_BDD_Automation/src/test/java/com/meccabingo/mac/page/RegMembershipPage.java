package com.meccabingo.mac.page;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

public class RegMembershipPage {
	private WebActions webActions;
	private LogReporter logReporter;
	private Utilities utilities;
	private WaitMethods wait;

	public RegMembershipPage(WebActions webActions, LogReporter logReporter, WaitMethods wait, Utilities utilities) {
		this.webActions = webActions;
		this.logReporter = logReporter;
		this.utilities = utilities;
		this.wait = wait;

	}
	
	String greencolor = "#5ea85a";
	String greycolor = "#e6dae6";
	String redcolor = "#e22c2c";
	By locator = By.xpath("");
	
	
	By title = By.xpath("//li/span/p[contains(text(),'Mr')]");
	By titletext = By.xpath("//li/span/p[contains(text(),'Title')]");
	By name = By.xpath("//li/span/p[contains(text(),'John Ray')]");
	By nametext = By.xpath("//li/span/p[contains(text(),'Name')]");
	By DOB = By.xpath("//li/span/p[contains(text(),'5th June 1996')]");
	By dateofbirthtext = By.xpath("//li/span/p[contains(text(),'Date of Birth')]");
	
	By username = By.xpath("//input[contains(@id,'username')]");
	By password = By.xpath("//input[contains(@id,'password')]");
	By selectAll = By.xpath("//button[contains(@class,'colour-button-green')]");
	By email = By.xpath("//input[contains(@id,'gdpr-email')]");
	By sms = By.xpath("//input[contains(@id,'gdpr-sms')]");
	By phone = By.xpath("//input[contains(@id,'gdpr-phone')]");
	By post = By.xpath("//input[contains(@id,'gdpr-post')]");
	By offersandcommunication = By.xpath("");
	By depositlimit = By.xpath("");
	By MPC = By.xpath("//input[contains(@id,'gdpr-not-receive')]");
	By register = By.xpath("//button/span[contains(text(),'Register')]");
	By successmessage = By.xpath("");
	By EmailAdrress = By.xpath("//input[contains(@type,'email')]");
	
	By errorofemail = By.xpath("");
	By errorofmobile = By.xpath("");
	By errorofusername = By.xpath("");
	By errorofpassword = By.xpath("");
	By errorofmarketingpreferencefield = By.xpath("");
	By errorofpostcode = By.xpath("");
	By countryname = By.xpath("");
	By postcodename = By.xpath("");
	By gibcountry = By.xpath("");
	By agecheckboxofmembership = By.xpath("//*[@id=\"id-agreed\"]");
	
	
	public void verify() {
		logReporter.log("", webActions.checkElementDisplayed(locator));
	}  
	public void clickOn() {
		logReporter.log("", webActions.click(locator));
	}	
	public void verifyTitle() {
		logReporter.log("", webActions.checkElementDisplayed(title));
		logReporter.log("", webActions.checkElementDisplayed(titletext));
	}
	public void verifyName() {
		logReporter.log("", webActions.checkElementDisplayed(name));
		logReporter.log("", webActions.checkElementDisplayed(nametext));
	}
	
	public void verifyDateofBirth() {
		logReporter.log("", webActions.checkElementDisplayed(DOB));
		logReporter.log("", webActions.checkElementDisplayed(dateofbirthtext));
	}
	
	public void verifyUsernameTextbox() {
		logReporter.log("", webActions.checkElementDisplayed(username));
	}
	public void verifyPasswordTextbox() {
		logReporter.log("", webActions.checkElementDisplayed(password));
	}
	public void verifySelectAllButton() {
		logReporter.log("", webActions.checkElementDisplayed(selectAll));	
	}
	public void verifyCheckboxes(String options) {
		switch (options) {
		case "Email":
			logReporter.log("", webActions.checkElementDisplayed(email));
			break;
		case "SMS":
			logReporter.log("", webActions.checkElementDisplayed(sms));
			break;
		case "Phone":
			logReporter.log("", webActions.checkElementDisplayed(phone));
			break;
		case "Post":
			logReporter.log("", webActions.checkElementDisplayed(post));
			break;

		}

	}
	public void verifyOffersCheckbox() {
		logReporter.log("", webActions.checkElementDisplayed(offersandcommunication));
	} 
	public void verifyDepositLimitSection() {
		logReporter.log("", webActions.checkElementDisplayed(depositlimit));
	}  
	public void clickOnSelectAllCTA() {
		logReporter.log("", webActions.click(selectAll));
	}
	public void clickOnMarketingPreferenceCheckbox() {
		logReporter.log("", webActions.click(MPC));
	}	
	public void clickOnRegisterButton() {
		logReporter.log("", webActions.click(register));
	}
	public void verifySuccessMessage() {
		logReporter.log("", webActions.checkElementDisplayed(successmessage));
	}
	public void verifyCheckboxIsNotSelected() {
		if (!webActions.selectCheckbox(email, false)) {
			logReporter.log("Check element", true);
		}
	}  
	public void verifyRedColorBelowEmailAdrress() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(EmailAdrress, "border-bottom-color", redcolor));
	}
	public void enterInvalidEmailAdrress() {
		logReporter.log("enter invalid email", webActions.setText(EmailAdrress, utilities.getEmail()));
	}
	public void verifyErrorMessageBelowEmailAdrress() {
		logReporter.log("", webActions.checkElementDisplayed(errorofemail));
	}
	public void verifyErrorMessageBelowEmailAdrressInRedColor() {
		logReporter.log("", webActions.checkCssValue(errorofemail, "color", redcolor));
	}
	public void verifyErrorMessageBelowMobilenumber() {
		logReporter.log("", webActions.checkElementDisplayed(errorofmobile));
	}
	
	public void verifyErrorMessageBelowUsername() {
		logReporter.log("", webActions.checkElementDisplayed(errorofusername));
	}
	public void verifyErrorMessageBelowPassword() {
		logReporter.log("", webActions.checkElementDisplayed(errorofpassword));
	}
	public void enterExistingUsername(String name) {
		logReporter.log("", webActions.setTextWithClear(username, name));
	}
	public void verifyErrorMessageBelowMarketingPreferenceField() {
		logReporter.log("", webActions.checkElementDisplayed(errorofmarketingpreferencefield));
	}
	public void verifyErrorMessageBelowPostcode() {
		logReporter.log("", webActions.checkElementDisplayed(errorofpostcode));
	}
	public void verifyCountryNameIsDisplayed() {
		logReporter.log("", webActions.checkElementDisplayed(countryname));
	}
	public void verifyPostcodeIsDisplayed() {
		logReporter.log("", webActions.checkElementDisplayed(postcodename));
	}
	public void clickOnGibRoiCountry() {
		logReporter.log("", webActions.selectFromDropDown(gibcountry, "GIB"));
	}
	public void clickOnAgeCheckboxofMembership() {
		logReporter.log("", webActions.clickUsingJS(agecheckboxofmembership));
	}
}
