
package com.meccabingo.mac.page;

import org.openqa.selenium.By;
import com.generic.WebActions;
import com.generic.logger.LogReporter;

import io.cucumber.datatable.DataTable;

/**
 * @author Datta Mehtre (Expleo)
 *
 */
public class PromotionPage
{
	private WebActions objWebActions;
	private LogReporter logReporter;
	
	public PromotionPage(WebActions webActions, LogReporter logReporter) 
	{
		this.objWebActions = webActions;
		this.logReporter = logReporter;
	}
	
	By PromotionTab = By.xpath("(//a[@href='/promotions'])[1]");
	By InstantBonusCard = By.xpath("//h3[contains(.,'INSTANT Bonus BAT Testing')]");
	By ButtonClaim = By.xpath("//apollo-activate-bonus-cta[@promotion-type='claim']/child::button[contains(.,'Claim')]");
	By ClaimedButton = By.xpath("//apollo-activate-bonus-cta[@promotion-type='claim']/child::button[contains(.,'Claimed')]");
	By ClaimButtonFromDetailPage = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='Claim']");
	By LearnMoreButtonFromInstantBonusCard = By.xpath("//apollo-activate-bonus-cta[@promotion-type='claim']//preceding-sibling::a");
	By TandCLinkFromInstantBonusCard = By.xpath("//a[@href='/promotions/instant-bonus#terms-and-conditions']");
	By PromotionHeading = By.xpath("//h2[contains(.,'Promotions')]");
	By ClaimedStatusOnPromotionDetailPage = By.xpath("//apollo-activate-bonus-cta[@cta-done-text='Claimed']");
	By FeaturedGameSectionIsDisplayed = By.xpath("(//apollo-slot-tile)[2]");
	By CashBackBonusOptInButton = By.xpath("//apollo-activate-bonus-cta[@title='CASHBACKONTOTALSTAKE Bonus BAT Testing']/child::button[contains(.,'Opt-in')]");
	By CashBackBonusLearnMoreButton = By.xpath("//apollo-activate-bonus-cta[@title='CASHBACKONTOTALSTAKE Bonus BAT Testing']/preceding-sibling::a[contains(.,'Learn more')]");
	By CashbackBonusTandCLink = By.xpath("//a[@href='/promotions/cashback-on-total-stake#terms-and-conditions']");
	By FeaturedGameSectionForCashbackOptIn = By.xpath("//div[@class='promotion-info-featured-games ']/child::div[@class='promotion-info-featured-games-inner']");
	By OptedInButtonForCashbackBonusDetailPage = By.xpath("//apollo-activate-bonus-cta[@cta-done-text='Opted In']");
	By OptInButtonForCashbackBonusDetailPage = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='Opt-in']");
	By CashbackBonusOptedIn = By.xpath("//apollo-activate-bonus-cta[@title='CASHBACKONTOTALSTAKE Bonus BAT Testing']/child::button[contains(.,'Opted In')]");
	
	public void clickOnPramotionTab()
	{
		logReporter.log("Check Pramotion tab is displyed:", objWebActions.checkElementDisplayed(PromotionTab));
		logReporter.log("Click on Pramotion tab :", objWebActions.click(PromotionTab));
	}
	
	public void VerifyBonusCardIsPresent(String bonusCard)
	{
		switch(bonusCard)
		{
			case"Instant bonus":
			{
				logReporter.log("Check Pramotion heading is dispalyed::::",objWebActions.checkElementDisplayedWithMidWait(PromotionHeading));
				objWebActions.executeJavascript("window.scrollBy(0,500)");
				objWebActions.checkElementDisplayedWithMidWait(InstantBonusCard);
				logReporter.log("Check Instant bonus card is available :",objWebActions.checkElementDisplayed(InstantBonusCard));
				break;
			}
			
			case"Cashback opt in":
			{
				By CashbackOptIn = By.xpath("//h3[contains(.,'CASHBACKONTOTALSTAKE Bonus BAT Testing')]");
				objWebActions.executeJavascript("window.scrollBy(0,1000)");
				logReporter.log("Check cashback Opt In bonus card is available :",objWebActions.checkElementDisplayed(CashbackOptIn));
				break;
			}
		}
	}

	public void VerifyBonusIsNotClaimed(String bonusName)
	{
		switch(bonusName)
		{
			case "Instant bonus":
			{
				logReporter.log("Instant Bonus is not claimed ", objWebActions.checkElementDisplayed(ButtonClaim));
				break;
			}
			case "Cashback opt in bonus":
			{
				objWebActions.executeJavascript("window.scrollBy(0,1000)");
				logReporter.log("Cashback Opt in bonus is not claimed ", objWebActions.checkElementDisplayed(CashBackBonusOptInButton));
				break;
			}
		}
	}
	
		
	public void verifyBonusCardComponents(String bonusName, String text )
	{
		String BonusCard = new String("Cashback Opt in bonus");
	
		if(bonusName.contains("Cashback Opt in bonus"))
		{
			switch (text)
			{

				case "Opt in": 
					logReporter.log("check element > >", objWebActions.checkElementDisplayed(CashBackBonusOptInButton));
					break;
				case "Learn more": 
					logReporter.log("check element > >", objWebActions.checkElementDisplayed(CashBackBonusLearnMoreButton));
					break;
				case "T&Cs apply": 
					logReporter.log("check element > >", objWebActions.checkElementDisplayed(CashbackBonusTandCLink));
					break;
				case "Opted in": 
					objWebActions.checkElementDisplayedWithMidWait(CashbackBonusOptedIn);
					logReporter.log("check element > >", objWebActions.checkElementDisplayed(CashbackBonusOptedIn));
					break;
			}
		}
		else if(BonusCard=="Instant bonus")
		{
			switch(text)
			{
			case "Claim": 
				logReporter.log("check element > >", objWebActions.checkElementDisplayed(ButtonClaim));
				break;
			case "Claimed": 
				logReporter.log("check element > >", objWebActions.checkElementDisplayed(ClaimedButton));
				break;
			case "Learn more": 
				logReporter.log("check element > >", objWebActions.checkElementDisplayed(LearnMoreButtonFromInstantBonusCard));
				break;
			case "T&Cs apply": 
				logReporter.log("check element > >", objWebActions.checkElementDisplayed(TandCLinkFromInstantBonusCard));
				break;
			}
		}
		else
		{
			logReporter.log("Bonus name is not available ", false);
		}
		
	}
	
	
	public void VerifyStausOnPromotion(String status)
	{
		switch(status)
		{
			case"Claimed":
				objWebActions.executeJavascript("window.scrollBy(0,500)");
				logReporter.log("verify Claimed status on page", objWebActions.checkElementDisplayed(ClaimedStatusOnPromotionDetailPage));
		 	break;
		 	
			case"Opted in":
				objWebActions.executeJavascript("window.scrollBy(0,500)");
				logReporter.log("verify Opted in Status on page", objWebActions.checkElementDisplayed(OptedInButtonForCashbackBonusDetailPage));
		 	break;
		 	
		}
	}

	public void VerifyFeaturedGameSectionIsDisplayed() 
	{
		logReporter.log("verify Featured game section on page", objWebActions.checkElementDisplayed(FeaturedGameSectionIsDisplayed));
	}

	public void ClickOnButtonFromBonusCard(String buttonName, String bonusCard)
	{
	
		switch(buttonName)
		{
			case"Opt in":
				objWebActions.executeJavascript("window.scrollBy(0,500)");
				By OptInButton = By.xpath("//apollo-activate-bonus-cta[@title='CASHBACKONTOTALSTAKE Bonus BAT Testing']/child::button");
				logReporter.log("Click on Opt in button ", objWebActions.click(OptInButton));
				break;
			case"Claim":
				logReporter.log("Click on Opt in button ", objWebActions.click(ButtonClaim));
				break;
		}
	}

	public void ClickOnButtonsFromPromotionDetailPage(String buttonName) 
	{
		switch(buttonName)
		{
			case"Claim":
				logReporter.log("Click on Claim button from Instant promotion detail page::",objWebActions.click(ClaimButtonFromDetailPage));
		}
	}
}
