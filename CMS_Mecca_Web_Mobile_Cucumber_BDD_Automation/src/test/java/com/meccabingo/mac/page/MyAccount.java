package com.meccabingo.mac.page;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.server.handler.FindElements;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.webdriver.DriverProvider;

public class MyAccount {

	private WebActions webActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;

	public MyAccount(WebActions webActions, DriverProvider driverProvider,LogReporter logReporter) {
		this.webActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;

	}
	String greencolor = "#009D7A";
	
	By Username = By.xpath("//h4[contains(text(),'AUTOMECCA2020')]");
	By Closebutton = By.xpath("//a[contains(@class,'icon-close')]");
	By backbutton = By.xpath("//a[contains(@class,'icon-arrow-left')]");
	By PlayableBalance = By.xpath("//h5[contains(text(),'Playable Balance')]");
	By Cashier = By.xpath("//a[contains(@heading,'Cashier')]");
	By Bonuses = By.xpath("//a[contains(@heading,'Bonuses')]");
	By Messages = By.xpath("//a[contains(@heading,'Message')]");
	By Accountdetails = By.xpath("//a[contains(@heading,'Account Details')]");
	By Responsiblegambling = By.xpath("//a[contains(@heading,'Responsible Gambling')]");
	By Logouticon = By.xpath("//a[contains(text(),'Logout')]/i");
	By Logoutlink = By.xpath("//a[contains(text(),'Logout')]");
	By Recentlyplayedsection = By.xpath("//h4[contains(text(),'Recently Played')]");
	By Email = By.xpath("//a[contains(text(),'Support@meccabingo.com')]");
	By Phonenumber = By.xpath("//div[contains(@class,'contacts-wrapper')]/span/p[contains(text(),'Call')]");
	By Livechat = By.xpath("//i[contains(@class,'icon-live-chat')]");
	By LiveHelpLink = By.xpath("//p[contains(text(),'Live h')]");

	By PlayableBalanceAmount = By.xpath("//h5[contains(text(),'Playable Balance')]/span");
	By PlayableBalancePlusIcon = By.xpath("//i[contains(@class,'icon-expand ')]");
	By PlayableBalanceMinusIcon = By.xpath("//i[contains(@class,'icon-collapse opened')]");
	By PlayableBalanceCashText = By
			.xpath("//div[contains(@class,'playable-balance-details')]/div/p[contains(text(),'Cash')]");
	By PlayableBalanceCashAmount = By
			.xpath("//div[contains(@class,'playable-balance-details')]/div/p[contains(text(),'Cash')]/span");
	By PlayableBalanceBonusesText = By
			.xpath("//div[contains(@class,'playable-balance-details')]/div/p[contains(text(),'Bonuses')]");
	By PlayableBalanceBonusesAmount = By
			.xpath("//div[contains(@class,'playable-balance-details')]/div/p[contains(text(),'Bonuses')]/span");
	By PlayableBalanceDetailedButton = By.xpath("//div[contains(@class,'playable-balance-details')]/div/button[1]");
	By PlayableBalanceDepositButton = By.xpath("//div[contains(@class,'playable-balance-details')]/div/button[2]");
	By balancetext = By.xpath("//h4[contains(text(),'Balance')]");

	// By getPageTitle(String pageTitle) {
	// return By.xpath("//h4[contains(text(),'" + pageTitle + "')]");
	// }

	By MyAccountButton = By.xpath("//a[contains(@class,'open-myaccount')]/i");
	// Cashier
	By CashierPageTitle = By.xpath("//h4[contains(text(),'Cashier')]");

	By DepositOnCashierPage = By.xpath("//a[contains(@heading,'Deposit')]");
	By WithdrawalOnCashierPage = By.xpath("//div[contains(@class,'my-account-menu')]/a[2]");
	By PendingWithdrawalsOnCashierPage = By.xpath("//a[contains(@heading,'Pending Withdrawals')]");
	By TransactionHistoryOnCashierPage = By.xpath("//a[contains(@heading,'Transaction History')]");
	By BalanceOnCashierPage = By.xpath("//a[contains(@heading,'Balance')]");

	// Cashier deposit
	By TitleOnCashierDepositPage = By.xpath("//h4[contains(text(),'Deposit')]");
	By DepositLimitLink = By.xpath("//a[contains(text(),'Why not set deposit limits?')]");
	By SafechargeFrame = By.xpath("//form[contains(@id,'PPPParametersForm')]/div");

	By TitleOnCashierWithdrawalPage = By.xpath("//h4[contains(text(),'Withdrawal')]");
	By AmountText = By.xpath("//h3[contains(text(),'Choose amount of withdraw')]");
	By AlertBox = By
			.xpath("//span[contains(text(),'Please note: once you withdraw the cash you can’t reverse withdrawals')]");
	By AmountTextbox = By.xpath("//input[contains(@id,'input-amount')]");
	By PasswordTextbox = By.xpath("//input[contains(@id,'input-password')]");
	By NextButton = By.xpath("//button/span[contains(.,'Next')]");

	By TitleOnCashierPendingWithdrawalsPage = By.xpath("//h4[contains(text(),'Pending Withdrawals')]");

	By TitleOnCashierTransactionHistoryPage = By.xpath("//h4[contains(text(),'Transaction History')]");
	By ActivityBox = By.xpath("//div[contains(@class,'dialog-box')]");
	By HistoryTable = By.xpath("//div[contains(@class,'my-account')]");

	By TitleOnCashierBalancePage = By.xpath("//h4[contains(text(),'Balance')]");
	By DepositLimitOnBalancePage = By.xpath("//a[contains(text(),'Set a Deposit limit')]");
	By BlanceBox = By.xpath("//div[contains(@class,'content-box')]");

	// Bonuses
	By BonusesPageTitle = By.xpath("//h4[contains(text(),'Bonuses')]");
	By EnterBonusCode = By.xpath("//a[contains(@heading,'Enter bonus code')]");
	By EnterBonusCodeTitle = By.xpath("//h4[contains(text(),'Enter promotional code')]");
	By EnterBonusCodeText = By.xpath("//h6[contains(text(),'Enter bonus code')]");
	By EnterBonusCodeField = By.xpath("//input[contains(@id,'bonus-code')]");
	By SubmitButton = By.xpath("//button[contains(@type,'submit')]");

	By ActiveBonus = By.xpath("//a[contains(@heading,'Active bonuses')]");
	By ActiveBonusTitle = By.xpath("//h4[contains(text(),'Active bonuses')]");
	By ActiveBonuses = By.xpath("//*[@id=\"overlay-level-3\"]/div/section/div[2]/div/div[1]/div");

	By BonusHistory = By.xpath("//a[contains(@heading,'Bonus History')]");
	By BonusHistoryTitle = By.xpath("//h4[contains(text(),'Bonus History')]");
	By HistoryBox = By.xpath("//div[contains(@class,'dialog-box')]");

	// Messages
	By MessagesPageTitle = By.xpath("//h4[contains(text(),'Message')]");

	// Account Details
	By AccountdetailsPageTitle = By.xpath("//h4[contains(text(),'Account Details')]");
	By DetailsBox = By.xpath("//div[contains(@class,'content-box-details')]");
	By EditButton = By.xpath("//button[@type='button']/span[text()='Edit']");
	By ChangePassword = By.xpath("//span[text()='Change Password']");
	By ChangePasswordTitle = By.xpath("//h4[contains(text(),'Change Password')]");
	By CurrentPassword = By.xpath("//input[contains(@id,'old-password')]");
	By NewPassword = By.xpath("//input[contains(@id,'new-password')]");
	By UpdateButton = By.xpath("//button/span[contains(.,'UPDATE')]");

	By MarketinPreferences = By.xpath("//a[contains(@heading,'Marketing Preferences')]");
	By MarketinPreferencesTitle = By.xpath("//h4[contains(text(),'Marketing Preferences')]");
	By EmailCheckbox = By.xpath("//input[contains(@id,'gdpr-email')]");
	By SMSCheckbox = By.xpath("//input[contains(@id,'gdpr-sms')]");
	By PhoneCheckbox = By.xpath("//input[contains(@id,'gdpr-phone')]");
	By PostCheckbox = By.xpath("//input[contains(@id,'gdpr-post')]");
	By SelectAllCheckbox = By.xpath("//input[contains(@id,'gdpr-all')]");

	// Responsible Gambling
	By ResponsiblegamblingPageTitle = By.xpath("//h4[contains(text(),'Responsible Gambling')]");
	
	//Reality check
	By RealityCheck = By.xpath("//a[contains(@heading,'Reality Check')]");
	By RealityCheckTitle = By.xpath("//h4[contains(text(),'Reality Check')]");
	By textboxtext = By.xpath("//h5[text()='Tell me more']");
	By uncollapsibleicon = By.xpath("(//i[contains(@class,'icon-expand ')])[2]");
	By textonpage = By.xpath("//p[contains(text(),'Reality Checks are a helpful way of keeping track of the time you have spent playing our games')]");
	By option = By.xpath("");
	By tellmemoreicon = By.xpath("");
	By setyourreminder = By.xpath("");
	By noreminderbutton = By.xpath("");
	By firstbutton = By.xpath("");
	By secondbutton = By.xpath("");
	By thirdbutton = By.xpath("");
	By savechangesbutton = By.xpath("");
	
	
	By DepositLimits = By.xpath("//a[contains(@heading,'Deposit Limits')]");
	By DepositLimitsTitle = By.xpath("//h4[contains(text(),'Deposit Limits')]");
	
	By Takeabreak = By.xpath("//a[contains(@heading,'Take a break')]");
	By TakeabreakTitle = By.xpath("//h4[contains(text(),'Take a break')]");
	By TextBox = By.xpath("//p[contains(text(),'break period')]");
	By BreakCollapsibleBox = By.xpath("//div[contains(@class,'dialog-box-collapsible-title')]");
	By BreakTimeQuestion = By.xpath("//h5[contains(text(),'How long would you like to take a break?')]");
	By BreakButton = By.xpath("//button[contains(@type,'button')]/span[contains(text(),'1 Day')]");

	By BreakButtons(String buttons) {
		return By.xpath("//button[contains(@type,'button')]/span[contains(text(),'" + buttons + "')]");
	}
	By BreakPeriodText = By.xpath("//p[contains(text(),'If you would like to take a temporary break from gambling, you can choose a break period from 1 day up to 6 weeks.')]");
	By BreakRules = By.xpath("");
	
	By PasswordInput = By.xpath("//input[contains(@placeholder,'Password')]");
	By TakeABreak = By.xpath("//button/span[contains(.,'Take A Break')]");
	
	By ConfirmationPopup = By.xpath("//div[contains(@class,'take-a-break-confirmation')]");
	By TakeaBreakLogOutButton = By.xpath("//button/span[contains(text(),'Take a break & Log Out')]");
	By CancelButton = By.xpath("//button[contains(@type,'button')]/span[contains(.,'Cancel')]");

	By SelfExclude = By.xpath("//a[contains(@heading,'Self Exclude')]");
	By SelfExcludeTitle = By.xpath("//span[contains(text(),'Self Exclude')]");
	By Question = By.xpath("//div[contains(text(),'Do you feel you have a problem with gambling?')]");

	By GamStop = By.xpath("//a[contains(@heading,'GamStop')]");
	By GamStopTitle = By.xpath("//h4[contains(text(),'GamStop')]");
	By GamLink = By.xpath("//a[contains(@title,'http://www.gamstop.co.uk/')]");
    By GamStoptext = By.xpath("");
	By successmessage = By.xpath("//h2[contains(text(),'Deposit successful')]"); 
	By closebutton;
    
    By city = By.id("input-town-city");
	By county = By.id("input-county");
	By addressLine1 = By.id("input-address-line1");
    
    private String iFrameId = "payment-process";
    
    private int messageCount;
    
	public void verifyPlayableBalanceText() {
		logReporter.log("Verify playable balance text", webActions.checkElementDisplayed(PlayableBalance));
	}

	public void verifyPlayableBalanceAmount() {
		logReporter.log("Verify playable balance amount", webActions.checkElementDisplayed(PlayableBalanceAmount));
	}

	public void verifyPlayableBalanceIcon() {
		logReporter.log("Verify playable balance plus icon", webActions.checkElementDisplayed(PlayableBalancePlusIcon));
	}

	public void clickOnPlayableBalanceIcon() {
		logReporter.log("Click on playable balance plus icon", webActions.click(PlayableBalancePlusIcon));
	}

	public void clickOnPlayableBalanceMinusIcon() {
		logReporter.log("Click on playable balance minus icon", webActions.click(PlayableBalanceMinusIcon));
	}

	public void verifyPlayableBalanceMinusIcon() {
		logReporter.log("Click on playable balance minus icon",
				webActions.checkElementDisplayed(PlayableBalanceMinusIcon));
	}

	public void clickOnPlayableBalanceDetailedButton() {
		logReporter.log("click on detailed view button", webActions.click(PlayableBalanceDetailedButton));
	}

	public void clickOnPlayableBalanceDepositButton() {
		logReporter.log("click on deposit button", webActions.click(PlayableBalanceDepositButton));
	}

	public void clickOnBackbutton() {
		logReporter.log("click on back button", webActions.click(backbutton));
	}

	public void verifyBalancePage() {
		logReporter.log("Verify playable balance text", webActions.checkElementDisplayed(balancetext));
	}

	public void clickOnLiveChatLink() {
		logReporter.log("click on live chat link", webActions.click(Livechat));
	}

	public void verifyMessageCountOnMessageText() {
		By MessagesCount = By.xpath("//div[contains(@class,'messages-count')]");
		logReporter.log("message count on message text", webActions.checkElementDisplayed(MessagesCount));
	}

	public void clickUsernameOnMyAccountPage() {
		if (webActions.click(Username))
			logReporter.log("click on username on my account page", true);
	}

	public void clickOnCloseButton() {
		logReporter.log("click on close button", webActions.click(Closebutton));
	}

	public void verifyMyAccountButton() {
		logReporter.log("verify my account window gets closed", webActions.checkElementDisplayed(MyAccountButton));
	}

	public void clickOnOption(String option) {
		By options = By.xpath("//span[contains(text(),'" + option + "')]");
		logReporter.log("click on selected option", webActions.click(options));
	}

	public void verifyPageTitle(String title) {
		By titles = By.xpath("//span[contains(text(),'" + title + "')]");
		logReporter.log("verify titles on page", webActions.checkElementDisplayed(titles));
	}

	public void clickOnCashierDepositBtn() {
		logReporter.log("click on deposit button", webActions.click(DepositOnCashierPage));
	}

	public void verifyCashierDepositPage() {
		logReporter.log("verify deposit page", webActions.checkElementDisplayed(TitleOnCashierDepositPage));
	}

	public void clickOnCashierWithdrawalBtn() {
		logReporter.log("click on deposit button", webActions.click(WithdrawalOnCashierPage));
	}

	public void verifyCashierWithdrawalPage() {
		logReporter.log("verify deposit page", webActions.checkElementDisplayed(TitleOnCashierWithdrawalPage));
	}

	public void clickOnCashierPendingWithdrawalsBtn() {
		logReporter.log("click on deposit button", webActions.click(PendingWithdrawalsOnCashierPage));
	}

	public void verifyCashierPendingWithdrawalsPage() {
		logReporter.log("verify deposit page", webActions.checkElementDisplayed(TitleOnCashierPendingWithdrawalsPage));
	}

	public void clickOnCashierTransactionHistoryBtn() {
		logReporter.log("click on deposit button", webActions.click(TransactionHistoryOnCashierPage));
	}

	public void verifyCashierTransactionHistoryPage() {
		logReporter.log("verify deposit page", webActions.checkElementDisplayed(TitleOnCashierTransactionHistoryPage));
	}

	public void clickOnCashierBalanceBtn() {
		logReporter.log("click on deposit button", webActions.click(BalanceOnCashierPage));
	}

	public void verifyCashierBalancePage() {
		logReporter.log("verify deposit page", webActions.checkElementDisplayed(TitleOnCashierBalancePage));
	}

	public void clickOnChangePassword() {
		logReporter.log("click on change password link", webActions.click(ChangePassword));
	}

	public void verifyUpdateBtnIsDisabled() {
		logReporter.log("verify update button id disabled", webActions.checkElementDisplayed(UpdateButton));
	}

	public void verifyUpdateBtnIsEnabled() {
		logReporter.log("verify update button id disabled", webActions.checkElementDisplayed(UpdateButton));
	}

	public void enterValidCurrentPassword() {
		logReporter.log("enter current password", webActions.setText(CurrentPassword, "Password123"));
	}

	public void enterValidNewPassword() {
		logReporter.log("enter new password", webActions.setText(NewPassword, "Password123"));
	}

	public void clickOnBreakTimeButton() {
		logReporter.log("click on Break Time Button", webActions.click(BreakButton));
	}

	public void verifyBreakPeriodText() {
		logReporter.log("verify Break Period Text", webActions.checkElementDisplayed(BreakPeriodText));
	}

	public void clickOnUncollapseIcon() {
		//logReporter.log("click on uncollapse icon on take a break page", webActions.click());
	}

	public void verifyButtonGetsHighlighted() {
		String value = "#f8ab20";
		logReporter.log("verify One Break Time Button gets highlighted",
				webActions.checkCssValue(BreakButton, "color", value));
	}

	public void clickOnTakeABreakButton() {
		logReporter.log("click on take a Break Button", webActions.click(TakeABreak));
	}

	public void verifyConfirmationBox() {
		logReporter.log("verify confirmation box", webActions.checkElementDisplayed(ConfirmationPopup));
	}

	public void clickBreakOnPopup() {
		logReporter.log("click on break button on popup", webActions.click(TakeaBreakLogOutButton));

	}
	public void clickLogoutOnPopup() {
		logReporter.log("click on logout button on popup", webActions.click(CancelButton));

	}
	//reality check page
	
	public void verifyTextInTextboxOnRealityPage() {
		logReporter.log("verify text in textbox", webActions.checkElementDisplayed(textboxtext));
	}
	public void clickOnUncollapsibleIconOnRealityPage() {
		logReporter.log("click on Uncollapsible Icon on reality page ", webActions.click(uncollapsibleicon));		
	}
	public void verifyTextOnRealityPage() {
		logReporter.log("verify text on reality page", webActions.checkElementDisplayed(textonpage));
	}
	public void selectOptionOnRealityPage() {
		logReporter.log("click on single option on reality page", webActions.click(option));
		
	}
	public void clickOnSaveChangesOnRealityPage() {
		logReporter.log("click on save changes on reality page", webActions.click(savechangesbutton));
	}
	public void verifySuccessMessageOnLoginPage() {
		logReporter.log("", webActions.checkElementDisplayedWithMidWait(successmessage));
	}
	public void closeSuccessPopup() {
	logReporter.log("", webActions.click(closebutton));
	}
	
	public void enterPasswdForCurrentPassword(String passwd) {
		logReporter.log("Enter password for current password", webActions.setText(CurrentPassword, passwd));
	}
	public void verifyGreenColorBelowCurrentPassword() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(CurrentPassword, "border-bottom-color", greencolor));
	}
	
	public void enterPasswdForNewPassword(String passwd) {
		logReporter.log("Enter password for new password", webActions.setText(NewPassword, passwd));
	}
	public void verifyGreenColorBelowNewPassword() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(NewPassword, "border-bottom-color", greencolor));
	}
	
	public void verifyPaypalWindow() {
		String windowTitle = "";
		logReporter.log("",webActions.switchToWindowUsingTitle(windowTitle));
	}
	
	
	public void verifyAllFieldsOnPage(String texts) {
		switch (texts) {
		case "Username":
			logReporter.log("Check Username text ", webActions.checkElementDisplayed(Username));
			break;
		case "Close button":
			logReporter.log("Check Close button", webActions.checkElementDisplayed(Closebutton));
			break;
		case "Playable Balance":
			logReporter.log(" Check Payable Balance", webActions.checkElementDisplayed(PlayableBalance));
			break;
		case "Cashier":
			logReporter.log("Check Cashier", webActions.checkElementDisplayed(Cashier));
			break;
		case "Bonuses":
			logReporter.log(" Check Bonuses", webActions.checkElementDisplayed(Bonuses));
			break;
		case "Messages":
			logReporter.log(" Check Messages", webActions.checkElementDisplayed(Messages));
			break;
		case "Account details":
			logReporter.log(" Check Account details", webActions.checkElementDisplayed(Accountdetails));
			break;
		case "Responsible gambling":
			logReporter.log(" Check Responsible gambling", webActions.checkElementDisplayed(Responsiblegambling));
			break;
		case "Log out icon":
			logReporter.log(" Check Log out icon", webActions.checkElementDisplayed(Logouticon));
			break;
		case "Log out link":
			logReporter.log(" Check Log out link", webActions.checkElementDisplayed(Logoutlink));
			break;
		case "Recently played section":
			logReporter.log(" Check Recently played section", webActions.checkElementDisplayed(Recentlyplayedsection));
			break;
		case "Email":
			logReporter.log(" Check Email", webActions.checkElementDisplayed(Email));
			break;
		case "Phone number":
			logReporter.log(" Check Phone number", webActions.checkElementDisplayed(Phonenumber));
			break;
		case "Live chat":
			logReporter.log(" Check Live chat", webActions.checkElementDisplayed(Livechat));
			break;
		}
	}

	public void verifyAllFieldsOnPlayableBalanceSection(String texts) {
		switch (texts) {
		case "Playable Balance":
			logReporter.log(" Check Payable Balance", webActions.checkElementDisplayed(PlayableBalance));
			break;
		case "playable balance amount":
			logReporter.log(" Check Payable Balance", webActions.checkElementDisplayed(PlayableBalanceAmount));
			break;
		case "playable balance minus icon":
			logReporter.log(" Check playable balance amount",
					webActions.checkElementDisplayed(PlayableBalanceMinusIcon));
			break;
		case "cash text":
			logReporter.log(" Check cash text", webActions.checkElementDisplayed(PlayableBalanceCashText));
			break;
		case "cash amount":
			logReporter.log(" Check cash amount", webActions.checkElementDisplayed(PlayableBalanceCashAmount));
			break;
		case "bonuses text":
			logReporter.log(" Check bonuses text", webActions.checkElementDisplayed(PlayableBalanceBonusesText));
			break;
		case "bonuses amount":
			logReporter.log(" Check bonuses amount", webActions.checkElementDisplayed(PlayableBalanceBonusesAmount));
			break;
		case "detailed view button":
			logReporter.log(" Check etailed view button",
					webActions.checkElementDisplayed(PlayableBalanceDetailedButton));
			break;
		case "deposit button":
			logReporter.log(" Check deposit button", webActions.checkElementDisplayed(PlayableBalanceDepositButton));
			break;
		}
	}

	public void verifyOptionsOnCashierPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", webActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", webActions.checkElementDisplayed(backbutton));
			break;
		case "Deposit":
			logReporter.log(" Check Deposit button", webActions.checkElementDisplayed(DepositOnCashierPage));
			break;
		case "Withdrawal":
			logReporter.log(" Check Withdrawal button", webActions.checkElementDisplayed(WithdrawalOnCashierPage));
			break;
		case "Pending Withdrawals":
			logReporter.log(" Check Pending Withdrawals button",
					webActions.checkElementDisplayed(PendingWithdrawalsOnCashierPage));
			break;
		case "Transaction History":
			logReporter.log(" Check Transaction History button",
					webActions.checkElementDisplayed(TransactionHistoryOnCashierPage));
			break;
		case "Balance":
			logReporter.log(" Check Balance button", webActions.checkElementDisplayed(BalanceOnCashierPage));
			break;

		case "Email":
			logReporter.log(" Check Email", webActions.checkElementDisplayed(Email));
			break;
		case "Phone number":
			logReporter.log(" Check Phone number", webActions.checkElementDisplayed(Phonenumber));
			break;
		case "Live chat":
			logReporter.log(" Check Live chat", webActions.checkElementDisplayed(Livechat));
			break;
		}
	}

	public void clickFieldsOnPlayableBalanceSection(String options) {
		switch (options) {
		case "cash text":
			if (!webActions.click(PlayableBalanceCashText))
				logReporter.log(" Click cash text", true);
			break;
		case "cash amount":
			if (!webActions.click(PlayableBalanceCashAmount))
				logReporter.log(" Click cash amount", true);
			break;
		case "bonuses text":
			if (!webActions.click(PlayableBalanceBonusesText))
				logReporter.log(" Click bonuses tex", true);
			break;
		case "bonuses amount":
			if (!webActions.click(PlayableBalanceBonusesAmount))
				logReporter.log(" Click bonuses amount", true);
			break;

		}

	}

	public void verifyOptionsOnResponsibleGamblingPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", webActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", webActions.checkElementDisplayed(backbutton));
			break;
		case "Responsible Gambling Title":
			logReporter.log(" Check Responsible Gambling Title",
					webActions.checkElementDisplayed(ResponsiblegamblingPageTitle));
			break;
		case "Reality Check":
			logReporter.log(" Check Reality Check button", webActions.checkElementDisplayed(RealityCheck));
			break;
		case "Deposit Limits":
			logReporter.log(" Check Deposit Limits button", webActions.checkElementDisplayed(DepositLimits));
			break;
		case "Take a break":
			logReporter.log(" Check Take a break button", webActions.checkElementDisplayed(Takeabreak));
			break;
		case "Self Exclude":
			logReporter.log(" Check Self Exclude button", webActions.checkElementDisplayed(SelfExclude));
			break;
		case "Gamstop":
			logReporter.log(" Check Gamstop button", webActions.checkElementDisplayed(GamStop));
			break;
		case "Email":
			logReporter.log(" Check Email", webActions.checkElementDisplayed(Email));
			break;
		case "Phone number":
			logReporter.log(" Check Phone number", webActions.checkElementDisplayed(Phonenumber));
			break;
		case "Live chat":
			logReporter.log(" Check Live chat", webActions.checkElementDisplayed(Livechat));
			break;
		}

	}

	public void verifyOptionsOnDepositPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", webActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", webActions.checkElementDisplayed(backbutton));
			break;
		case "Deposit Title":
			logReporter.log(" Check Deposit Title", webActions.checkElementDisplayed(TitleOnCashierDepositPage));
			break;
		case "Live help link":
			logReporter.log(" Check Live help link", webActions.checkElementDisplayed(LiveHelpLink));
			break;
		case "Deposit limit link":
			logReporter.log(" Check Deposit limit link", webActions.checkElementDisplayed(DepositLimitLink));
			break;
		case "Safecharge frame":
			logReporter.log(" Check Take a break button", webActions.checkElementDisplayed(SafechargeFrame));
			break;

		}

	}

	public void verifyOptionsOnAccountDetailsPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", webActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", webActions.checkElementDisplayed(backbutton));
			break;
		case "Account Details Title":
			logReporter.log(" Check account details Title", webActions.checkElementDisplayed(AccountdetailsPageTitle));
			break;
		case "Account Details window":
			logReporter.log(" Check account details window", webActions.checkElementDisplayed(DetailsBox));
			break;
		case "Edit button":
			logReporter.log(" Check Edit button", webActions.checkElementDisplayed(EditButton));
			break;
		case "Change Password":
			logReporter.log(" Check change password", webActions.checkElementDisplayed(ChangePassword));
			break;
		case "Marketing Preference":
			logReporter.log(" Check marketing preference button",
					webActions.checkElementDisplayed(MarketinPreferences));
			break;
		case "Email":
			logReporter.log(" Check Email", webActions.checkElementDisplayed(Email));
			break;
		case "Phone number":
			logReporter.log(" Check Phone number", webActions.checkElementDisplayed(Phonenumber));
			break;
		case "Live chat":
			logReporter.log(" Check Live chat", webActions.checkElementDisplayed(Livechat));
			break;
		}
	}

	public void verifyOptionsOnTakeABreakPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", webActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", webActions.checkElementDisplayed(backbutton));
			break;
		case "Take a break Title":
			logReporter.log(" Check Take a break Title", webActions.checkElementDisplayed(TakeabreakTitle));
			break;
		case "Live help link":
			logReporter.log(" Check Live help link", webActions.checkElementDisplayed(LiveHelpLink));
			break;
		case "Text box":
			logReporter.log(" Check Text box", webActions.checkElementDisplayed(TextBox));
			break;
		case "Break collapsible box":
			logReporter.log(" Check Break collapsible box", webActions.checkElementDisplayed(BreakCollapsibleBox));
			break;
		case "Break time question":
			logReporter.log(" Check Break time question", webActions.checkElementDisplayed(BreakTimeQuestion));
			break;
		case "Break times buttons":
			logReporter.log(" Check Break times buttons", webActions.checkElementDisplayed(BreakButton));
			break;

		}

	}

	public void verifyOptions(String options) {
		switch (options) {
		case "Locked until date":
			logReporter.log(" Check Locked until date",webActions.checkElementDisplayed(BreakPeriodText));
			break;
		case "Break rules":
			logReporter.log(" Check Break rules", webActions.checkElementDisplayed(BreakRules));
			break;
		case "Password field":
			logReporter.log(" Check Password field", webActions.checkElementDisplayed(PasswordInput));
			break;
		case "Take a break button":
			logReporter.log(" Check Take a break button",webActions.checkElementDisplayed(TakeABreak));
			break;

		}
	}

	public void verifyOptionsOnRealityCheckPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", webActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", webActions.checkElementDisplayed(backbutton));
			break;
		case "Reality Check Title":
			logReporter.log(" Check Take a break Title", webActions.checkElementDisplayed(RealityCheckTitle));
			break;
		case "Live help link":
			logReporter.log(" Check Live help link", webActions.checkElementDisplayed(LiveHelpLink));
			break;
		case "Tell me more collapsible icon":
			logReporter.log(" Check Tell me more collapsible icon",webActions.checkElementDisplayed(tellmemoreicon));
			break;
		case "Set your reminder":
			logReporter.log(" Check Set your reminder",webActions.checkElementDisplayed(setyourreminder));
			break;
		case "No reminder button":
			logReporter.log(" Check No reminder button",webActions.checkElementDisplayed(noreminderbutton));
			break;
		case "30 min button":
			logReporter.log(" Check 30 min button", webActions.checkElementDisplayed(firstbutton));
			break;
		case "1 hour button":
			logReporter.log(" Check 1 hour button", webActions.checkElementDisplayed(secondbutton));
			break;
		case "2 hour button":
			logReporter.log(" Check 2 hour button", webActions.checkElementDisplayed(thirdbutton));
			break;
		case "Save changes button":
			logReporter.log(" Check Save changes button",webActions.checkElementDisplayed(savechangesbutton));
			break;
		}

	}


	public void verifyOptionsOnGamstopPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", webActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", webActions.checkElementDisplayed(backbutton));
			break;
		case "Gamstop Title":
			logReporter.log(" Check Gamstop Title", webActions.checkElementDisplayed(GamStopTitle));
			break;
		case "Gamstop Text":
			logReporter.log(" Check Gamstop Text", webActions.checkElementDisplayed(GamStoptext));
			break;
		}
	}
	
	public void verifyBalanceSectionDisplayed() {
		By balanceSection = By.xpath("//apollo-balance-block");
		webActions.processElement(balanceSection);
		webActions.checkElementEnabled(balanceSection);
		logReporter.log("verify 'balance section' > >",
				webActions.checkElementDisplayedWithMidWait(balanceSection));
	}
	
	public void clickOnMyAccount() {
		By myAccount = By.xpath("//i[contains(@class,'my-account')]");
		webActions.processElement(myAccount);
		if (webActions.checkElementDisplayedWithMidWait(myAccount))
			logReporter.log("Click back to top button> >", webActions.click(myAccount));
	}
	
	public void clickMenuOption(String strMenuName) {
		By locator = By.xpath("//a[contains(@heading,'" + strMenuName + "')]");
		webActions.processElement(locator);
		if (webActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click back button> >", webActions.click(locator));
	}

	public void verifyMenuHeaderDisplayed(String strMenuName) {
		By locator = By.xpath("//h4[text()='" + strMenuName + "']");
		webActions.processElement(locator);
		logReporter.log("Check menuname in header : " + strMenuName + "> >",
				webActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void enterWithdrawalAmt(String strAmt) {
		By withdrawAmt = By.id("input-amount");
		webActions.invokeOnLocator(withdrawAmt, "clearText");
		logReporter.log("enter withdrawal amount  : > >", webActions.setText(withdrawAmt, strAmt));
	}
	
	public void enterWithdrawalPassword(String passwd) {
		By locator = By.id("input-password");
		logReporter.log("enter withdrawal amount  : > >", webActions.setText(locator, passwd));
	}
	
	public void clickSpan(String strName) {
		By locator = By.xpath("//span[text()='" + strName + "']");
		if (webActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click span '" + strName, webActions.clickUsingJS(locator));
	}
	
	public void verifyLiveHelpDisplayed() {
		By liveHelp = By.xpath("//a[contains(text(),'Live help')]");
		logReporter.log("Check Live help : > >", webActions.checkElementDisplayedWithMidWait(liveHelp));
	}
	
	public void transactionFilterBoxDisplayed() {
		By locator = By.xpath("//div[contains(@class,'ip-select')]");
		logReporter.log("Transaction filter box displayed >>",
				webActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void datesPopulatedWithTodayDate() {
		By locator = By.xpath("(//div[contains(@class,'date-picker')]//input)[2]");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		LocalDateTime now = LocalDateTime.now();  
		System.out.println(dtf.format(now));  
		String todayDate = dtf.format(now);
		String actualDate = webActions.getAttribute(locator, "value");
		if(actualDate.equals(todayDate))
			logReporter.log("Dates populated with today's date", true);
		else
			logReporter.log("Dates populated with today's date", false);
	}
	
	public void transactionHistoryBoxDisplayed() {
		By locator = By.xpath("//table[@class='my-account-table']");
		logReporter.log("Transaction History box displayed >>",
				webActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void selectFilterActivity(String strFilterActivity) {
		By locator = By.xpath("//option[text()='"+strFilterActivity+"']");
		logReporter.log("click filter activity  : > >", webActions.click(locator));
	}
	
	public void verifyTransactionHistoryRecords(String activity) {
		boolean flag=true;
		List  rows = objDriverProvider.getWebDriver().findElements(By.xpath("//table[@class='my-account-table']/tbody/tr")); 
        System.out.println("No of rows are : " + rows.size());
		int totalNoOfRows = rows.size();
		for(int i=1;i<totalNoOfRows;i++) {
			String rowText= objDriverProvider.getWebDriver().findElement(By.xpath("//table[@class='my-account-table']/tbody/tr["+i+"]/td[2]")).getText();
			System.out.println("row text = " + rowText);
			if(rowText.contains(activity)==false) {
				flag=false;
				break;
			}
				
		}
		
		if(flag)
			logReporter.log("Matching records found", true);
		else
			logReporter.log("Matching records found", false);
	}
	
	public void clickFilterButton() {
		By locator = By.xpath("//button[text()='Filter']");
		logReporter.log("click filter button  : > >", webActions.click(locator));
		webActions.checkElementDisplayedWithMidWait(By.xpath("//table[@class='my-account-table']"));
	}
	
	public void optionDisplayed(String strOption) {
		By locator = By.xpath("//option[text()='"+strOption+"']");
		logReporter.log("option displayed  : > >", webActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void selectSortBy(String strSortBy) {
		By locator = By.xpath("//option[text()='"+strSortBy+"']");
		logReporter.log("click sort by  : > >", webActions.click(locator));
	}
	
	public void collapseRecord() {
		By locator = By.xpath("(//table[@class='my-account-table']//i[contains(@class,'plus')])[1]");
		logReporter.log("Collapse first record", webActions.click(locator));
	}
	
	public void verifyCollapsedRecord() {
		By locator1 = By.xpath("//td[contains(text(),'Reference:')]");
		logReporter.log("Reference displayed  : > >", webActions.checkElementDisplayedWithMidWait(locator1));
		By locator2 = By.xpath("//td[contains(text(),'Wallet:')]");
		logReporter.log("Wallet displayed  : > >", webActions.checkElementDisplayedWithMidWait(locator2));
		By locator3 = By.xpath("//td[contains(text(),'Balance')]");
		logReporter.log("Balance displayed  : > >", webActions.checkElementDisplayedWithMidWait(locator3));
	}
	
	public void verifyDeleteIconDisplayed() {
		By locator = By.xpath("//i[contains(@class,'delete')]");
		logReporter.log("delete icon displayed  : > >", webActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void verifyMessagesTableDisplayed() {
		By locator = By.xpath("//section[contains(@class,'my-account-messages')]");
		logReporter.log("Messages table displayed  : > >", webActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void clickFirstMessage() {
		By locator = By.xpath("(//section[contains(@class,'my-account-messages')]//a[contains(@class,'messages')])[1]");
		logReporter.log("click first message  : > >", webActions.click(locator));
	}
	public void verifyMessageInDetail() {
		By locator1 = By.xpath("//i[contains(@class,'icon-back')]");
		logReporter.log("Message back button displayed  : > >", webActions.checkElementDisplayedWithMidWait(locator1));
		By locator2 = By.xpath("//i[contains(@class,'icon-next')]");
		logReporter.log("Message next button displayed  : > >", webActions.checkElementDisplayedWithMidWait(locator2));
	}
	
	public void clickMsgDeleteButton() {
		By locator = By.xpath("//i[contains(@class,'icon-delete')]");
		logReporter.log("click message delete button  : > >", webActions.click(locator));
	}
	
	public void verifyLinkOfSubMenuPage(String strLink) {
		By locator = By.xpath("//a[contains(@href,'" + strLink + "')]");
		logReporter.log("Link : " + strLink, webActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void verifyH5TextDisplayed(String strText) {
		By locator = By.xpath("//h5[contains(text(),'" + strText + "')]");
		logReporter.log("Text displayed >>", webActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void verifyMsgFromParaTag(String strErrorMsg) {
		By locator = By.xpath("//p[contains(text(),'" + strErrorMsg + "')]");
		logReporter.log("Err msg " + strErrorMsg + " displayed >",
				webActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void verifySpanDisplayed(String strName) {
		By locator = By.xpath("//span[text()='" + strName + "']");
		logReporter.log("span '" + strName + "' displayed >>",
				webActions.checkElementDisplayedWithMidWait(locator));
	}

	public void switchToParent() {
		webActions.switchToParentWindow();
	}
	
	public int getMessageCount() {
		return messageCount;
	}
    
    public void setMessageCount() {
    	By locator = By.xpath("//div[@class='slideout-overlay-heading']/h4");
    	String headerText = webActions.getText(locator);
    	int start = headerText.indexOf("(");
    	messageCount = Integer.parseInt(headerText.substring(start + 1, headerText.length()-1));
    }
    
    public void verifyMsgCountReduced() {
    	By locator = By.xpath("//div[@class='slideout-overlay-heading']/h4");
    	String headerText = webActions.getText(locator);
    	int start = headerText.indexOf("(");
    	int actualMsgCount = Integer.parseInt(headerText.substring(start + 1, headerText.length()-1));
    	if(actualMsgCount < messageCount)
    		logReporter.log("Message count is reduced >",true);
    	else
    		logReporter.log("Message count is reduced >",false);
    }
    
    public void verifyMenuOptions(String strMenuName) {
		By locator = By.xpath("//a[contains(@heading,'" + strMenuName + "')]");
		logReporter.log("Check menu : " + strMenuName + "> >",
				webActions.checkElementDisplayedWithMidWait(locator));
	}
    
    public void enterPostalCode(String strPostalCode) {
		By locator = By.xpath("//input[@placeholder='Postcode']");
		logReporter.log("clear postal code >" + strPostalCode + ">", webActions.setText(locator, ""));
		logReporter.log("enter postal code >" + strPostalCode + ">", webActions.setText(locator, strPostalCode));
		if (strPostalCode.equals("") != true && strPostalCode.length() <= 8) {
			int randomSubscript = Integer.parseInt(RandomStringUtils.random(1, false, true));
			By searchDropDown;
			if (randomSubscript <= 7 && randomSubscript != 0)
				searchDropDown = By.xpath("//ul[contains(@class,'postcode')]/li[" + randomSubscript + "]");
			else
				searchDropDown = By.xpath("//ul[contains(@class,'postcode')]/li[1]");

			// if (webActions.checkElementDisplayedWithMidWait(searchDropDown))
			if (webActions.checkElementExists(searchDropDown))
				webActions.click(searchDropDown);
		}
	}

    public void enterAddressLine1(String str) {
		webActions.invokeOnLocator(addressLine1, "clearText");
		logReporter.log("enter address line1", webActions.setText(addressLine1, str));

	}
    
    public void enterCity(String str) {
		logReporter.log("enter city", webActions.setText(city, str));
	}
    
    public void enterCounty(String str) {
		webActions.invokeOnLocator(county, "clearText");
		logReporter.log("enter county", webActions.setText(county, str));
	}
    
    public void reminderButtonHighlighted(String strBtn) {
		By locator = By.xpath("//span[contains(text(),'" + strBtn + "')]/ancestor::button");
		webActions.processElement(locator);
		String strValue = webActions.getAttribute(locator, "class");
		if (strValue.contains("yellow"))
			logReporter.log(strBtn + " button is hightlighted  : > >", true);
		else
			logReporter.log(strBtn + " button is hightlighted  : > >", false);

	}
    
    public void enterLimit(String strLimit) {
    	By locator = By.id("deposit-limits");
		logReporter.log("enter limit", webActions.setText(locator, strLimit));
    	
    }
    
    public void clickDepositLimitButton(String strName) {
    	By locator = By.xpath("(//span[text()='"+strName+"'])[2]");
    	logReporter.log("click limit button", webActions.click(locator));
    }
    
    public void clickButtonHavingGivenText(String strName) {
    	By locator = By.xpath("//button[text()='"+strName+"']");
    	logReporter.log("click limit button", webActions.click(locator));
    }
    
    public void reduceDepositLimitByOne() {
    	By locator = By.id("deposit-limits");
    	String actualLimit = webActions.getText(locator, "value");
    	int decreasedLimit = Integer.parseInt(actualLimit) -1;
    	clickButtonHavingGivenText("Reset limit");
    	enterLimit(String.valueOf(decreasedLimit));
    }
    
    public void increaseDepositLimitByOne() {
    	By locator = By.id("deposit-limits");
    	String actualLimit = webActions.getText(locator, "value");
    	int increasedLimit = Integer.parseInt(actualLimit) +1;
    	clickButtonHavingGivenText("Reset limit");
    	enterLimit(String.valueOf(increasedLimit));
    }
    
    public void enterPasswordForTakeBreak(String strPassword) {
		By locator = By.xpath("//input[@placeholder='Password']");
		logReporter.log("enter take break password  : > >", webActions.setText(locator, strPassword));
	}
    
    public void clickLink(String linkName) {
		By locator = By.xpath("//a[contains(@href,'" + linkName + "')]");
		logReporter.log("Click Link : " + linkName, webActions.click(locator));
	}
    
    public void enterRandomEmailIdWithGivenSuffix(String suffixName) {
    	By editBoxEmail = By.xpath("//input[@placeholder='Email address']");
		// to form random email text
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("MMddhhmm");
		String datetime = ft.format(dNow);
		String strEmail = "k" + datetime + suffixName;
		logReporter.log("enter 'Random Email Id' > >" + strEmail, webActions.setText(editBoxEmail, strEmail));
	}
    
    public void clickCloseButtonFromLiveChat() {
    	By locator = By.xpath("//a[contains(@class,'icon-cross')]");
    	if(webActions.checkElementDisplayed(locator))
    		logReporter.log("Click close of live chat : ", webActions.click(locator));
    }
    
    public void clickOnPlusSignOfTakeABreakPage() {
		By locator = By.xpath("(//i[contains(@class,'plus')])[2]");
		if (webActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click plus sign of take break> >", webActions.click(locator));
	}
    
    public void verifyDivTextDisplayed(String strText) {
		By locator = By.xpath("//div[contains(text(),'" + strText + "')]");
		logReporter.log("Text displayed >>", webActions.checkElementDisplayedWithMidWait(locator));
	}
    
    public void enterPasswordOnTakeABreakPage() {
    	By locator = By.xpath("//input[@placeholder='Password']");
    	logReporter.log("Enter password >>", webActions.setText(locator, "Test@1234"));
    }
}