package com.meccabingo.mac.page;

import org.openqa.selenium.By;


import com.generic.WebActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

import io.cucumber.java.en.Then;

public class ClubsPage {
	WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;

	
	public ClubsPage(DriverProvider driverProvider,WaitMethods waitMethods, WebActions webActions,
			LogReporter logReporter) {
		this.objDriverProvider = driverProvider;
		this.waitMethods = waitMethods;
		this.objWebActions = webActions;
		this.logReporter = logReporter;
	}

	public void searchClub(String str) {
		By search = By.xpath("//input[@type='search']");
		if (objWebActions.checkElementDisplayedWithMidWait(search))
			logReporter.log("populate text for search> >", objWebActions.setText(search, str));
	}

	public void selectFirstClubFromList() {
		By locator = By.xpath("//div[contains(@class,'find-venue-list')]");
		By firstClub = By.xpath("//ul[@class='find-venue-results']/li[1]"); 
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click first club from list> >", objWebActions.clickUsingJS(firstClub));
	}
	
	public void clickMoreInfo() {
		By locator = By.xpath("//a[text()='More Info']");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click more info> >", objWebActions.clickUsingJS(locator));
	}
	
	public void clickFavorite() {
		By locator = By.xpath("//a[contains(@class,'favourite')]");
		//By locator = By.xpath("//apollo-club-favorite-cta");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click favorite> >", objWebActions.click(locator));
	}
	
	public void clickLinkText(String strText) {
		By locator = By.xpath("//a[contains(text(),'"+strText+"')]");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click link text> >", objWebActions.click(locator));
	}
	
	public void verifyMarkedAsFavorite() {
		By locator = By.xpath("//a[contains(@class,'hovered')]");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("marked as favorite> >",true);
		else
			logReporter.log("marked as favorite> >",false);
	}
	
	public void verifyNotMarkedAsFavorite() {
		By locator = By.xpath("//a[contains(@class,' hovered')]");
		waitMethods.sleep(4);
		if (objWebActions.checkElementExists(locator))
			logReporter.log("Not marked as favorite> >",false);
		else
			logReporter.log("Not marked as favorite> >",true);
	}
	
	public void enterHomePhone(String str) {
		By locator = By.id("home-phone");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("set text home phone> >", objWebActions.setText(locator,str));
	}
	
	public void enterJoinClubEmail(String str) {
		By locator = By.xpath("//input[@type='email']");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("set text joine club email> >", objWebActions.setText(locator,str));
	}
	
	public void userJoinedClub() {
		By locator = By.xpath("//h3[contains(text(),'Congratulation')]");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("joined club successfully > >", true);
		else
			logReporter.log("joined club successfully > >", false);
	}
}
