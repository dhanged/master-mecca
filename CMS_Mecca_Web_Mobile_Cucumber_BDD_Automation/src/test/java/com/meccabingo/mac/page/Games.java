package com.meccabingo.mac.page;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.webdriver.DriverProvider;

public class Games {
	private WebActions webActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;

	public Games(WebActions webActions, DriverProvider driverProvider, LogReporter logReporter) {
		this.webActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
	}

	public void clickOnAutoInBingo() {
		By autobtn = By.xpath("//apollo-bingo-container/div/div/ul/li[contains(text(),'Auto')]");
		logReporter.log("click on 'Auto button in bingo section' > >", webActions.click(autobtn));
	}

	public void clickOnAutoInGames() {
		By autobtn = By.xpath("//apollo-game-container/div/div/ul/li[contains(text(),'Auto')]");
		logReporter.log("click on 'Auto button in games section' > >", webActions.click(autobtn));
	}

	public void hoverOnIbtnOfHerotile(String nameofthegame) {
		By ibtn = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/a/i");
		if (!webActions.mouseHover(ibtn)) {
			logReporter.log("Check element", true);
		}
	}

	public void hoverOnIbtnOfGametile(String nameofthegame) {
		By ibtn = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/a/i");
		logReporter.log("Hover on i of normal tile", webActions.mouseHover(ibtn));

	}

	public void clickOnPrizeofGametile(String nameofthegame) {
		By prizebtn = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/div/div[1]");
		if (!webActions.click(prizebtn)) {
			logReporter.log("Check element", true);
		}

	}
	
	public void verifyOpacityOfImageOnGameDetailsPage(String nameofthegame) {
		String opacity = "0.4";
		By image = By.xpath("//h2[contains(text(),'" + nameofthegame
				+ "')]/parent::div/parent::div/div[contains(@class,'opacity')]");
		logReporter.log("verify opacity of image", webActions.checkCssValue(image, "opacity", opacity));

	}
	
	public void verifyContainsUrl(String url) {
		String currentURL = objDriverProvider.getWebDriver().getCurrentUrl();
		logReporter.log("verify URL > >" + url, currentURL.contains(url));
	}
	
	public void clickJoinNowOfFirstBingoGame() {
		//By locator = By.xpath("(//h2[text()='Bingo']/following::button[contains(@class,'-green')])[1]");
		By locator = By.xpath("(//apollo-play-bingo-cta//button)[1]");
		webActions.processElement(locator);
		if (webActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click Join Now> >", webActions.click(locator));
	}
	
	public void clickInfoOfFirstBingoGame() {
		By locator = By.xpath("(//apollo-play-bingo-cta//following::a)[1]");
		webActions.processElement(locator);
		webActions.scrollToElement(locator);
		if (webActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click i> >", webActions.click(locator));
	}
}
