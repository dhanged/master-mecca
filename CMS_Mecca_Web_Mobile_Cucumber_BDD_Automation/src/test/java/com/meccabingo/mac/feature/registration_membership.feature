Feature: Registration Page (Membership)

#EMC-45
@MAC
Scenario: Check whether system displays “Membership number sign up” screen with “Card membership number”, “Date of Birth” fields, "Terms & Conditions" checkbox, "Next" CTA on click of “No (I have a membership card)” CTA from First step of Registration page 
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Verify following option displays on “Membership number sign up” screen 
|Card membership number” field|
|Date of Birth fields|
|Terms & Conditions checkbox|
|Next button|

@MAC
Scenario: Check whether Next Cta gets active when user enters membership number and DOB in correct format and selected “Terms & Conditions”  checkbox on “Membership number sign up” screen 
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276932"
Then Enter Date of birth "11" "08" "1996"
Then Click on age checkbox on membership 
Then Verify Next button gets enabled

#EMC-771
@MAC
Scenario: Check whether system displays error message along with Login link when user enters existing membership number while registration

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276932"
Then Enter Date of birth "11" "08" "1996"
Then Click on age checkbox on membership 
Then Click on Next button
Then Verify error message "Good news - it looks like you’ve already got an account! Enter your username and password to log in. Forgotten your password? You can reset it here."

#EMC-801 
@MAC
Scenario: if membership user with membership details are not found with backend then  the system should display an error message below the “Card membership number” field

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "123456456"
Then Enter Date of birth "11" "08" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Verify message "Something’s not quite right. Contact "

#EMC-770
@MAC
Scenario: Check whether system displays Accoutn details and Registration Form for entered membership user when Number and DOB entered correctly

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Verify title is displayed
Then Verify firstname is displayed
Then Verify surname is displayed
Then Verify date of birth is displayed
Then Verify "Not you?" link
Then Verify username field is displayed
Then Verify password field is displayed
Then Verify "Select All" button
Then Verify following checkboxes:
|Email|
|SMS|
|Phone|
|Post| 
Then Verify "I’d rather not receive offers and communications" checkbox

@MAC
Scenario: Check whether system navigate back to Registration page 1 when user click on “Not you?“ link from account detaisl section from Registration page 2

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on Not you? link
Then Verify link navigate to 'Sign Up' page

@MAC
Scenario: Check whether system correct validations for Email Address fields on Membership Registration page 2

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Enter invalid email address
Then Verify error message "You must enter a valid email address"
Then Verify grey line is displayed below email field
Then Enter valid email on second page
Then Verify green line is displayed below email field

@MAC
Scenario: Check whether system correct validations for Mobile fields on Membership Registration page 2

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Enter invalid mobile number
Then Verify help text "Mobile number"
Then Verify grey line is displayed below mobile number field
Then Enter valid mobile number
Then Verify green line is displayed below mobile number field

@MAC
Scenario: Check whether system correct validations for Username fields on Membership Registration page 2

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Enter invalid username
Then Verify help text "Please enter your username. Must be between 6-15 characters"
Then Verify grey line is displayed below username field
Then Enter valid username
Then Verify green line is displayed below username field

@MAC
Scenario: Check whether system correct validations for Password fields on Membership Registration page 2

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Enter invalid password
Then Verify help text "Please enter password"
Then Verify grey line is displayed below password field
Then Enter valid password
Then Verify green line is displayed below password field

@MAC
Scenario: Check whether system correct validations for Marketing Preferences section on Membership Registration page 2

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on email checkbox
Then Click on SMS checkbox
Then Click on phone checkbox
Then Click on post checkbox
Then Click on select all button
Then Click on marketing preference checkbox
Then Verify selected checkbox gets deselect

@MAC
Scenario: Check whether user abel to regsiter user with Memebrship details when all details are correctly entered

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Enter all values correctly
Then Click on register button
Then Verify success message is displayed

#EMC-804
@MAC
Scenario: Check wehther system displays correct validation for Email address field when data entered empty or in invalid format

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on disabled register button
Then Verify red line is displayed below email field
Then Enter incorrect email address
Then Verify error message below email field
Then Verify error message displayed in red color

@MAC
Scenario: Check wehther system displays correct validation for Mobile Number field when data entered empty or in invalid format

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on disabled register button
Then Verify red line is displayed below mobile number field
Then Enter incorrect mobile number
Then Verify error message below mobile number field
Then Verify error message displayed in red color

@MAC
Scenario: Check wehther system displays correct validation for Username field when data entered empty or in invalid format

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on disabled register button
Then Verify red line is displayed below username field
Then Enter incorrect username
Then Verify error message below username field
Then Verify error message displayed in red color
Then Clear username field
Then Enter existing username "automecca2020"
Then Verify error message displayed in red color


@MAC
Scenario: Check wehther system displays correct validation for Password field when data entered empty or in invalid format

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on disabled register button
Then Verify red line is displayed below password field
Then Enter incorrect password
Then Verify error message below password field
Then Verify error message displayed in red color

@MAC
Scenario: Check wehther system displays correct validation for Marketing preferences field when data entered empty

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on disabled register button
Then Verify error message below marketing preference field field
Then Verify error message displayed in red color

#EMC-1024
@MAC
Scenario: System dsiplays Correct validations for Postcode field on Membership Registration Page 2

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Enter invalid postcode
Then Verify red line is displayed below postcode field
Then Verify error message below postcode field field
Then Verify error message displayed in red color

@MAC
Scenario: System dsiplays correct validation for All fields from Address section when user entering address manually on membership Registration page 2

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on enter address manually button
Then Verify address line 1 field
Then Verify address line 2 field
Then Verify town/city field
Then Verify country field
Then Verify postcode field
#Then Enter incorrect data in all fields
#Then Verify error message
#Then Verify red line is displayed below all field

#EMC-1025
@MAC
Scenario: Check whether system displays Country and postcode  by default and all other fields are hidden on Mebership Registration Page 2

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Verify country name is displayed
Then Verify postcode is displayed

@MAC
Scenario: Check whether Address fields will get populated and “Enter address manually“ CTA will be hidden when user selects GIB or ROI Country field

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on GIB/ROI country
Then Verify enter address manually button gets disappear

@MAC
Scenario: Check whether system displays all address fields when user select Enter address manually CTA on membership Registrtaion page 

Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277086"
Then Enter Date of birth "05" "06" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on enter address manually button
Then Verify first address line field
Then Verify second address line field
Then Verify town/city field
Then Verify country field
Then Verify postcode field
#Then Enter all details in valid format
#Then Verify green line displays below all fields
