Feature: Game and bingo container

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby from Bingo tile available on Homepage
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on Join Now button of first game of bingo section
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby from Bingo tile available on Listing Page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby from Bingo details Page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby from Bingo Search tile available under search results
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Best" in Search field from header
Then Click on play now button from search
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from Bingo tile available on Homepage
Then Click on Join Now button of first game of bingo section
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from bingo tile available on Listing Page
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from bingo details Page
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from Bingo Search tile available under search results
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Best" in Search field from header
Then Click on play now button from search
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby after successful registration when accessing from bingo tile available on Homepage
Then Click on Join Now button of first game of bingo section
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "CA13 9BH"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Click on deposit close button
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby after successful registration when accessing from bingo tile available on Listing Page
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "CA8 9LA"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Click on deposit close button
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful registration when accesing from game details Page
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "CA16 6BQ"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Click on deposit close button
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to navigated to Page from where jouney started after successful registration when accessing game from Bingo Search tile available under search results
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Best" in Search field from header
Then Click on play now button from search
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "CA11 9DQ"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "15"
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby after successful registration and deposit when accessing from bingo tile available on Homepage 
Then Click on Join Now button of first game of bingo section
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "CA13 9BH"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "15"
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch Bingo lobby after successful registration and deposit  when accessing from bingo tile available on Listing Page
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "CA8 9LA"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "15"
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful registration and deposit when accesing from game details Page
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "CA16 6BQ"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "15"
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"


@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful registration and deposit when accessing game from Bingo Search tile available under search results
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Best" in Search field from header
Then Click on play now button from search
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "CA11 9DQ"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "15"
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Switch to child window
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"


#EMC-11
#bingo hero tile
@MAC
Scenario: Check the actions on bingo game tile when user clicks on below sections:-
|Image|
|hover over "i" flag|
|title of a hero tile|
|click the "i" flag| 
|click the top flag or jackpot flag|
|click the price/prize/type of bingo|
Then Click on image of the hero tile "Turbo Gold"
Then Hover on i of the hero tile "Turbo Gold"
Then User clicks on i button of hero tile "Turbo Gold"
Then Click on jackpot flag of the hero tile "Turbo Gold"
Then Click on price of the hero tile "Turbo Gold"

#EMC-100
#games hero tile
@MAC
Scenario: Check the actions on game hero tile when user clicks on below sections:
|image of the game hero tile|
|title of a hero tile|
|description of a hero tile|
|user hover over "i" flag of a hero tile|
|click "i" flag of a hero tile|
Then Click on image of the hero tile "Fluffy Favourites"
Then Click on title of the hero tile "Fluffy Favourites"
Then Click on description of the hero tile "Fluffy Favourites"
Then Hover on i of the hero tile "Fluffy Favourites"
Then User clicks on i button of hero tile "Fluffy Favourites"

#EMC-88
#bingo tile
@MAC
Scenario: Check the actions on bingo tile when user clicks on below sections:
|Image of the tile|
|Hover over "i" flag|
|Title of the tile|
| "i" flag |
|price/prize/type of bingo|
Then Click on image of the bingo tile "Turbo Gold"
Then Click on title of the bingo tile "Turbo Gold"
Then Hover on i of the bingo tile "Turbo Gold"
Then User clicks on i button of bingo tile "Turbo Gold"
Then Click on prize of the bingo tile "Turbo Gold"

#EMC-350
#bingo hero tile
@MAC
Scenario: Check the actions on bingo hero tile when user clicks on below sections:
|image of the bingo hero tile|
|title of a hero tile|
|description of a hero tile|
|user hover over "i" flag of a hero tile|
|"i" flag of a hero tile|
Then Click on image of the hero tile "Gold Rush" 
Then Click on title of the hero tile "Gold Rush"
Then Click on description of the hero tile "Gold Rush"
Then Hover on i of the hero tile "Gold Rush"
Then User clicks on i button of hero tile "Gold Rush"

#EMC-991
@MAC
Scenario: Check whether opticity layer gets apllied for background image on Game/Bingo details page accroding to opticity value set in CMS
Then User clicks on i button of hero tile "Fluffy Favourites"
Then Verify background image of "Fluffy Favourites" has the opacity layer applied in game details page