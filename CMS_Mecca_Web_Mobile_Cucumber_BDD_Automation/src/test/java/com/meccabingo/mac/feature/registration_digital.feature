Feature: Registration Page (Digital)

#EMC-59
@MAC
Scenario: Check whether all marketing preference checkboxes (Email, SMS, Phone and Post) are checked when user select 'Select All' CTA from marketing preference section
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on select all in marketing preference section
Then Verify following checkboxes are selected
|Email|
|SMS|
|Phone|
|Post|

@MAC
Scenario: Check whether user able to select single marketing preference checkboxes (Email, SMS, Phone and Post) from marketing preference section
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on email checkbox
Then Click on SMS checkbox
Then Click on phone checkbox
Then Click on post checkbox

#EMC-871
@MAC
Scenario: Check whether Marketing preference checkboxes are displayed in red color and displays err below fields when user do not select any of the option from Marketing preference section and click on Register button
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on disable Register CTA
Then Verify error message "Please select contact preferences" is displayed in red color 

#EMC-863
@MAC
Scenario: Check whether system displays green line below the username field when user enters username in valid format
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Verify green line displays below username field

@MAC
Scenario: Check whether system displays error message when user enters the existing username in username field
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the existing username "automecca2020" in username field
Then Verify error message "The supplied username is already associated with a different user"

#EMC-61
@MAC
Scenario: Check whether below fields are displayed on second step of registration journey:
- Title
- First name
- Surname
- Date of birth
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify fields are displayed on page
|Title|
|First name|
|Surname|
|Date of birth| 
|Username|
|Password|

@MAC
Scenario: Check whether all titles are displayed on second step of registration journey:
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify all titles are displayed on page
|Ms|
|Mr|
|Miss|
|Mrs|
|Mx|


@MAC
Scenario: Check whether user able to select any option from Title section
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Select any option from title section
Then Verify selected option gets highlighted

@MAC
Scenario: Check whether green line is displayed when user enter the first name in correct format
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the firstname in valid format
Then Verify green line displays below firstname field

@MAC
Scenario: Check whether green line is displayed when user enter the surname in correct format
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the surname in valid format
Then Verify green line displays below surname field

@MAC
Scenario: Check whether green line is displayed when user enter the DOB in correct format
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter Date of birth "11" "08" "1996"
Then Verify green line displays below DOB field

#EMC-62
#covered in test case EMC-61
#@MAC
Scenario: Desktop_Check whether below fields are displayed on second step of registration journey:
- Username
- Password

@MAC
Scenario: Check whether system displays green line below the password field when user enters the password in correct format
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the password in valid format
Then Verify green line displays below password field

@MAC
Scenario: Check whether system displays green line below the username field when user enters username in valid format (non existing user)
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Verify green line displays below username field


@MAC
Scenario Outline: Check whether system displays grey line along with password guidance below the password field:
- Please use at least one capital letter
- Please use at least one number
- Please use at least one lower case
- Minimum 8 characters
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the password in valid format
Then Verify <text> below password field
 Examples:
|text|
|Please use at least one capital letter|
|Please use at least one number|
|Please use at least one lower case|
|Minimum 8 characters|

#EMC-63
@MAC
Scenario: Check whether Yes option is selected by default on first step registration page
#Given Invoke Mecca Site
Then Click on Join Now button
Then Verify Yes option is selected

@MAC
Scenario: Check whether green line is displayed below email address field when user enter the email address in correct format
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Verify green line displays below email field

@MAC
Scenario: Check whether system displays error message and displays Login screen when user enters existing Email address and click on Next button
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter registered email address "emc685test@mailinator.com"
Then Click on disabled Next button
Then Verify error message "You must confirm you are 18 years or over and agree with the T&Cs"

@MAC
Scenario: Check whether system displays Registration Page 2 when user enters valid email address n tick TnC check box and click on Next button
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify select all button

#EMC-810
@MAC
Scenario: Check whether error message displayed under respective fields when user clicks on next button on first step registration page
Then Click on Join Now button
Then Click on disabled Next button
Then Verify error message "You must enter a valid email address"  
Then Verify error message "You must agree to the terms and conditions in order to continue" 

@MAC
Scenario: Check whether help text disappear and error message display when user enters invalid email address on first step registration screen and leave field
Then Click on Join Now button
Then Enter invalid email address
Then Click on disabled Next button
Then Verify help text gets disappear 
Then Verify error message "You must enter a valid email address"

#EMC-60
@MAC
Scenario: Check whether system displays correct validation when user selects Country field on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Verify UK is default value in country field
Then Select republic of ireland 
Then Verify enter address manually button gets disappear
Then Verify first address line field
Then Verify second address line field
Then Verify town/city field
Then Verify country field
Then Select united kingdom
Then Verify enter address manually button
Then Verify first address line field gets disappear
Then Verify second address line field gets disappear
Then Verify town/city field gets disappear
Then Verify country field gets disappear

@MAC
Scenario: Check whether system displays correct validation when user selects Postcode field on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on postcode field
Then Verify grey line displays below postcode field
Then Verify help text appears below postcode field
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Verify first address line field
Then Verify second address line field
Then Verify town/city field

@MAC
Scenario: Check whether system displays correct validation when user selected Enter address manually option for entering Address on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on enter address manually button
Then Enter valid input in address first line
Then Enter valid input in town/city field
Then Verify green line displays below postcode field
Then Verify green line displays below first address field
Then Verify green line displays below town/city field

@MAC
Scenario: Check whether system displays correct validation for Mobiel Number field on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on mobile number field
Then Verify grey line displays below mobile number field
Then Enter valid mobile number
Then Verify green line displays below mobile number field


#EMC-868
@MAC
Scenario: Check whether system displays correct validations for postcode when user enters incomplete or invalid postcode on Digital Registration page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter invalid postcode
Then Verify red line displays below postcode field
Then Verify error message "Please enter a valid postcode"

@MAC
Scenario: Check whether system displays correct validations for Address fields when user select enter address manually option on Digital Registration page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify second page of registration displayed
Then Click on enter address manually button
Then Enter invalid input in first address line 
Then Verify red line displays below first address line field
Then Verify help text gets disappear
Then Enter invalid input in second address line 
Then Verify red line displays below second address line field
Then Verify help text gets disappear

@MAC
Scenario: Check whether system displays correct validations for Mobiel number fields when entered incorrect or incomplete information on Digital Registration page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter invalid mobile number
Then Verify red line displays below mobile number field
Then Verify error message "Please enter a valid mobile number"
Then Verify help text gets disappear 

#EMC-1401
@MAC
Scenario: Check whether system do not display Dr and Mx fro title while regsitration
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify title "Dr" does not appear


