Feature: Game window

@MACOptimized 
Scenario: Check whether user able to navigated to Page from where jouney started after successful registration when accessing game from Game Search tile available under search results
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "cHILLI cON" in Search field from header
Then Click on play now button from search
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Click on deposit close button
Then Verify game launches successfully

@MACOptimized @MACRegression
Scenario: Check whether user able to launch and Play game after successful registration and Deposit when accessing game from Game tile available on Homepage
Then Navigate through hamburger to "slot" menu 
Then Click on Play Now button from Game "Rainbow Riches"
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "10"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
Then Verify game launches successfully

@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful registration and Deposit when accessing game from Game tile available on Listing Page
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "10"
Then Click on deposit button
Then Switch to popup
Then Click on close button on popup
Then Verify game launches successfully

@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful registration and Deposit when accessing game from game details Page
Then User clicks on i button of game "Jive Talking"
Then Verify user navigate to game details page
Then Click on play now button on game details page
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "10"
Then Click on deposit button
Then Switch to popup
Then Click on close button on popup
Then Verify game launches successfully

@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful registration and Deposit when accessing game from Game Search tile available under search results
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "cHILLI cON" in Search field from header
Then Click on play now button from search
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "10"
Then Click on deposit button
Then Switch to popup
Then Click on close button on popup
Then Verify game launches successfully

@MACOptimized
Scenario: Check whether user able to launch and Play game after successful registration when accesing from game details Page
Then User clicks on i button of game "Jive Talking"
Then Verify user navigate to game details page
Then Click on play now button on game details page
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
Then Verify game launches successfully

@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful registration when accessing from Game tile available on Listing Page
Then Click on Play Now button from Game "Rainbow Riches"
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
Then Verify game launches successfully

@MACOptimized @MACRegression
Scenario: Check whether user able to launch and Play game after successful registration when accessing from Game tile available on Homepage
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
Then Verify game launches successfully

@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful login when accessing from game details Page
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Play Now button from Game details
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Verify game launches successfully

@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful login when accessing from Game Search tile available under search results
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Extra Fruity" in Search field from header
Then Click on play now button from search
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify game launches successfully

@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful login when accessing from Game tile available on Listing Page
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button 

@MACOptimized 
Scenario: Check whether user able to launch and Play game after successful login when accessing from Game tile available on Homepage
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button 

@MACOptimized @MACRegression
Scenario: Check whether user able to launch and Play game from Game Search tile available under search results
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "cHILLI cON" in Search field from header
Then Click on play now button from search
Then Verify system displays login window
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify game launches successfully

@MACOptimized 
Scenario: Check whether user able to launch and Play game from game details Page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Play Now button from Game details
Then Verify game launches successfully

@MACOptimized 
Scenario: Check whether user able to launch and Play game from Game tile available on Listing Page
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button 

@MACOptimized 
Scenario: Check whether user able to launch and Play game from Game tile available on Homepage
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button

#@EMC-683
@MAC
Scenario: Check whether system displays login window when user access game in real mode in not logged in mode
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify system displays login window

@MAC
Scenario: Check whether system displays login window when user access game in demo mode from Game Details Page in not logged in mode 
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Free play button
And Verify system displays login window 
 
#EMC-220 
@MAC
Scenario: Check whether system loads game window for selected game in real mode when user selects Play Now button from game container section
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify system loads game window for selected game in real mode

#EMC-218
@MAC
Scenario: Check whether game window top bar contains :
-“Exit game“ arrow
-Session time (Only Real play)
-Play for Real CTA (Only Demo play)
-Title of the game
-Expand CTA
-Deposit CTA
-My account CTA
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button
Then Click on my account from game
Then Username "automecca2020" as header displayed
Then Click close 'X' icon
Then Click on Exit Game CTA


@MAC
Scenario: Check whether game window top bar contains “Exit game“ arrow in real mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window top bar displays “Exit game“ arrow button

@MAC
Scenario: Check whether game window top bar contains “Session time“ in real mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window top bar displays Session time 

@MAC
Scenario: Check whether game window top bar contains “Play for Real CTA“ in demo mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
And Verify Game window top bar displays Play for Real CTA button 

@MAC
Scenario: Check whether game window top bar contains “Title of the game“ in real mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window top bar displays Title of the game

@MAC
Scenario: Check whether game window top bar contains “Expand CTA“ in real mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window top bar displays Expand CTA button

@MAC
Scenario: Check whether game window top bar contains “Deposit CTA“ in real mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window top bar displays Deposit CTA button

@MAC @MACRegression
Scenario: Check whether game window top bar contains “My account CTA“ in real mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window top bar displays My account CTA button

#EMC-221
@MAC @MACRegression
Scenario: Check whether system displays the sand clock icon and session time in mm:ss format for Game window on launching game in real play mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window top bar displays session time in mm:ss format 

@MAC @MACRegression
Scenario: Check whether system do not display session time for Game window on launching game in demo play mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Verify user navigate to game details page
Then Click on Free play button
And Verify system do not display session time for Game window

#EMC-224
@MAC @MACRegression
Scenario: Check whether system displays the top navigation bar when game window is displayed in demo mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Free play button
And Verify Game window header displays following components :
|“Exit game“ arrow|
|Play for Real CTA (Only Demo play)|
|Title of the game|
|Expand CTA|
|Deposit CTA|
|My account CTA|

#repeat with #218
Scenario: Check whether system loads same game in real play modeon click of  “Play for Real“ CTA on game window in demo mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123""
Then User clicks on login button from login window
Then User clicks on i button of game "Pirates Free Spins Edition"
Then Click on Free play button
Then Click on Play for Real CTA button
And Verify game launches in real play mode

#EMC-230
@MAC
Scenario: Check whether system expands game window when user clicks on expand CTA from game window in real play mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then Click on expand button
Then Verify game gets expanded
Then Click on reduce button
Then Verify game gets reduced 

@MAC
Scenario: Check whether system displays "the top navigation bar" and "background image" for expanded game window in real play mode
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then Click on expand button
Then Verify header is displayed 
Then Verify background image is displayed 

@MAC
Scenario: Check whether system displays "the top navigation bar" and "background image" for reduced game window  in real play mode
Then Navigate through hamburger to "slots" menu
Then User clicks on Login Button from header of the Page
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Click on Play Now button from Game
Then Click on expand button
Then Click on reduce button
Then Verify header is displayed 
Then Verify background image is displayed 


#EMC-237
@MAC
Scenario: Check whether system launch game (accessed from Game tile) in real play after completing registration journey with deposit
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "CA11 9DQ"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "10"
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Verify game launches successfully

@MAC @MACRegression
Scenario: Check whether system launch game (accessed from Game details Page) in real play after completing registration journey with deposit
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "5" 
Then Click on deposit button
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Verify game launches successfully

@MAC
Scenario: Check whether system launch game (accessed from Game details Page) in free play after completing registration journey with deposit
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Free play button
And Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "10"
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Verify game launches successfully


#EMC-685
@MAC
Scenario: Check whether system launch game (accessed from Game tile) in real play after completing registration journey without deposit
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
Then Verify game launches successfully

@MAC @MACRegression
Scenario: Check whether system launch game (accessed from Game details Page) in real play after completing registration journey without deposit
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Verify user navigate to game details page
Then Click on play now button on game details page
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
Then Verify game launches successfully

@MAC
Scenario: Check whether system launch game (accessed from Game details Page) in free play after completing registration journey without deposit
Then User clicks on i button of game "Pirates Free Spins Edition"
Then Click on Free play button
And Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
Then Verify game launches successfully


#EMC-264
@MAC @MACRegression
Scenario: Check whether system launch bingo lobby after successful Login when accessing form search tile 
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Pir" in Search field from header
Then Click on play now button from search
Then Verify system displays login window
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify game launches successfully


#EMC-367
@MAC @MACRegression
Scenario: Check whether system launch bingo lobby after successful Login when accessing form Bingo tile
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Verify system displays login window
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
Then Verify game launches successfully


#EMC-211
@MAC @MACRegression
Scenario: Check whether system launch bingo lobby on click of join now button from Search tile
Then User clicks on Login Button from header of the Page
Then Verify system displays login window
Then User enters username "automecca2020"
Then User enters password "Password123"
Then User clicks on login button from login window
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Best" in Search field from header
Then Click on play now button from search
Then Verify game launches successfully