Feature: Pre Buy

@MACRegression
Scenario: Check whether system displays pre-buy pop up after successful Login when user clicks on Pre-buy Cta from bingo tile in logged out mode
Then Click pre-buy from bingo tile
And User enters username "automecca2020"
And User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Verify pre-buy popup is displayed

@MACRegression
Scenario: Check whether system displays pre-buy pop up after successful Registration and deposit when user clicks on Pre-buy Cta from bingo tile in logged out mode
Then Click pre-buy from bingo tile
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "CA11 9DQ"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount "10"
Then Click on Deposit button from Deposit page
Then Click close button from deposit successful popup
Then Verify balance section is displayed
Then Verify pre-buy popup is displayed

@MACRegression
Scenario: Check whether system displays pre-buy pop up after successful Registration without deposit when user clicks on Pre-buy Cta from bingo tile in logged out mode
Then Click pre-buy from bingo tile
Then Verify system displays login window
Then Click on sign up link
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
Then Verify balance section is displayed
Then Verify pre-buy popup is displayed

@MACRegression
Scenario: Check whether system displays confirmation pop up message (“You have successfully purchased tickets“) when user purchase ticket using pre-buy pop up
Given User clicks on Login Button from header of the Page
And User enters username "automecca2020"
And User enters password "Password123"
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click pre-buy from bingo tile
Then Verify pre-buy popup is displayed
Then Select ticket "6"
Then Click button having text "Buy Tickets"
Then Verify text "You have successfully purchased tickets" displayed