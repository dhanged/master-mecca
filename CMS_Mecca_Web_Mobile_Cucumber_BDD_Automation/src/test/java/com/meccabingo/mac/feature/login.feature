Feature: Login 

@MACOptimized 
Scenario: Check whether system send username remainder mail on Registered Email ID when user enters valid Email in Email Address field and click on Send username reminder CTA from Forgotten your username Page
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot password link
Then Verify system displays Forgotten your password page with header as 'Forgot your password'
Then Enter email id "johnoliver@mailinator.com"
Then Click on 'Send reset instructions' button
Then Switch to child window
Then Verify system navigate user to "https://www.mailinator.com/" url
Then Enter email "johnoliver@mailinator.com"
Then Click on go from search
Then Verify email in inbox
Then Verify username inside mail

#@MAC
Scenario: Check whether user able to change password using received link
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Click on deposit close button
Then Click on my account button
Then Click on logout button
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot password link
And Enter random generated username
Then User clicks on 'Send reset instructions'
Then Switch to child window
Then Verify system navigate user to "https://www.mailinator.com/" url
Then Enter generated random email
Then Click on go from search
Then Verify email in inbox
And Click on reset password link inside mail
Then Switch to child window
Then Verify system navigate user to "" url
Then User entes new password
And User retype the password
And Click on reset password button
Then Login with new password

@MACOptimized @MACRegression
Scenario: Check whether system send reset instructions mail on Registered Email ID when user enters username in Username field and click on Send Reset instructions CTA from Forgotten your password Page
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot password link
Then User enters username "johnoliver"
Then User clicks on 'Send reset instructions'
#Then Verify success message
Then Switch to child window
Then Verify system navigate user to "https://www.mailinator.com/" url
Then Enter email id "johnoliver@mailinator.com"
Then Click on go from search
Then Verify email in inbox
And Verify reset link inside mail

@MACOptimized 
Scenario: Login Failed 3rd Attempt_Check whether system displays hyperlinks for error message when user fails to login after 3rd attempt
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "testman008"
And User enters password "Password1234"
Then User clicks on login button
Then Click on close button
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "testman008"
And User enters password "Password1234"
Then User clicks on login button
Then Click on close button
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "testman008"
And User enters password "Password1236"
Then User clicks on login button
And Verify 'Forgot username' link
And Verify 'Forgot password' link

@MACOptimized @MACRegression
Scenario: Check whether the system displays a notification message when Self Excluded Status user tries to Login to the Site
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "testman011"
And User enters password "Password123"
Then User clicks on login button
Then Verify text "The user is self excluded" displayed

@MACOptimized 
Scenario: Check whether the system displays a notification message when Suspended Status user tries to Login to the Site
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "meccaauto01"
And User enters password "Password123"
Then User clicks on login button
Then Verify text "The user is suspended" displayed

@MACOptimized
Scenario: Check whether the system displays a notification message when Closed Status user tries to Login to the Site
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "testman013"
And User enters password "Password123"
Then User clicks on login button
Then Verify text "User account closed" displayed

@MACOptimized @MACRegression
Scenario: Check whether the system displays a notification message when user tries to login with invalid details
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "meccaauto01"
And User enters password "Password123"
Then User clicks on login button
Then Verify text "Your login details aren't quite right. Forgotten your password? You can reset it" displayed

@MAC 
Scenario: Check whether system displays Title, close “X“ icon, Fileds, Buttons, Links and Support area on Login page
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
And Verify system displays login Title
And Verify close 'X' icon
And Verify 'Username' field
And Verify 'Password' field
And Verify Toggle within password field
And Verify Remember me checkbox
And Verify 'Log In' CTA
And Verify 'Forgot username' link
And Verify 'Forgot password' link
And Verify 'New to Mecca Bingo?' text
And Verify 'Sign up' link
And Verify Help contact information
And Verify 'Live Chat' link

@MAC 
Scenario: Check whether Login button gets enabled when user enters Valid details in Username and password field
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "testmecca0503"
And User enters password "Test@1234"
Then Verify Login button gets enabled

@MAC @MACRegression
Scenario: Check whether when user login successfully to the site login overlay gets closed and display page from whether user started journey
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "meccaauto01"
And User enters password "Password123"
Then User clicks on login button
Then Refresh the window
Then Verify user is successfully logged in

@MAC @MACRegression
Scenario: Check whether system remember username for next login when user selected Remember me tick box while login to site
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "meccaauto01"
Then User enters password "Password123"
Then User clicks on 'Remember Me' tick
Then User clicks on login button
Then Click on my account button
Then Click on logout button
Then User clicks on Login Button from header of the Page
Then Verify Username auto pupulated with "meccaauto01"

@MAC
Scenario: Check whether system displays Title “Forgotten your password”, Back arrow, close “X“ icon, helper text “Username” field, “Send reset instructions” CTA, “Forgotten username” link and  Help contact information, and “Live support“ link on Forgotten your password page
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot password link
Then Verify system displays Forgotten your password page with header as 'Forgot your password'
And Verify back arrow '<'
And Verify close 'X' icon
And Verify text "Please supply the following account information and we'll send you an email with password reset instructionss"
And Verify 'Username' field
And Verify 'Send reset instructions' CTA
And Verify 'Forgot username' link
And Verify Help contact information
And Verify 'Live Chat' link


@MAC 
Scenario: Check whether system displays Success message when system sends email on Registered Email ID
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot password link
Then Verify system displays Forgotten your password page with header as 'Forgot your password'
Then User enters username "testmecca0503"
Then User clicks on 'Send reset instructions'
Then Verify Success Message displayed to user
Then Verify 'I didnot receive an email' link


@MAC 
Scenario: Check whether system displays Title, back arrow, close “X“ icon, helper text, Fileds, Buttons, Links and Support area on Forgotten your password page
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot usename link
Then Verify system displays Forgotten your username page with header as 'Forgotten your Username'
And Verify back arrow '<'
And Verify close 'X' icon
And Verify helper text "Please supply the following account information and we'll send you an email with username"
And Verify 'Email' field
And Verify 'Send usename reminder' CTA
And Verify 'Forgot password' link
And Verify Help contact information
And Verify 'Live Chat' link


@MAC 
Scenario: Check whether system displays Success message when system sends username remainder email on Registered Email ID
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot usename link
Then Verify system displays Forgotten your username page with header as 'Forgotten your Username'
Then User enters email "testmecca0503@mailinator.com"
Then User clicks on 'Send username reminder'
Then Verify Success Message displayed to user
Then Verify 'I didnot receive an email' link

@MAC
Scenario: Check whether the system displays the error message when user third time attempts invalid/incorrect username and password
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "tests1"
Then User enters password "Password"
Then User clicks on login button
#And Verify login window shows error message "Your login details aren't quite right. Forgotten your password? You can reset it here."
Then Verify text "Your login details aren't quite right. Forgotten your password? You can reset it" displayed
Then Click on close button
When User clicks on Login Button from header of the Page
Then User enters username "tests1"
Then User enters password "45"
Then User clicks on login button
And Verify login window shows error message "Your login details aren't quite right. Forgotten your password? You can reset it here."
Then Click on close button
When User clicks on Login Button from header of the Page
Then User enters username "tests1"
Then User enters password "67"
Then User clicks on login button
And Verify login window shows error message "Looks like you’re having some trouble signing in. Try resetting your password, requesting ausername reminder, or call us on 0800 083 1988."

@MAC @MACRegression
Scenario: Check whether the system displays a notification message when user tries to login during “break set period” and does not allow user to login
When User clicks on Login Button from header of the Page
Then User enters username "autobreak2020"
Then User enters password "Password123"
Then User clicks on login button
Then Verify break message "You have chosen to take a break from Mecca Bingo. " displayed

