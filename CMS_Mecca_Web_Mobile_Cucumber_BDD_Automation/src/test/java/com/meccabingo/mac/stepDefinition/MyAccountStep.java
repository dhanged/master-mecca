package com.meccabingo.mac.stepDefinition;

import org.openqa.selenium.By;

import com.generic.utils.Utilities;
import com.meccabingo.mac.page.MyAccount;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java8.En;

public class MyAccountStep implements En {

	private MyAccount myAccount;
	private Utilities utilities;

	public MyAccountStep(MyAccount myAccount, Utilities utilities) {
		this.myAccount = myAccount;
		this.utilities = utilities;

		
		Then("^Verify following options are displayed on my account page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount.verifyAllFieldsOnPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});

		Then("^Verify playable balance text$", () -> this.myAccount.verifyPlayableBalanceText());

		Then("^Verify playable balance amount$", () -> this.myAccount.verifyPlayableBalanceAmount());

		Then("^Verify playable balance icon$", () -> this.myAccount.verifyPlayableBalanceIcon());

		Then("^Click on playable balance icon$", () -> this.myAccount.clickOnPlayableBalanceIcon());

		Then("^Verify following options are displayed on playable balance section:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount
						.verifyAllFieldsOnPlayableBalanceSection(this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});

		Then("^Verify playable balance minus icon$", () -> this.myAccount.verifyPlayableBalanceMinusIcon());

		Then("^Click on playable balance minus icon$", () -> this.myAccount.clickOnPlayableBalanceMinusIcon());

		Then("^Verify on playable balance icon$", () -> this.myAccount.verifyPlayableBalanceIcon());

		Then("^Click on following options:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount.clickFieldsOnPlayableBalanceSection(this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});

		Then("^Click on detailed view button$", () -> this.myAccount.clickOnPlayableBalanceDetailedButton());

		Then("^Verify button navigates to balance page$", () -> this.myAccount.verifyBalancePage());

		Then("^Click on back button$", () -> this.myAccount.clickOnBackbutton());

		Then("^Click on \"([^\"]*)\" option$", (String option) -> this.myAccount.clickOnOption(option));

		Then("^Verify title \"([^\"]*)\"$", (String title) -> this.myAccount.verifyPageTitle(title));

		Then("^Verify message count on message text$", () -> this.myAccount.verifyMessageCountOnMessageText());

		Then("^Click on username from top$", () -> this.myAccount.clickUsernameOnMyAccountPage());

		Then("^Click on Live chat link$", () -> this.myAccount.clickOnLiveChatLink());

		Then("^Click on close button$", () -> this.myAccount.clickOnCloseButton());

		Then("^Verify my account window gets closed$", () -> this.myAccount.verifyMyAccountButton());

		Then("^Verify following option on cashier page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount.verifyOptionsOnCashierPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}

		});

		Then("^Click on withdrawal on cashier page$", () -> this.myAccount.clickOnCashierWithdrawalBtn());

		Then("^Click on change password link$", () -> this.myAccount.clickOnChangePassword());

		Then("^Verify update button is disabled$", () -> this.myAccount.verifyUpdateBtnIsDisabled());

		Then("^Enter valid input in current password$", () -> this.myAccount.enterValidCurrentPassword());

		Then("^Enter valid input in new password$", () -> this.myAccount.enterValidNewPassword());

		Then("^Verify update button is enabled$", () -> this.myAccount.verifyUpdateBtnIsEnabled());

		Then("^Verify following option on responsible gambling page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount
						.verifyOptionsOnResponsibleGamblingPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}

		});

		Then("^Verify following option on deposit page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount.verifyOptionsOnDepositPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});

		Then("^Verify success message displays on screen$", () -> this.myAccount.verifySuccessMessageOnLoginPage());

		Then("^Close success message$", () -> this.myAccount.closeSuccessPopup());

		Then("^Verify login window gets closed$", () -> {
		});

		Then("^Verify following option on account details page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount.verifyOptionsOnAccountDetailsPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}

		});

		Then("^Verify following option on take a break page:$", (DataTable arg1) -> {
		});

		Then("^Click on break time button$", () -> this.myAccount.clickOnBreakTimeButton());

		Then("^Verify following options:$", (DataTable arg1) -> {
		});

		Then("^Verify break period text$", () -> this.myAccount.verifyBreakPeriodText());

		Then("^Verify information text$", () -> {
		});

		When("^User click on any one day break button$", () -> this.myAccount.clickOnBreakTimeButton());

		Then("^Verify selected button gets highlited$", () -> this.myAccount.verifyButtonGetsHighlighted());

		When("^User enters password$", () -> {
		});

		When("^User click on take a break button$", () -> this.myAccount.clickOnTakeABreakButton());

		Then("^Verify confirmation popup$", () -> this.myAccount.verifyConfirmationBox());

		Then("^Verify popup contians take a break, logout, cancel button$", () -> {
		});

		When("^Click on take a break on popup$", () -> this.myAccount.clickBreakOnPopup());

		When("^User click on logout button$", () -> this.myAccount.clickLogoutOnPopup());
//reality check page

		Then("^Verify following option on  reality check page:$", (DataTable arg1) -> {

		});

		Then("^Verify text in text box$", () -> this.myAccount.verifyTextInTextboxOnRealityPage());

		Then("^Click on uncollapsible icon$", () -> this.myAccount.clickOnUncollapsibleIconOnRealityPage());

		Then("^Verify system displays text$", () -> this.myAccount.verifyTextOnRealityPage());

		When("^User select any one option$", () -> this.myAccount.selectOptionOnRealityPage());

		When("^Click on save changes button$", () -> this.myAccount.clickOnSaveChangesOnRealityPage());

		// When("^Click on back button$", () -> this.myAccount.clickOnBackbutton());

		Then("^Verify changes get highlighted in account$", () -> {
		});

		Then("^Verify following option on gamstop page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount
						.verifyOptionsOnGamstopPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});

		Then("^Verify text box$", () -> {

		});

		Then("^Click on \"([^\"]*)\"$", (String arg1) -> {
		});

		Then("^Verify link opens in new window$", () -> {
		});

		Then("Click on deposit button", () -> {
		});

		Then("User enters current password {string}", (String string) -> {
			this.myAccount.enterPasswdForCurrentPassword(string);
		});

		Then("Verify green line displays below current password field", () -> this.myAccount.verifyGreenColorBelowCurrentPassword());

		Then("Click on new passwword field", () -> {

		});

		Then("Verify guideline below new password field", () -> {

		});

		Then("User enters new password {string}", (String string) -> {
			this.myAccount.enterPasswdForNewPassword(string);
		});

		Then("Verify green line displays below new password field", () -> this.myAccount.verifyGreenColorBelowNewPassword());

		Then("Switch to paypal child window", () -> {

		});

		Then("Click on pay now", () -> {

		});

		Then("Switch to deposit window", () -> {

		});

		Then("Switch to paysafe child window", () -> {

		});

		Then("Enter paysafe account no {string}", (String string) -> {

		});

		Then("Click paysafe agree checkbox", () -> {

		});

		Then("Click on pay button", () -> {

		});

		Then("Click on uncollaps icon", () -> {

		});

		Then("Verify my account window", () -> {

		});

		Then("Click outside the window", () -> {

		});

		Then("Enter generated password", () -> {
				
			this.myAccount.enterPasswordOnTakeABreakPage();
		});

		Then("Verify following option on enter bonus code page:", (io.cucumber.datatable.DataTable dataTable) -> {

		});

		Then("Verify submit button is disabled", () -> {

		});

		When("User enter bonus code", () -> {

		});

		Then("Verify submit button is enabled", () -> {

		});

		Then("Click on edit button", () -> {

		});

		Then("Verify following option on edit details page:", (io.cucumber.datatable.DataTable dataTable) -> {

		});

		Then("Enter postcode {string}", (String string) -> {

		});

		Then("Select one address", () -> {

		});

		Then("Verify address is selected", () -> {

		});

		When("User enters invalid password", () -> {

		});

		When("Click on update button", () -> {

		});

		Then("Verify error message", () -> {

		});

		When("User enters valid password", () -> {

		});

		Then("Verify confirmation message", () -> {

		});

		Then("Verify following options are displayed on bonuses page:", (io.cucumber.datatable.DataTable dataTable) -> {

		});

		Then("Verify {string} option", (String string) -> {

		});

		Then("Verify information box", () -> {

		});

		Then("Verify button {string}", (String string) -> {

		});

		Then("Click on button {string}", (String string) -> {

		});

		Then("Verify date is visible", () -> {

		});

		Then("Verify password field", () -> {

		});

		Then("Click on uncollapse button", () -> {

		});

		Then("Click on self exclude button", () -> {

		});

		Then("Enter correct password", () -> {

		});

		Then("Verify following options are displayed on balance page:", (io.cucumber.datatable.DataTable dataTable) -> {

		});
	}
	
	@Then("^Verify balance section is displayed$")
	public void verify_balance_section_is_displayed() {
		this.myAccount.verifyBalanceSectionDisplayed();
	}
	
	@Then("^Click on my account$")
	public void click_on_my_account() {
		this.myAccount.clickOnMyAccount();
	}

	@Then("^Click menu option \"([^\"]*)\"$")
	public void click_menu_option(String arg1) {
		this.myAccount.clickMenuOption(arg1);
	}

	@Then("^Verify \"([^\"]*)\" as header displayed$")
	public void verify_as_header_displayed(String arg1) {
		this.myAccount.verifyMenuHeaderDisplayed(arg1);
	}
	
	@Then("^Enter withdrawal amount \"([^\"]*)\"$")
	public void enter_withdrawal_amount(String arg1) {
		this.myAccount.enterWithdrawalAmt(arg1);
	}
	
	@Then("^Enter withdrawal password \"([^\"]*)\"$")
	public void enter_withdrawal_password(String arg1) {
		this.myAccount.enterWithdrawalPassword(arg1);
	}
	
	@Then("^click button having label as \"([^\"]*)\"$")
	public void click_span(String spanName) {
		this.myAccount.clickSpan(spanName);
	}
	
	@Then("^Verify 'Live help' link$")
	public void verify_Live_help_link_on_mobile() {
		this.myAccount.verifyLiveHelpDisplayed();
	}
	
	@Then("^Verify transaction filter box displayed$")
	public void Verify_transaction_filter_box_displayed() {
		this.myAccount.transactionFilterBoxDisplayed();
	}
	
	@Then("^Verify dates are populated with today date$")
	public void Verify_dates_are_populated_with_today_date() {
		this.myAccount.datesPopulatedWithTodayDate();
	}
	
	@Then("^Verify transaction history box$")
	public void Verify_transaction_history_box() {
		this.myAccount.transactionHistoryBoxDisplayed();
	}
	
	@Then("^Select \"([^\"]*)\" filter activity$")
	public void select_filter_activity(String strFilterActivity) {
		this.myAccount.selectFilterActivity(strFilterActivity);
	}
	
	@Then("^Verify records are found of \"([^\"]*)\"$")
	public void verifyTransactionHistoryRecords(String activity) {
		this.myAccount.verifyTransactionHistoryRecords(activity);
	}
	@Then("^Click button Filter$")
	public void Click_button_Filter() {
		this.myAccount.clickFilterButton();
	}
	
	@Then("^Verify option \"([^\"]*)\" displayed$")
	public void Verify_option_displayed(String strOption) {
		this.myAccount.optionDisplayed(strOption);
	}
	
	@Then("^Select SortBy \"([^\"]*)\"$")
	public void Select_SortBy(String strSortBy) {
		this.myAccount.selectSortBy(strSortBy);
	}
	
	@Then("^Collapse one record$")
	public void collapse_one_record() {
		this.myAccount.collapseRecord();
	}
	
	@Then("^Verify the collapsed record$")
	public void Verify_the_collapsed_record() {
		this.myAccount.verifyCollapsedRecord();
	}
	
	@Then("^Open first message$")
	public void Open_first_message() {
		this.myAccount.clickFirstMessage();
	}
	
	@Then("^Verify message in detail$")
	public void Verify_message_in_detail() {
		this.myAccount.verifyMessageInDetail();
	}
	
	@Then("^Click delete button$")
	public void Click_delete_button() {
		this.myAccount.clickMsgDeleteButton();
	}
	
	@Then("Verify delete icon displayed")
	public void verify_delete_icon_displayed() {
		this.myAccount.verifyDeleteIconDisplayed();
	}
	
	@Then("Verify messages table displayed")
	public void verify_messages_table_displayed() {
		this.myAccount.verifyMessagesTableDisplayed();
	}
	
	@Then("^Verify link \"([^\"]*)\" of sub menu page$")
	public void verify_link_of_sub_menu_page(String arg1) {
		this.myAccount.verifyLinkOfSubMenuPage(arg1);
	}
	
	@Then("^Verify bold text \"([^\"]*)\" displayed$")
	public void Verify_bold_text_displayed(String strText) {
		this.myAccount.verifyH5TextDisplayed(strText);
	}
	
	@Then("^Verify message \"([^\"]*)\"$")
	public void Verify_error_message_from_para_tag(String strErrorMsg) {
		this.myAccount.verifyMsgFromParaTag(strErrorMsg);
	}
	
	@Then("^Verify button having label as \"([^\"]*)\" displayed$")
	public void verify_span_displayed(String strName) {
		this.myAccount.verifySpanDisplayed(strName);
	}
	
	@Then("^Switch to parent window$")
	public void switch_to_parent_window() {
		this.myAccount.switchToParent();
	}
	
	@Then("Read the message count")
	public void read_the_message_count() {
		this.myAccount.setMessageCount();
	}
	
	@Then("Verify that message count is reduced")
	public void verify_that_message_count_is_reduced() {
		this.myAccount.verifyMsgCountReduced();
	}
	
	@Then("^Verify following my account menu options:$")
	public void verify_following_my_account_menu_options(DataTable dt) {
		for (int i = 0; i < utilities.getListDataFromDataTable(dt).size(); i++) {
			this.myAccount.verifyMenuOptions(utilities.getListDataFromDataTable(dt).get(i));
		}
	}
	
	@Then("^Enter postal code \"([^\"]*)\"$")
	public void enter_postal_code(String strPostalCode) {
		this.myAccount.enterPostalCode(strPostalCode);
	}
	
	@Then("^Enter address line1 \"([^\"]*)\"$")
	public void enter_address_line(String arg2) {
		this.myAccount.enterAddressLine1(arg2);
	}
	
	@Then("^Enter city \"([^\"]*)\"$")
	public void enter_city(String arg1) {
		this.myAccount.enterCity(arg1);
	}
	
	@Then("^Enter county \"([^\"]*)\"$")
	public void enter_county(String arg1) {
		this.myAccount.enterCounty(arg1);
	}
	
	@Then("^Verify \"([^\"]*)\" reminder button is highlighted$")
	public void verify_reminder_button_is_highlighted(String arg1) {
		this.myAccount.reminderButtonHighlighted(arg1);
	}
	
	@Then("^Enter limit \"([^\"]*)\"$")
	public void enter_limit(String strLimit) {
		this.myAccount.enterLimit(strLimit);
	}

	@Then("^Click deposit limit button \"([^\"]*)\"$")
	public void click_deposit_limit_button(String strName) {
		this.myAccount.clickDepositLimitButton(strName);
	}
	
	@Then("Click button having text {string}")
	public void click_button_having_text(String strName) {
		this.myAccount.clickButtonHavingGivenText(strName);
	}
	
	@Then("Reduce deposit limit by one")
	public void Reduce_deposit_limit_by_one() {
		this.myAccount.reduceDepositLimitByOne();
	}
	
	@Then("Increase deposit limit by one")
	public void Increase_deposit_limit_by_one() {
		this.myAccount.increaseDepositLimitByOne();
	}
	
	@Then("^Enter password \"([^\"]*)\" for take a break$")
	public void enter_password_for_take_a_break(String strPassword) {
		this.myAccount.enterPasswordForTakeBreak(strPassword);
	}
	
	@Then("^click link \"([^\"]*)\"$")
	public void click_link(String link) {
		this.myAccount.clickLink(link);
	}
	
	@Then("^Enter random email id with suffix \"([^\"]*)\"$")
	public void enter_random_email_id_with_suffix_on_mobile(String arg1) {
		this.myAccount.enterRandomEmailIdWithGivenSuffix(arg1);
	}
	
	@Then("Click close button from live chat")
	public void clickCloseButtonOfLiveChat() {
		this.myAccount.clickCloseButtonFromLiveChat();
	}
	
	@Then("^Click on plus sign of take a break page$")
	public void click_on_plus_sign_of_take_a_break_page() {
		this.myAccount.clickOnPlusSignOfTakeABreakPage();
	}
	
	@Then("^Verify information text \"([^\"]*)\" displayed$")
	public void Verify_information_text_displayed(String strText) {
		this.myAccount.verifyDivTextDisplayed(strText);
	}
}
