
package com.meccabingo.mac.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.mac.page.SearchPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class SearchPageStep {

	private Utilities objUtilities;
	private SearchPage objSearchPage;

	public SearchPageStep(Utilities utilities, SearchPage searchPage) {
		this.objUtilities = utilities;
		this.objSearchPage = searchPage;
	}

	@When("^User clicks on search filed$")
	public void user_clicks_on_search_filed() {
		objSearchPage.clickSearchLoupe();
	}

	@Then("^Verify Search overlay opens$")
	public void verify_Search_overlay_opens() {
		objSearchPage.verifySearchOverLay();
	}

	@When("^Search Game \"([^\"]*)\" in Search field from header$")
	public void search_Game_in_Search_field_from_header(String txt) {
		objSearchPage.enterGameName(txt);
	}

	@Then("^Verify system displays search results to user$")
	public void verify_system_displays_search_results_to_user() {
		objSearchPage.verifySearchSection();
	}

	@Then("^Verify system displays following options for search title:$")
	public void verify_system_displays_following_options_for_search_title(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objSearchPage.verifyGameDetails(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^User enters three characters in search filed$") // random values in search box
	public void user_enters_three_characters_in_search_filed() {
		objSearchPage.enterGameName("qas");
	}

	@Then("^Verify system displays search result section$")
	public void verify_system_displays_search_result_section() {
		objSearchPage.verifyResultSectionWithNoMatch();
	}

	@Then("^Verify user able to view 'Quick Links' and 'What others are playing' section in search overlay$")
	public void verify_user_able_to_view_Quick_Links_and_What_others_are_playing_section_in_search_overlay() {
		objSearchPage.verifyQuickLinksAndWhatOthersArePlayingInSearchOverLay();
	}

	@Then("^Verify 'Quick Links' and 'Whats others are playing' section dissapear$")
	public void verify_Quick_Links_and_Whats_others_are_playing_section_dissapear() {
		objSearchPage.verifySearchSection();
	}

	@Then("^Click on i button from search$")
	public void click_on_i_button_from_search() {
		objSearchPage.clickOnSearchibtn();
	}

	@Then("^Verify user navigate to game details page$")
	public void verify_user_navigate_to_game_details_page() {
		objSearchPage.verifyGameDetailsPage();

	}

	@Then("^Click on play now button from search$")
	public void click_on_play_now_button_from_search() {
		objSearchPage.clickOnSearchPlayNowbtn();

	}

	@Then("^Click on sign up link$")
	public void click_on_sign_up_link() {
		objSearchPage.clickOnSignUpLink();
	}

	@Then("^Click on deposit close button$")
	public void click_on_deposit_close_button() {
		objSearchPage.clickOnDepositClosebtn();
	}
	
	@Then("^Enter card number \"([^\"]*)\"$")
	public void enter_card_number(String cardnumber)  {
	    objSearchPage.enterCardNumber(cardnumber);
	   
	}

	@Then("^Enter card date \"([^\"]*)\"$")
	public void enter_card_date(String carddate)  {
	   objSearchPage.enterCardDate(carddate);
	}

	@Then("^Enter security code$")
	public void enter_security_code() {
	   objSearchPage.enterSecurityCode();
	}

	@Then("^Enter the amount \"([^\"]*)\"$")
	public void enter_the_amount(String amt) throws Throwable {
	    objSearchPage.enterAmount(amt);
	}

	@Then("^Switch to popup$")
	public void switch_to_popup() throws Throwable {
	    objSearchPage.switchToSuccessPopup();
	}

	@Then("^Click on close button on popup$")
	public void click_on_close_button_on_popup() throws Throwable {
	  objSearchPage.clickOnCloseButtonOnPopup();
	}
	
	@Then("Close search")
	public void close_search() {
		objSearchPage.closeSearch();
	}
	
	@Then("Check checkbox of include clubs in search result")
	public void check_checkbox_of_include_clubs_in_search_result() {
		objSearchPage.checkCheckboxOfIncludeClubs();
	}
}
