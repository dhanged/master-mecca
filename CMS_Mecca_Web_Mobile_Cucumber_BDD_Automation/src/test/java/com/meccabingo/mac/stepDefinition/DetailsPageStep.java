package com.meccabingo.mac.stepDefinition;

import com.meccabingo.mac.page.DetailsPage;

import io.cucumber.java8.En;

public class DetailsPageStep implements En {

	private DetailsPage detailsPage;

	public DetailsPageStep(DetailsPage detailsPage) {
		this.detailsPage = detailsPage;

		Then("Navigate to Live Casino Page", () -> this.detailsPage.navigateToLiveCasinoPgae());

		Then("Verify login details page of game {string}",
				(String nameofgame) -> this.detailsPage.verifyLoginDetailsPage(nameofgame));

		Then("Verify title of the game of game {string}",
				(String nameofgame) -> this.detailsPage.verifyTitleOfGame(nameofgame));

		Then("Verify brief description", () -> this.detailsPage.verifyBriefDescriptionf());

		Then("Verify backround image of game", () -> this.detailsPage.verifyBackroundImageOfGame());

		Then("Verify main image of game", () -> this.detailsPage.verifyMainImageOfGame());

		Then("Verify information box of game", () -> this.detailsPage.verifyInformationBoxOfGame());

		Then("Scroll up the box", () -> this.detailsPage.scrollUpTheBox());

		Then("Scroll down the box", () -> this.detailsPage.scrollDownTheBox());

		Then("Click on join now from details page", () -> this.detailsPage.clickOnJoinNowfromDetailsPage());

		Then("Verify game launches in new window", () -> this.detailsPage.verifyGameLaunchesInNewWindow());

		Then("Verify {string} in information box", (String text) -> this.detailsPage.verifyTextInInformationBox(text));

		Then("Verify join now button in information box", () -> this.detailsPage.verifyJoinNowButtonInInformationBox());

		Then("Verify Next game starts at in information box", () -> this.detailsPage.verifyNextGameStartsAtText());

		Then("Verify AVAILABLE ON in information box", () -> this.detailsPage.verifyAVAILABLEONText());

		Then("Hove on i button of {string}", (String type) -> this.detailsPage.hoverOnIButton(type));

		Then("Verify help text {string} for {string}", (String helptext,String type) -> this.detailsPage.verifyHelpText(helptext, type));

	}
}
