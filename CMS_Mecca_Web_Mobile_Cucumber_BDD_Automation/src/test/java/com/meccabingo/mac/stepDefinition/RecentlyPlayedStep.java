package com.meccabingo.mac.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.mac.page.RecentlyPlayedPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java8.En;

/**
 * @author Namrata Donikar Harshvardhan Yadav, (Expleo)
 *
 */
public class RecentlyPlayedStep implements En {

	private RecentlyPlayedPage recentlyPlayedPage; 
	private Utilities utilities;

	public RecentlyPlayedStep(RecentlyPlayedPage recentlyPlayedPage, Utilities utilities) 
	{
		this.recentlyPlayedPage = recentlyPlayedPage;
		this.utilities = utilities;
 
		Then("^Verify \"([^\"]*)\" tab displayed on home page$", (String header) -> this.recentlyPlayedPage.verifyTabDisplayedOnHomePage(header));
		
		Then("^Verify You dont have any recently played games message displayed for slot section$", () -> this.recentlyPlayedPage.verifyYouDontHaveAnyRecentlyPlayedGamesMessageDisplayedForSlot());
		
		Then("^Verify You dont have any recently played games message displayed for bingo section$", () -> this.recentlyPlayedPage.verifyYouDontHaveAnyRecentlyPlayedGamesMessageDisplayedForBingo());
		
		Then("^Select \"([^\\\"]*)\" from top navigation menu$", (String menu) -> this.recentlyPlayedPage.selectNavigationMenu(menu));
		
		Then("^Verify below options in recently played section of bingo$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.recentlyPlayedPage.verifyOptionsForRecentlyPlayed("bingo",this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});
		
		Then("^Verify below options in recently played section of slot$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.recentlyPlayedPage.verifyOptionsForRecentlyPlayed("slot",this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});
		
		Then("^Verify that others also played displayed for \"([^\\\"]*)\" section$", (String section) -> this.recentlyPlayedPage.verifyOthersAlsoPlayedDisplayed(section));
	}
}
