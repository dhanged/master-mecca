Feature: Game window

#@EMC-683
@iOS
Scenario: Check whether system displays login window when user access game in real mode in not logged in mode
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then Verify Login Page displayed on mobile

@iOS
Scenario: Check whether system displays login window when user access game in demo mode from Game Details Page in not logged in mode 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Free play button
Then Verify Login Page displayed on mobile
 
#EMC-220 
@iOS
Scenario: Check whether system loads game window for selected game in real mode when user selects Play Now button from game container section
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify system loads game window for selected game in real mode

#EMC-218
@iOS
Scenario: Check whether game window bottom bottom bar contains :
-“Exit game“ arrow
-Session time (Only Real play)
-Play for Real CTA (Only Demo play)
-Deposit CTA
-My account CTA

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button

#EMC-221
@iOS
Scenario: Check whether system displays the sand clock icon and session time in mm:ss format for Game window on launching game in real play mode
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window top bar displays session time in mm:ss format 

@iOS
Scenario: Check whether system do not display session time for Game window on launching game in demo play mode
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
And Verify system do not display session time for Game window

#EMC-224
@iOS @iOSRegression
Scenario: Check whether system displays the top navigation bar when game window is displayed in demo mode
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Free play button
And Verify Game window header displays following components :
|“Exit game“ arrow|
|Play for Real CTA (Only Demo play)|
|Deposit CTA|
|My account CTA|

@iOS @iOSRegression
Scenario: Check whether system loads same game in real play mode on click of  “Play for Real“ CTA on game window in demo mode
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Free play button
Then Click on Play for real CTA button
Then Verify game launches successfully

#EMC-230
@iOS
Scenario: Check whether system do not display expand CTA on game window in real play mode
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify system do not display expand CTA in Game window


#EMC-685 

@iOS
Scenario: Check whether system launch game (accessed from Game tile) in real play after completing registration journey without deposit
 
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game tile "Pirates Free Spins Edition"
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP4 3TS"
Then verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode

@iOS
Scenario: Check whether system launch game (accessed from Game details Page) in real play after completing registration journey without deposit
 
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then User clicks on i button of game
Then Click on Play Now button from Game details
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP4 3TS"
Then verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
And Verify Game window bottom bar displays “Exit game“ arrow button


#EMC-237
@iOS @iOSRegression
Scenario: Check whether system launch game (accessed from Game tile) in real play after completing registration journey with deposit
 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "slots" menu
Then User clicks on i button of game
Then Click on Free play button
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP4 3TS"
Then verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
And Verify Game window bottom bar displays “Exit game“ arrow button


#EMC-237 

@iOS @iOSRegression
Scenario: Check whether system launch game (accessed from Game tile) in real play after completing registration journey with deposit
 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game tile "Extra Fruity"
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP4 3TS"
Then verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then verify "Deposit" as header displayed
Then Navigate link of paysafe
Then Click show more 
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
And Verify system loads game window for selected game in real mode

#EMC-237 
@iOS
Scenario: Check whether system launch game (accessed from Game details Page) in real play after completing registration journey with deposit
 
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then User clicks on i button of game
Then Click on Play Now button from Game details
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP4 3TS"
Then verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then verify "Deposit" as header displayed
Then Navigate link of paysafe
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
And Verify Game window bottom bar displays “Exit game“ arrow button


#EMC-237
@iOS
Scenario: Check whether system launch game in free play (accessed from Game details Page) after completing registration journey with deposit
 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "slots" menu
Then User clicks on i button of game
Then Click on Free play button
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP4 3TS"
Then verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then verify "Deposit" as header displayed
Then Navigate link of paysafe
Then Click show more
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
And Verify Game window bottom bar displays “Exit game“ arrow button

#For Bingo games
@iOS @iOSRegression
Scenario: Check whether system launch bingo lobby after successful Login when accessing form search tile

Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Jive" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on allow popup
Then Verify device navigate user to containing url "bingo.meccabingo.com"


@iOS
Scenario: Check whether system launch bingo lobby after successful Login when accessing form Bingo tile

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on allow popup
Then Verify device navigate user to containing url "bingo.meccabingo.com"


@iOS
Scenario: Check whether system launch bingo lobby after successful Registration when accessing form Bingo tile

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "AB51 5XZ"
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
Then Verify balance section is displayed
Then Click on allow popup
Then Verify device navigate user to containing url "bingo.meccabingo.com"


@iOS @iOSRegression
Scenario: Check whether system launch bingo lobby on click of join now button from Search tile

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Jive" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Click on allow popup
Then Verify device navigate user to containing url "bingo.meccabingo.com"
