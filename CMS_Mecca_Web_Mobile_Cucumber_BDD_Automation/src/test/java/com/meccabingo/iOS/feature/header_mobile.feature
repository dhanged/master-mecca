Feature: Header
#EMC-511
@iOS
Scenario: Mobile_Portrait / Landscape_Check whether search overlay close and display menu when user clicks on hamburger icon
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on hamburger menu
Then Verify search get closed 
Then Verify menu gets open

@iOS
Scenario: Mobile_Portrait / Landscape_Check whether menu close and display search overlay when user clicks on search icon
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on search icon
Then Verify search gets open
Then Verify menu gets closed

#EMC-823
@iOS
Scenario: Check whether system displays Join Now button when user access site first time
Given Invoke the mecca site on Mobile in Portrait Mode
Then Verify header displays join now button

@iOS @iOSRegression
Scenario: Check whether system displays Not a member yet? Register here instead Join Now button
Given  Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Click on my account button on mobile
Then Click on logout button on mobile
Then Verify header displays Not a member yet? Register here link

#EMC-35 : Header - Balance
#EMC-35 : Header - Balance switch
#EMC-35 : Header - Balance block


@iOS  
Scenario: Check whether user able to view Playble balance in terms of Title and Amount
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify title as balance is displayed
Then Verify amount under balance section is displayed


@iOS  @iOSRegression
Scenario: Check whether no action triggered  when user clicks on Balance title  when balance is shown and not shown also when user click on 5 dots
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Hide the balance
Then Click on balance title
Then Verify balance section is displayed
Then click on five dots
Then Verify balance section is displayed

#EMC-236 :  Search - Launch Game - Alternative flow - Logged out

@iOS @iOSRegression
Scenario: Check whether game is loaded successfully when user log in to the site after invoking game from Search overlay in logged out mode

Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Ace Ventura" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
And Verify Game window bottom bar displays Deposit CTA button

#EMC-210

@iOS
Scenario: Check whether selecte game gets loads when user clicks on Play now button from search results

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Ace Ventura" in Search field from header on Mobile
Then Click on Play Now button from searched Game
And Verify Game window bottom bar displays Deposit CTA button

#EMC-326
@iOS @iOSRegression
Scenario: Check whether system displays Hamburger menu, Mecca logo,  Search icon, Deposit CTA,  My Account Icon and balance block at Second Line in logged state at Header
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify following options of myaccount
|Hamburger menu|
|Mecca logo|
|Deposit CTA|
|My account CTA|
|no of unread messages|