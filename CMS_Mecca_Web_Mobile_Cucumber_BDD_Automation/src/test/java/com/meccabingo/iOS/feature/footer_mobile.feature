Feature: Footer

#Mobile EMC-22

@iOS @iOSRegression
Scenario: Check whether system navigate user to respective URL when user clicks on any block

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on Facebook block from footer on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://www.facebook.com/MeccaBingo" url

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Instagram block from footer on Mobile
Then Click on allow popup
#Then Verify device navigate user to "https://www.instagram.com/meccabingo/" url
Then Verify device navigate user to containing url "www.instagram.com"

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on Twitter block from footer on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://twitter.com/MeccaBingo" url

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on Youtube block from footer on Mobile 
Then Click on allow popup
Then Verify device navigate user to "https://www.youtube.com/user/MeccaBingoClubs" url

#Mobile EMC-27
@iOS
Scenario: Check whether system navigate to appropriate URL when user clicks on any Link

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Privacy Policy Link on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://qa01-mecc-cms2.rankgrouptech.net/privacy-policy" url

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Terms and Conditions Link on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://qa01-mecc-cms2.rankgrouptech.net/terms-and-conditions" url

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Affiliates Link on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://www.rankaffiliates.com/" url

#EMC-36 : Footer - GamCare, 18, GamStop, Gambling commision, Gambling control Logos

@iOS
Scenario: Check whether user able to view following logos at Footer section:
-GamCare
-18
-GamStop
-Gambling commision
-Gambling control
-IBAS
-ESSA

Given Invoke the mecca site on Mobile in LandScape Mode
Then Verify user able to view following logos at Footer in partners block on mobile:
|ESSA|
|IBAS|
|18|
|Gambling Control|
|GamCare|
|GamStop|
|Gambling Commission|


@iOS @iOSRegression
Scenario: Check whether user able to navigate to configured URL when user clicks on "ESSA" Logo 

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on "ESSA" logo on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://www.essa.uk.com/" url

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on "IBAS" logo on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://www.ibas-uk.com/" url

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on "Gambling Control" logo on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://www.gamblingcontrol.org/" url

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on "GamCare" logo on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://www.gamcare.org.uk/" url

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on "GamStop" logo on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://www.gamstop.co.uk/" url

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on "Gambling Commission" logo on Mobile
Then Click on allow popup
Then Verify device navigate user to "https://www.gamblingcommission.gov.uk/Home.aspx" url


#EMC-272 : Footer - Verisign Secured

@iOS @iOSRegression
Scenario: Check whether user able to view Verisign Secured logo at Footer section

Given Invoke the mecca site on Mobile in LandScape Mode
Then Verify user should be able to view Verisign Secured logo at Footer section on Mobile


#EMC-23 : Footer - Trusted Partners - Payment methods

@iOS
Scenario: Check whether system displays following section for Trusted partners components:
-Title
-Description 
-Logos

Given Invoke the mecca site on Mobile in LandScape Mode
Then Verify device displays following section for payment providers components:
|Title|
|Description|
|Logos|

#EMC-10 : Footer - Rank group

@iOS
Scenario: Check whether user navigate to Configured link on click of hyperlinked text

Given Invoke the mecca site on Mobile in Portrait Mode
Then click link "www.gamblingcontrol.org"
Then Click on allow popup
Then Verify device navigate user to "https://www.gamblingcontrol.org/" url

Given Invoke the mecca site on Mobile in Portrait Mode
Then click link "gamblingcommission.gov.uk"
Then Click on allow popup
Then Verify device navigate user to containing url "https://www.gamblingcommission.gov.uk"

Given Invoke the mecca site on Mobile in Portrait Mode
Then click link "begambleaware.org"
Then Click on allow popup
Then Verify device navigate user to "https://www.begambleaware.org/" url

Given Invoke the mecca site on Mobile in Portrait Mode
Then click link "www.rank.com"
Then Click on allow popup
Then Verify device navigate user to "https://www.rank.com/en/index.html" url