Feature: Pre Buy

@iOSReg
Scenario: Check whether system displays pre-buy pop up after successful Login when user clicks on Pre-buy Cta from bingo tile in logged out mode
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click pre-buy from bingo tile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify pre-buy popup is displayed

@iOSReg
Scenario: Check whether system displays pre-buy pop up after successful Registration and deposit when user clicks on Pre-buy Cta from bingo tile in logged out mode
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click pre-buy from bingo tile
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP16 0EE"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
Then Verify balance section is displayed
Then Verify pre-buy popup is displayed

@iOSReg
Scenario: Check whether system displays pre-buy pop up after successful Registration without deposit when user clicks on Pre-buy Cta from bingo tile in logged out mode
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click pre-buy from bingo tile
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP3 9TY"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
Then Verify balance section is displayed
Then Verify pre-buy popup is displayed

@iOSReg
Scenario: Check whether system displays confirmation pop up message (“You have successfully purchased tickets“) when user purchase ticket using pre-buy pop up
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click pre-buy from bingo tile
Then Verify pre-buy popup is displayed
Then Select ticket "6"
Then Click button having text "Buy Tickets"
Then Verify text "You have successfully purchased tickets" displayed