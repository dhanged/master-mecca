Feature: Games/Bingo container

#EMC-11
@iOS
Scenario: Mobile_Portrait/Landscape_Check the actions on game tile when user clicks on below sections:-
-Image
-title of a hero tile
-click the "i" CTA
-click the top flag or jackpot flag

Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on image of the game
Then Verify Login Page displayed on mobile
Then Click close 'X' icon on mobile
Then Navigate through hamburger to "bingo" menu
Then Click on title of the game
Then user should remain on bingo container page
Then Navigate through hamburger to "bingo" menu
Then click on top flag of bingo game
Then Verify Login Page displayed on mobile

#EMC-100 this fails due to site cantbe reached
@iOS
Scenario: Mobile_Portrait/Landscape_Check the actions on game hero tile when user clicks on below sections:-
- the image of the game hero tile
- the title of a hero tile
- the description of a hero tile
- the top flag or the bottom flag of a hero tile
- click "i" CTA of a hero tile
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on description of the game
Then Click on title of the game
Then Click on info button of first game of bingo section
Then Verify game detail page open successfully
Then Navigate back to window on Mobile
Then Navigate through hamburger to "bingo" menu
Then Click on image of the game
Then click on top flag of bingo game

#EMC-89
@iOS
Scenario: Mobile_Portrait / Landscape_Check whether when horizontal scroll enabled Game tiles are displaying in one row and content is scrollable
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "slot" menu
Then Verify user can scroll slot game tiles horizontally

@iOS
Scenario: Mobile_Portrait / Landscape_Check whether when vertical scroll enabled Game tiles are distributed in several rows and user able to scroll Up and Down to move through container 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then scroll page till footer of the page
Then Verify back on top CTA is displayed
Then Click on back to top button

#Bingo container
#EMC-88
@iOS
Scenario: Mobile_Portrait/Landscape_Check the actions on bingo tile when user clicks on below sections:-
-Image
-the title of hero tile
-click the "i" CTA
-click the top flag or jackpot flag
-click the price/prize/type of bingo

Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "bingo" menu
Then Click on image of the game
Then Verify Login Page displayed on mobile
Then Click close 'X' icon on mobile
Then Navigate through hamburger to "bingo" menu
Then Click on title of the game
Then user should remain on bingo container page
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Verify game detail page open successfully
Then Navigate back to window on Mobile
Then Navigate through hamburger to "bingo" menu
Then Click on prize of the bingo tile
Then user should remain on bingo container page
 
#EMC-350
@iOS
Scenario: Mobile_Portrait/Landscape_Check the actions on bingo hero tile when user clicks on below sections:-
- the image of the bingo hero tile
- the title of a hero tile
- the description of a hero tile
- the top flag or the bottom flag of a hero tile
- click "i" CTA of a hero tile 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on image of the game
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Click on allow popup
Then Verify balance section is displayed
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"
Then Switch to parent window on mobile
Then Navigate through hamburger to "bingo" menu
Then Click on title of the game
Then user should remain on bingo container page
Then click on top flag of bingo game
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"
Then Switch to parent window on mobile
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Navigate back to window on Mobile
Then Click on prize of the bingo tile
Then user should remain on bingo container page

#EMC-29
@iOS
Scenario: Mobile_Portrait / Landscape_Check whether when horizontal scroll enabled Bingo tiles are displaying in one row and content is scrollable
Given Invoke the mecca site on Mobile in Portrait Mode
Then Verify user can scroll bingo game tiles horizontally

@iOS
Scenario: Check whether system launch bingo lobby in separate window when user clicks on Join Now button from Bingo Tile
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Click on allow popup
Then Verify balance section is displayed
Then Verify device navigate user to containing url "bingo.meccabingo.com"