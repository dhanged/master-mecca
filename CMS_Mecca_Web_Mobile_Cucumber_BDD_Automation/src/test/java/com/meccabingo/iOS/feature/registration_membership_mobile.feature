Feature: Registration - Membership


#Mobile EMC-45

@iOS
Scenario: Check whether system displays “Membership number sign up” screen with “Card membership number”, “Date of Birth” fields, "Terms & Conditions" checkbox, "Next" CTA on click of “No (I have a membership card)” CTA from First step of Registration page

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Verify membership sign up screen

@iOS
Scenario: Check whether system displays green boarder for “Card membership number” and DOB fields when user enters Membership number and DOB in correct format on “Membership number sign up” screen
 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Enter membership number "123456789"
Then Verify green line displayed for membership number
Then Enter DOB "29-08-1990" on mobile
Then Verify green line displayed for DOB


@iOS
Scenario: Check whether Next Cta gets active when user enters membership number and DOB in correct format and selected “Terms & Conditions”  checkbox on “Membership number sign up” screen 
 
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Enter membership number "123456789"
Then Enter DOB "29-08-1990" on mobile
Then Check Agree Checkbox on mobile
Then Verify next button is enabled

@iOS
Scenario: Check whether system displays Accoutn details and Registration Form for entered membership user when Number and DOB entered correctly
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Enter membership number "907277185"
Then Enter DOB "29-08-1990" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify "h5" with text "Your personal details" displayed
Then verify "a" with text "Not You?" displayed
Then Verify error message "Title" from para tag
Then Verify error message "Name" from para tag
Then Verify error message "Date of Birth" from para tag
Then Enter random username
Then Enter password "Password123"
Then Click on "SelectAll" Button on mobile

@iOS
Scenario: Check whether system navigate back to Registration page 1 when user click on “Not you?“ link from account detaisl section from Registration page 2

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Enter membership number "907277180"
Then Enter DOB "29-08-1985" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Click "a" tag with text "Not you?"
Then verify span "NO( I have a membership card)" displayed

#Mobile EMC-801
@iOS
Scenario: if membership user with membership details are not found with backend then  the system should display an error message below the “Card membership number” field as Unable to get customer details
 
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Enter membership number "907277153"
Then Enter DOB "29-08-1990" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify error message "Unable to get customer details"
Then Verify error message "Unable to get customer details" from para tag

