Feature: Registration


#Mobile EMC-59

@iOS
Scenario: Check whether all marketing preference checkboxes (Email, SMS, Phone and Post) are checked when user select 'Select All' CTA from marketing preference section

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Click on "SelectAll" Button on mobile
Then contact preferences checkboxes are checked

@iOS
Scenario: Check whether user able to select single marketing preference checkboxes (Email, SMS, Phone and Post) from marketing preference section

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Click on "post" checkbox on mobile
Then Verify "post" checkbox is checked on mobile

#Mobile EMC-863

@iOS
Scenario: Check whether system displays green line below the username field when user enters username in valid format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Verify green line below username field displayed


@iOS
Scenario: Check whether system displays error message when user enters the existing username in username field

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter existing email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify general error is displayed


#Mobile EMC-871

@iOS
Scenario: Check whether Marketing preference checkboxes  are displayed in red color and displays err below fields when user do not select any of the option from Marketing preference section and click on Register button

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Click on "Register" Button on mobile
Then Verify red background color displayed for "email" checkbox on mobile
Then Verify red background color displayed for "sms" checkbox on mobile
Then Verify red background color displayed for "phone" checkbox on mobile
Then Verify red background color displayed for "post" checkbox on mobile
Then Verify red background color displayed for "not-receive" checkbox on mobile
Then Verify please select contact preference error displayed on mobile

#Mobile EMC-810

@iOS
Scenario: Check whether error message displayed under respective fields when user clicks on next button on first step registration page

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Click on Next Button on mobile
Then Verify error message "You must enter your Email Address"
Then Verify error message "Please confirm you are 18 years old"


@iOS
Scenario: Check whether help text disappear and error message display when user  enters invalid email address on first step registration screen and leave field

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter invalid email id on mobile
Then Click on Next Button on mobile
Then Verify error message "You must enter a valid email address"
Then Verify message "Please enter you email address" not displayed


#Mobile EMC-61
@iOS
Scenario: Check whether below fields are displayed on second step of registration journey:

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify registration page displays following options
|First name|
|Sur name|
|Title|
|Date of birth|

@iOS
Scenario: Check whether user able to select any option from Title section

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Click on "Mr" title on mobile
Then Verify "Mr" title is selected



@iOS
Scenario: Check whether green line is displayed when user enter the first name in correct format

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter firstname "wer" on mobile
Then Verify green line displayed for firstname

@iOS
Scenario: Check whether green line is displayed when user enter the surname in correct format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter surname "wer" on mobile
Then Verify green line displayed for surname

@iOS
Scenario: Check whether green line is displayed when user enter the date of birth in correct format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter DOB "29-08-1990" on mobile
Then Verify green line displayed for DOB


#Mobile EMC-62
@iOS
Scenario: Check whether below fields are displayed on second step of registration journey:
-Username
-Password

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify registration page displays following options
|Username|
|Password|

@iOS
Scenario: Check whether system displays green line below the username field when user enters username in valid format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter random username
Then verify green line is displayed for username




@iOS
Scenario: Check whether system displays green line below the password field when user enters the password in correct format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter password "Password123"
Then verify green line is displayed for password

@iOS
Scenario: Check whether system displays grey line along with password guidance below the password field:
- Please use at least one capital letter
- Please use at least one number
- Please use at least one lower case
- Minimum 8 characters

Given Invoke the mecca site on Mobile in Portrait Mode

Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter password " "
Then Verify registration page displays following password guidelines
|Please use at least one capital letter|
|Please use at least one number|
|Please use at least one lower case|
|Minimum 8 characters|


#Mobile EMC-63
@iOS
Scenario: Check whether Yes option is selected by default on first step registration page

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then verify button "Yes" selected


#Mobile EMC-63

@iOS
Scenario: Check whether system displays Registration Page 2 when user enters valid email address n tick TnC check box and click on Next button

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed

#Mobile EMC-1026
@iOS
Scenario: Check whether system displays Documents required pop up when “KYC check not possible“ response received while registration

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id with suffix ".kycrejected@mailinator.com" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
#Then Enter firstname "qwwe" on mobile
Then Enter random firstname on mobile
#Then Enter surname "sdfzcdfsd" on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
#Then Enter DOB "29-08-1993" on mobile
#Then Enter mobile number "02922453555"
Then Enter random mobile number
Then click Enter address manually button
Then enter random address line1
Then enter random city
Then enter random county
#Then Enter postal code "CF642SU"
Then Enter random postal code
Then verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then verify span "Upload Documents" displayed
Then verify span "Live Chat" displayed
Then Click on back button
Then verify span "Upload Documents" displayed
Then verify span "Live Chat" displayed


@iOS
Scenario: Check whether system open live chat iframe in a separate window when user clicks on Live Chat option from Documenst required pop up

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id with suffix ".kycrejected@mailinator.com" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP8 4TY"
Then verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then verify span "Upload Documents" displayed
Then verify span "Live Chat" displayed
Then click span "Live Chat"
Then Click on allow popup
Then Switch to child window on mobile
Then Verify device navigate user to "https://rank.secure.force.com/chat?cid=VerificationMecca#/liveSupport" url


@iOS
Scenario: Check whether system open Upload documents iframe in a separate window when user clicks on Live Chat option from Documenst required pop up

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id with suffix ".kycrejected@mailinator.com" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP11 1RJ"
Then verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then verify span "Upload Documents" displayed
Then verify span "Live Chat" displayed
Then click span "Upload Documents"
Then Click on allow popup
Then Switch to child window on mobile
Then Verify device navigate user to "https://rank.secure.force.com/chat?cid=VerificationMecca#/phoneEmail" url

@iOS
Scenario: Check whether system do not display Dr and Mx fro title while regsitration
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify span "Dr" not displayed
Then verify span "Mx" not displayed

@iOS
Scenario: Check whether  System displays Header and Footer for Registration Screen 1
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then verify "h4" with text "Sign Up" displayed
Then verify "p" with text "HELP" displayed
And Verify back arrow '<' on mobile
Then Click on back button
Then verify "id-agreed" checkbox is unchecked