Feature: Optimized pack Mobile

#Login
@MobileOptimized 
Scenario: Login Success_Check whether when user login successfully to the site login overlay gets closed and display page from whether user started journey
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "meccaauto01" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile

@MobileOptimized
Scenario: Check whether system remember username for next login when user selected Remember me tick box while login to site
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "meccaauto01" on mobile
And User enters password "Password123" on mobile
Then User clicks on 'Remember Me' tick on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on logout button on mobile
Then User clicks on Login Button from header of the Page on mobile
Then Verify Username auto pupulated with "meccaauto01" on mobile

@MobileOptimized
Scenario: Check whether system send reset instructions mail on Registered Email ID when user enters username in Username field and click on Send Reset instructions CTA from Forgotten your password Page
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User clicks on forgot password link
Then User enters username "johnoliver"
Then User clicks on 'Send reset instructions'
Then Verify success message
Then Switch to child window
Then Verify system navigate user to "https://www.mailinator.com/" url
Then Enter email id "johnoliver@mailinator.com"
Then Click on go from search
Then Verify email in inbox
And Verify reset link inside mail

@MobileOptimized
Scenario: Check whether the system displays a notification message when user tries to login with invalid details
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "meccaauto01123" on mobile
And User enters password "Password1" on mobile
Then User clicks on login button on mobile
Then Verify text "Your login details aren't quite right. Forgotten your password? You can reset it" displayed


@MobileOptimized
Scenario: Check whether the system displays a notification message when user tries to login during “break set period” and does not allow user to login
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman015" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify text "You have chosen to take a break from Mecca Bingo. You can login again from" displayed

@MobileOptimized
Scenario: Check whether the system displays a notification message when Closed Status user tries to Login to the Site
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman013" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify text "User account closed" displayed

@MobileOptimized
Scenario: Check whether the system displays a notification message when Suspended Status user tries to Login to the Site
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman012" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify text "The user is suspended" displayed

@MobileOptimized
Scenario: Check whether the system displays a notification message when Self Excluded Status user tries to Login to the Site
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman011" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify text "The user is self excluded" displayed

@MobileOptimized
Scenario: Login Failed 3rd Attempt_Check whether system displays hyperlinks for error message when user fails to login after 3rd attempt
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman010" on mobile
And User enters password "Password1234" on mobile
Then User clicks on login button on mobile
Then Click close 'X' icon on mobile
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman010" on mobile
And User enters password "Password1234" on mobile
Then User clicks on login button on mobile
Then Click close 'X' icon on mobile
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman010" on mobile
And User enters password "Password1234" on mobile
Then User clicks on login button on mobile
Then Verify text "Looks like you’re having some trouble signing in. Try" displayed
And Verify 'Forgot username' link on mobile
And Verify 'Forgot password' link on mobile


#Myaccount
@MobileOptimized
Scenario: Check whether system displays all required sections on My Account Overlay page when accessed

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Username "automecca2020" as header displayed
Then Verify Balance block in Collapsed state
Then Verify following my account menu options:
| Cashier |
| Message |
| Bonuses |
| Account Details |
| Responsible Gambling |
Then Verify recently played menu exist
And Verify close 'X' icon on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile

@MobileOptimized
Scenario: Check whether system navigate user to respective page when user clicks on Detailed View CTA and Deposit CTA
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Click on Detailed view button
Then Verify user navigate to balance overlay page
Then Click on back button 
Then Click on Deposit button of balance page
Then Verify user navigate to deposit overlay page

@MobileOptimized
Scenario: Check whether system displays correct amount under Cash, Reward and Bonus Amount section in uncollapsed state
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Verify Balance block in expanded state

@MobileOptimized
Scenario: Check whether system displays success message when clicks on Withdraw button from Safecharge iFrame when user have active bonuses for account
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
Then Enter withdrawal amount "10"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then Verify text "Are you sure" displayed
Then click button having label as "Continue"
Then Click withdraw
Then Verify withdraw is successful 


@MobileOptimized
Scenario: Check whether system displays success message when clicks on Withdraw button from Safecharge iFrame  when user do not have active bonuses for account
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
Then Enter withdrawal amount "10"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then Verify text "Are you sure" displayed
Then click button having label as "Continue"
Then Click withdraw
Then Verify withdraw is successful 

@MobileOptimized
Scenario: Check whether system navigate user to safecharge frame when user accept TnC For PoPF 
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "E1 6AN"
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Click close 'X' icon on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Verify message "We hold your balance in a designated bank account so that, in the event of insolvency, sufficient funds are always available for you to withdraw at any time. This represents the medium level of protection, based on the categories provided by the UK Gambling Commission"
Then Verify button having label as "Next" displayed
Then Check Agree Checkbox on mobile
Then click button having label as "Next"
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@MobileOptimized
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Credit Card and click on Deposit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Click saved card "3139"
Then Enter CVV "123"
Then Enter amount to deposit "5"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@MobileOptimized
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paypal and click on Deposit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Click saved card "davidh"
Then Enter amount to deposit "5"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@MobileOptimized
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paysafe and click on Deposit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Click saved card "paysafe"
Then Enter amount to deposit "5"
Then Click on Deposit button from Deposit page
Then Switch to child window on mobile
Then Enter paysafe account "0000000009903207"
Then Click paysafe agree
Then click button having label as "Pay"
Then Switch to parent window on mobile
Then Verify deposit is successful
Then Click close button from deposit successful popup

@MobileOptimized
Scenario: Check whether user navigate to respective section on click of links from My Account > Balance overlay
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Balance"
Then Verify "Balance" as header displayed
Then Verify link "deposit_limits" of sub menu page
Then Verify 'Live help' link on mobile
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile
Then Verify bold text "Playable Balance" displayed
Then Verify message "Cash"
Then Verify message "All game Bonuses"
Then Verify button having label as "Deposit" displayed
Then Verify link "BonusHistory" of sub menu page

@MobileOptimized
Scenario: Check whether system displays Header and Body section on My Account Transaction 
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile
Then Verify 'Live help' link on mobile
Then Verify transaction filter box displayed
Then Verify dates are populated with today date
Then Verify transaction history box

@MobileOptimized
Scenario: Check whether system displays Transactions History according to option selected from list 
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Deposits" filter activity
Then Click button Filter
Then Verify records are found of "Deposit"
Then Select "Withdrawals" filter activity
Then Click button Filter
Then Verify records are found of "Withdrawal"
Then Select "Withdrawals" filter activity
Then Click button Filter
Then Select "Net deposits" filter activity
Then Select "Bonuses" filter activity
Then Click button Filter

@MobileOptimized
Scenario: Check whether system displays new drop down list when user selects Net Deposits from filter option
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Net deposits" filter activity
Then Verify option "Last 24 Hours" displayed
Then Verify option "6 Months" displayed
Then Verify option "Last Week" displayed
Then Verify option "All Time" displayed
Then Select SortBy "Last 30 Days"

@MobileOptimized
Scenario: Check whether system displays Collapsed and uncollapsed version for Transactions
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Deposits" filter activity
Then Click button Filter
Then Collapse one record
Then Verify the collapsed record

@MobileOptimized 
Scenario: Check whether system displays header and body section for Messages overlay
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile
Then Verify 'Live help' link on mobile
Then Verify delete icon displayed
Then Verify messages table displayed

@MobileOptimized 
Scenario: Check whether system displays Body section for message single view

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then Verify delete icon displayed
Then Open first message
Then Verify message in detail
Then Verify delete icon displayed
And Verify close 'X' icon on mobile
Then Verify 'Live help' link on mobile

@MobileOptimized
Scenario: Check whether user abet o delete message using Delete button from Message overlay
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then Open first message
Then Verify message in detail
Then Click delete button

@MobileOptimized
Scenario: Check whether system updates message count from Title section, Message sub menu icon and My account icon from header when user read / delete unread message
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then Read the message count
Then Open first message
Then Click delete button
Then Verify that message count is reduced
Then Click on back button
Then Read the message count
Then Open first message
Then Verify that message count is reduced

@MobileOptimized
Scenario: Check whether system displays account details page contains users information on Account Details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile
Then Verify following my account menu options:
| Header |
| Change Password |
| Marketing Preferences |


@MobileOptimized
Scenario: Check whether  user able to edit details from My Account > Edit Details Page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then click button having label as "Edit"
Then Verify "Edit Details" as header displayed
Then click button having label as "Clear"
Then click button having label as "Enter address manually"
Then Enter address line1 "1 Owen Close"
Then Enter city "Fareham"
Then Enter county "Hampshire"
Then Enter postal code "HP21 9NU"
Then Enter paypal password "Password123"
Then click button having label as "Update"
Then Verify text "Account details updated" displayed

@MobileOptimized
Scenario: Check whether user able to update options selected for marketting preferences from my account 
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then Click menu option "Marketing Preferences"
Then Verify "Marketing Preferences" as header displayed
Then Click on "post" checkbox on mobile
Then Verify "post" checkbox is checked on mobile
Then Click on "post" checkbox on mobile
Then Verify "post" checkbox is unchecked on mobile
Then Click on "all" checkbox on mobile
Then contact preferences checkboxes are checked
Then Click on "all" checkbox on mobile
Then contact preferences checkboxes are unchecked
Then click button having label as "Update"
Then Verify text "Account details updated" displayed

@MobileOptimized
Scenario: Check whether user able to set new relaity check by selecting any of the options from list  on My Account Responsible gaming - Reality Checks page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam02" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then click button having label as "3 mins"
Then click button having label as "Save changes"
Then Click close 'X' icon on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then Verify "3 mins" reminder button is highlighted

@MobileOptimized
Scenario: Check whether system displays error message when user enters  limit equal or smaller than next higher limits
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam02" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Verify button having label as "£" displayed 
Then Verify button having label as "Set Limit" displayed 
Then Verify button having label as "Monthly" displayed 
Then Verify button having label as "Weekly" displayed 
Then Verify button having label as "Daily" displayed 
Then Click deposit limit button "Monthly"
Then Click button having text "Reset limit"
Then Enter limit "400"
Then Verify error message "Your monthly limit must be greater than your weekly limit"
Then Click deposit limit button "Weekly"
Then Click button having text "Reset limit"
Then Enter limit "40"
Then Verify error message "Your weekly limit must be greater than your daily limit"
Then Click deposit limit button "Daily"
Then Click button having text "Reset limit"
Then Enter limit "1"
Then Verify error message "Please enter an amount greater than 5"

@MobileOptimized
Scenario: Check whether system changes deposit limits immidiatly when user decrese limits
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testman007" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Click deposit limit button "Monthly"
Then Reduce deposit limit by one
Then click button having label as "Set Limit"
Then Verify message "Your limits have been updated successfully"
Then click button having label as "Close"


@MobileOptimized
Scenario: Check whether system do not apply changes deposit limits when user increases limits
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam02" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Click deposit limit button "Weekly"
Then Increase deposit limit by one
Then click button having label as "Set Limit"
Then Verify message "After 24 hours you will be able to activate your new limit."
Then click button having label as "Close"

@MobileOptimized
Scenario: Check whether user gets logged out from Site and unabel to login back till break time completed on click of “Take a break & Log Out” CTA from Confirmation Pop up
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam04" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then Verify "Take a break" as header displayed
Then Verify button having label as "1 Day" displayed
Then Verify button having label as "2 Days" displayed
Then Verify button having label as "6 Weeks" displayed
Then click button having label as "1 Day"
Then Verify message "Locked until: "
Then Verify button having label as "Take A Break" displayed
Then click button having label as "1 Day"
Then Enter password "Password123" for take a break
Then click button having label as "Take A Break"
Then Verify message " you are choosing to start your break. Your account will be locked until "
Then Verify button having label as "Take a break & Log Out" displayed
Then click button having label as "Take a break & Log Out"
Then Verify header displays Not a member yet? Register here link

@MobileOptimized
Scenario: Check whether user gets self excluded and loghed out from site when user clicks on Self Exclude and Log out cta from confirmation pop up
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam04" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "Yes"
Then click button having label as "6 Months"
Then Enter password "Password123" for take a break
Then click button having label as "Yes, I want to Self Exclude"
Then Verify text "Are you sure?" displayed
Then Verify button having label as "Self exclude & Log out" displayed
Then click button having label as "Cancel"

@MobileOptimized
Scenario: Check whether system displays Text box on My Account Responsible gaming - Gamstop page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "GamStop"
Then Verify "GamStop" as header displayed
Then click link "http://www.gamstop.co.uk/"
Then Switch to child window on mobile
Then Verify device navigate user to "https://www.gamstop.co.uk/" url

@MobileOptimized
Scenario: Check whether digital user gets registered successfully on Click of Register button on entering all valid details on Page 1 and Page 2
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "AB30 1JB"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@MobileOptimized
Scenario: Check whether digital user gets registered successfully on Click of Register button on entering all valid details on Page 1 and Page 2
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "AB30 1JB"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@MobileOptimized
Scenario: Check whether  system displays error when user tries to register user with details who is already suspended/ blocked
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter "testman012@mailinator.com" email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify message "An error has occurred and has been logged, please contact system administrator"

@MobileOptimized
Scenario: Check whether system displays Documents required pop up when “KYC check not possible“ response received while registration
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id with suffix ".kycrejected@mailinator.com" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then click Enter address manually button
Then Enter random address line1
Then Enter random city
Then Enter random county
Then Enter postal code "AB35 5XB"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify button having label as "Upload Documents" displayed
Then Verify button having label as "Live Help" displayed

#Search
@MobileOptimized
Scenario: Check whether system displays Search overlay with all required fields
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Chilli Con" in Search field from header on Mobile
Then Verify device displays search results to user
Then Click button having text "Clear"
Then Verify user able to view 'Quick Links'

@MobileOptimized
Scenario: Check whether system displays Search results for Slots Games for Valid Text
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Chi" in Search field from header on Mobile
Then Verify system displays search results to user 

@MobileOptimized
Scenario: Check whether system displays Search results for bingo Games for Valid Text
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Emoji" in Search field from header on Mobile
Then Verify system displays search results to user  


@MobileOptimized
Scenario: Check whether system displays Search results for Clubs for Valid Text and Include clubs in search result checkbox is ON 
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Mecc" in Search field from header on Mobile
Then Check checkbox of include clubs in search result
Then Verify system displays search results to user
 
#GameWindow
@MobileOptimized
Scenario: Check whether game window top bar contains :
“Exit game“ arrow
Session time (Only Real play)
Title of the game
Expand CTA
Deposit CTA
My account CTA 

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button
Then Click on my account from game
Then Username "automecca2020" as header displayed
Then Click close 'X' icon on mobile
Then Click on Exit Game CTA

@MobileOptimized
Scenario: Check whether game window top bar contains :
“Exit game“ arrow
Play for Real CTA (Only Demo play)
Title of the game
Expand CTA
Deposit CTA
My account CTA

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Free play button
And Verify Game window bottom bar displays My account CTA button
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
Then Verify game launches successfully

#GameLaunch
@MobileOptimized
Scenario: Check whether user able to launch and Play game from Game tile available on Homepage
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button

@MobileOptimized
Scenario: Check whether user able to launch and Play game from Game tile available on Listing Page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button

@MobileOptimized
Scenario: Check whether user able to launch and Play game from game details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Play Now button from Game details
Then Verify game launches successfully

@MobileOptimized
Scenario: Check whether user able to launch and Play game from Game Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Extra Fruity" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify game launches successfully

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful login when accessing from Game tile available on Homepage
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button 

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful login when accessing from Game tile available on Listing Page
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
And Verify Game window bottom bar displays “Exit game“ arrow button
And Verify Game window bottom bar displays Deposit CTA button
And Verify Game window bottom bar displays My account CTA button 

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful login when accessing from game details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Play Now button from Game details
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify game launches successfully

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful login when accessing from Game Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Extra Fruity" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify game launches successfully

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration when accessing from Game tile available on Homepage
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game tile "Pirates Free Spins Edition"
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP16 0EE"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration when accessing from Game tile available on Listing Page
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game tile "Pirates Free Spins Edition"
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP16 0EE"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration when accesing from game details Page
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Play Now button from Game details
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP3 8HS"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode

@MobileOptimized
Scenario: Check whether user able to navigated to Page from where jouney started after successful registration when accessing game from Game Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Extra Fruity" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP3 9TY"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration and Deposit when accessing game from Game tile available on Homepage
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game tile "Pirates Free Spins Edition"
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP16 0EE"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
And Verify Game window bottom bar displays “Exit game“ arrow button

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration and Deposit when accessing game from Game tile available on Listing Page
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game tile "Pirates Free Spins Edition"
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP16 0EE"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
And Verify Game window bottom bar displays “Exit game“ arrow button

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration and Deposit when accessing game from game details Page
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slots" menu
Then Click on info button of first game of slot section
Then Click on Play Now button from Game details
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP3 8HS"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
And Verify Game window bottom bar displays “Exit game“ arrow button

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration and Deposit when accessing game from Game Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Extra Fruity" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP3 9TY"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

#BingoLaunch

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby from Bingo tile available on Homepage
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Click on Join Now button of first game of bingo section
Then Switch to child window on mobile
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby from Bingo tile available on Listing Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Switch to child window on mobile
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby from Bingo details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify game detail page open successfully

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby from Bingo Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Best" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from Bingo tile available on Homepage
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Switch to child window on mobile
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"


@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from bingo tile available on Listing Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Switch to child window on mobile
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from bingo details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify game detail page open successfully

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from Bingo Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Best" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful registration when accessing from bingo tile available on Homepage 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA11 0QG"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful registration when accessing from bingo tile available on Listing Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA2 7SX"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration when accesing from game details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA10 3DU"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to navigated to Page from where jouney started after successful registration when accessing game from Bingo Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Best" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA14 4LU"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful registration and deposit when accessing from bingo tile available on Homepage 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA11 0QG"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful registration and deposit  when accessing from bingo tile available on Listing Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA2 7SX"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration and deposit when accesing from game details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA10 3DU"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration and deposit when accessing game from Bingo Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Best" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA14 4LU"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"