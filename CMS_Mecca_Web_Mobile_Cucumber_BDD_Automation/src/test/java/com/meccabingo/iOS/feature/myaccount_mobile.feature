Feature: My Account

#Mobile EMC-1031
@iOS  @iOSRegression
Scenario: Check whether user able to view Playble balance in terms of Title and Amount

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Username "automecca2020" as header displayed
Then Verify Balance block in Collapsed state
Then verify following my account menu options:
| HEADER |
| Cashier |
| Message |
| Bonuses |
| Account Details |
| Responsible Gambling |
And Verify close 'X' icon on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile
#Then verify recently played menu exist

@iOS
Scenario: Check whether system naviate user to repective pages when user clicks on Links / Menu availabel on My Account Overlay

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then verify "Cashier" as header displayed
Then Click on back button
#Then Verify message count displayed
Then Click on logout button on mobile
Then Verify header displays Not a member yet? Register here link 

#Mobile EMC-1032
@iOS  
Scenario: Check whether system naviate user to repective pages when user clicks on Links / Menu availabel on My Account Overlay

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then verify "Cashier" as header displayed
Then verify following my account menu options:
| Header |
| Deposit |
| Withdrawal |
| Pending Withdrawals |
| Transaction History |
| Balance |
And Verify close 'X' icon on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile


@iOS
Scenario: Check whether system naviate user to repective pages when user clicks on Links / Menu availabel on My Account > Cashier Overlay

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then verify "Cashier" as header displayed
And Verify close 'X' icon on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile
Then Click menu option "Deposit"
Then verify "Deposit" as header displayed
Then Click on back button
Then Click menu option "Withdrawal"
Then verify "Withdrawal" as header displayed
Then Click on back button
Then Click menu option "Transaction History"
Then verify "Transaction History" as header displayed
Then Click on back button
Then Click menu option "Balance"
Then verify "Balance" as header displayed

#Mobile EMC-1100

@iOS 
Scenario: System displays Balance block in Collapsed state when user access My Account Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Verify Balance block in Collapsed state

@iOS 
Scenario: System displays Balance block in expanded state when user access My Account Page and click on + button from Balance section
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Verify Balance block in expanded state

@iOS 
Scenario: System displays Balance block in expanded state when user access My Account Page and click on + button from Balance section
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Verify Balance block in expanded state
Then Click top section of balance box
Then Verify Balance block in Collapsed state

@iOS 
Scenario: System displays Balance block in expanded state when user access My Account Page and click on + button from Balance section
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Click on cash title
Then Verify Balance block in expanded state
Then Click on bonuses amount
Then Verify Balance block in expanded state

@iOS 
Scenario: System displays Balance block in expanded state when user access My Account Page and click on + button from Balance section
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Click on Detailed view button
Then Verify user navigate to balance overlay page
Then Click on Deposit button
Then verify "Deposit" as header displayed

#EMC-1030
@iOS
Scenario: Check whether system displays Header and body on Change Password page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then verify "Account Details" as header displayed
Then Click menu option "Change Password"
Then verify "Change Password" as header displayed
Then Verify 'Live help' link on mobile
Then Verify current password displayed
Then Verify new password displayed
Then verify update button displayed
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile


#EMC-1030
@iOS
Scenario: Check whether system allow user to enter current password and new password in respective fields
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then verify "Account Details" as header displayed
Then Click menu option "Change Password"
Then verify "Change Password" as header displayed
Then Enter current password "Password123"
Then Verify green line displayed for current password
Then Enter new password "Password123"
Then Verify registration page displays following password guidelines with Right tick
|Please use at least one capital letter|
|Please use at least one number|
|Please use at least one lower case|
|Minimum 8 characters|


#EMC-1030
@iOS
Scenario: Check whether Update CTA is active when user enters Current passsword and New password in valid format
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then verify "Account Details" as header displayed
Then Click menu option "Change Password"
Then verify "Change Password" as header displayed
Then verify update CTA is disabled
Then Enter current password "Password123"
Then Enter new password "Password123"
Then verify update CTA is enabled

#EMC-1034
@iOS
Scenario: Check whether Update CTA is active when user enters Current passsword and New password in valid format
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
And Verify close 'X' icon on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile
Then verify following my account menu options:
|Header|
|Reality Check|
|Deposit Limits|
|Take a break|
|Self Exclude|
|GamStop|


#EMC-1034
@iOS
Scenario: Check whether system Navigate to respective Pages when click on Links available on Responsible Gambling page

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then verify "Reality Check" as header displayed
Then Click on back button
Then Click menu option "Deposit Limits"
Then verify "Deposit Limits" as header displayed
Then Click on back button
Then Click menu option "Take a break"
Then verify "Take a break" as header displayed
Then Click on back button
Then Click menu option "Self Exclude"
Then verify "Self Exclude" as header displayed
Then Click on back button
Then Click menu option "GamStop"
Then verify "GamStop" as header displayed

#EMC-165
@iOS
Scenario: Check whether system displays header and body for Withdraw screen 1
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then verify "Withdrawal" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then verify title "Choose amount of withdraw" of sub menu page
Then verify available funds section of withdrawal sub menu page
Then verify "input-password" field of sub menu page
Then verify "input-amount" field of sub menu page
Then click span "Next"
#this to validate infomartion
Then Verify error message "How long?" from para tag
Then Verify error message "Please note:" from para tag

@iOS
Scenario: Check whether system displays info text for Withdraw screen 1
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
#this to validate infomartion
Then Verify error message "How long? Withdrawals can take 1 to 3 workings days to reach you depending on the payment method used to withdraw." from para tag
Then Verify error message "Please note: we will not charge for deposits or withdrawals however, some card providers may impose a charge.If we are unable to process withdrawals directly to your payment method you will be directed to withdraw via bank transfer." from para tag

#EMC-165
@iOS
Scenario: Check whether system allow user to enter withdraw amount in amount field from Withdraw screen 1

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "2"
Then verify helptext "Minimum of £10, unless your balance is less than £10" displayed
Then click span "Next"
Then Enter withdrawal amount ""
Then Enter withdrawal amount "0"
Then click span "Next"
Then Enter withdrawal amount "7"
Then click span "Next"
Then Verify error message "Withdrawal amount invalid. You must enter an amount more than £10"

#EMC-165
@iOS
Scenario: Check whether system allow withdraw amount less than 10 only when it is equal to user's available funds balance amount

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "9"
Then click span "Next"
Then Verify error message "Withdrawal amount invalid"

#EMC-165
@iOS
Scenario: Check whether Next button gets active when Amount and Password fields entered correctly

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "12"
Then Enter withdrawal password "Password123"
Then verify span button "Next" enabled


#EMC-165
@iOS
Scenario: Check whether system navigate user to Withdraw Screen 2 when click on Next button from Screen 1

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "12"
Then Enter withdrawal password "Password123"
Then click span "Next"
Then verify "Withdrawal" as header displayed


#EMC-1083
@iOS
Scenario: Check whether system displays Header and body on withdraw Screen 2

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password123"
Then click span "Next"
Then verify "Withdrawal" as header displayed
#Then verify title "Are you sure" of sub menu page
#this to validate infomartion
#Then Verify error message "Any active bonus balances will be forfeited as a result - please check your" from para tag
#Then verify link "BonusHistory" of sub menu page
#Then verify span "Continue" displayed
#Then verify span "Cancel" displayed

@iOS
Scenario: Check whether system navigate user to respective pages from Withdraw screen 2 on click

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password123"
Then click span "Next"
Then verify "Withdrawal" as header displayed
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password123"
Then click span "Next"
Then click span "Continue"
Then verify "Withdrawal" as header displayed
Then Click on back button
Then verify "Cashier" as header displayed



#EMC-1084
@iOS
Scenario: Check whether system navigate user to respective pages from Withdraw screen 2 on click

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password123"
Then click span "Next"
Then click span "Continue"
Then verify "Withdrawal" as header displayed
Then Click show more
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile

@iOS
Scenario: Check whether system displays success message when clicks on Withdraw button from Safecharge iFrame

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password123"
Then click span "Next"
Then Navigate link of paysafe
Then Click withdraw
Then Verify withdraw is successful
Then Click close button from deposit successful popup

#EMC-64
@iOS
Scenario: Check whether system displays Header , Body and Safecharge iFrame is on Deposit page for user have already made a FTD (First time deposit)

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then verify link "deposit_limits" of sub menu page
Then Verify 'Live help' link on mobile


@iOS
Scenario: Check whether system displays Header , Body on Deposit page for user have never made deposit

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "AB51 5NY"
Then verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Click close 'X' icon on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then verify "id-agreed" field of sub menu page
#this to validate infomartion
Then Verify error message "We hold your balance in a designated bank account so that, " from para tag
Then verify span "Next" displayed

@iOS @iOSRegression
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Credit Card and click on Depsit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then verify "Deposit" as header displayed
Then Navigate link of paysafe
Then Click saved card "3139"
Then Enter CVV "123"
Then Enter amount to deposit "20"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@iOS @iOSRegression
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paypal and click on Depsit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then verify "Deposit" as header displayed
Then Navigate link of paysafe
Then Click saved card "davidh"
Then Enter amount to deposit "20"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@iOS @iOSRegression
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paysafe and click on Depsit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then verify "Deposit" as header displayed
Then Navigate link of paysafe
Then Click saved card "paysafe"
Then Enter amount to deposit "20"
Then Click on Deposit button from Deposit page
Then Switch to child window on mobile
Then Enter paysafe account "0000000009903207"
Then Click paysafe agree
Then click span "Pay"
Then Switch to parent window on mobile
Then Verify deposit is successful
Then Click close button from deposit successful popup

#EMC - 167
@iOS
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paysafe and click on Depsit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then verify "Account Details" as header displayed
Then verify div "content-box-details" displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile
Then verify following my account menu options:
| Header |
| Change Password |
| Marketing Preferences |


#EMC - 172
@iOS
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Take a break page

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then verify "Take a break" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then Verify 'Live help' link on mobile
Then verify span "1 Day" displayed
Then verify span "2 Days" displayed
Then verify span "6 Weeks" displayed
Then click span "1 Day"
Then Verify error message "Locked until: " from para tag
Then verify span "Take A Break" displayed
Then verify div "field-row ip-password" displayed

@iOS
Scenario: Check whether system displays Text box along with “Why to take a break“ Box + a collapse/uncollapse icon on My Account Responsible gaming - Take a break page

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then verify "Take a break" as header displayed
#this is to validate info
Then Verify error message "If you would like to take a temporary break from gambling, you can choose a break period from 1 day up to 6 weeks" from para tag
Then Click on plus sign
Then Verify error message "If you wish to take a break for a different length of time, please contact Customer Support and a member of our team will be able to help you. By choosing to take a break, you will not be able to access your account and you will be prevented from gambling until your break period has finished. Once you have confirmed that you wish to take a break, we will be unable to reverse this process. We will endeavour to stop all marketing materials from being sent to you within 24 hours of your request being received." from para tag
Then Verify error message "You may extend your break period at any time by contacting our Customer Support Team." from para tag
Then Verify error message "Your break will end automatically once the selected time period has passed and your account will be re-opened at this time." from para tag

#EMC-172
@iOS
Scenario: Check whether system displays Take a break confirmation pop up when user selects any of the option for Take a break and Password on My Account Responsible gaming - Take a break page and click on Take a break CTA

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoios1" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then verify "Take a break" as header displayed
Then click span "1 Day"
Then Enter password "Password123" for take a break
Then click span "Take A Break"
Then Verify error message " you are choosing to start your break. Your account will be locked until " from para tag
Then verify span "Take a break & Log Out" displayed
Then verify span "Cancel" displayed


@iOS
Scenario: Check whether user gets logged out from Site and unabel to login back till break time completed on click of “Take a break & Log Out” CTA from Confirmation Pop up

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoios1" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then verify "Take a break" as header displayed
Then click span "1 Day"
Then Enter password "Password123" for take a break
Then click span "Take A Break"
Then click span "Take a break & Log Out"
Then Verify header displays Not a member yet? Register here link

#EMC-175
@iOS
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Reality Checks page

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then verify "Reality Check" as header displayed
Then Verify 'Live help' link on mobile
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then Verify error message "Set your reminder" from para tag
Then verify span "No reminder" displayed
Then verify span "3 mins" displayed
Then verify span "1 hour" displayed
Then verify div "collapsible-title" displayed
Then verify span "Save changes" displayed

@iOS
Scenario: Check whether system displays Text box on My Account Responsible gaming - Reality Checks page

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then verify "Reality Check" as header displayed
Then Verify error message "Reality Checks are a helpful way of keeping track of the time you have spent playing our games." from para tag
Then Verify error message "You can choose how often you would like to receive a Reality Check below. You will see a reminder each time you reach your chosen Reality Check time interval during your gaming session and you will have the option to continue with your game or stop playing." from para tag
Then Verify error message "Your gaming session will begin when you place your first real money bet and will end when you log out" from para tag
Then Click on plus sign
Then Verify error message "Some games will also offer you the facility to set a Reality Check within the game and your in-game Reality Check preferences will apply to the time you spend playing these games." from para tag
Then Verify error message "You can change your Reality Check settings at any time by selecting a new interval below. Any changes to your Reality Check settings after your gaming session has started will take effect the next time you login to the site." from para tag

@iOS
Scenario: Check whether user able to set new relaity check by selecting any of the options from list  on My Account Responsible gaming - Reality Checks page

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then verify "Reality Check" as header displayed
Then click span "3 mins"
Then click span "Save changes"
Then Click close 'X' icon on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then verify "Reality Check" as header displayed
Then verify "3 mins" reminder button is highlighted

@iOS @iOSRegression
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Gamstop page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "GamStop"
Then verify "GamStop" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
And Verify error message "If you are considering self-exclusion, you may wish to register with GAMSTOP." from para tag
And Verify error message "GAMSTOP is a free service that enables you to self-exclude from all online gambling companies licensed in Great Britain." from para tag

@iOS
Scenario: Check whether system displays Text box on My Account Responsible gaming - Gamstop page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "GamStop"
Then verify "GamStop" as header displayed
Then click link "http://www.gamstop.co.uk/"
Then Verify device navigate user to "https://www.gamstop.co.uk/" url

#EMC-223
@iOS
Scenario: Check whether user able to access my account from Game window
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate to Auto tab of Slot games
Then Click on Play Now button from Game
And Verify system loads game window for selected game in real mode
Then Click on my account of game
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode

@iOS
Scenario: Check whether user able to continue game play after completing any actions from My Account Overlay
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Navigate to Auto tab of Slot games
Then Click on Play Now button from Game
And Verify system loads game window for selected game in real mode
Then Click on my account of game
Then Click menu option "Cashier"
Then verify "Cashier" as header displayed
Then Click menu option "Deposit"
Then verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode
Then Click on my account of game
Then Click menu option "Cashier"
Then verify "Cashier" as header displayed
Then Click menu option "Withdrawal"
Then verify "Withdrawal" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode
Then Click on my account of game
Then Click menu option "Account Details"
Then verify "Account Details" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode


@iOS
Scenario: Check whether user gets logged out from game window when user Take a break
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoios3" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then verify "Take a break" as header displayed
Then click span "1 Day"
Then Enter password "Password123" for take a break
Then click span "Take A Break"
Then click span "Take a break & Log Out"
Then Verify header displays Not a member yet? Register here link


@iOS
Scenario: Check whether  System displays Header and Body on My Account > Enter bonus code [Screen 1] page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Bonuses"
Then verify "Bonuses" as header displayed
Then click span "Enter bonus code"
Then verify "Enter promotional code" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then Verify 'Live help' link on mobile
Then verify "bonus-code" field of sub menu page
Then verify span "Submit" displayed
Then Click on back button
Then verify "Bonuses" as header displayed
Then Click close 'X' icon on mobile
Then Verify balance section is displayed

@iOS
Scenario: Check whether  System displays Submit CTA in active state when user enters Bonus code in Bonus code field
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Bonuses"
Then verify "Bonuses" as header displayed
Then click span "Enter bonus code"
Then verify "Enter promotional code" as header displayed
Then Enter bonus code "Rtt"
Then verify span button "Submit" enabled

@iOS
Scenario: Check whether  System Navigates to approprite pages on My Account > Bonuses
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Bonuses"
Then verify "Bonuses" as header displayed
Then click span "Enter bonus code"
Then verify "Enter promotional code" as header displayed
Then Click on back button
Then verify "Bonuses" as header displayed
Then click span "Active bonuses"
Then verify "Active bonuses" as header displayed
Then Click on back button
Then verify "Bonuses" as header displayed
Then click span "Bonus History"
Then verify "Bonus History" as header displayed

@iOS
Scenario: Check whether  System displays Header and Body for My Account - Responsible gaming - Self Exclusion screen 1
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then verify "Self Exclude" as header displayed
Then verify span "Yes" displayed
Then verify span "No" displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile

@iOS
Scenario: Check whether  System displays different options for My Account - Responsible gaming - Self Exclusion screen 2(NO)
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then verify "Self Exclude" as header displayed
Then click span "No"
Then verify span "Reality Check" displayed
Then verify span "Deposit Limits" displayed
Then verify span "Take a Break" displayed

@iOS @iOSRegression
Scenario: Check whether  System displays different options for My Account - Responsible gaming - Self Exclusion screen 2(Yes)
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoios04" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then verify "Self Exclude" as header displayed
Then click span "Yes"
Then verify "Self Exclude" as header displayed
Then verify "h5" with text "How long do you want to lock your account for?" displayed
Then verify span "6 Months" displayed
Then verify span "1 Year" displayed
Then verify span "2 Years" displayed
Then verify span "5 Years" displayed
Then click span "6 Months"
Then Verify error message "Locked until: " from para tag
Then Enter password "Password123" for take a break
Then verify span "Yes, I want to Self Exclude" displayed

@iOS @iOSRegression
Scenario: Check whether  System displays Header and Body on My Account > Edit Details Page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then verify "Account Details" as header displayed
Then click span "Edit"
Then verify "Edit Details" as header displayed
Then verify Address Line1 populated with "5 Queensway"
Then verify City populated with "Leeds"
Then verify County populated with "West Yorkshire"
Then verify Postcode populated with "LS25 1AZ"
Then verify "password" field of sub menu page
Then verify span "Update" displayed
Then Click on back button
Then verify "Account Details" as header displayed

@iOS
Scenario: Check whether  user able to edit details from My Account > Edit Details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then verify "Account Details" as header displayed
Then click span "Edit"
Then verify "Edit Details" as header displayed
Then click "1" clear button
Then Enter random email id on mobile
Then click "2" clear button
Then enter address line1 "1 Owen Close"
Then enter city "Fareham"
Then enter county "Hampshire"


@iOS
Scenario: Check whether user able select populated address for address fields from My Account > Edit Details Page 
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then verify "Account Details" as header displayed
Then click span "Edit"
Then verify "Edit Details" as header displayed
Then click span "Clear"
Then click span "Enter address manually"
Then enter address line1 "1 Owen Close"
Then enter city "Fareham"
Then enter county "Hampshire"
Then Enter static postal code "PO16 7GZ"

@iOS
Scenario: Check whether changed details gets saved for customer account on click of Update CTA
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then verify "Account Details" as header displayed
Then click span "Edit"
Then verify "Edit Details" as header displayed
Then click span "Clear"
Then click span "Enter address manually"
Then enter address line1 "1 Owen Close"
Then enter city "Fareham"
Then enter county "Hampshire"
Then Enter postal code "HP21 9NU"
Then Enter paypal password "ssword123"
Then click span "Update"
Then Verify error message "You have entered a wrong password"
Then Enter paypal password "Password123"
Then click span "Update"
Then verify "h4" with text "Account details updated" displayed

@iOS
Scenario: Check whether  System navigat eon respective pages on click of options for My Account - Responsible gaming - Self Exclusion screen 2(NO)
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then verify "Self Exclude" as header displayed
Then click span "No"
Then verify span "Reality Check" displayed
Then verify span "Deposit Limits" displayed
Then verify span "Take a Break" displayed
Then verify "div" with text "Alternatively, please get in touch if you want to Self Exclude for different reasons on 0800 083 1990" displayed
Then click span "Reality Check"
Then verify "Reality Check" as header displayed
Then Click on back button
Then click span "Deposit Limits"
Then verify "Deposit Limits" as header displayed
Then Click on back button
Then click span "Take a Break"
Then verify "Take a break" as header displayed

@iOS
Scenario: Check whether  System displays text box on Self Exclusion Screen 2(yes)
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then verify "Self Exclude" as header displayed
Then click span "Yes"
Then Verify error message "You can block yourself from playing with Mecca for a chosen period of time. This can only be reversed by written request after that period has ended..." from para tag
Then Click on plus sign
Then Verify error message "If you believe you might have a problem with gambling, it is advisable to stop gambling altogether, or for a prolonged period. We would also recommend that you seek guidance from the support agencies listed on our keepitfun website...." from para tag


@iOS
Scenario: Check whether  System displays extended version when user select  one of the 4 options: 6 months/ 1 year/ 1 years/ 5 years from My Account - Responsible gaming - Self Exclusion screen 2(Yes)
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoios4" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then verify "Self Exclude" as header displayed
Then click span "Yes"
Then click span "6 Months"
Then Verify error message "Locked until: " from para tag
Then verify div "field-row ip-password" displayed
Then verify span "Yes, I want to Self Exclude" displayed


@iOS
Scenario: Check whether  System displays Self Exclude Confirmation pop up when user selects Self Exclude CTA from overly page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoios04" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then verify "Self Exclude" as header displayed
Then click span "Yes"
Then click span "6 Months"
Then Enter password "Password123" for take a break
Then click span "Yes, I want to Self Exclude"
Then verify "h4" with text "Are you sure?" displayed
Then verify span "Self exclude & Log out" displayed
Then click span "Cancel"

@iOS
Scenario: Check whether system displays Top section and bottom section on Balance box section for My Account > Balance overlay

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then verify "Cashier" as header displayed
Then Click menu option "Balance"
Then verify "Balance" as header displayed
Then verify link "deposit_limits" of sub menu page
Then Verify 'Live help' link on mobile
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile
Then verify "h5" with text "Playable Balance" displayed
Then Verify error message "Cash" from para tag
Then Verify error message "All game Bonuses" from para tag
Then verify span "Deposit" displayed
Then verify link "BonusHistory" of sub menu page

@iOS
Scenario: Check whether user navgate to respective section on click of links from My Account > Balance overlay
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then verify "Cashier" as header displayed
Then Click menu option "Balance"
Then verify "Balance" as header displayed
Then click span "Deposit"
Then verify "Deposit" as header displayed
Then Click on back button
Then click link "deposit_limits"
Then verify "Deposit Limits" as header displayed
Then Click on back button
Then click link "BonusHistory"
Then verify "Bonus History" as header displayed

@iOS
Scenario: Check whether system displays Head and bosy for Deposit Limit page

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoandroid03" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then verify "Deposit Limits" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then Verify 'Live help' link on mobile
Then Verify error message "We recommend that you set deposit limits to help you manage your spending" from para tag
Then verify "h5" with text "Current limit" displayed
Then verify "Daily" reminder button is highlighted
Then verify span "Weekly" displayed
Then verify span "Monthly" displayed

@iOS
Scenario: Check whether system displays Extended version for Deposi Limit page when no pending limit available

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoandroid03" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then verify "Deposit Limits" as header displayed
Then verify span "£" displayed 
Then verify span "Set Limit" displayed 

