Feature: Details

@iOS
Scenario: Check whether opticity layer gets apllied for background image on Game details page accroding to opticity value set in CMS 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on info button of first game of bingo section
Then Verify background image has the opacity layer applied in game details page

@iOS
Scenario: Check whether system displays Top section on Bingo details page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on info button of first game of bingo section
Then Verify game detail page open successfully

#@iOS
Scenario: Check whether system launch bingo lobby on click of Join Now button from Bingo details page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on info button of first game of bingo section
Then Verify game detail page open successfully
Then Click on join now button from bingo details
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Click on allow popup
Then Verify balance section is displayed
Then Verify device navigate user to containing url "bingo.meccabingo.com"
