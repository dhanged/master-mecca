Feature: Recently Played

@iOSReg
Scenario: Check whether system displays Image, Title and Close button for bingo game tile available in recently played section
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify below options in recently played section of bingo
|Header|
|Image|
|Title|
|Close button|

@iOSReg
Scenario: Check whether system displays Image, Title and Close button for slot game tile available in recently played section
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify below options in recently played section of bingo
|Header|
|Image|
|Title|
|Close button|

@iOSReg
Scenario: Check whether System displays Other also played section under recently played tab with list configured in CMS for slots
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam06" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify that others also played displayed for "slot" section

@iOSReg
Scenario: Check whether System displays Other also played section under recently played tab with list configured in CMS for bingo
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam06" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify that others also played displayed for "bingo" section

@iOSReg
Scenario: Check whether System displays recently played tab evnthough user do not have any recently played games for Slots Game Panel
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam06" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify You dont have any recently played games message displayed for slot section

@iOSReg
Scenario: Check whether System displays recently played tab evnthough user do not have any recently played games for Bingo Game Panel
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam06" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify You dont have any recently played games message displayed for bingo section