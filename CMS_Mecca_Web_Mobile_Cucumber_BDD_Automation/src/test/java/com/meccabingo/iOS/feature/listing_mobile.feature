Feature: Listing

#EMC-688
@iOS 
Scenario: Check whether system displays Carousel,Title,Tabs,Game panel block and Generic content block (Text)
Given Invoke the mecca site on Mobile in Portrait Mode
Then verify below components
|HEADER|
|Carousel|
|Tabs|
|Bingo Lobby CTA|


@iOS 
Scenario: Check whether user able to select Tabs from Listsing pages
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate to Auto tab of Slot games

#EMC-1131
@iOS
Scenario: Check whether system displays configured sections on bingo listing page
Given Invoke the mecca site on Mobile in Portrait Mode
Then click bingo tab from header
Then verify below components
|HEADER|
|Carousel|
|Tabs|
|Bingo Lobby CTA|

@iOS 
Scenario: Check whether system loads respective games configured for Tabs selected on Bingo listing page 
Then click bingo tab from header
Then Navigate to Auto tab of Bingo games