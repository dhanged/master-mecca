
package com.meccabingo.iOS.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = ".", strict = true, plugin = {"json:target/JSON/iOS.json",
		"rerun:target/SyncFails.txt", /*"com.generic.cucumberReporting.CustomFormatter"*/}, 
		tags = "@iOS23", 
		dryRun = false, monochrome = true,
		glue = { "com.hooks", "com.meccabingo.iOS.stepDefinition"}
)

public class iOSRunner extends AbstractTestNGCucumberTests {
}