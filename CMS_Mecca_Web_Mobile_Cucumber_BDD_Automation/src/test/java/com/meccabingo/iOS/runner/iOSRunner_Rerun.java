
package com.meccabingo.iOS.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "@target/SyncFails.txt", 
		strict = true, plugin = { "json:target/JSON/iOSRerun.json"}, 		
		monochrome = true,
		glue = { "com.hooks", "com.meccabingo.iOS.stepDefinition" }
)

public class iOSRunner_Rerun extends AbstractTestNGCucumberTests {
}
