package com.meccabingo.iOS.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobileErrorPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobileErrorPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void navigateToWrongURL() {
		String strCurrentURL = objAppiumDriverProvider.getAppiumDriver().getCurrentUrl();
		strCurrentURL = strCurrentURL + "wrong";
		System.out.println("Wrong URL  " + strCurrentURL);
		objAppiumDriverProvider.getAppiumDriver().navigate().to(strCurrentURL);
		System.out.println("after URL  " + objAppiumDriverProvider.getAppiumDriver().getCurrentUrl());
	}

	public void errorDisplayed() {
		By error = By.xpath("//h1[contains(text(),'404 Error')]");
		logReporter.log("Check Error 404 > >", objMobileActions.checkElementDisplayedWithMidWait(error));
	}
}
