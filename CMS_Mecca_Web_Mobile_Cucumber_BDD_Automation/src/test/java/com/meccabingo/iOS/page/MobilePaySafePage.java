package com.meccabingo.iOS.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobilePaySafePage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobilePaySafePage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void enterPaySafeAccount(String strAcc) {
		By locator = By.xpath("//input[contains(@class,'input-pin')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("enter acc> >", objMobileActions.setText(locator,strAcc));
	}
	
	public void clickPaySageAgree() {
		By locator = By.id("acceptTerms");
		By addPin = By.xpath("//span[contains(text(),'Add PIN')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(addPin)) {
			//objMobileActions.che(locator)
			logReporter.log("click agree > >", objMobileActions.clickUsingJS(locator));
		}
	}

}
