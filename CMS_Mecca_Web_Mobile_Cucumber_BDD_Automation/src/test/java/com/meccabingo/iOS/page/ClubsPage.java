package com.meccabingo.iOS.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;

import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

import io.cucumber.java.en.Then;

public class ClubsPage {
	MobileActions objMobileActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;

	

	public ClubsPage(DriverProvider driverProvider,WaitMethods waitMethods, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objDriverProvider = driverProvider;
		this.waitMethods = waitMethods;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void searchClub(String str) {
		By search = By.xpath("//input[@type='search']");
		if (objMobileActions.checkElementDisplayedWithMidWait(search))
			logReporter.log("populate text for search> >", objMobileActions.setText(search, str));
	}

	public void selectFirstClubFromList() {
		By locator = By.xpath("//ul[@class='find-venue-results']");
		By firstClub = By.xpath("//ul[@class='find-venue-results']/li[1]"); 
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click first club from list> >", objMobileActions.click(firstClub));
	}
	
	public void clickMoreInfo() {
		By locator = By.xpath("//a[text()='More Info']");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click more info> >", objMobileActions.click(locator));
	}
	
	public void clickFavorite() {
		By locator = By.xpath("//i[contains(@class,'favorites')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click favorite> >", objMobileActions.click(locator));
	}
	
	public void clickLinkText(String strText) {
		By locator = By.xpath("//a[contains(text(),'"+strText+"')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click link text> >", objMobileActions.click(locator));
	}
	
	public void verifyMarkedAsFavorite() {
		By locator = By.xpath("//a[contains(@class,'hovered')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("marked as favorite> >",true);
		else
			logReporter.log("marked as favorite> >",false);
	}
	
	public void verifyNotMarkedAsFavorite() {
		By locator = By.xpath("//a[contains(@class,' hovered')]");
		waitMethods.sleep(2);
		if (objMobileActions.checkElementExists(locator))
			logReporter.log("Not marked as favorite> >",false);
		else
			logReporter.log("Not marked as favorite> >",true);
	}
	
	public void enterHomePhone(String str) {
		By locator = By.id("home-phone");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("set text home phone> >", objMobileActions.setText(locator,str));
	}
	
	public void enterJoinClubEmail(String str) {
		By locator = By.xpath("//input[@type='email']");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("set text joine club email> >", objMobileActions.setText(locator,str));
	}
	
	public void userJoinedClub() {
		By locator = By.xpath("//h3[contains(text(),'Congratulation')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("joined club successfully > >", true);
		else
			logReporter.log("joined club successfully > >", false);
	}
}
