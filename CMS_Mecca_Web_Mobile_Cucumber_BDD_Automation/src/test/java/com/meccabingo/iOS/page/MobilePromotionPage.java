package com.meccabingo.iOS.page;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobilePromotionPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobilePromotionPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void clickXpath(String strXpath) {
		By locator = By.xpath(strXpath);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click xpath", objMobileActions.click(locator));
	}

	public void verifyXpathDisplayed(String strXpath) {
		By locator = By.xpath(strXpath);
		logReporter.log("check xpath", true, objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void scrollTillXpath(String strXpath) {
		By locator = By.xpath(strXpath);
		objMobileActions.androidScrollToElement(locator);
	}

	public void clickLearnMore() {
		By locator = By.xpath("(//a[text()='Learn more'])[1]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click learn more", objMobileActions.click(locator));
	}

	public void claimButtonDisplayed() {
		By locator = By.xpath("(//button[text()='Claim'])[1]");
		logReporter.log("click learn more", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}
	
}
