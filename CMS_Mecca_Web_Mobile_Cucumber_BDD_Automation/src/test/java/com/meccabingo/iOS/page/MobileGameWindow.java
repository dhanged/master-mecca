package com.meccabingo.iOS.page;

import org.openqa.selenium.By;
import com.generic.MobileActions;
import com.generic.logger.LogReporter;

public class MobileGameWindow {

	By exitarrowbtn = By.xpath("//i[contains(@class,'icon-arrow-left')]");
	By sessiontime = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[2]/i");
	By playforrealbtn = By.xpath("//button[contains(.,'Free Play')]");
	By titleofthegame = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[3]");
	By expandbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[4]/a/i");
	By reducebtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[4]/a/i");
	By depositbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[5]");
	By myaccountbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/li[2]");
	By sessiontimewithmmss = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[2]/span");

	private MobileActions mobileActions;
	private LogReporter logReporter;

	public MobileGameWindow(MobileActions mobileActions, LogReporter logReporter) {
		this.mobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void clickOnPlayNow() {
		By playNowbtn = By.xpath("//h2[text()='Slots & Games']/following::apollo-slot-tile[1]//button[text()='Play Now']");
		mobileActions.processMobileElement(playNowbtn);
        mobileActions.checkElementEnabled(playNowbtn);
		logReporter.log("click element play now > >", mobileActions.click(playNowbtn));

	}

	public void clickOnPlayNowFromGameDetails() {
		By playNowbtn = By.xpath("(//button[text()='Play Now'])[1]");
		logReporter.log("click element play now > >", mobileActions.click(playNowbtn));

	}

	public void clickOnPlayNowFromGameTile(String strGameDesc) {
		By playNowbtn = By.xpath("//p[text()='" + strGameDesc + "']/following::button[1]");
		logReporter.log("click element play now> >", mobileActions.click(playNowbtn));

	}

	public void clickOnibtn() {
		//By ibtn = By.xpath("//apollo-slot-hero-tile//button[contains(@class,'info')]");
		By ibtn = By.xpath(
				"(//apollo-bingo-tile)[1]//div[contains(@class,'game-panel-buttons-wrapper')]//i[contains(@class,'info')]");
		mobileActions.processMobileElement(ibtn);
		//mobileActions.waitTillPageLoadCompletes();
		if(mobileActions.checkElementDisplayedWithMidWait(ibtn))
			logReporter.log("click on 'i' > >", mobileActions.clickUsingJS(ibtn));
	}
	
	public void clickOnibtnOfGameTile() {
        By ibtn = By.xpath("(//apollo-slot-tile)[1]//button[contains(@class,'info')]");
        mobileActions.processMobileElement(ibtn);
        logReporter.log("click on 'i' > >", mobileActions.click(ibtn));
  }

	public void clickOnibtnOfHeroTile(String nameofthegame) {
		By ibtn = By.xpath("//p[contains(text(),'" + nameofthegame
				+ "')]/ancestor::apollo-slot-hero-tile/div[contains(@class,'game-panel-image')]/div[contains(@class,'game-panel-info')]/i");
		logReporter.log("click on 'i' > >", mobileActions.click(ibtn)); // change apollo-slot-hero-tile for hero tile
	}

	public void clickExpandbutton() {
		logReporter.log("click on expand button > >", mobileActions.click(expandbtn));

	}

	public void verifyNonExpandbutton() {
		if (!mobileActions.checkElementDisplayed(expandbtn))
			logReporter.log("check element > >", true);
	}

	public void clickReducebutton() {
		logReporter.log("click on reduce button > >", mobileActions.click(reducebtn));
	}

	public void clickOnFreePlayBtn() {
		By freeplaybtn = By.xpath("//button[contains(.,'Free Play')]");
		logReporter.log("click on 'Free play button' > >", mobileActions.click(freeplaybtn));
	}

	public void verifyExitArrowBtn() {
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(exitarrowbtn));
	}

	public void verifySessionTime() {
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(sessiontime));
	}

	public void verifyNonSessionTime() {
		if (!mobileActions.checkElementDisplayed(sessiontime))
			logReporter.log("check element > >", true);
	}

	public void verifySessionTimeInmmssformat() {
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(sessiontimewithmmss));
	}

	public void verifyPlayforRealBtn() {
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(playforrealbtn));
	}

	public void clickOnPlayforRealBtn() {
		logReporter.log("check element > >", mobileActions.click(playforrealbtn));
	}

	public void verifyTitleoftheGame() {
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(titleofthegame));
	}

	public void verifyNonExpandBtn() {
		if (!mobileActions.checkElementDisplayed(expandbtn))
			logReporter.log("check element > >", true);
	}

	public void verifyDepositBtn() {
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(depositbtn));
	}

	public void verifyMyaccountBtn() {
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(myaccountbtn));
	}

	public void verifyDemoheadercomponents(String text) {
		System.out.println("#### "+text);
		switch (text) {

		case "“Exit game arrow": {
			System.out.println("#### "+text);
			logReporter.log("check element > >", mobileActions.checkElementDisplayed(exitarrowbtn));
			break;
		}

		case "Play for Real CTA": {
			logReporter.log("check element > >", mobileActions.checkElementDisplayed(playforrealbtn));
			break;
		}
		case "Deposit CTA": {
			logReporter.log("check element > >", mobileActions.checkElementDisplayed(depositbtn));
			break;
		}
		case "My account CTA": {
			logReporter.log("check element > >", mobileActions.checkElementDisplayed(myaccountbtn));
			break;
		}
		}
	}

	public void verifyHeaderInGame() {
		By header = By.xpath("//div[contains(@class,'game-window-header')]");
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(header));

	}

	public void verifyBackroungimageInGame() {

		By bagroundimage = By.xpath("//div[contains(@style,'background-image')]");
		// By.xpath("//*[contains(@id,'1073148485177582543')]"); //
		// *[contains(@id,'Desktop')]/body/canvas
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(bagroundimage));
	}

	public void clickOnImageOfHerotile(String nameofthegame) {
		By image = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/img");
		if (!mobileActions.click(image)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnTitleOfHerotile(String nameofthegame) {
		By title = By.xpath("//p[contains(text(),'" + nameofthegame + "')]");
		if (!mobileActions.click(title)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnDescriptionOfHerotile(String nameofthegame) {
		By description = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/p[2]");
		if (!mobileActions.click(description)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnPrizeofHerotile() {
		By prizebtn = By.xpath("//p[contains(text(),'Turbo Gold')]/parent::div/div/div[1]");
		if (!mobileActions.click(prizebtn)) {
			logReporter.log("Check element", true);
		}
	}

	public void navigateToAutoTabOfSlotGame() {

		//By locator = By.xpath("//span[text()='Auto']");
		//By selectObj = By.xpath("(//div[contains(@class,'ip-apollo-select-input')])[2]");

		By locator = By.xpath("(//li[text()='Auto'])[2]");
		By selectObj = By.xpath("//h3[text()='Slots & Games']/following::div[contains(@class,'ip-apollo-select-input')][1]/i");
		By slideOutOverlay= By.xpath("//div[@class='slideout-overlay open']");
		//mobileActions.waitTillElementIsNotDisplayed(slideOutOverlay);
		if (mobileActions.checkElementDisplayedWithMidWait(selectObj))
			mobileActions.click(selectObj);
		if (mobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("auto tab of slot game > >", mobileActions.click(locator));
	}

	public void navigateToMentionedTabOfSlotGame(String strTabName) {

		By locator = By.xpath("//span[text()='" + strTabName + "']");
		By selectObj = By.xpath("(//h3[text()='Slots & Games']//following::div[contains(@class,'ip-apollo-select-input')])[1]");
		By slideOutOverlay= By.xpath("//div[@class='slideout-overlay open']");
		//mobileActions.waitTillElementIsNotDisplayed(slideOutOverlay);
        if (mobileActions.checkElementDisplayedWithMidWait(selectObj)) {
               mobileActions.androidScrollToElement(selectObj);
               mobileActions.click(selectObj);
        }

	}

	public void verifyOpacityLayer() {
		By locator = By.xpath("//div[contains(@class,'opacity')]");
		logReporter.log("opacity applied > >", mobileActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void clickMyAccountOfGame() {
		By myAccount = By.xpath("(//i[contains(@class,'my-account')])[2]");
		mobileActions.processMobileElement(myAccount);
		if (mobileActions.checkElementDisplayedWithMidWait(myAccount))
			logReporter.log("Click back to top button> >", mobileActions.click(myAccount));
	}
	
	public void clickJoinNowOfFirstBingoGame() {
		By locator = By.xpath("(//apollo-play-bingo-cta)[2]/button");
		mobileActions.processMobileElement(locator);
		if (mobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click Join Now> >", mobileActions.click(locator));
	}
	
	public void clickInfoOfFirstBingoGame() {
		By locator = By.xpath("(//apollo-play-bingo-cta//following::a)[1]/i");
		mobileActions.processMobileElement(locator);
		if (mobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click i> >", mobileActions.click(locator));
	}
	
	public void clickInfoOfFirstSlotGame() {
		By locator = By.xpath("(//h2[text()='Slots & Games']//following::i)[2]");
		mobileActions.processMobileElement(locator);
		if (mobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click i> >", mobileActions.click(locator));
	}
	
	public void clickJoinNowFromGameDetails() {
		By locator = By.xpath("(//button[text()='Join Now'])[1]");
		mobileActions.processMobileElement(locator);
		if (mobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click join now from bingo detail> >", mobileActions.click(locator));
	}
	
	public void waitTillSlideOutOverlayNotDisplayed() {
		By locator = By.xpath("//div[@class='slideout-overlay open']");
		
	}
	
	public void clickExitGameCTA() {
		mobileActions.processMobileElement(exitarrowbtn);
		if (mobileActions.checkElementDisplayedWithMidWait(exitarrowbtn))
			logReporter.log("Click Exit Game> >", mobileActions.click(exitarrowbtn));
	}
	
	public void clickMyAccFromGame() {
		By locator = By.xpath("//span[text()='Deposit']/following::i[contains(@class,'my-account')]");
		mobileActions.processMobileElement(locator);
		if (mobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click my acc from game> >", mobileActions.click(locator));
	}
}
