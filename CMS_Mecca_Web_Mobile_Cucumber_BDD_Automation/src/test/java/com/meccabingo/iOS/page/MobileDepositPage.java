package com.meccabingo.iOS.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobileDepositPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	private String iFrameId="payment-process";
	
	public MobileDepositPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void clickOnDepositBesidesMyAcct() {
		By btnDeposit = By.xpath("//*[@id='logged-in-bar']/button");
		if (objMobileActions.checkElementDisplayedWithMidWait(btnDeposit))
			logReporter.log("Click deposit besides my acct button> >", objMobileActions.click(btnDeposit));
	}

	public void clickShowMore() {
		By showMore = By.xpath("//span[contains(text(),'Show More')]");
		objMobileActions.invokeActionOnMobileLocator(showMore, "checkElementEnabled");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(showMore))
			logReporter.log("Click Show more button> >", objMobileActions.click(showMore));
		objMobileActions.switchToDefaultContent();
	}
	
	public void selectPaymentMethod(String strPaymtMethod) {
		By pMethod = By.xpath("//div[@title='"+strPaymtMethod+"']"); 
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(pMethod))
			logReporter.log("Click payment method> >", objMobileActions.click(pMethod));
		objMobileActions.switchToDefaultContent();	
	}
	
	public void enterCardNumber(String strNo) {
		By cardNo = By.xpath("//input[@id='cc_card_number']");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(cardNo))
			logReporter.log("enter card no> >", objMobileActions.setText(cardNo, strNo));
		objMobileActions.switchToDefaultContent();	
	}
	
	public void enterExpiry(String strExpiry) {
		/**By month = By.xpath("//select[@id='cc_exp_month']");
		By year = By.xpath("//select[@id='cc_exp_year']");
		String[] str = strExpiry.split("/");
		String strMonth = str[0];
		String strYear= str[1];
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		logReporter.log("select month> >", objMobileActions.selectFromCustomDropDown(month, strMonth));
		logReporter.log("select year> >", objMobileActions.selectFromCustomDropDown(year, strYear));
		objMobileActions.switchToDefaultContent();	*/
		
		By expiry = By.id("cc-exp-date");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(expiry))
			logReporter.log("enter exp date> >", objMobileActions.setText(expiry, strExpiry));
		objMobileActions.switchToDefaultContent();	
		
	}
	
	public void enterCVV(String strCVV) {
		By objCVV = By.xpath("//input[@id='cc_cvv2']");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(objCVV))
			logReporter.log("enter card no> >", objMobileActions.setText(objCVV, strCVV));
		objMobileActions.switchToDefaultContent();	
	}
	
	public void enterDepositAmt(String strAmt) {
		By amt = By.xpath("//input[contains(@placeholder,'amount here')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(amt))
			logReporter.log("enter amount> >", objMobileActions.setText(amt, strAmt));
		objMobileActions.switchToDefaultContent();	
	}
	
	public void clickDepositButton() {
		By locator = By.id("continueButton");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click Deposit button> >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}
	
	public void verifyDepositSuccessful() {
		By locator = By.xpath("//h2[contains(text(),'Deposit successful')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		logReporter.log("Verify Deposit Successful > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
		objMobileActions.switchToDefaultContent();
	}
	
	public void clickCloseFromDepositSuccessfulPopup() {
		By locator = By.xpath("//button[contains(@class,'modal_close')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click close > >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}
	
	public void clickWithdraw() {
		By locator = By.xpath("//button[contains(text(),'Withdraw')]");
		//objMobileActions.invokeActionOnMobileLocator(By.id(iFrameId), "checkelementdisplayed");
		//objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		objMobileActions.switchToFrameUsingIndex(0);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click withdraw > >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}
	
	public void verifyWithdrawSuccessful() {
		By locator = By.xpath("//h2[contains(text(),'Withdrawal request received')]");
		//objMobileActions.invokeActionOnMobileLocator(By.id(iFrameId), "checkelementdisplayed");
		
		objMobileActions.switchToFrameUsingIndex(0);//(iFrameId);
		logReporter.log("Verify Withdraw Successful > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
		objMobileActions.switchToDefaultContent();
	}
	
	public void selectSavedCard(String strCard) {
		By locator;
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if(strCard.equals("paysafe")==true)
			locator = By.xpath("//div[contains(@class,'paysafe')]");
		else
			locator =  By.xpath("//div[contains(@title,'"+strCard+"')]");
		
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click saved card > >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}
	
	public void clickPayPal() {
		By locator = By.xpath("//img[contains(@title,'PayPal')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click PayPal > >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}
	
	public void clickPaySafe() {
		By locator = By.xpath("//img[contains(@title,'paysafe')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click spaysafe > >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}
	
	public void navigateToPaySafeURL() {
		By locator = By.id("payment-process");
		if (objMobileActions.checkElementExists(locator)) {
			String strURL = objMobileActions.getAttribute(locator, "src");
			objAppiumDriverProvider.getAppiumDriver().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
			objAppiumDriverProvider.getAppiumDriver().get(strURL);
			//objMobileActions.waitTillPageLoadCompletes();
		}
	}
}
