
package com.meccabingo.iOS.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class MobileLogInPage {

	By closeXicon = By.xpath("//a[contains(@class,'icon-close')]");
	By tbUserName = By.xpath("//input[@id='input-username']");
	By tbPassword = By.xpath("//input[@id='input-password']");
	By rememberMeCheckbox = By.xpath("//*[contains(text(),'Remember Me')]/preceding-sibling::input[@type='checkbox']");
	By btnLogIn = By.xpath("//span[@class='button-content']/parent::button[@type='submit']"); // button[contains(text(),'Login')//
																								// and//
																								// @type='submit'],
																								// //*[@id=\"header\"]/header/div[2]/button
	By forgotUserNameLink = By.xpath("//a[contains(text(),'Forgot username?')]");
	By forgotPasswordLink = By.xpath("//a[contains(text(),'Forgot password?')]");
	By linkSignUP = By.xpath("//a[contains(text(),'Sign Up')]");

	By sendResetInstructions = By.xpath("//*[@class='login-wrapper-form']/button/child::span");
	By resetPasswordSuccessHeading = By.xpath("//*[contains(text(),'Success')]");
	By resetPasswordSuccessMessage = By.xpath("//*[contains(text(),'Success')]/following-sibling::p");
	By txtEmailAddress = By.xpath("//input[@id='input-email']");
	By sendUsernameReminder = By.xpath("//*[@class='login-wrapper-form']/button/child::span");
	By btnCTALogIn = By.xpath("//button[contains(@class,'yellow') and @type='submit']"); // *[@id=\"overlay-level-3\"]/div/section/div[2]/form/button
																							// //button[contains(@class,'button
																							// button-large')]/child::span

	By MyAccountButton = By.xpath("//a[contains(@class,'open-myaccount')]/i");

	// private WebActions objMobileActions;
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobileLogInPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void verifyLoginHeaderTitle() {
		By loginHeaderText = By.xpath("//h4[contains(text(),'Login')]"); // h4[starts-with(text(),'Login')]
		logReporter.log("Check Login window header > >", objMobileActions.checkElementDisplayed(loginHeaderText));
	}

	public void clickJoinNowButton() {
		By btnJoinNow = By.xpath("//button[contains(text(),'Join Now')]");
		logReporter.log("click 'join now button' > >", objMobileActions.click(btnJoinNow));

	}

	public void enterUserName(String userName) {
		if(objMobileActions.checkElementDisplayedWithMidWait(tbUserName))
		logReporter.log("Enter Value in userName > >", objMobileActions.setText(tbUserName, userName));
	}

	public void enterPassword(String password) {
		logReporter.log("Enter Value in password > >", objMobileActions.setText(tbPassword, password));
	}

	public void clickLogin() {
		logReporter.log("click 'login button' > >", objMobileActions.click(btnLogIn));
	}

	public void verifyLoginDisabled() {
		By btnLogInDisabled = By.xpath("//button[contains(text(),'Login') and contains(@class,'button-disabled')]");
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(btnLogInDisabled));
	}

	public void verifyLogInErrorMessage(String errorMessage) {
		// System.out.println("Text captured: " +
		// objMobileActions.getText(logInErrorMessage, "text"));
		//
		// logReporter.log("Check error Message ", errorMessage,
		// objMobileActions.getText(logInErrorMessage, "text"));
		By errormessage = By.xpath("//h4[contains(text(),'" + errorMessage + "')]");
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(errormessage));
	}

	public void verifyLoginPage() {
		// By loginHeader = By.xpath("//div[@class='slideout-overlay-heading']");
		By loginHeader = By.xpath("//button[@type='submit']/span[text()='Login']");
		
		logReporter.log("Check login page displayed", objMobileActions.checkElementDisplayed(loginHeader));

	}

	public void verifyCloseXicon() {

		logReporter.log("Check Close X icon displayed", objMobileActions.checkElementDisplayed(closeXicon));
	}

	public void clickOnCloseXicon() {

		logReporter.log("Check Close X icon displayed", objMobileActions.click(closeXicon));
	}

	public void verifyTBUserNameDisplayed() {
		logReporter.log("Check Username textbox displayed", objMobileActions.checkElementDisplayed(tbUserName));

	}

	public void verifyTBPasswordDisplayed() {
		logReporter.log("Check Password textbox displayed", objMobileActions.checkElementDisplayed(tbPassword));
	}

	public void verifyPasswordToggle() {
		By passwordToggle = By.xpath("//input[@id='input-password']/following::button[contains(text(),'Show')]");
		logReporter.log("Check Password toggle displayed", objMobileActions.checkElementDisplayed(passwordToggle));

	}

	public void verifyRememberMeCheckbox() {
		logReporter.log("Check Remember Me checkbox displayed",
				objMobileActions.checkElementDisplayed(rememberMeCheckbox));
	}

	public void verifyLoginCTA() {
		logReporter.log("Check login Button displayed", objMobileActions.checkElementDisplayed(btnCTALogIn));
	}

	public void veryForgotUserNameLink() {

		logReporter.log("Check Forgot UserName Link displayed",
				objMobileActions.checkElementDisplayed(forgotUserNameLink));

	}

	public void veryForgotPasswordLink() {

		logReporter.log("Check Forgot Password Link displayed",
				objMobileActions.checkElementDisplayed(forgotPasswordLink));

	}

	public void verifyNewToMeccaText() {
		By newToMeccaText = By
				.xpath("//a[contains(text(),'Sign Up')]/parent::p[contains(text(),'New to Mecca bingo')]");
		logReporter.log("Check New to Mecca text displayed", objMobileActions.checkElementDisplayed(newToMeccaText));
	}

	public void verifySignUpLinkDisplayed() {

		logReporter.log("Check Sign Up Link displayed", objMobileActions.checkElementDisplayed(linkSignUP));

	}

	public void clickOnSignUpLink() {
		logReporter.log("Click sign up", objMobileActions.click(linkSignUP));
	}

	public void verifyHelpContactDetails() {
		By contactUsSection = By.xpath("//p[contains(text(),'Contact')]");
		By contactUsCallNumber = By.xpath("//a[contains(@href,'tel')]");
		By contactUsemail = By.xpath("//a[contains(@href,'mailto:')]");

		logReporter.log("Check Contact Us section displayed", objMobileActions.checkElementDisplayed(contactUsSection));
		logReporter.log("Check Contact Us Call NUmber displayed",
				objMobileActions.checkElementDisplayed(contactUsCallNumber));
		logReporter.log("Check Contact Us Email displayed", objMobileActions.checkElementDisplayed(contactUsemail));
	}

	public void verifyLiveChat() {
		By liveChat = By.xpath("//i[contains(@class,'livechat')]");
		logReporter.log("Check Live Chat displayed", objMobileActions.checkElementDisplayed(liveChat));
	}

	public void verifyLogInEnabled() {
		if (!objMobileActions.checkElementDisplayed(
				By.xpath("//button[contains(@class,'yellow') and @type='submit' and @disabled]")))
			logReporter.log("Check login button Enabled", true);

		// logReporter.log("Check login button Enabled",
		// objMobileActions.checkElementEnabled(btnLogIn));
	}

	public void clickOnRememberMe() {
		logReporter.log("Clcik Remember Me checkbox", objMobileActions.click(rememberMeCheckbox));
	}

	public void VerifyTextFromUserName(String useName) {
		// logReporter.log("Verify Text from UserName", useName,
		// objMobileActions.getText(tbUserName, "value"));
	}

	public void verifyForgottenPasswordHeader() {
		By forgottenPasswordPageHeader = By
				.xpath("//div[@class='slideout-overlay-heading']/h4[contains(text(),'Forgot your password')]");
		logReporter.log("Check 'Forgotten Password' header",
				objMobileActions.checkElementDisplayed(forgottenPasswordPageHeader));
	}

	public void clickOnForgotPasswordLink() {
		logReporter.log("Click 'Forgot Password'", objMobileActions.click(forgotPasswordLink));
	}

	public void verifyBackArrow() {
		By backArrow = By.xpath("//a[contains(@class,'icon-arrow-left')]");
		logReporter.log("Check 'Back Arrow'", objMobileActions.checkElementDisplayed(backArrow));

	}

	/*
	 * public void verifyHelperTextFromForgottenPasswordPage(String text) { By
	 * forgottenPasswordPageHelperText =
	 * By.xpath("//div[contains(@class,'slideout-overlay-content')]/p");
	 * logReporter.log("Check 'Forgotten Password additional info'", text,
	 * objMobileActions.getText(forgottenPasswordPageHelperText , "text")); }
	 */

	public void verifySendResetInstructionsCTA() {
		logReporter.log("Check 'Send Reset Instructions 'CTA'",
				objMobileActions.checkElementDisplayed(sendResetInstructions));
	}

	public void clickOnSendResetInstructions() {
		By sendResetInstructions = By.xpath("//*[@class='login-wrapper-form']/button/child::span");
		logReporter.log("Click 'Send reset instructions' Link", objMobileActions.click(sendResetInstructions));
	}

	public void verifyPasswordResetSuccessMessage() {

		By resetPasswordSuccessHeading = By.xpath("//*[contains(text(),'Success')]");
		By resetPasswordSuccessMessage = By.xpath("//*[contains(text(),'Success')]/following-sibling::p");

		logReporter.log("Check 'Reset Password Sucess'",
				objMobileActions.checkElementDisplayed(resetPasswordSuccessHeading));
		logReporter.log("Check 'Reset Password Sucess Message'",
				objMobileActions.checkElementDisplayed(resetPasswordSuccessMessage));
	}

	public void verifyIDidnotReceiveAnEmailLink() {
		By iDidnotReceiveAnEmail = By.xpath("//div[contains(@class,'login-wrapper-links')]");
		logReporter.log("Check 'I didnot receive an email' link",
				objMobileActions.checkElementDisplayed(iDidnotReceiveAnEmail));
	}

	public void clickOnForgotUsernameLink() {
		logReporter.log("Click 'Forgot username' Link", objMobileActions.click(forgotUserNameLink));
	}

	public void verifyForgottenUsernameHeader() {
		By forgottenUsernamePageHeader = By
				.xpath("//div[@class='slideout-overlay-heading']/h4[contains(text(),'Forgotten your')]");
		logReporter.log("Check 'Forgotten Username' header",
				objMobileActions.checkElementDisplayed(forgottenUsernamePageHeader));
	}

	/*
	 * public void verifyHelperTextFromForgottenUsernamePage(String text) { By
	 * forgottenUsernamePageHelperText = By.
	 * xpath("//div[contains(@class,'slideout-overlay-content')]/p[contains(text(),'Please supply the following account')]"
	 * ); logReporter.log("Check 'Forgotten username additional info'", text,
	 * objMobileActions.getText(forgottenUsernamePageHelperText , "text")); }
	 **/

	public void verifyEmailFieldFromForgottenUsername() {
		logReporter.log("Check 'Email' from Forgotten Username",
				objMobileActions.checkElementDisplayed(txtEmailAddress));
	}

	public void verifySendUsernameReminderCTA() {
		logReporter.log("Check 'Send Username Reminder 'CTA'",
				objMobileActions.checkElementDisplayed(sendUsernameReminder));
	}

	public void enterEmailAddressInForgotUsernameSection(String emailAddress) {
		logReporter.log("Enter Value in email address > >", objMobileActions.setText(txtEmailAddress, emailAddress));
	}

	public void clickOnSendUsernameReminder() {
		logReporter.log("Click 'Send Username Reminder'", objMobileActions.click(sendUsernameReminder));
	}

	public void clickOnLoginCTAbutton() {

		logReporter.log("Click on CTA login button", objMobileActions.click(btnCTALogIn));
	}

	public void verifyHelpTextFromForgottenUsernamePage(String text) {
		By forgottenPasswordPageHelperText = By.xpath("//div[contains(@class,'slideout-overlay-content')]/p");
		logReporter.log("Check 'Forgotten Password additional info'", text,
				objMobileActions.getText(forgottenPasswordPageHelperText));

	}

	public void clickOnMyAccountButtonFromHeader() {
		logReporter.log("Click on My Account button", objMobileActions.click(MyAccountButton));

	}

	public void clickOnLogoutbutton() {
		By LogoutButton = By.xpath("//i[contains(@class,'logout')]");
		objMobileActions.checkElementDisplayedWithMidWait(LogoutButton);
		logReporter.log("Click on logout button", objMobileActions.click(LogoutButton));
	}
	/**
	 * public void clearUsernameText() { logReporter.log("Clear username",
	 * objMobileActions.clearText(tbUserName)); } public void clearPasswordText() {
	 * logReporter.log("Clear password", objMobileActions.clearText(tbPassword)); }
	 */
}
