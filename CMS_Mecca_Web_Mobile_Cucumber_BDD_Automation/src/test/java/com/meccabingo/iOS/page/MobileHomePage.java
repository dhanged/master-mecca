/**
 * 
 */
package com.meccabingo.iOS.page;

import org.openqa.selenium.By;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;

import com.generic.utils.WaitMethods;
import com.google.common.base.Function;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class MobileHomePage {

	By logoBingoHeader = By.xpath("//*[contains(@id,'header')]/a/picture");

	By footer_SocialMediaBlock = By.xpath("//div[contains(@class,'footer-social')]");
	By footer_UsefulLinkBlock = By.xpath("//ul[@class='useful-links']");
	By footer_PartnersLogoSection = By.xpath("//ul[@class='partners']");
	By footer_privacyAndSecurityBlock = By.xpath("//div[@class='footer-row'] ");
	By footer_PaymentProvidersBlock = By.xpath("//div[@class='footer-payments']");
	By loginButton = By.xpath("//button[text()='Login']");
	By myAccounticon = By.xpath("//i[@class='my-account']");

	private LogReporter logReporter;
	private AppiumDriverProvider objAppiumDriverProvider;
	private MobileActions objMobileActions;
	private WaitMethods objWaitMethods;
	private Configuration objConfiguration;

	public MobileHomePage(LogReporter logReporter,WaitMethods waitMethods, AppiumDriverProvider appiumDriverProvider,
			Configuration configuration, MobileActions mobileActions) {
		this.logReporter = logReporter;
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.objConfiguration = configuration;
		this.objWaitMethods = waitMethods;
	}

	public void launchAppURL() {
		objAppiumDriverProvider.getAppiumDriver().get(objConfiguration.getConfig("web.Url"));
		//objWaitMethods.waitForElementToBeDisplayed(logoBingoHeader, 30);
		//objAppiumDriverProvider.getAppiumDriver().
		try {
			WebDriverWait wait = new WebDriverWait(objAppiumDriverProvider.getAppiumDriver(), 30);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(logoBingoHeader));
		}
		catch (TimeoutException ex) {
			//return null;
			ex.printStackTrace();
		}
	}

	public void clickOnBackToTop() {
		By locator = By.xpath("//apollo-scroll-top");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			objMobileActions.click(locator);
	}

	public void verifyDeviceIsLandscape() {
		boolean result = false;
		if (ScreenOrientation.LANDSCAPE.equals(objAppiumDriverProvider.getAppiumDriver().getOrientation()))
			result = true;
		logReporter.log("Check Device is in LandScape mode > >", result);
	}

	public void verifyHeaderLogo() {
		//objWaitMethods.getElementFluent(logoBingoHeader, 20, 10);
		logReporter.log("check logo on homepage > >",
				objMobileActions.checkElementDisplayedWithMidWait(logoBingoHeader));
	}
	
	public void clickLetsPlay() {
		By locator = By.xpath("//a[contains(text(),'Let')]");
		if(objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click lets play > >", objMobileActions.click(locator));
	}

	public void clickCookieContinueButton() {
		//this is to click on Continue button of cookies
		By btnContinue = By.xpath("//button[text()='Continue']");
		if(objMobileActions.checkElementDisplayedWithMidWait(btnContinue))
			logReporter.log("click continue button > >", objMobileActions.click(btnContinue));
	}

	public void scrollToSocialMediaBlock() {
		objMobileActions.androidScrollToElement(footer_SocialMediaBlock);
	}

	public void scrollToUsefulLinkBlock() {

		objMobileActions.androidScrollToElement(footer_UsefulLinkBlock);

	}

	public void scrollToLogoSection() {
		objMobileActions.androidScrollToElement(footer_PartnersLogoSection);

	}

	public void scrollToprivacyAndSecurityBlock() {
		objMobileActions.androidScrollToElement(footer_privacyAndSecurityBlock);

	}

	public void scrollToPaymentProvidersBlock() {
		objMobileActions.androidScrollToElement(footer_PaymentProvidersBlock);

	}

	public void clickOnLogInButton() {
		// objMobileActions.clickUsingJS(loginButton);
		logReporter.log("click on 'LogIn button' > >", objMobileActions.click(loginButton));

	}

	public void verifyRegisterNowLink() {
		By registernowlink = By.xpath("//a[contains(text(),'Not a member')]");
		logReporter.log("Verify join now button", objMobileActions.checkElementDisplayed(registernowlink));
	}

	public void verifyHeaderJoinNowbtn() {
		By joinnowbtn = By.xpath("//a[contains(text(),'Join Now')]");
		logReporter.log("Verify join now button", objMobileActions.checkElementDisplayed(joinnowbtn));
	}

	public void verifybattenberg() {
		By battenberg = By.xpath("//apollo-battenberg");
		logReporter.log("verify batternberg block", objMobileActions.checkElementDisplayed(battenberg));
	}

	public void verifyBlockInOneRow() {
		By onerowblock = By.xpath("//section[contains(@class,'three-columns')]");
		logReporter.log("Verify block", objMobileActions.checkElementDisplayed(onerowblock));
	}

	public void verifyThreeBlock() {
		By blockone = By.xpath("//section[contains(@class,'three-columns')]//h4[contains(text(),'Help')]");
		By blocktwo = By.xpath("//section[contains(@class,'three-columns')]//h4[contains(text(),'Contact')]");
		By blockthree = By.xpath("//section[contains(@class,'three-columns')]//h4[contains(text(),'Community')]");

		logReporter.log("Verify block one", objMobileActions.checkElementDisplayed(blockone));
		logReporter.log("Verify block two", objMobileActions.checkElementDisplayed(blocktwo));
		logReporter.log("Verify block three", objMobileActions.checkElementDisplayed(blockthree));

	}

	public void clickOnHamburgerMenu() {
		By hamburgermenu = By.xpath("//div[contains(@class,'hamburger')]");
		logReporter.log("click on 'Hamburger Menu' > >", objMobileActions.click(hamburgermenu));
	}

	public void verifyHamburgerMenu() {
		By hamburgermenuheader = By.xpath("//*[contains(@class,'hamburger')]");
		logReporter.log("verify 'Hamburger Menu' > >", objMobileActions.checkElementDisplayed(hamburgermenuheader));
	}

	public void verifyClosedHamburgerMenu() {
		By hamburgermenuheader = By.xpath("//*[contains(@class,'hamburger')]");
		if (!objMobileActions.checkElementDisplayed(hamburgermenuheader))
			logReporter.log("verify 'Hamburger Menu' > >", true);
	}

	public void clickOnSearchIcon() {
		By search_loupe = By.xpath("//button[contains(@class,'search-open-btn')]");
		logReporter.log("click 'Search Loupe' > >", objMobileActions.click(search_loupe));
	}

	public void verifySearchMenu() {
		By search_loupe = By.xpath("//button[contains(@class,'search-open-btn')]");
		logReporter.log("click 'Search Loupe' > >", objMobileActions.checkElementDisplayed(search_loupe));
	}

	public void verifyClosedSearchMenu() {
		By searchmenu = By.xpath("//input[contains(@class,'search-input')]");
		if (!objMobileActions.checkElementDisplayed(searchmenu))
			logReporter.log("click 'Search Loupe' > >", true);
	}

	public void checkSurveyAndCloseIt() {
		logReporter.log("entererd  checkSurveyAndCloseIt > >", true);
		By survey = By.xpath("//div[contains(@class,'sv__survey')]");
		By btnPlus = By.xpath("//div[contains(@class,'_plus-button')]");
		By btnClose = By.xpath("//button[contains(@class,'sv__btn-close')]");
		if(objMobileActions.checkElementDisplayedWithMidWait(survey)) {
			logReporter.log("Survey displayed > >", true);
			if(objMobileActions.checkElementDisplayed(btnPlus))
				objMobileActions.click(btnPlus);
			if(objMobileActions.checkElementDisplayedWithMidWait(btnClose))
				objMobileActions.click(btnClose);
		}
	}
	public void checkSurveyAndCloseItForPortrait() {
		By survey = By.xpath("//div[contains(@class,'sv__survey')]");
		By btnPlus = By.xpath("//div[contains(@class,'_plus-button')]");
		By btnClose = By.xpath("//button[contains(@class,'sv__btn-close')]");
		if(objMobileActions.checkElementExists(survey)) {
			if(objMobileActions.checkElementExists(btnPlus))
				objMobileActions.click(btnPlus);
			if(objMobileActions.checkElementExists(btnClose))
				objMobileActions.click(btnClose);
		}
	}
	
	public void checkSurveyAndCloseItForLandScape() {
		By survey = By.xpath("//div[contains(@class,'sv__survey')]");
		By btnClose = By.xpath("//button[contains(@class,'sv__btn-close')]");
		if(objMobileActions.checkElementExists(survey)) {
			if(objMobileActions.checkElementDisplayedWithMidWait(btnClose))
				objMobileActions.click(btnClose);
		}
	}

}
