package com.meccabingo.iOS.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobileDetailsPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobileDetailsPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void navigateThroughHamburgerMenu(String strMenuName) {
		By hamburgerMenu = By.xpath("//apollo-top-navigation");
		if (objMobileActions.checkElementDisplayedWithMidWait(hamburgerMenu))
			logReporter.log("Click hamburger menu> >", objMobileActions.click(hamburgerMenu));
		By locator = By.xpath("//a[contains(@href,'" + strMenuName + "')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click menu> >", objMobileActions.click(locator));
	}
}
