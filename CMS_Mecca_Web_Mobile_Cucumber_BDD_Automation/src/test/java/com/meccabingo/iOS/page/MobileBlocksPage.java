package com.meccabingo.iOS.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobileBlocksPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobileBlocksPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void clickOnBackToTopButton() {
		By backToTop = By.xpath("//apollo-scroll-top");
		if (objMobileActions.checkElementDisplayedWithMidWait(backToTop))
			logReporter.log("Click back to top button> >", objMobileActions.click(backToTop));
	}
	
	public void verifyMultipleWinnersFeedAvailableWithMultipleBoxes() {
		By locator = By.xpath("//section[contains(@class,'winners-carousel')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("check multiple winners feed> >", objMobileActions.click(locator));
	}

	public void verifyBackOnTopCTADisplayed() {
		By backToTop = By.xpath("//apollo-scroll-top");
		logReporter.log("Click back to top button> >", objMobileActions.checkElementDisplayedWithMidWait(backToTop));
	}

	public void scrollPageTillFooter() {
		By locator = By.xpath("//span[text()='Twitter']");
		objMobileActions.androidScrollToElement(locator);
		logReporter.log("scroll till footer", true);

	}

}
