/**
 * 
 */
package com.meccabingo.iOS.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
/**
 * @author Namrata Donikar Harshvardhan Yadav, (Expleo)
 *
 */
public class RecentlyPlayedPage { 

	private MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public RecentlyPlayedPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}
	
	public void verifyTabDisplayedOnHomePage(String header) {
		By locator = By.xpath("//h2[contains(text(),'" + header + "')]");
		logReporter.log("Verify '" + header + "' tab displayed on home page", objMobileActions.checkElementDisplayed(locator));
	}
 	
	public void verifyYouDontHaveAnyRecentlyPlayedGamesMessageDisplayedForSlot() {
		By locator = By.xpath("(//div[text()='You have no Recently Played Games to view'])[1]");
		logReporter.log("Verify 'You dont have any recently played games' message displayed for slot", objMobileActions.checkElementDisplayed(locator));
	}
	
	public void verifyYouDontHaveAnyRecentlyPlayedGamesMessageDisplayedForBingo() {
		By locator = By.xpath("(//div[text()='You have no Recently Played Games to view'])[2]");
		logReporter.log("Verify 'You dont have any recently played games' message displayed for bingo", objMobileActions.checkElementDisplayed(locator));
	}
	
	public void verifyOptionsForRecentlyPlayed(String strSection,String text) {
		By locator;
		switch (text) {

		case "Image": {
			locator = By.xpath("(//apollo-recently-played-"+strSection+"-tile)[1]");
			logReporter.log("check Image > >", objMobileActions.checkElementDisplayed(locator));
			break;
		}

		case "Title": {
			locator = By.xpath("(//apollo-recently-played-"+strSection+"-tile)[1]//p[contains(@class,'played-title')]");
			logReporter.log("check Title > >", objMobileActions.checkElementDisplayed(locator));
			break;
		}

		case "Close button": {
			locator = By.xpath("(//apollo-recently-played-"+strSection+"-tile)[1]/a[contains(@class,'close')]");
			logReporter.log("check Close button > >", objMobileActions.checkElementDisplayed(locator));
			break;
		}
		}

	}
	
	public void selectNavigationMenu(String navigationMenu) {
		By locator = By.xpath("//ul[contains(@class,'top-navigation')]/apollo-top-navigation-item//a[text()='" + navigationMenu + "']");
		logReporter.log("Select navigation menu '" + navigationMenu + "'", objMobileActions.click(locator));
	}
	
	public void verifyOthersAlsoPlayedDisplayed(String section) {
		By locator=null;
		switch(section) {
		case "slot": 
				locator = By.xpath("(//p[contains(text(),'Others also played')])[1]");
				break;
		case "bingo": 
				locator = By.xpath("(//p[contains(text(),'Others also played')])[2]");
				break;
		}
		logReporter.log("check others also played > >", objMobileActions.checkElementDisplayed(locator));
	}
}
