/**
 * 
 */
package com.meccabingo.iOS.page;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Random;
import java.util.RandomAccess;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;

/**
 * @author Dayanand Dhange (Expleo)
 *
 */
public class MobileRegistrationPage {

	By linkJoinNow = By.xpath("//a[text()='Join Now']");
	By btnNext = By.xpath("//input[@id='id-agreed']/following::button[2]/span");
	By btnYes = By.xpath("//button[@name='join-with-email']");
	By btnSelectedYes = By.xpath("//button[@name='join-with-email' and contains(@class,'pink')]");
	By editBoxEmail = By.xpath("//input[@placeholder='Email address']");
	By membershipNumber = By.xpath("//input[contains(@placeholder,'Membership')]");

	By btnLoginPopupClose = By.xpath("//a[contains(@class,'icon-close')]");

	// contact preferences section
	By btnSelectAll = By.xpath("//button[contains(@class,'button-green')]");
	By btnRegister = By.xpath("//button[@type='submit' and contains(@class,'button-fullwidth')]/span[text()='Register']");
	//By btnRegister = By.xpath("//span[text()='Register']");

	By errorSelectContactPreferenceMsg = By
			.xpath("//ul[@class='errors']/li[text()='Please select contact preference']");

	By editBoxUsername = By.id("input-username");
	By editBoxPassword = By.id("input-password");
	By titleSection = By.xpath("//div[contains(@class,'title-section')]");
	By editBoxFirstName = By.id("input-firstname");
	By editBoxSurName = By.id("input-surname");
	By editBoxDOBDay = By.id("input-dob-day");
	By editBoxDOBMonth = By.id("input-dob-month");
	By editBoxDOBYear = By.id("input-dob-year");
	By dobSection = By.xpath("//div[contains(@class,'dob-inner')]");
	By mobileNumber = By.xpath("//input[@id='input-mobile']");


	String greencolor = "rgba(0, 157, 122, 1)";// "#5ea85a";
	// rgba(230, 218, 230, 1)
	// #78B174

	private LogReporter logReporter;
	// private AppiumDriverProvider objAppiumDriverProvider;
	private MobileActions objMobileActions;

	public MobileRegistrationPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {

		// this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void enterRandomEmail() {
		// to form random email text
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyMMddhhmmssMs");
		String datetime = ft.format(dNow);
		String strEmail = "iot" + datetime + "@mailinator.com";
		logReporter.log("enter 'Random Email Id' > >", objMobileActions.setText(editBoxEmail, strEmail));
	}

	public void enterRandomUsername() {
		// to form random username text
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyMMddhhmmssMs");
		String datetime = ft.format(dNow);
		String strUsername = "Meiot" + datetime;
		strUsername = strUsername.substring(0, 15);
		logReporter.log("enter username' > >", objMobileActions.setText(editBoxUsername, strUsername));
	}

	public void enterExistingEmail() {
		String strEmail = "test01@mailinator.com";
		logReporter.log("enter 'Random Email Id' > >", objMobileActions.setText(editBoxEmail, strEmail));
	}

	public void enterInvalidEmail() {
		String strEmail = "test01.com";
		logReporter.log("enter invalid email > >", objMobileActions.setText(editBoxEmail, strEmail));
		objMobileActions.sendKeys(editBoxEmail, Keys.TAB);
	}

	public void enterRegistrationUsername(String strUserName) {
		logReporter.log("enter registration username' > >", objMobileActions.setText(editBoxUsername, strUserName));
	}

	public void clickOnAgreeCheckBox() {
		By chkboxAgree = By.xpath("//input[@id='id-agreed']");
		if (objMobileActions.checkElementDisplayedWithMidWait(chkboxAgree))
			logReporter.log("check checkbox > >", objMobileActions.selectCheckbox(chkboxAgree, true));
	}

	public void clickOnButton(String strBtnName) {
		By locator = null;

		switch (strBtnName) {
		case "JoinNow":
			locator = linkJoinNow;
			break;
		case "Next":
			locator = btnNext;
			break;
		case "SelectAll":
			locator = btnSelectAll;
			break;
		case "Yes":
			locator = btnYes;
			break;
		case "Close":
			locator = btnLoginPopupClose;
			break;
		case "Register":
			locator = btnRegister;
			break;

		default:
			System.out.println(strBtnName + " : mentioned button is not implemented ");
		}
		if (locator != null) {
			if (objMobileActions.checkElementDisplayedWithMidWait(locator)) {
				if (strBtnName.equals("Register")) {
					// objMobileActions.clickUsingActionClass(locator);
					objMobileActions.processMobileElement(locator);
					objMobileActions.checkElementEnabled(locator);
					objMobileActions.clickUsingJS(locator);
				} else
					logReporter.log("click button >" + strBtnName + " >", objMobileActions.click(locator));
			}
		}
	}

	public void clickOnNextButton() {
		logReporter.log("click Next button > >", objMobileActions.clickUsingJS(btnNext));
	}

	public void verifyAllCheckboxesChecked() {
		By chkboxEmail = By.xpath("//input[@id='gdpr-email']");
		By chkboxSMS = By.xpath("//input[@id='gdpr-sms']");
		By chkboxPhone = By.xpath("//input[@id='gdpr-phone']");
		By chkboxPost = By.xpath("//input[@id='gdpr-post']");

		String valueEmail = objMobileActions.getAttribute(chkboxEmail, "value");
		String valueSMS = objMobileActions.getAttribute(chkboxSMS, "value");
		String valuePhone = objMobileActions.getAttribute(chkboxPhone, "value");
		String valuePost = objMobileActions.getAttribute(chkboxPost, "value");
		if (valueEmail.equals("true") == true && valueSMS.equals("true") == true && valuePhone.equals("true") == true
				&& valuePost.equals("true") == true)
			logReporter.log("All the checkboxes are checked", true);
		else
			logReporter.log("All the checkboxes are checked", false);

	}

	public void clickCheckbox(String chkboxName) {
		By locator = By.xpath("//input[@type='checkbox' and @id='gdpr-" + chkboxName + "']");
		logReporter.log("check checkbox > >", objMobileActions.click(locator));
	}

	public void verifyCheckboxIsChecked(String chkboxName) {
		By locator = By.xpath("//input[@id='gdpr-" + chkboxName + "']");
		String checkValue = objMobileActions.getAttribute(locator, "value");
		if (checkValue.equals("true"))
			logReporter.log(chkboxName + "checkbox is checked :", true);
		else
			logReporter.log(chkboxName + "checkbox is checked :", false);

	}

	public void verifyEmailGreenColorDisplayed() {
		// By divEmailGreenColor = By.xpath("//div[@class='field-row ip-text-form
		// passed']");
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(editBoxEmail, "border-bottom-color", greencolor));

	}

	public void verifyGeneralErrorDisplayed() {
		By headerError = By.xpath("//h4[text()='General Error']");
		By paraErrorText = By
				.xpath("//p[text()='The supplied email address is already associated with a different user']");

		if (objMobileActions.checkElementDisplayed(headerError)
				&& objMobileActions.checkElementDisplayed(paraErrorText))
			logReporter.log(
					"Error 'The supplied email address is already associated with a different user' is displayed",
					true);
		else
			logReporter.log(
					"Error 'The supplied email address is already associated with a different user' is displayed",
					false);

	}

	public void verifyRedBackgroundColorDisplayedForCheckbox(String chkboxName) {
		By locator = By.xpath("//div[contains(@class,'checkbox-only error')]/input[@id='gdpr-" + chkboxName + "']");
		logReporter.log("red background color displayed for checkbox " + chkboxName,
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void errorSelectContactPreferenceMsgDisplayed() {
		logReporter.log("Please select contact preference error message displayed ",
				objMobileActions.checkElementDisplayedWithMidWait(errorSelectContactPreferenceMsg));
	}

	public void verifyErrorMessage(String strErrMsg) {
		By locator = By.xpath("//ul[@class='errors']/li[contains(text(),'" + strErrMsg + "')]");
		logReporter.log("Error message " + strErrMsg + " displayed",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyMessageIsNotDisplayed(String strMsg) {
		By locator = By
				.xpath("//div[@class='inputHintDiv hide']/span[@class='inputHintBody' and text()='" + strMsg + "']");
		// if (!objMobileActions.checkElementDisplayed(locator))
		logReporter.log("Message " + strMsg + " not displayed", objMobileActions.checkElementDisplayed(locator));
	}

	public void enterFirstName(String strFirstName) {
		logReporter.log("enter first name >" + strFirstName + ">",
				objMobileActions.setText(editBoxFirstName, strFirstName));
	}
	
	public void enterRandomFirstName() {
		String randomText = RandomStringUtils.random(12, true, false);
		logReporter.log("enter random first name >" + randomText + ">",
				objMobileActions.setText(editBoxFirstName, randomText));
	}

	public void verifyGreenColorForFirstName() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(editBoxFirstName, "border-bottom-color", greencolor));
	}

	public void enterSurName(String strSurName) {
		logReporter.log("enter sur name >" + strSurName + ">", objMobileActions.setText(editBoxSurName, strSurName));
	}
	
	public void enterRandomSurName() {
		String randomText = RandomStringUtils.random(12, true, false);
		logReporter.log("enter random sur name >" + randomText + ">",
				objMobileActions.setText(editBoxSurName, randomText));
	}
	
	public void verifyGreenColorForSurName() {
		// By surNameGreenColor = By.xpath("//input[@id='input-surname' and @class='
		// valid']");
		// logReporter.log("Green line displayed for sur name >> ",
		// objMobileActions.checkElementDisplayed(surNameGreenColor));
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(editBoxSurName, "border-bottom-color", greencolor));

	}

	public void enterDOB(String strDOB) {
		By editBoxDOBDay = By.id("input-dob-day");
		By editBoxDOBMonth = By.id("input-dob-month");
		By editBoxDOBYear = By.id("input-dob-year");

		String strArray[] = strDOB.split("-");
		logReporter.log("enter day >" + strArray[0] + ">", objMobileActions.setText(editBoxDOBDay, strArray[0]));
		logReporter.log("enter month >" + strArray[1] + ">", objMobileActions.setText(editBoxDOBMonth, strArray[1]));
		logReporter.log("enter year >" + strArray[2] + ">", objMobileActions.setText(editBoxDOBYear, strArray[2]));
	}
	
	public void enterRandomDOB() {
		Random random =  new Random();
		int minDay = (int) LocalDate.of(1901, 1, 1).toEpochDay();
		int maxDay = (int) LocalDate.of(2000, 1, 1).toEpochDay();
		long randomDay = minDay + ((Random) random).nextInt(maxDay - minDay);

		LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);
		String strArray[] = randomBirthDate.toString().split("-");
		
		logReporter.log("enter random day >" + strArray[2] + ">", objMobileActions.setText(editBoxDOBDay, strArray[2]));
		logReporter.log("enter random month >" + strArray[1] + ">", objMobileActions.setText(editBoxDOBMonth, strArray[1]));
		logReporter.log("enter random year >" + strArray[0] + ">", objMobileActions.setText(editBoxDOBYear, strArray[0]));

	}


	public void verifyGreenColorForDOB() {
		By dobGreenColor = By.xpath("//div[contains(@class,'field-row ip-dob')]");
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(dobGreenColor, "border-bottom-color", "rgba(0, 157, 122, 1)"));

	}

	public void enterPassword(String strPassword) {
		logReporter.log("enter password", objMobileActions.setText(editBoxPassword, strPassword));
	}

	public void verifyRegistrationPageDetails(String text) {

		switch (text) {

		case "First name": {
			logReporter.log("check element >First name >", objMobileActions.checkElementDisplayed(editBoxFirstName));
			break;
		}

		case "Sur name": {
			logReporter.log("check element >Surname >", objMobileActions.checkElementDisplayed(editBoxSurName));
			break;
		}

		case "Title": {
			logReporter.log("check element >Title >", objMobileActions.checkElementDisplayed(titleSection));
			break;
		}

		case "Date of birth": {
			logReporter.log("check element >Date of birth >", objMobileActions.checkElementDisplayed(dobSection));
			break;
		}

		case "Username": {
			logReporter.log("check element >Username >", objMobileActions.checkElementDisplayed(editBoxUsername));
			break;
		}

		case "Password": {
			logReporter.log("check element >Password >", objMobileActions.checkElementDisplayed(editBoxPassword));
			break;
		}

		}

	}

	public void clickOnTitle(String strTitle) {
		By locator = By.xpath("//*[@type='button']/span[text()='" + strTitle + "']");
		logReporter.log("click title >" + strTitle + " >", objMobileActions.click(locator));
	}

	public void verifyTitleIsSelected(String strTitle) {
		By locator = By.xpath("//button[contains(@class,'pink')]/span[text()='" + strTitle + "']");
		logReporter.log("title selected >" + strTitle + ">", objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyButtonIsSelected(String strBtn) {
		By locator = By.xpath("//button[contains(@class,'pink')]/span[text()='" + strBtn + "']");
		logReporter.log("button selected >" + strBtn + ">", objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyUsernameGreenlineDisplayed() {
		By usernameGreenColor = By.xpath("//div[contains(@class,'passed')]/input[@id='input-username']");
		logReporter.log("Green line displayed for username >> ",
				objMobileActions.checkElementDisplayed(usernameGreenColor));
	}

	public void verifyPasswordGreenlineDisplayed() {
		By passwordGreenColor = By.xpath("//div[contains(@class,'passed')]/input[@id='input-password']");
		logReporter.log("Green line displayed for password >> ",
				objMobileActions.checkElementDisplayed(passwordGreenColor));
	}

	public void verifySeocondPageOfRegistrationDisplayed() {
		By secondPageRegistrationForm = By.xpath("//form[@class='join-now-form']");
		logReporter.log("Page 2 of registration displayed  >> ",
				objMobileActions.checkElementDisplayed(secondPageRegistrationForm));
	}

	public void verifyPasswordGuildelineDisplayed(String strGuideline) {

		By locator = By.xpath("//li[text()='" + strGuideline + "']");
		logReporter.log("Password guideline '" + strGuideline + "' displayed >>",
				objMobileActions.checkElementDisplayed(locator));
	}

	public void clickOnNoButton() {
		By noButton = By.xpath("//button[@name='join-with-membership']");
		objMobileActions.androidScrollToElement(noButton);
		logReporter.log("click no button >", objMobileActions.click(noButton));
	}

	public void verifyMembershipSignUpScreen() {

		By DOB = By.xpath("//div[contains(@class,'field-row ip-dob')]");
		By chkboxAgree = By.xpath("//input[@id='id-agreed']");
		logReporter.log("edit box membership number displayed >> ",
				objMobileActions.checkElementDisplayed(membershipNumber));
		logReporter.log("DOB displayed >> ", objMobileActions.checkElementDisplayed(DOB));
		logReporter.log("Agree chkbox displayed >> ", objMobileActions.checkElementDisplayed(chkboxAgree));
		logReporter.log("Next button displayed >> ", objMobileActions.checkElementDisplayed(btnNext));
	}

	public void enterMembershipNo(String number) {
		logReporter.log("enter menbership number", objMobileActions.setText(membershipNumber, number));
	}

	public void greenLineDisplayedForMembershipNo() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(membershipNumber, "border-bottom-color", greencolor));
	}

	public void verifyNextButtonEnabled() throws InterruptedException {
		System.out.println("Value:" + objMobileActions.getAttribute(btnNext, "disabled"));
		if (objMobileActions.getAttribute(btnNext, "disabled") == null)
			logReporter.log("Next button enabled > >", true);
		else
			logReporter.log("Next button enabled > >", false);
	}

	public void clickAgreeCheck() {
		By locator = By.xpath("//input[contains(@id,'agreed')]");
		objMobileActions.androidScrollToElement(locator);
		objMobileActions.click(locator);
		// logReporter.log("Click agree", objMobileActions.clickByTap(locator));
	}

	public void verifyErrorFromParaTag(String strErrorMsg) {
		By locator = By.xpath("//p[contains(text(),'" + strErrorMsg + "')]");
		logReporter.log("Err msg " + strErrorMsg + " displayed >",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void enterRegistrationMobileNumber(String strMobileNo) {
		By locator = By.xpath("//input[@id='input-mobile']");
		logReporter.log("enter mobile no >" + strMobileNo + ">", objMobileActions.setText(locator, strMobileNo));
		objMobileActions.sendKeys(locator, Keys.TAB);
	}
	
	public void enterRandomRegistrationMobileNumber() {
		String randomText = "0" + RandomStringUtils.random(10, false, true);
		logReporter.log("enter random mobile no >" + randomText + ">", objMobileActions.setText(mobileNumber, randomText));
	}

	public void selectCountry(String strCountryName) {
		By locator = By.xpath("//option[text()='"+strCountryName+"']");
		logReporter.log("select country >" + strCountryName + ">",
				objMobileActions.click(locator));
	}

	public void enterPostalCode(String strPostalCode) {
		By locator = By.xpath("//input[@placeholder='Postcode']");
		logReporter.log("clear postal code >" + strPostalCode + ">", objMobileActions.setText(locator, ""));
		logReporter.log("enter postal code >" + strPostalCode + ">", objMobileActions.setText(locator, strPostalCode));
		if (strPostalCode.equals("") != true && strPostalCode.length() <= 8) {
			int randomSubscript = Integer.parseInt(RandomStringUtils.random(1, false, true));
			By searchDropDown;
			if (randomSubscript <= 7 && randomSubscript != 0)
				searchDropDown = By.xpath("//ul[contains(@class,'postcode')]/li[" + randomSubscript + "]");
			else
				searchDropDown = By.xpath("//ul[contains(@class,'postcode')]/li[1]");

			// if (objMobileActions.checkElementDisplayedWithMidWait(searchDropDown))
			if (objMobileActions.checkElementExists(searchDropDown))
				objMobileActions.click(searchDropDown);
		}
	}
	
	public void enterRandomPostalCode() {
		By locator = By.xpath("//input[@placeholder='Postcode']");
		//Generate random postal code
		int count=1;
		//for(count=0;count<5;count++) {
			String str1=RandomStringUtils.random(2, true, false);
			str1=str1.toUpperCase();
			String str2=RandomStringUtils.random(2, false, true);
			String str3=RandomStringUtils.random(2, true, false);
			str3=str3.toUpperCase();
			String postalCode = str1+str2+str3;
			objMobileActions.setText(locator, "");
			objMobileActions.setText(locator, postalCode);
			///By searchDropDown = By.xpath("//ul[contains(@class,'postcode')]/li["+count+"]");
			//if (objMobileActions.checkElementDisplayedWithMidWait(searchDropDown)) {
			//	objMobileActions.click(searchDropDown);
				//break;
			//}
		//}
	}
	
	public void enterRandomEmailIdWithGivenSuffix(String suffixName) {
		// to form random email text
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("MMddhhmm");
		String datetime = ft.format(dNow);
		String strEmail = "k" + datetime + suffixName;
		logReporter.log("enter 'Random Email Id' > >" + strEmail, objMobileActions.setText(editBoxEmail, strEmail));
	}
	
	public void verifySpanDisplayed(String strName ) {
		By locator = By.xpath("//span[text()='" + strName + "']");
		logReporter.log("span '" + strName + "' displayed >>",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void clickSpan(String strName) {
		By locator = By.xpath("//span[text()='" + strName + "']");
		if(objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click span '" + strName,
					objMobileActions.clickUsingJS(locator));
	}
	
	public void verifyDivDisplayed(String strName) {
		By locator = By.xpath("//div[contains(@class,'"+strName+"')]");
		logReporter.log("Div '" + strName + "' displayed >>",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void enterStaticPostalCode(String strPostalCode) {
		By locator = By.xpath("//input[@placeholder='Postcode']");
		objMobileActions.invokeActionOnMobileLocator(locator, "clearText");
		logReporter.log("enter postal code >" + strPostalCode + ">", objMobileActions.setText(locator, strPostalCode));
		if(strPostalCode.equals("")!=true && strPostalCode.length() <=8  ) {
			//int randomSubscript = Integer.parseInt(RandomStringUtils.random(1, false, true));
			By searchDropDown = By.xpath("//ul[contains(@class,'postcode')]/li[1]");
			//By searchDropDown = By.xpath("//ul[contains(@class,'postcode')]/li["+randomSubscript+"]");
			if (objMobileActions.checkElementDisplayedWithMidWait(searchDropDown))
				objMobileActions.click(searchDropDown);
		}
	}
	
	public void verifySpanNotDisplayed(String strName ) {
		By locator = By.xpath("//span[text()='" + strName + "']");
		if(objMobileActions.checkElementExists(locator))
			logReporter.log("span not displayed'" + strName + "' displayed >>",
					false);
		else
			logReporter.log("span not displayed'" + strName + "' displayed >>",
					true);
	}
	
	public void verifyH4HeadingDisplayed(String strText) {
		By locator = By.xpath("//h4[text()='" + strText + "']");
		if (objMobileActions.checkElementExists(locator))
			logReporter.log("text displayed'" + strText + "' displayed >>", true);
	}

	public void verifyHelpTextDisplayed() {
		By locator = By.xpath("//p[contains(text(),'HELP')]");
		logReporter.log("Help text displayed >>", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyAgreeCheckboxUnchecked() {
		By chkboxAgree = By.xpath("//label[@for='id-agreed']/ancestor::div/input");
		if (objMobileActions.getAttribute(chkboxAgree, "value").equals("false"))
			logReporter.log("check agree checkbox", true, true);
		else
			logReporter.log("check agree checkbox", true, false);
	}
}
