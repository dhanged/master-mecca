package com.meccabingo.iOS.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.iOS.page.MobileOthersPage;

import io.cucumber.java.en.Then;

public class MobileOthersPageStep {

	private MobileOthersPage objMobileOthersPage;
	Utilities utilities;

	public MobileOthersPageStep(MobileOthersPage mobileOthersPage) {
		this.objMobileOthersPage = mobileOthersPage;
	}

	@Then("^Verify cookie banner displayed$")
	public void verify_cookie_banner_displayed() {
		objMobileOthersPage.verifyCookieBannerDisplayed();
	}

	@Then("^Verify Continue button displayed$")
	public void verify_Continue_button_displayed() {
		objMobileOthersPage.verifyContinueButtonDisplayed();
	}
	
}
