package com.meccabingo.iOS.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.iOS.page.MobileDepositPage;

import io.cucumber.java.en.Then;

public class MobileDepositPageStep {

	private MobileDepositPage objMobileDepositPage;
	Utilities utilities;

	public MobileDepositPageStep(MobileDepositPage mobileDepositPage) {

		this.objMobileDepositPage = mobileDepositPage;
	}

	@Then("^Click on Deposit button besides myaccount$")
	public void click_on_Deposit_button_besides_myaccount() {
		objMobileDepositPage.clickOnDepositBesidesMyAcct();
	}

	@Then("^Click show more$")
	public void click_show_more() {
		objMobileDepositPage.clickShowMore();
	}

	@Then("^Click payment method as \"([^\"]*)\"$")
	public void click_payment_method_as(String strPayMethod) {
		objMobileDepositPage.selectPaymentMethod(strPayMethod);
	}

	@Then("^Enter Card number \"([^\"]*)\"$")
	public void enter_Card_number(String strCNo) {
		objMobileDepositPage.enterCardNumber(strCNo);
	}

	@Then("^Enter Expiry \"([^\"]*)\"$")
	public void enter_Expiry(String strExpiry) {
		objMobileDepositPage.enterExpiry(strExpiry);
	}

	@Then("^Enter CVV \"([^\"]*)\"$")
	public void enter_CVV(String strCVV) {
		objMobileDepositPage.enterCVV(strCVV);
	}

	@Then("^Enter amount to deposit \"([^\"]*)\"$")
	public void enter_amount_to_deposit(String strAmt) {
		objMobileDepositPage.enterDepositAmt(strAmt);
	}

	@Then("^Click on Deposit button from Deposit page$")
	public void click_on_Deposit_button_from_Deposit_page() {
		objMobileDepositPage.clickDepositButton();
	}
	
	@Then("^Verify deposit is successful$")
	public void verify_deposit_is_successful() {
		objMobileDepositPage.verifyDepositSuccessful();
	}

	@Then("^Click close button from deposit successful popup$")
	public void click_close_button_from_deposit_successful_popup() {
		objMobileDepositPage.clickCloseFromDepositSuccessfulPopup();
	}
	
	@Then("^Click withdraw$")
	public void click_withdraw() {
		objMobileDepositPage.clickWithdraw();
	}
	
	@Then("^Verify withdraw is successful$")
	public void verify_withdraw_is_successful() {
		objMobileDepositPage.verifyWithdrawSuccessful();
	}

	@Then("^Click saved card \"([^\"]*)\"$")
	public void click_saved_card(String arg1) {
		objMobileDepositPage.selectSavedCard(arg1);
	}
	
	@Then("^Click PayPal$")
	public void click_PayPal() {
		objMobileDepositPage.clickPayPal();
	}
	
	@Then("^Click PaySafe$")
	public void click_PaySafe() {
		objMobileDepositPage.clickPaySafe();
	}
	
	@Then("^Navigate link of paysafe$")
	public void navigate_link_of_paysafe() {
		objMobileDepositPage.navigateToPaySafeURL();
	}
}
