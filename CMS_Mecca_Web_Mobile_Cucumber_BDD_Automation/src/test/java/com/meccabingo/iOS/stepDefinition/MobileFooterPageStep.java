/**
 * 
 */
package com.meccabingo.iOS.stepDefinition;

import com.generic.StepBase;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.utils.Utilities;
import com.meccabingo.iOS.page.MobileFooterPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class MobileFooterPageStep {

	private Utilities objUtilities;
	private StepBase objStepBase;
	private MobileFooterPage objMobileFooterPage;

	public MobileFooterPageStep(Utilities utilities, StepBase stepBase, MobileFooterPage mobileFooterPage,
			AppiumDriverProvider appiumDriverProvider) {

		this.objUtilities = utilities;
		this.objStepBase = stepBase;
		this.objMobileFooterPage = mobileFooterPage;
	}

	@Then("Click on allow popup")
	public void clickOnAllowPopup() throws InterruptedException {
		objMobileFooterPage.clickOnAllowPopup();
		Thread.sleep(1000);
	}

	@Then("Navigate back to previous page")
	public void Navigate_back_to_previous_page() {
		objMobileFooterPage.navigateBackToPreviousPage();
	}

	@Then("^Click on Facebook block from footer on Mobile$")
	public void click_on_Facebook_block_from_footer_on_Mobile() {
		objMobileFooterPage.clickOnFacebook();
	}

	@Then("^Switch to child window on mobile$")
	public void switch_to_child_window_on_mobile() throws InterruptedException {
		objMobileFooterPage.switchToChild();
	}

	@Then("^Switch to parent window on mobile$")
	public void switch_to_parent_window_on_mobile() throws InterruptedException {
		objMobileFooterPage.switchToParent();
	}

	@Then("^Verify device navigate user to \"([^\"]*)\" url$")
	public void verify_device_navigate_user_to_url(String url) throws InterruptedException {
		objMobileFooterPage.verifyUrl(url);
	}

	@Then("^Click on Instagram block from footer on Mobile$")
	public void click_on_Instagram_block_from_footer_on_Mobile() {
		objMobileFooterPage.clickOnInstagram();
	}

	@Then("^Click on Twitter block from footer on Mobile$")
	public void click_on_Twitter_block_from_footer_on_Mobile() {
		objMobileFooterPage.clickOnTwitter();
	}

	@Then("^Click on Youtube block from footer on Mobile$")
	public void click_on_Youtube_block_from_footer_on_Mobile() {
		objMobileFooterPage.clickOnYoutube();
	}

	@Then("^Click on Privacy Policy Link on Mobile$")
	public void click_on_Privacy_Policy_Link_on_Mobile() {
		objMobileFooterPage.clickOnPrivacyPolicyLink();
	}

	@Then("^Click on Terms and Conditions Link on Mobile$")
	public void click_on_Terms_and_Conditions_Link_on_Mobile() {
		objMobileFooterPage.clickOnTermsAndConditionsLink();
	}

	@Then("^Click on Our Mecca promise Link on Mobile$")
	public void click_on_Our_Mecca_promise_Link_on_Mobile() {
		objMobileFooterPage.clickOnOurMeccaPromiseLink();
	}

	@Then("^Click on Affiliates Link on Mobile$")
	public void click_on_Affiliates_Link_on_Mobile() {
		objMobileFooterPage.clickOnAffiliatesLink();
	}

	@Then("^Click on Mecca Club Terms Link on Mobile$")
	public void click_on_Mecca_Club_Terms_Link_on_Mobile() {
		objMobileFooterPage.clickOnMeccaClubTermsLink();
	}

	@Then("^Click on Play Online Casino Link on Mobile$")
	public void click_on_Play_Online_Casino_Link_on_Mobile() {
		objMobileFooterPage.clickOnPlayOnlineCasinoLink();
	}

	@Then("^Click on Mecca Blog Link on Mobile$")
	public void click_on_Mecca_Blog_Link_on_Mobile() {
		objMobileFooterPage.clickOnMeccaBlogLink();
	}

	@Then("^Verify user able to view following logos at Footer in partners block on mobile:$")
	public void verify_user_able_to_view_following_logos_at_Footer_in_partners_block_on_mobile(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objMobileFooterPage.verifyPartnersLogoDetails(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^User clicks on \"([^\"]*)\" logo on Mobile$")
	public void user_clicks_on_logo_on_Mobile(String option) {
		objMobileFooterPage.clickOnPartnersLogo(option);
	}

	@Then("^Verify user should be able to view Verisign Secured logo at Footer section on Mobile$")
	public void verify_user_should_be_able_to_view_Verisign_Secured_logo_at_Footer_section_on_Mobile() {
		objMobileFooterPage.verifyVeriSignLogo();
	}

	@Then("^Verify device displays following section for payment providers components:$")
	public void verify_device_displays_following_section_for_payment_providers_components(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objMobileFooterPage.verifyPaymentProvidersBlockInFooter(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^User clicks on Alderney Gambling Control Commission link on Mobile$")
	public void user_clicks_on_Alderney_Gambling_Control_Commission_link_on_Mobile() {
		objMobileFooterPage.click_Alderney_Gambling_Control_Commission_link();

	}

	@Then("^User clicks on UK Gambling Commission link on Mobile$")
	public void user_clicks_on_UK_Gambling_Commission_link_on_Mobile() {
		objMobileFooterPage.click_UK_Gambling_Commission_link();
	}

	@Then("^User click on BeGambleAware link on Mobile$")
	public void user_click_on_BeGambleAware_link_on_Mobile() {
		objMobileFooterPage.click_BeGambleAware_link();
	}

	@Then("^User clicks on Rank Group link on Mobile$")
	public void user_clicks_on_Rank_Group_link_on_Mobile() {
		objMobileFooterPage.click_Rank_Group_link();
	}
	
	@Then("^Close current browser window$")
	public void close_current_browser_window() {
		objMobileFooterPage.closeCurrentBrowser();
	}
}
