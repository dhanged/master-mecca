
package com.meccabingo.iOS.stepDefinition;

import com.meccabingo.iOS.page.MobileLogInPage;

import io.cucumber.java.en.Then;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class MobileLogInPageStep {

	private MobileLogInPage objMobileLogInPage;

	public MobileLogInPageStep(MobileLogInPage mobileLogInPage) {

		this.objMobileLogInPage = mobileLogInPage;
	}

	@Then("^Verify Login Page displayed on mobile$")
	public void verify_Login_Page_displayed_on_mobile() {
		objMobileLogInPage.verifyLoginPage();
	}

	@Then("^Verify device displays login Title on mobile$")
	public void verify_system_displays_login_Title_on_mobile() {
		objMobileLogInPage.verifyLoginHeaderTitle();
	}

	@Then("^Verify close 'X' icon on mobile$")
	public void verify_close_X_icon_on_mobile() {
		objMobileLogInPage.verifyCloseXicon();
	}

	@Then("^Click close 'X' icon on mobile$")
	public void Click_close_X_icon_on_mobile() {
		objMobileLogInPage.clickOnCloseXicon();
	}

	@Then("^Verify 'Username' field on mobile$")
	public void verify_Username_field_on_mobile() {
		objMobileLogInPage.verifyTBUserNameDisplayed();
	}

	@Then("^Verify 'Password' field on mobile$")
	public void verify_Password_field_on_mobile() {
		objMobileLogInPage.verifyTBPasswordDisplayed();
	}

	@Then("^Verify Toggle within password field on mobile$")
	public void verify_Toggle_within_password_field_on_mobile() {
		objMobileLogInPage.verifyPasswordToggle();
	}

	@Then("^Verify Remember me checkbox on mobile$")
	public void verify_Remember_me_checkbox_on_mobile() {
		objMobileLogInPage.verifyRememberMeCheckbox();
	}

	@Then("^Verify 'Log In' CTA on mobile$")
	public void verify_Log_In_CTA_on_mobile() {
		objMobileLogInPage.verifyLoginCTA();
	}

	@Then("^Verify 'Forgot username' link on mobile$")
	public void verify_Forgot_username_link_on_mobile() {
		objMobileLogInPage.veryForgotUserNameLink();
	}

	@Then("^Verify 'Forgot password' link on mobile$")
	public void verify_Forgot_password_link_on_mobile() {
		objMobileLogInPage.veryForgotPasswordLink();
	}

	@Then("^Verify 'New to Mecca Bingo\\?' text on mobile$")
	public void verify_New_to_Mecca_Bingo_text_on_mobile() {
		objMobileLogInPage.verifyNewToMeccaText();
	}

	@Then("^Verify 'Sign up' link on mobile$")
	public void verify_Sign_up_link_on_mobile() {
		objMobileLogInPage.verifySignUpLinkDisplayed();
	}

	@Then("^Verify Help contact information on mobile$")
	public void verify_Help_contact_information_on_mobile() {
		objMobileLogInPage.verifyHelpContactDetails();
	}

	@Then("^Verify 'Live Chat' link on mobile$")
	public void verify_Live_Chat_link_on_mobile() {
		objMobileLogInPage.verifyLiveChat();
	}

	@Then("^User enters username \"([^\"]*)\" on mobile$")
	public void user_enters_username_on_mobile(String userName) {
		objMobileLogInPage.enterUserName(userName);
	}

	@Then("^User enters password \"([^\"]*)\" on mobile$")
	public void user_enters_password_on_mobile(String password) {
		objMobileLogInPage.enterPassword(password);
	}

	@Then("^Verify Login button gets enabled on mobile$")
	public void verify_Login_button_gets_enabled_on_mobile() {
		objMobileLogInPage.verifyLogInEnabled();
	}

	@Then("^User clicks on login button on mobile$")
	public void user_clicks_on_login_button_on_mobile() {
		objMobileLogInPage.clickLogin();
	}

	@Then("^Verify user is successfully logged in on mobile$")
	public void verify_user_is_successfully_logged_in_on_mobile() {
		//objMobileLogInPage.clickOnRememberMe();
	}

	@Then("^User clicks on 'Remember Me' tick on mobile$")
	public void user_clicks_on_Remember_Me_tick_on_mobile() {
		objMobileLogInPage.clickOnRememberMe();
	}

	@Then("^Verify Username auto pupulated with \"([^\"]*)\" on mobile$")
	public void verify_Username_auto_pupulated_with_on_mobile(String userName) {
		objMobileLogInPage.VerifyTextFromUserName(userName);
	}

	@Then("^User clicks on forgot password link on mobile$")
	public void user_clicks_on_forgot_password_link_on_mobile() {
		objMobileLogInPage.clickOnForgotPasswordLink();
	}

	@Then("^Verify system displays Forgotten your password page with header as 'Forgotten your password' on mobile$")
	public void verify_system_displays_Forgotten_your_password_page_with_header_as_Forgotten_your_password_on_mobile() {
		objMobileLogInPage.verifyForgottenPasswordHeader();
	}

	@Then("^Verify back arrow '<' on mobile$")
	public void verify_back_arrow_on_mobile() {
		objMobileLogInPage.verifyBackArrow();
	}

	@Then("^Verify text \"([^\"]*)\" on mobile$")
	public void verify_text_on_mobile(String text) {
		// objMobileLogInPage.verifyHelperTextFromForgottenPasswordPage(text);
	}

	@Then("^Verify 'Send reset instructions' CTA on mobile$")
	public void verify_Send_reset_instructions_CTA_on_mobile() {
		objMobileLogInPage.verifySendResetInstructionsCTA();
	}

	@Then("^User clicks on 'Send reset instructions' on mobile$")
	public void user_clicks_on_Send_reset_instructions_on_mobile() {
		objMobileLogInPage.clickOnSendResetInstructions();
	}

	@Then("^Verify Success Message displayed to user on mobile$")
	public void verify_Success_Message_displayed_to_user_on_mobile() {
		objMobileLogInPage.verifyPasswordResetSuccessMessage();
	}

	@Then("^Verify 'I didnot receive an email' link on mobile$")
	public void verify_I_didnot_receive_an_email_link_on_mobile() {
		objMobileLogInPage.verifyIDidnotReceiveAnEmailLink();
	}

	@Then("^User clicks on forgot usename link on mobile$")
	public void user_clicks_on_forgot_usename_link_on_mobile() {
		objMobileLogInPage.clickOnForgotUsernameLink();
	}

	@Then("^Verify system displays Forgotten your username page with header as 'Forgotten your username' on mobile$")
	public void verify_system_displays_Forgotten_your_username_page_with_header_as_Forgotten_your_username_on_mobile() {
		objMobileLogInPage.verifyForgottenUsernameHeader();
	}

	@Then("^Verify helper text \"([^\"]*)\" on mobile$")
	public void verify_helper_text_on_mobile(String text) {
		// objMobileLogInPage.verifyHelperTextFromForgottenUsernamePage(text);
	}

	@Then("^Verify 'Email' field on mobile$")
	public void verify_Email_field_on_mobile() {
		objMobileLogInPage.verifyEmailFieldFromForgottenUsername();
	}

	@Then("^Verify 'Send usename reminder' CTA on mobile$")
	public void verify_Send_usename_reminder_CTA_on_mobile() {
		objMobileLogInPage.verifySendUsernameReminderCTA();
	}

	@Then("^User enters email \"([^\"]*)\" on mobile$")
	public void user_enters_email_on_mobile(String emailAddress) {
		objMobileLogInPage.enterEmailAddressInForgotUsernameSection(emailAddress);
	}

	@Then("^User clicks on 'Send username reminder' on mobile$")
	public void user_clicks_on_Send_username_reminder_on_mobile() {
		objMobileLogInPage.clickOnSendUsernameReminder();
	}

	@Then("^Click on my account button on mobile$")
	public void click_on_my_account_button_on_mobile() {
		objMobileLogInPage.clickOnMyAccountButtonFromHeader();
	}

	@Then("^Click on logout button on mobile$")
	public void click_on_logout_button_on_mobile() {
		objMobileLogInPage.clickOnLogoutbutton();
	}

	@Then("^Click on 'Sign Up' link$")
	public void Click_on_Sign_Up_link() {
		objMobileLogInPage.clickOnSignUpLink();
	}
}
