package com.meccabingo.iOS.stepDefinition;

import org.openqa.selenium.By;

import com.generic.utils.Utilities;
import com.meccabingo.iOS.page.MobileRegistrationMembershipPage;

import io.cucumber.java.en.Then;

public class MobileRegistrationMembershipPageStep {

	private MobileRegistrationMembershipPage objMobileRegistrationMembershipPage;
	Utilities utilities;

	public MobileRegistrationMembershipPageStep(MobileRegistrationMembershipPage mobileRegistrationMembershipPage) {

		this.objMobileRegistrationMembershipPage = mobileRegistrationMembershipPage;
	}

	@Then("^Click \"([^\"]*)\" tag with text \"([^\"]*)\"$")
	public void click_tag_with_text(String strTag, String strText) {
		objMobileRegistrationMembershipPage.clickTagWithText(strTag,strText);
	}
	
}
