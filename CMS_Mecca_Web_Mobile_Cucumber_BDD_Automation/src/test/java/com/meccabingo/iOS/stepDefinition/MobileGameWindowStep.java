package com.meccabingo.iOS.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.iOS.page.MobileGameWindow;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

public class MobileGameWindowStep {

	private MobileGameWindow mobileGameWindow;
	private Utilities utilities;

	public MobileGameWindowStep(MobileGameWindow mobileGameWindow, Utilities utilities) {
		this.mobileGameWindow = mobileGameWindow;
		this.utilities = utilities;
	}

	@Then("^Navigate to Auto tab of Slot games$")
	public void navigate_to_Auto_tab_of_Slot_games() {
		mobileGameWindow.navigateToAutoTabOfSlotGame();
	}

	@Then("^Navigate to \"([^\"]*)\" tab of Slot games$")
	public void Navigate_to_tab_of_Slot_games(String strTabName) {
		mobileGameWindow.navigateToMentionedTabOfSlotGame(strTabName);
	}

	@Then("^Click on Play Now button from Game$")
	public void click_on_Play_Now_button_from_Game() {
		mobileGameWindow.clickOnPlayNow();
	}

	@Then("^Click on Play Now button from Game tile \"([^\"]*)\"$")
	public void click_on_Play_Now_button_from_Game_tile(String strGameDesc) {
		mobileGameWindow.clickOnPlayNowFromGameTile(strGameDesc);
	}

	@Then("^Click on Play Now button from Game details$")
	public void click_on_Play_Now_button_from_Game_details() {
		mobileGameWindow.clickOnPlayNowFromGameDetails();
	}

	@Then("^User clicks on i button of game$")
	public void user_clicks_on_i_button_of_game() {
		mobileGameWindow.clickOnibtn();
	}

	@Then("^Click on Free play button$")
	public void click_on_Free_play_button() {
		mobileGameWindow.clickOnFreePlayBtn();
	}

	@Then("^Verify system loads game window for selected game in real mode$")
	public void verify_system_loads_game_window_for_selected_game_in_real_mode() {
		mobileGameWindow.verifyBackroungimageInGame();
	}

	@Then("^Verify Game window bottom bar displays “Exit game“ arrow button$")
	public void verify_Game_window_bottom_bar_displays_Exit_game_arrow_button() {
		mobileGameWindow.verifyExitArrowBtn();
	}

	@Then("^Verify Game window bottom bar displays Session time$")
	public void verify_Game_window_bottom_bar_displays_Session_time() {
		mobileGameWindow.verifySessionTime();
	}

	@Then("^Verify Game window bottom bar displays Play for Real CTA button$")
	public void verify_Game_window_bottom_bar_displays_Play_for_Real_CTA_button() {
		mobileGameWindow.verifyPlayforRealBtn();
	}

	@Then("^Verify Game window bottom bar displays Deposit CTA button$")
	public void verify_Game_window_bottom_bar_displays_Deposit_CTA_button() {
		mobileGameWindow.verifyDepositBtn();
	}

	@Then("^Verify Game window bottom bar displays My account CTA button$")
	public void verify_Game_window_bottom_bar_displays_My_account_CTA_button() {
		mobileGameWindow.verifyMyaccountBtn();
	}

	@Then("^Verify Game window top bar displays session time in mm:ss format$")
	public void verify_Game_window_top_bar_displays_session_time_in_mm_ss_format() {
		mobileGameWindow.verifySessionTimeInmmssformat();
	}

	@Then("^Verify system do not display session time for Game window$")
	public void verify_system_do_not_display_session_time_for_Game_window() {
		mobileGameWindow.verifyNonSessionTime();
	}

	@Then("^Verify Game window header displays following components :$")
	public void verify_Game_window_header_displays_following_components(DataTable dt) {
		for (int i = 0; i < utilities.getListDataFromDataTable(dt).size(); i++) {
			mobileGameWindow.verifyDemoheadercomponents(utilities.getListDataFromDataTable(dt).get(i));

		}
	}

	@Then("^Click on Play for real CTA button$")
	public void click_on_Play_for_real_CTA_button() {
		mobileGameWindow.clickOnPlayforRealBtn();
	}

	@Then("^Verify game launches successfully$")
	public void verify_game_launches_successfully() {
		mobileGameWindow.verifyBackroungimageInGame();
	}

	@Then("^Verify system do not display expand CTA in Game window$")
	public void verify_system_do_not_display_expand_CTA_in_Game_window() {
		mobileGameWindow.verifyNonExpandbutton();
	}

	@Then("^Verify background image has the opacity layer applied in game details page$")
	public void Verify_background_image_has_the_opacity_layer_applied_in_game_details_page() {
		mobileGameWindow.verifyOpacityLayer();
	}

	
	@Then("^Click on my account of game$")
	public void click_on_my_account_of_game() {
		mobileGameWindow.clickMyAccountOfGame();
	}
	
	@Then("^User clicks on i button of game tile$")
    public void user_clicks_on_i_button_of_game_tile() {
          mobileGameWindow.clickOnibtnOfGameTile();
    }
	
	@Then("^Click on Join Now button of first game of bingo section$")
	public void click_on_Join_Now_button_of_first_game_of_bingo_section() {
		mobileGameWindow.clickJoinNowOfFirstBingoGame();
	}
	
	@Then("^Click on info button of first game of slot section$")
	public void click_on_info_button_of_first_game_of_slot_section() {
		mobileGameWindow.clickInfoOfFirstSlotGame();
	}
	
	@Then("^Click on info button of first game of bingo section$")
	public void click_on_info_button_of_first_game_of_bingo_section() {
		mobileGameWindow.clickInfoOfFirstBingoGame();
	}
	
	@Then("^Click on join now button from bingo details$")
	public void click_on_join_now_button_from_bingo_details() {
		mobileGameWindow.clickJoinNowFromGameDetails();
	}
	
	@Then("Click on Exit Game CTA")
	public void Click_on_Exit_Game_CTA() {
		mobileGameWindow.clickExitGameCTA();
	}
	
	@Then("Click on my account from game")
	public void Click_on_my_account_from_game() {
		mobileGameWindow.clickMyAccFromGame();
	}

}
