package com.meccabingo.iOS.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.iOS.page.MobilePaySafePage;

import io.cucumber.java.en.Then;

public class MobilePaySafePageStep {

	private MobilePaySafePage objMobilePaySafePage;
	Utilities utilities;

	public MobilePaySafePageStep(MobilePaySafePage mobilePaySafePage) {

		this.objMobilePaySafePage = mobilePaySafePage;
	}

	@Then("^Enter paysafe account \"([^\"]*)\"$")
	public void enter_paysafe_account(String arg1) {
		objMobilePaySafePage.enterPaySafeAccount(arg1);
	}

	@Then("^Click paysafe agree$")
	public void click_paysafe_agree() {
		objMobilePaySafePage.clickPaySageAgree();
	}

	@Then("^Click paysafe pay$")
	public void click_paysafe_pay() {
		
	}
}
