package com.meccabingo.iOS.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.android.page.MobileDetailsPage;

import io.cucumber.java.en.Then;

public class MobileDetailsPageStep {

	private MobileDetailsPage objMobileDetailsPage;
	Utilities utilities;

	public MobileDetailsPageStep(MobileDetailsPage mobileDetailsPage) {
		this.objMobileDetailsPage = mobileDetailsPage;
	}

	@Then("^Navigate through hamburger to \"([^\"]*)\" menu$")
	public void Click_on_back_to_top_button(String strMenuName) {
		objMobileDetailsPage.navigateThroughHamburgerMenu(strMenuName);
	}
}
