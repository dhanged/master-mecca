package com.meccabingo.iOS.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.iOS.page.MobileBlocksPage;

import io.cucumber.java.en.Then;

public class MobileBlocksPageStep {

	private MobileBlocksPage objMobileBlocksPage;
	Utilities utilities;

	public MobileBlocksPageStep(MobileBlocksPage mobileBlocksPage) {

		this.objMobileBlocksPage = mobileBlocksPage;
	}

	@Then("^Click on back to top button$")
	public void Click_on_back_to_top_button() {
		objMobileBlocksPage.clickOnBackToTopButton();
	}
	
	@Then("^Verify winners feed available with multiple boxes$")
	public void Verify_winners_feed_available_with_multiple_boxes() {
		objMobileBlocksPage.verifyMultipleWinnersFeedAvailableWithMultipleBoxes();
	}

	@Then("^Verify back on top CTA is displayed$")
	public void Verify_back_on_top_CTA_is_displayed() {
		objMobileBlocksPage.verifyBackOnTopCTADisplayed();
	}

	@Then("scroll page till footer of the page")
	public void scroll_page_till_footer_of_the_page() {
		objMobileBlocksPage.scrollPageTillFooter();
	}
}
