package com.meccabingo.iOS.stepDefinition;

import org.openqa.selenium.By;

import com.generic.utils.Utilities;
import com.meccabingo.iOS.page.ClubsPage;

import io.cucumber.java.en.Then;

public class ClubsPageStep {

	private ClubsPage objClubsPage;
	Utilities utilities;

	public ClubsPageStep(ClubsPage clubPage) {

		this.objClubsPage = clubPage;
	}

	@Then("Search club {string}")
	public void search_club(String string) {
		objClubsPage.searchClub(string);
	}
	
	@Then("Click on more Info")
	public void click_on_more_info() {
		objClubsPage.clickMoreInfo();
	}
	
	@Then("Select first club that appears in list")
	public void select_first_club_that_appears_in_list() {
		objClubsPage.selectFirstClubFromList();
	}
	
	@Then("Mark it as favorite")
	public void mark_it_as_favorite() {
		objClubsPage.clickFavorite();
	}
	
	@Then("Unfavorite it")
	public void unfavorite_it() {
		objClubsPage.clickFavorite();
	}
	
	@Then("Click link having text as {string}")
	public void click_link_having_text_as(String string) {
		objClubsPage.clickLinkText(string);
	}
	
	@Then("Verify club marked as favorite")
	public void verify_club_marked_as_favorite() {
		objClubsPage.verifyMarkedAsFavorite();
	}
	
	@Then("Verify club favorite tag is removed")
	public void verify_club_favorite_tag_is_removed() {
		objClubsPage.verifyNotMarkedAsFavorite();
	}
	
	@Then("Enter Home phone {string}")
	public void enter_home_phone(String string) {
		objClubsPage.enterHomePhone(string);
	}
	
	@Then("Enter join club email {string}")
	public void enter_join_club_email(String string){
		objClubsPage.enterJoinClubEmail(string);
	}
	
	@Then("Verify user is joined club successfully")
	public void verify_user_joined_club_successfully( ) {
		objClubsPage.userJoinedClub();
	}
}
