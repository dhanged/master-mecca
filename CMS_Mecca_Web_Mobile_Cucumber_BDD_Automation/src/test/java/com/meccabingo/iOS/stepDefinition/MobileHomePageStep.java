
package com.meccabingo.iOS.stepDefinition;

import org.openqa.selenium.ScreenOrientation;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.meccabingo.iOS.page.MobileHomePage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class MobileHomePageStep {

	private MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;

	private MobileHomePage objMobileHomePage;

	// Local variables
	public MobileHomePageStep(MobileActions mobileActions, AppiumDriverProvider appiumDriverProvider,
			MobileHomePage mobileHomePage) {

		this.objMobileActions = mobileActions;
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileHomePage = mobileHomePage;
	}

	@Given("^Invoke the mecca site on Mobile in Portrait Mode$")
	public void invoke_the_mecca_site_on_Mobile_in_Portrait_Mode() {
		objAppiumDriverProvider.getAppiumDriver().rotate(ScreenOrientation.PORTRAIT);
		System.out.println("Switched to Portrait mode . . . . ");
		// objMobileActions.pageRefresh();
		objMobileHomePage.launchAppURL();
		objMobileHomePage.verifyHeaderLogo();
		objMobileHomePage.clickLetsPlay();
		objMobileHomePage.clickCookieContinueButton();

		//objMobileHomePage.checkSurveyAndCloseItForLandScape();
	}

	@Given("^Invoke the mecca site on Mobile in LandScape Mode$")
	public void invoke_the_mecca_site_on_Mobile_in_LandScape_Mode() {
		objAppiumDriverProvider.getAppiumDriver().rotate(ScreenOrientation.LANDSCAPE);
		System.out.println("Switched to LandScape mode . . . . ");
		// objMobileActions.pageRefresh();
		objMobileHomePage.verifyDeviceIsLandscape();
		objMobileHomePage.launchAppURL();
		objMobileHomePage.verifyHeaderLogo();
		//objMobileHomePage.checkSurveyAndCloseItForLandScape();
		objMobileHomePage.clickCookieContinueButton();
	}

	@When("^Navigate to Social media Block in footer on Mobile$")
	public void navigate_to_Social_media_Block_in_footer_on_Mobile() {
		objMobileHomePage.scrollToSocialMediaBlock();
	}

	@When("^Navigate to useful link block in footer on Mobile$")
	public void navigate_to_useful_link_block_in_footer_on_Mobile() {
		objMobileHomePage.scrollToUsefulLinkBlock();
	}

	@Then("^Navigate back to window on Mobile$")
	public void navigate_back_to_window_on_Mobile() {
		objAppiumDriverProvider.getAppiumDriver().navigate().back();
		objMobileHomePage.verifyHeaderLogo();
	}

	@When("^Navigate to logo section in footer on Mobile$")
	public void navigate_to_logo_section_in_footer_on_Mobile() {
		objMobileHomePage.scrollToLogoSection();
	}

	@Then("^Navigate to privacy and security block in footer on Mobile$")
	public void navigate_to_privacy_and_security_block_in_footer_on_Mobile() {
		objMobileHomePage.scrollToprivacyAndSecurityBlock();
	}

	@When("^Navigate to secure payments block in footer on Mobile$")
	public void navigate_to_secure_payments_block_in_footer_on_Mobile() {
		objMobileHomePage.scrollToPaymentProvidersBlock();
	}

	@When("^Navigate to rank group block in footer on Mobile$")
	public void navigate_to_rank_group_block_in_footer_on_Mobile() {
		this.navigate_to_privacy_and_security_block_in_footer_on_Mobile();
	}

	@When("^User clicks on Login Button from header of the Page on mobile$")
	public void user_clicks_on_Login_Button_from_header_of_the_Page_on_mobile() {
		objMobileHomePage.clickOnLogInButton();
	}

	@Then("^Click on hamburger menu$")
	public void click_on_hamburger_menu() {
		objMobileHomePage.clickOnHamburgerMenu();
	}

	@Then("^Verify search get closed$")
	public void verify_search_get_closed() {
		objMobileHomePage.verifyClosedSearchMenu();
	}

	@Then("^Verify menu gets open$")
	public void verify_menu_gets_open() {
		objMobileHomePage.verifyClosedHamburgerMenu();
	}

	@Then("^Click on search icon$")
	public void click_on_search_icon() {
		objMobileHomePage.clickOnSearchIcon();
	}

	@Then("^Verify search gets open$")
	public void verify_search_gets_open() {
		objMobileHomePage.verifySearchMenu();
	}

	@Then("^Verify menu gets closed$")
	public void verify_menu_gets_closed() {
		objMobileHomePage.verifyClosedHamburgerMenu();
	}

	@Then("^Verify header displays join now button$")
	public void verify_header_displays_join_now_button() {
		objMobileHomePage.verifyHeaderJoinNowbtn();
	}

	@Then("^Verify header displays Not a member yet\\? Register here link$")
	public void verify_header_displays_Not_a_member_yet_Register_here_link() {
		objMobileHomePage.verifyRegisterNowLink();
	}

	@Then("^Verify blocks contain one row$")
	public void verify_blocks_contain_one_row() {
		objMobileHomePage.verifyBlockInOneRow();
	}

	@Then("^Verify all three blocks$")
	public void verify_all_three_blocks() {
		objMobileHomePage.verifyThreeBlock();
	}

	@Then("^Verify card is displayed in battenberg block$")
	public void verify_card_is_displayed_in_battenberg_block() {
		objMobileHomePage.verifybattenberg();
	}
}
