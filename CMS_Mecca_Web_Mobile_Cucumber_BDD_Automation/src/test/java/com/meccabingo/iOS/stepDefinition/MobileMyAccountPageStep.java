package com.meccabingo.iOS.stepDefinition;

import org.openqa.selenium.By;

import com.generic.StepBase;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.utils.Utilities;
import com.meccabingo.android.page.MobileMyAccountPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

public class MobileMyAccountPageStep {

	private MobileMyAccountPage objMobileMyAccountPage;
	private Utilities objUtilities;
	
	String greycolor = "rgba(179, 179, 179, 1)";
	String greencolor = "rgba(94, 168, 90, 1)";

	public MobileMyAccountPageStep(MobileMyAccountPage mobileMyAccountPage, Utilities utilities, StepBase stepBase,
			AppiumDriverProvider appiumDriverProvider) {

		this.objMobileMyAccountPage = mobileMyAccountPage;
		this.objUtilities = utilities;
	}

	@Then("^Click on my account$")
	public void click_on_my_account() {
		objMobileMyAccountPage.clickOnMyAccount();
	}

	@Then("^Verify Balance block in Collapsed state$")
	public void verify_Balance_block_in_Collapsed_state() {
		objMobileMyAccountPage.verifyPlayableBalanceSectionInCollpasedStateDisplayed();
	}

	@Then("^Click on plus sign$")
	public void click_on_plus_sign() {
		objMobileMyAccountPage.clickOnPlusSign();
	}

	@Then("^Verify Balance block in expanded state$")
	public void verify_Balance_block_in_expanded_state() {
		objMobileMyAccountPage.verifyPlayableBalanceSectionInExpandedStateDisplayed();
	}

	@Then("^Click top section of balance box$")
	public void click_top_section_of_balance_box() {
		objMobileMyAccountPage.clickOnTopSectionOfBalanceBlock();
	}

	@Then("^Click on Deposit button$")
	public void click_on_Deposit_button() {
		objMobileMyAccountPage.clickDeposit();
	}

	@Then("^Click on Detailed view button$")
	public void click_on_Detailed_view_button() {
		objMobileMyAccountPage.clickDetailedView();
	}

	@Then("^Verify user navigate to balance overlay page$")
	public void verify_user_navigate_to_balance_overlay_page() {
		objMobileMyAccountPage.verifyBalanceOverlayPage();
	}

	@Then("^Verify user navigate to deposit overlay page$")
	public void verify_user_navigate_to_deposit_overlay_page() {
		objMobileMyAccountPage.verifyDepositOverlayPage();
	}

	@Then("^Click on cash title$")
	public void click_on_cash_title() {
		objMobileMyAccountPage.clickOnCash();
	}

	@Then("^Click on bonuses amount$")
	public void click_on_bonuses_amount() {
		objMobileMyAccountPage.clickOnBonusAmt();
	}

	@Then("^Username \"([^\"]*)\" as header displayed$")
	public void username_as_header_displayed(String uname) {
		objMobileMyAccountPage.usernameDisplayed(uname);
	}

	@Then("^verify following my account menu options:$")
	public void verify_following_my_account_menu_options(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objMobileMyAccountPage.verifyMenuOptions(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^verify logout button on mobile$")
	public void verify_logout_button_on_mobile() {
		objMobileMyAccountPage.logoutButtonDisplayed();
	}

	@Then("^verify recently played menu exist|$")
	public void verify_recently_played_menu_exist() {
		objMobileMyAccountPage.recentlyPlayedButtonDisplayed();
	}

	@Then("^Click on LiveChat$")
	public void click_on_LiveChat() {
		objMobileMyAccountPage.clickLiveChat();
	}

	@Then("^Verify device navigate user to containing url \"([^\"]*)\"$")
	public void verify_device_navigate_user_to_containing_url(String url) {
		objMobileMyAccountPage.verifyContainsUrl(url);
	}

	@Then("^Click menu option \"([^\"]*)\"$")
	public void click_menu_option(String arg1) {
		objMobileMyAccountPage.clickMenuOption(arg1);
	}

	@Then("^verify \"([^\"]*)\" as header displayed$")
	public void verify_as_header_displayed(String arg1) {
		objMobileMyAccountPage.verifyMenuHeaderDisplayed(arg1);
	}

	@Then("^Click on back button$")
	public void click_on_back_button() {
		objMobileMyAccountPage.clickBackButton();
	}

	@Then("^Verify message count displayed$")
	public void verify_message_count_displayed() {
		objMobileMyAccountPage.verifyMessageCountDisplayed();
	}
	
	@Then("^Verify 'Live help' link on mobile$")
	public void verify_Live_help_link_on_mobile() {
		objMobileMyAccountPage.verifyLiveHelpDisplayed();
	}

	@Then("^Verify current password displayed$")
	public void verify_current_password_displayed() {
		objMobileMyAccountPage.verifyCurrentPasswordDisplayed();
	}

	@Then("^Verify new password displayed$")
	public void verify_new_password_displayed() {
		objMobileMyAccountPage.verifyNewPasswordDisplayed();
	}

	@Then("^verify update button displayed$")
	public void verify_update_button_displayed() {
		objMobileMyAccountPage.verifyUpdateCTADisplayed();
	}
	
	@Then("^Enter current password \"([^\"]*)\"$")
	public void enter_current_password(String currentPassword) {
		objMobileMyAccountPage.enterCurrentPassword(currentPassword);
	}

	@Then("^Verify green line displayed for current password$")
	public void verify_green_line_displayed_for_current_password() {
		objMobileMyAccountPage.verifyGreenLineForCurrentPassword();
	}

	@Then("^Enter new password \"([^\"]*)\"$")
	public void enter_new_password(String newPassword) {
		objMobileMyAccountPage.enterNewPassword(newPassword);
	}

	@Then("^Verify registration page displays following password guidelines with Right tick$")
	public void verify_registration_page_displays_following_password_guidelines_with_Right_tick(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objMobileMyAccountPage
					.verifyPasswordGuildelineDisplayedWithRightTick(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}
	
	@Then("^verify update CTA is disabled$")
	public void verifyUpdateCTADisabled() {
		objMobileMyAccountPage.verifyUpdateCTADisabled();
	}
	
	@Then("^verify update CTA is enabled$")
	public void verifyUpdateCTAEnabled() {
		objMobileMyAccountPage.verifyUpdateCTAEnabled();
	}
	
	@Then("^verify title \"([^\"]*)\" of sub menu page$")
	public void verify_title_of_sub_menu_page(String arg1) {
		objMobileMyAccountPage.verifyTitleOfSubMenuPage(arg1);
	}
	
	@Then("^verify link \"([^\"]*)\" of sub menu page$")
	public void verify_link_of_sub_menu_page(String arg1) {
		objMobileMyAccountPage.verifyLinkOfSubMenuPage(arg1);
	}
	
	@Then("^verify available funds section of withdrawal sub menu page$")
	public void verify_available_funds_section_of_withdrawal_sub_menu_page() {
		objMobileMyAccountPage.verifyAvailableFundsSectionOfWithdrawal();
	}

	@Then("^verify \"([^\"]*)\" field of sub menu page$")
	public void verify_field_of_sub_menu_page(String arg1) {
		objMobileMyAccountPage.verifyFieldOfSubMenuPage(arg1);
	}
	
	@Then("^Enter withdrawal amount \"([^\"]*)\"$")
	public void enter_withdrawal_amount(String arg1) {
		objMobileMyAccountPage.enterWithdrawalAmt(arg1);
	}

	@Then("^verify helptext \"([^\"]*)\" displayed$")
	public void verify_helptext_displayed(String arg1) {
		objMobileMyAccountPage.helpTextDisplayed(arg1);
	}
	
	@Then("^verify green line for withdrawal amount$")
	public void verify_green_line_for_withdrawal_amount() {
		objMobileMyAccountPage.verifyGreenLineForWithdrawalAmount();
	}
	
	@Then("^Enter withdrawal password \"([^\"]*)\"$")
	public void enter_withdrawal_password(String arg1) {
		objMobileMyAccountPage.enterWithdrawalPassword(arg1);
	}

	@Then("^verify span button \"([^\"]*)\" enabled$")
	public void verify_span_button_enabled(String arg1) {
		objMobileMyAccountPage.verifySpanButtonEnabled(arg1);
	}
	
	@Then("^click link \"([^\"]*)\"$")
	public void click_link(String link) {
		objMobileMyAccountPage.clickLink(link);
	}

	@Then("^Enter password \"([^\"]*)\" for take a break$")
	public void enter_password_for_take_a_break(String strPassword) {
		objMobileMyAccountPage.enterPasswordForTakeBreak(strPassword);
	}
	
	@Then("^verify \"([^\"]*)\" reminder button is highlighted$")
	public void verify_reminder_button_is_highlighted(String arg1) {
		objMobileMyAccountPage.reminderButtonHighlighted(arg1);
	}
	
	@Then("^Enter bonus code \"([^\"]*)\"$")
	public void enter_bonus_code(String arg1) {
		objMobileMyAccountPage.enterBonusCode(arg1);
	}
	
	@Then("^click \"([^\"]*)\" clear button$")
	public void click_clear_button(String arg1) {
		objMobileMyAccountPage.clickClearButton(arg1);
	}

	@Then("^verify \"([^\"]*)\" with text \"([^\"]*)\" displayed$")
	public void verify_with_text_displayed(String arg1, String arg2) {
		objMobileMyAccountPage.verifyTagWithTextDisplayed(arg1,arg2);
	}
}
