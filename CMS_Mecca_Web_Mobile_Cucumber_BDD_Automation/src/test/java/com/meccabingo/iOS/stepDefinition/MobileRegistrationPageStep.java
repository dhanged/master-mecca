/**
 * 
 */
package com.meccabingo.iOS.stepDefinition;

import org.openqa.selenium.By;

import com.generic.StepBase;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.utils.Utilities;
import com.generic.webdriver.DriverProvider;
import com.hooks.GlobalHooks;
import com.meccabingo.iOS.page.MobileRegistrationPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

/**
 * @author Dayanand Dhange (Expleo)
 *
 */
public class MobileRegistrationPageStep {

	private Utilities objUtilities;
	private MobileRegistrationPage objMobileRegistrationPage;

	public MobileRegistrationPageStep(Utilities utilities, StepBase stepBase,
			MobileRegistrationPage mobileRegistrationPage, AppiumDriverProvider appiumDriverProvider) {

		this.objUtilities = utilities;
		// this.objStepBase = stepBase;
		this.objMobileRegistrationPage = mobileRegistrationPage;
	}

	@Then("^Enter random email id on mobile$")
	public void Enter_random_email_id_on_mobile() {
		objMobileRegistrationPage.enterRandomEmail();
	}

	@Then("^Enter invalid email id on mobile$")
	public void Enter_invalid_email_id_on_mobile() {
		objMobileRegistrationPage.enterInvalidEmail();
	}

	@Then("^Check Agree Checkbox on mobile$")
	public void Check_Agree_Checkbox_on_mobile() {
		objMobileRegistrationPage.clickOnAgreeCheckBox();
	}

	@Then("^Click on \"([^\"]*)\" Button on mobile$")
	public void click_on_Button_on_mobile(String strBtnName) {
		objMobileRegistrationPage.clickOnButton(strBtnName);
	}

	@Then("^Click on Next Button on mobile$")
	public void click_on_Next_Button_on_mobile() {
		objMobileRegistrationPage.clickOnNextButton();
	}

	@Then("^contact preferences checkboxes are checked$")
	public void contact_preferences_checkboxes_are_checked() {
		objMobileRegistrationPage.verifyAllCheckboxesChecked();
	}

	@Then("^Enter existing email id on mobile$")
	public void Enter_existing_email_id_on_mobile() {
		objMobileRegistrationPage.enterExistingEmail();
	}

	@Then("^Click on \"([^\"]*)\" checkbox on mobile$")
	public void Click_on_checkbox_on_mobile(String chkboxName) {
		objMobileRegistrationPage.clickCheckbox(chkboxName);
	}

	@Then("^Verify \"([^\"]*)\" checkbox is checked on mobile$")
	public void Verify_checkbox_is_checked_on_mobile(String chkboxName) {
		objMobileRegistrationPage.verifyCheckboxIsChecked(chkboxName);
	}

	@Then("^Verify green line below username field displayed$")
	public void Verify_green_line_below_username_field_displayed() {
		objMobileRegistrationPage.verifyEmailGreenColorDisplayed();
	}

	@Then("^Verify general error is displayed$")
	public void Verify_general_error_is_displayed() {
		objMobileRegistrationPage.verifyGeneralErrorDisplayed();
	}

	@Then("^Verify red background color displayed for \"([^\"]*)\" checkbox on mobile$")
	public void Verify_red_background_color_displayed_for_checkbox_on_mobile(String chkBoxName) {
		objMobileRegistrationPage.verifyRedBackgroundColorDisplayedForCheckbox(chkBoxName);
	}

	@Then("^Verify please select contact preference error displayed on mobile$")
	public void Verify_please_select_contact_preference_error_displayed_on_mobile() {
		objMobileRegistrationPage.errorSelectContactPreferenceMsgDisplayed();
	}

	@Then("^Verify error message \"([^\"]*)\"$")
	public void verify_error_message(String strErrMsg) {
		objMobileRegistrationPage.verifyErrorMessage(strErrMsg);
	}

	@Then("^Verify message \"([^\"]*)\" not displayed$")
	public void verify_message_not_displayed(String strMsg) {

		objMobileRegistrationPage.verifyMessageIsNotDisplayed(strMsg);
	}

	@Then("^Enter firstname \"([^\"]*)\" on mobile$")
	public void Enter_firstname_on_mobile(String strFirstName) {
		objMobileRegistrationPage.enterFirstName(strFirstName);
	}

	@Then("^Enter random firstname on mobile$")
	public void Enter_random_firstname_on_mobile() {
		objMobileRegistrationPage.enterRandomFirstName();
	}

	@Then("^Verify green line displayed for firstname$")
	public void verify_green_line_displayed_for_firstname() {
		objMobileRegistrationPage.verifyGreenColorForFirstName();
	}

	@Then("^Enter surname \"([^\"]*)\" on mobile$")
	public void Enter_surname_on_mobile(String strSurName) {
		objMobileRegistrationPage.enterSurName(strSurName);
	}
	
	@Then("^Enter random surname on mobile$")
	public void Enter_random_surname_on_mobile() {
		objMobileRegistrationPage.enterRandomSurName();
	}

	@Then("^Verify green line displayed for surname$")
	public void verify_green_line_displayed_for_surname() {
		objMobileRegistrationPage.verifyGreenColorForSurName();
	}

	@Then("^Enter DOB \"([^\"]*)\" on mobile$")
	public void Enter_DOB_on_mobile(String strDOB) {
		objMobileRegistrationPage.enterDOB(strDOB);
	}
	
	@Then("^Enter random DOB on mobile$")
	public void Enter_random_DOB_on_mobile() {
		objMobileRegistrationPage.enterRandomDOB();
	}

	@Then("^Verify green line displayed for DOB$")
	public void verify_green_line_displayed_for_DOB() {
		objMobileRegistrationPage.verifyGreenColorForDOB();
	}

	@Then("^Verify registration page displays following options$")
	public void verify_registration_page_displays_following_options(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objMobileRegistrationPage.verifyRegistrationPageDetails(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^Click on \"([^\"]*)\" title on mobile$")
	public void click_on_title_on_mobile(String strTitle) {
		objMobileRegistrationPage.clickOnTitle(strTitle);
	}

	@Then("^Verify \"([^\"]*)\" title is selected$")
	public void verify_title_is_selected(String strTitle) {
		objMobileRegistrationPage.verifyTitleIsSelected(strTitle);
	}

	@Then("^Enter random username$")
	public void enter_random_username() {
		objMobileRegistrationPage.enterRandomUsername();

	}

	@Then("^verify green line is displayed for username$")
	public void verify_green_line_is_displayed_for_username() {
		objMobileRegistrationPage.verifyUsernameGreenlineDisplayed();

	}

	@Then("^verify button \"([^\"]*)\" selected$")
	public void verify_button_selected(String strBtn) {
		objMobileRegistrationPage.verifyButtonIsSelected(strBtn);

	}

	@Then("^Enter password \"([^\"]*)\"$")
	public void enter_password(String strPassword) {
		objMobileRegistrationPage.enterPassword(strPassword);
	}

	@Then("^verify green line is displayed for password$")
	public void verify_green_line_is_displayed_for_password() {
		objMobileRegistrationPage.verifyPasswordGreenlineDisplayed();
	}

	@Then("^verify second page of registration displayed$")
	public void verify_second_page_of_registration_displayed() {
		objMobileRegistrationPage.verifySeocondPageOfRegistrationDisplayed();
	}

	@Then("^Verify registration page displays following password guidelines$")
	public void verify_registration_page_displays_following_password_guidelines(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objMobileRegistrationPage
					.verifyPasswordGuildelineDisplayed(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^Click on No button$")
	public void click_on_No_button() {
		objMobileRegistrationPage.clickOnNoButton();
	}

	@Then("^Verify membership sign up screen$")
	public void verify_membership_sign_up_screen() {
		objMobileRegistrationPage.verifyMembershipSignUpScreen();
	}

	@Then("^Enter membership number \"([^\"]*)\"$")
	public void enter_membership_number(String strMembershipNo) {
		objMobileRegistrationPage.enterMembershipNo(strMembershipNo);
	}

	@Then("^Verify green line displayed for membership number$")
	public void verify_green_line_displayed_for_membership_number() {
		objMobileRegistrationPage.greenLineDisplayedForMembershipNo();
	}

	@Then("^Click Agree Checkbox on mobile$")
	public void Click_Agree_Checkbox_on_mobile() {
		objMobileRegistrationPage.clickAgreeCheck();
	}

	@Then("^Verify next button is enabled$")
	public void Verify_next_button_is_enabled() throws InterruptedException {
		objMobileRegistrationPage.verifyNextButtonEnabled();
	}

	@Then("^Verify error message \"([^\"]*)\" from para tag$")
	public void Verify_error_message_from_para_tag(String strErrorMsg) {
		objMobileRegistrationPage.verifyErrorFromParaTag(strErrorMsg);
	}

	@Then("^Enter registration username \"([^\"]*)\"$")
	public void enter_registration_username(String strUserName) {
		objMobileRegistrationPage.enterRegistrationUsername(strUserName);
	}

	@Then("^Enter mobile number \"([^\"]*)\"$")
	public void enter_mobile_number(String strMobileNo) {
		objMobileRegistrationPage.enterRegistrationMobileNumber("");
		objMobileRegistrationPage.enterRegistrationMobileNumber(strMobileNo);
	}
	
	@Then("^Enter random mobile number$")
	public void enter_random_mobile_number() {
		objMobileRegistrationPage.enterRegistrationMobileNumber("");
		objMobileRegistrationPage.enterRandomRegistrationMobileNumber();
	}

	@Then("^Select country \"([^\"]*)\"$")
	public void select_country(String strCountryName) {
		objMobileRegistrationPage.selectCountry(strCountryName);
	}

	@Then("^Enter postal code \"([^\"]*)\"$")
	public void enter_postal_code(String strPostalCode) {
		objMobileRegistrationPage.enterPostalCode(strPostalCode);
	}
	
	@Then("^Enter random postal code$")
	public void Enter_random_postal_code() {
		objMobileRegistrationPage.enterRandomPostalCode();
	}
	
	@Then("^Enter random email id with suffix \"([^\"]*)\" on mobile$")
	public void enter_random_email_id_with_suffix_on_mobile(String arg1) {
		objMobileRegistrationPage.enterRandomEmailIdWithGivenSuffix(arg1);
	}

	@Then("^verify span \"([^\"]*)\" displayed$")
	public void verify_span_displayed(String strName) {
		objMobileRegistrationPage.verifySpanDisplayed(strName);
	}
	
	@Then("^click span \"([^\"]*)\"$" ) 
	public void click_span(String spanName) {
		objMobileRegistrationPage.clickSpan(spanName);
	}
	
	@Then("^verify div \"([^\"]*)\" displayed$")
	public void verify_div_displayed(String strName) {
		objMobileRegistrationPage.verifyDivDisplayed(strName);
	}
	
	@Then("^Enter static postal code \"([^\"]*)\"$")
	public void enter_static_postal_code(String strPostalCode) {
		objMobileRegistrationPage.enterStaticPostalCode(strPostalCode);
	}	
	
	@Then("^verify span \"([^\"]*)\" not displayed$")
	public void verify_span_not_displayed(String strName) {
		objMobileRegistrationPage.verifySpanNotDisplayed(strName);
	}

	@Then("^Verify text \"([^\"]*)\" displayed$")
	public void verify_text_displayed(String text) {
		objMobileRegistrationPage.verifyH4HeadingDisplayed(text);
	}

	@Then("^Verify help text displayed$")
	public void verify_help_text_displayed() {
		objMobileRegistrationPage.verifyHelpTextDisplayed();
	}

	@Then("^Verify agree checkbox is unchecked$")
	public void Verify_agree_checkbox_is_unchecked() {
		objMobileRegistrationPage.verifyAgreeCheckboxUnchecked();
	}
}
