package com.meccabingo.iOS.stepDefinition;


import com.meccabingo.iOS.page.MobileGamesPage;

import io.cucumber.java.en.Then;

public class MobileGamesStep {

	private MobileGamesPage mobileGames;

	
	public MobileGamesStep(MobileGamesPage mobileGames) {
		this.mobileGames = mobileGames;

	}

	@Then("^Click on image of the game \"([^\"]*)\"$")
	public void click_on_image_of_the_game(String nameofthegame) {
		mobileGames.clickOnImageOfTile(nameofthegame);
	}

	@Then("^Click on title of the game \"([^\"]*)\"$")
	public void click_on_title_of_the_game(String nameofthegame) {
		mobileGames.clickOnTitleoftheGame(nameofthegame);
	}

	@Then("^Click on prize of the bingo tile \"([^\"]*)\"$")
	public void click_on_prize_of_the_bingo_tile(String nameofthegame) {
		mobileGames.clickOnPrizeofTile();
	}

	@Then("^Verify game detail page open successfully$")
	public void verify_game_detail_page_open_successfully() {
		mobileGames.verifyGameDetailPage();

	}

	@Then("^Click on description of the game \"([^\"]*)\"$")
	public void click_on_description_of_the_game(String nameofthegame) {
		mobileGames.clickOnDescriptionOfTile(nameofthegame);
	}

	@Then("^Navigate to Auto tab of Bingo games$")
	public void navigate_to_Auto_tab_of_Bingo_games() {
		mobileGames.clickAutoTabOfBingoGames();
	}

	@Then("^Click on image of the game$")
	public void click_on_image_of_the_game() {
		mobileGames.clickHeroTileImage();
	}

	@Then("^Navigate back to previous page$")
	public void navigate_back_to_previous_page() {
		mobileGames.navigateToHomePage();
	}

	@Then("^Click on title of the game$")
	public void click_on_title_of_the_game() {
		mobileGames.clickTitleOfBingoGame();
	}

	@Then("^user should remain on bingo container page$")
	public void user_should_remain_on_bingo_container_page() {
		mobileGames.bingoContainerSectionDisplayed();
	}

	@Then("^User clicks on i button of game having hero tile$")
	public void user_clicks_on_i_button_of_game_having_hero_tile() {
		mobileGames.clickOniButtonOfBingoGame();
	}

	@Then("^click on top flag of bingo game$")
	public void click_on_top_flag_of_bingo_game() {
		mobileGames.clickTopFlagOfBingoGame();
	}

	@Then("^Verify user can scroll slot game tiles horizontally$")
	public void verify_user_can_scroll_slot_game_tiles_horizontally() {
		//mobileGames.scrollSlotGamesHorizantally();
	}

	@Then("^Verify user can scroll bingo game tiles horizontally$")
	public void verify_user_can_scroll_bingo_game_tiles_horizontally() {
		//mobileGames.scrollBingoGamesHorizantally();
	}

	@Then("^Click on description of the game$")
	public void click_on_description_of_the_game() {
		mobileGames.clickOnDescriptionOfGame();
	}

	@Then("^Click on prize of the bingo tile$")
	public void click_on_prize_of_the_bingo_tile() {
		mobileGames.clickOnPrize();
	}

	@Then("^Click on pre buy button of first game of bingo section$")
	public void click_on_pre_buy_button_of_first_game_of_bingo_section() {
		mobileGames.clickPreBuyOfFirstBingoGame();
	}
}
