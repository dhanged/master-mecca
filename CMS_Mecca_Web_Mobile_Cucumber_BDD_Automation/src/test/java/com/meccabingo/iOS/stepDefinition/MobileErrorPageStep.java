package com.meccabingo.iOS.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.iOS.page.MobileErrorPage;

import io.cucumber.java.en.Then;

public class MobileErrorPageStep {

	private MobileErrorPage objMobileErrorPage;
	Utilities utilities;

	public MobileErrorPageStep(MobileErrorPage mobileErrorPage) {

		this.objMobileErrorPage = mobileErrorPage;
	}

	@Then("^Navigate to wrong URL$")
	public void navigate_to_wrong_URL() {
		objMobileErrorPage.navigateToWrongURL();
	}

	@Then("^Error displayed$")
	public void error_displayed() {
		objMobileErrorPage.errorDisplayed();
	}

}
