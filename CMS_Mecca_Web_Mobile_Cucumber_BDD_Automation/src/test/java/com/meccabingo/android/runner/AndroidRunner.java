
package com.meccabingo.android.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = ".", strict = true, plugin = { "json:target/JSON/Android.json",
		"rerun:target/SyncFails.txt", /*"com.generic.cucumberReporting.CustomFormatter"*/ }, 
		tags = "@AndroidReg", 
		dryRun = false, monochrome = true,
		glue = { "com.hooks", "com.meccabingo.android.stepDefinition"}
)

public class AndroidRunner extends AbstractTestNGCucumberTests {
}
