package com.meccabingo.android.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.android.page.MobilePayPalPage;

import io.cucumber.java.en.Then;

public class MobilePayPalPageStep {

	private MobilePayPalPage objMobilePayPalPage;
	Utilities utilities;

	public MobilePayPalPageStep(MobilePayPalPage mobilePayPalPage) {

		this.objMobilePayPalPage = mobilePayPalPage;
	}

	@Then("^Enter paypal id \"([^\"]*)\"$")
	public void enter_paypal_id(String strID) {
		objMobilePayPalPage.enterPayPalID(strID);
	}

	@Then("^Click paypal next button$")
	public void click_paypal_next_button() {
		objMobilePayPalPage.clickPayPalNext();
	}

	@Then("^Enter paypal password \"([^\"]*)\"$")
	public void enter_paypal_password(String strPassword) {
		objMobilePayPalPage.enterPayPalPassword(strPassword);
	}

	@Then("^Click paypal login button$")
	public void click_paypal_login_button() {
		objMobilePayPalPage.clickPayPalLogin();
	}

	@Then("^Click paypal confirm and pay button$")
	public void click_paypal_confirm_and_pay_button() {
		objMobilePayPalPage.clickPayPalConfirm();
	}
}
