package com.meccabingo.android.stepDefinition;

import org.openqa.selenium.By;

import com.generic.utils.Utilities;
import com.meccabingo.android.page.MobileBlocksPage;
import com.meccabingo.android.page.MobileRegistrationDigitalPage;

import io.cucumber.java.en.Then;

public class MobileRegistrationDigitalPageStep {

	private MobileRegistrationDigitalPage objMobileRegistrationDigitalPage;
	Utilities utilities;

	public MobileRegistrationDigitalPageStep(MobileRegistrationDigitalPage mobileRegistrationDigitalPage) {

		this.objMobileRegistrationDigitalPage = mobileRegistrationDigitalPage;
	}

	@Then("^Verify help text \"([^\"]*)\" displayed for postcode$")
	public void verify_help_text_displayed_for_postcode(String strHelpText) {
		objMobileRegistrationDigitalPage.verifyHelpTextDisplayedForPostcode(strHelpText);
	}

	@Then("^Verify grey color displayed for postcode$")
	public void verify_grey_color_displayed_for_postcode() {
		objMobileRegistrationDigitalPage.verifyGreyColorDisplayedForPostcode();
	}

	@Then("^Verify Address Line1 populated with \"([^\"]*)\"$")
	public void verify_Address_Line_populated_with(String arg2) {
		objMobileRegistrationDigitalPage.verifyAddressLine1Populated(arg2);
	}

	@Then("^Verify City populated with \"([^\"]*)\"$")
	public void verify_City_populated_with(String arg1) {
		objMobileRegistrationDigitalPage.verifyCityPopulated(arg1);
	}

	@Then("^Verify County populated with \"([^\"]*)\"$")
	public void verify_County_populated_with(String arg1) {
		objMobileRegistrationDigitalPage.verifyCountyPopulated(arg1);
	}

	@Then("^Verify Postcode populated with \"([^\"]*)\"$")
	public void verify_Postcode_populated_with(String arg1) {
		objMobileRegistrationDigitalPage.verifyPostcodePopulated(arg1);
	}

	@Then("^click Enter address manually button$")
	public void click_Enter_address_manually_button() {
		objMobileRegistrationDigitalPage.clickEnterAddressManually();
	}

	@Then("^Verify postcode search button displayed$")
	public void verify_postcode_search_button_displayed() {
		objMobileRegistrationDigitalPage.verifyPostcodeSearchDisplayed();
	}

	@Then("^Enter address line1 \"([^\"]*)\"$")
	public void enter_address_line(String arg2) {
		objMobileRegistrationDigitalPage.enterAddressLine1(arg2);
	}

	@Then("^Enter random address line1$")
	public void enter_random_address_line1() {
		objMobileRegistrationDigitalPage.enterRandomAddressLine1();
	}

	@Then("^Verify green line displayed for address line1$")
	public void verify_green_line_displayed_for_address_line() {
		objMobileRegistrationDigitalPage.greenLineDisplayedForAddressLine1();
	}

	@Then("^Enter city \"([^\"]*)\"$")
	public void enter_city(String arg1) {
		objMobileRegistrationDigitalPage.enterCity(arg1);
	}

	@Then("^Enter random city$")
	public void enter_random_city() {
		objMobileRegistrationDigitalPage.enterRandomCity();
	}

	@Then("^Verify green line displayed for city$")
	public void verify_green_line_displayed_for_city() {
		objMobileRegistrationDigitalPage.greenLineDisplayedForCity();
	}

	@Then("^Enter county \"([^\"]*)\"$")
	public void enter_county(String arg1) {
		objMobileRegistrationDigitalPage.enterCounty(arg1);
	}

	@Then("^Enter random county$")
	public void enter_random_county() {
		objMobileRegistrationDigitalPage.enterRandomCounty();
	}

	@Then("^Verify green line displayed for county$")
	public void verify_green_line_displayed_for_county() {
		objMobileRegistrationDigitalPage.greenLineDisplayedForCounty();
	}

	@Then("^Verify green color displayed for postcode$")
	public void verify_green_color_displayed_for_postcode() {
		objMobileRegistrationDigitalPage.greenLineDisplayedForPostcode();
	}

	@Then("^Verify grey line displayed for mobile number$")
	public void verify_grey_line_displayed_for_mobile_number() {
		objMobileRegistrationDigitalPage.greyLineDisplayedForMobileNumber();
	}

	@Then("^Verify green line displayed for mobile number$")
	public void verify_green_line_displayed_for_mobile_number() {
		objMobileRegistrationDigitalPage.greenLineDisplayedForMobileNumber();
	}

	@Then("^Verify \"([^\"]*)\" checkbox is unchecked$")
	public void verify_checkbox_is_unchecked(String arg1) {
		objMobileRegistrationDigitalPage.verifyCheckboxIsUnchecked(arg1);
	}

	@Then("^Verify \"([^\"]*)\" textbox is not empty$")
	public void verify_textbox_is_not_empty(String arg1) {
		objMobileRegistrationDigitalPage.verifyTextBoxIsNotEmpty(arg1);
	}

	@Then("Verify Email address textbox is not empty")
	public void verify_email_address_textbox_is_not_empty() {
		objMobileRegistrationDigitalPage.verifyEmailTextBoxIsNotEmpty();
	}
}
