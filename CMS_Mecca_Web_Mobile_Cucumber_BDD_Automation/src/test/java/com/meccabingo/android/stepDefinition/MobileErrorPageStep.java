package com.meccabingo.android.stepDefinition;

import java.net.MalformedURLException;

import com.generic.utils.Utilities;
import com.meccabingo.android.page.MobileErrorPage;

import io.cucumber.java.en.Then;

public class MobileErrorPageStep {

	private MobileErrorPage objMobileErrorPage;
	Utilities utilities;

	public MobileErrorPageStep(MobileErrorPage mobileErrorPage) {

		this.objMobileErrorPage = mobileErrorPage;
	}

	@Then("^Navigate to wrong URL$")
	public void navigate_to_wrong_URL() throws MalformedURLException {
		objMobileErrorPage.navigateToWrongURL();
	}

	@Then("^Error displayed$")
	public void error_displayed() {
		objMobileErrorPage.errorDisplayed();
	}

}
