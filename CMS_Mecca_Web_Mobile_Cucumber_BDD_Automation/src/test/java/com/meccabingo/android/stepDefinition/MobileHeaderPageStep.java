package com.meccabingo.android.stepDefinition;

import com.generic.StepBase;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.utils.Utilities;
import com.meccabingo.android.page.MobileHeaderPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

public class MobileHeaderPageStep {

	private MobileHeaderPage objMobileHeaderPage;
	Utilities objUtilities;

	public MobileHeaderPageStep(MobileHeaderPage mobileHeaderPage, Utilities utilities, StepBase stepBase,
			AppiumDriverProvider appiumDriverProvider) {

		this.objMobileHeaderPage = mobileHeaderPage;
		this.objUtilities = utilities;
	}

	@Then("^Verify balance section is displayed$")
	public void verify_balance_section_is_displayed() {
		objMobileHeaderPage.verifyBalanceSectionDisplayed();
	}

	@Then("^Verify title as balance is displayed$")
	public void verify_title_as_balance_is_displayed() {
		objMobileHeaderPage.verifyBalaneAsTitleDisplayed();
	}

	@Then("^Verify amount under balance section is displayed$")
	public void verify_amount_under_balance_section_is_displayed() {
		objMobileHeaderPage.verifyAmountUnderBalanceSectionDisplayed();
	}

	@Then("^Hide the balance$")
	public void hide_the_balance() {
		objMobileHeaderPage.hideBalance();
	}

	@Then("^Click on balance title$")
	public void click_on_balance_title() {
		objMobileHeaderPage.clickBalanceTitle();
	}

	@Then("^click on five dots$")
	public void click_on_five_dots() {
		objMobileHeaderPage.clickFiveDots();
	}

	@Then("^Verify following options of myaccount$")
	public void verify_following_options(DataTable arg1) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(arg1).size(); i++) {
			objMobileHeaderPage.verifyOptions(objUtilities.getListDataFromDataTable(arg1).get(i));
		}
	}

}
