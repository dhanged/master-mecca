package com.meccabingo.android.stepDefinition;

import java.net.MalformedURLException;

import com.generic.utils.Utilities;
import com.meccabingo.android.page.MobilePromotionPage;

import io.cucumber.java.en.Then;

public class MobilePromotionPageStep {

	private MobilePromotionPage objMobilePromotionPage;
	Utilities utilities;

	public MobilePromotionPageStep(MobilePromotionPage mobilePromotionPage) {

		this.objMobilePromotionPage = mobilePromotionPage;
	}

	@Then("click learn more")
	public void click_learn_more() {
		objMobilePromotionPage.clickLearnMore();
	}

	@Then("Verify claim button displayed")
	public void verify_claim_button_displayed() {
		objMobilePromotionPage.claimButtonDisplayed();
	}
}
