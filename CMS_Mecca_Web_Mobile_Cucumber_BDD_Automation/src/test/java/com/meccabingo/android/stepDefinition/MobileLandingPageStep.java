/**
 * 
 */
package com.meccabingo.android.stepDefinition;

import com.generic.StepBase;
import com.generic.utils.Utilities;
//import com.grosvenor.pages.mobile.android.MobileLandingPage;

import io.cucumber.java.en.Then;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class MobileLandingPageStep {

	private Utilities objUtilities;
	private StepBase objStepBase;
	// private MobileLandingPage objMobileLandingPage;

	// Local variables
	public MobileLandingPageStep(Utilities utilities, StepBase stepBase)// MobileLandingPage mobileLandingPage
	{

		this.objUtilities = utilities;
		this.objStepBase = stepBase;
		// this.objMobileLandingPage = mobileLandingPage;
	}

	@Then("^The Grosenor Casino landing Page is loaded successfully$")
	public void the_Grosenor_Casino_landing_Page_is_loaded_successfully() {
		// objMobileLandingPage.verifyLoggedInBarPresent();
	}

}
