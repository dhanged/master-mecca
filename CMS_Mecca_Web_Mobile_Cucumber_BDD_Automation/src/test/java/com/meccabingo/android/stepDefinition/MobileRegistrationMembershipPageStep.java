package com.meccabingo.android.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.android.page.MobileRegistrationMembershipPage;

import io.cucumber.java.en.Then;

public class MobileRegistrationMembershipPageStep {

	private MobileRegistrationMembershipPage objMobileRegistrationMembershipPage;
	Utilities utilities;

	public MobileRegistrationMembershipPageStep(MobileRegistrationMembershipPage mobileRegistrationMembershipPage) {

		this.objMobileRegistrationMembershipPage = mobileRegistrationMembershipPage;
	}

	@Then("^Click \"([^\"]*)\" tag with text \"([^\"]*)\"$")
	public void click_tag_with_text(String strTag, String strText) {
		objMobileRegistrationMembershipPage.clickTagWithText(strTag, strText);
	}

}
