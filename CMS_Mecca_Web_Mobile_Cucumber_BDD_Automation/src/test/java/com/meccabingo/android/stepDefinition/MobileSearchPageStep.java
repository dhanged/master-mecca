/**
 * 
 */
package com.meccabingo.android.stepDefinition;

import com.generic.StepBase;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.utils.Utilities;
import com.hooks.GlobalHooks;
import com.meccabingo.android.page.MobileSearchPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class MobileSearchPageStep {

	private Utilities objUtilities;
	private GlobalHooks objGlobalHooks;
	private StepBase objStepBase;

	private MobileSearchPage objMobileSearchPage;

	public MobileSearchPageStep(Utilities utilities, StepBase stepBase, MobileSearchPage mobileSearchPage,
			AppiumDriverProvider appiumDriverProvider) {

		this.objUtilities = utilities;
		this.objStepBase = stepBase;
		this.objMobileSearchPage = mobileSearchPage;
	}

	@When("^User clicks on search filed on Mobile$")
	public void user_clicks_on_search_filed() {
		objMobileSearchPage.clickSearchLoupe();
	}

	@Then("^Verify Search overlay opens on Mobile$")
	public void verify_Search_overlay_opens() {
		objMobileSearchPage.verifySearchOverLay();
	}

	@When("^Search Game \"([^\"]*)\" in Search field from header on Mobile$")
	public void search_Game_in_Search_field_from_header(String txt) {
		objMobileSearchPage.enterGameName(txt);
	}

	@Then("^Verify device displays search results to user$")
	public void verify_device_displays_search_results_to_user() {
		objMobileSearchPage.verifySearchSection();
	}

	@Then("^Verify device displays following options for search title:$")
	public void verify_device_displays_following_options_for_search_title(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objMobileSearchPage.verifyGameDeatails(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^User enters three characters in search filed on Mobile$") // random values in search box
	public void user_enters_three_characters_in_search_filed() {
		objMobileSearchPage.enterGameName("qas");
	}

	@Then("^Verify device displays search result section$")
	public void verify_device_displays_search_result_section() {
		objMobileSearchPage.verifyResultSectionWithNoMatch();
	}

	@Then("^Click on Play Now button from searched Game$")
	public void Click_on_Play_Now_button_from_searched_Game() {
		objMobileSearchPage.clickPlayNowFromSearchedGame();
	}

	@Then("^Click on i button from searched Game$")
	public void Click_on_i_button_from_searched_Game() {
		objMobileSearchPage.clickiBtnFromSearchedGame();
	}

	@Then("^Verify user able to view 'Quick Links'$")
	public void verify_user_able_to_view_Quick_Links() {
		objMobileSearchPage.quickLinksDisplayed();
	}
	
	@Then("Check checkbox of include clubs in search result")
	public void check_checkbox_of_include_clubs_in_search_result() {
		objMobileSearchPage.checkCheckboxOfIncludeClubs();
	}
	
	@Then("^Verify system displays search results to user$")
	public void verify_system_displays_search_results_to_user() {
		objMobileSearchPage.verifySearchSection();
	}
}
