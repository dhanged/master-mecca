package com.meccabingo.android.stepDefinition;

import com.generic.StepBase;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.utils.Utilities;
import com.meccabingo.android.page.MobileListingPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

public class MobileListingPageStep {

	private MobileListingPage objMobileListingPage;
	Utilities utilities;

	public MobileListingPageStep(MobileListingPage mobileListingPage, Utilities utilities, StepBase stepBase,
			AppiumDriverProvider appiumDriverProvider) {
		this.objMobileListingPage = mobileListingPage;
		this.utilities = utilities;
	}

	@Then("^Verify below components$")
	public void verify_Game_window_header_displays_following_components(DataTable dt) {
		for (int i = 0; i < utilities.getListDataFromDataTable(dt).size(); i++) {
			objMobileListingPage.verifyComponents(utilities.getListDataFromDataTable(dt).get(i));

		}
	}
	//
	// @Then("^click bingo tab from header$")
	// public void click_bingo_tab_from_header() {
	// objMobileListingPage.clickBingoTabFromHeader();
	// }
}
