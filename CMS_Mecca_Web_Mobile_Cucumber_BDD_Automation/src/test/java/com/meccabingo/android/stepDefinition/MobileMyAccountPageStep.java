package com.meccabingo.android.stepDefinition;

import org.openqa.selenium.By;

import com.generic.StepBase;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.utils.Utilities;
import com.meccabingo.android.page.MobileMyAccountPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

public class MobileMyAccountPageStep {

	private MobileMyAccountPage objMobileMyAccountPage;
	private Utilities objUtilities;

	String greycolor = "rgba(179, 179, 179, 1)";
	String greencolor = "rgba(94, 168, 90, 1)";// "#5ea85a";

	public MobileMyAccountPageStep(MobileMyAccountPage mobileMyAccountPage, Utilities utilities, StepBase stepBase,
			AppiumDriverProvider appiumDriverProvider) {

		this.objMobileMyAccountPage = mobileMyAccountPage;
		this.objUtilities = utilities;
	}

	@Then("^Click on my account$")
	public void click_on_my_account() {
		objMobileMyAccountPage.clickOnMyAccount();
	}

	@Then("^Verify Balance block in Collapsed state$")
	public void verify_Balance_block_in_Collapsed_state() {
		objMobileMyAccountPage.verifyPlayableBalanceSectionInCollpasedStateDisplayed();
	}

	@Then("^Click on plus sign$")
	public void click_on_plus_sign() {
		objMobileMyAccountPage.clickOnPlusSign();
	}

	@Then("^Click on plus sign of take a break page$")
	public void click_on_plus_sign_of_take_a_break_page() {
		objMobileMyAccountPage.clickOnPlusSignOfTakeABreakPage();
	}

	@Then("^Verify Balance block in expanded state$")
	public void verify_Balance_block_in_expanded_state() {
		objMobileMyAccountPage.verifyPlayableBalanceSectionInExpandedStateDisplayed();
	}

	@Then("^Click top section of balance box$")
	public void click_top_section_of_balance_box() {
		objMobileMyAccountPage.clickOnTopSectionOfBalanceBlock();
	}

	@Then("^Click on Deposit button$")
	public void click_on_Deposit_button() {
		objMobileMyAccountPage.clickDeposit();
	}

	@Then("^Click on Detailed view button$")
	public void click_on_Detailed_view_button() {
		objMobileMyAccountPage.clickDetailedView();
	}

	@Then("^Verify user navigate to balance overlay page$")
	public void verify_user_navigate_to_balance_overlay_page() {
		objMobileMyAccountPage.verifyBalanceOverlayPage();
	}

	@Then("^Verify user navigate to deposit overlay page$")
	public void verify_user_navigate_to_deposit_overlay_page() {
		objMobileMyAccountPage.verifyDepositOverlayPage();
	}

	@Then("^Click on cash title$")
	public void click_on_cash_title() {
		objMobileMyAccountPage.clickOnCash();
	}

	@Then("^Click on bonuses amount$")
	public void click_on_bonuses_amount() {
		objMobileMyAccountPage.clickOnBonusAmt();
	}

	@Then("^Username \"([^\"]*)\" as header displayed$")
	public void username_as_header_displayed(String uname) {
		objMobileMyAccountPage.usernameDisplayed(uname);
	}

	@Then("^Verify following my account menu options:$")
	public void verify_following_my_account_menu_options(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objMobileMyAccountPage.verifyMenuOptions(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^Verify logout button on mobile$")
	public void verify_logout_button_on_mobile() {
		objMobileMyAccountPage.logoutButtonDisplayed();
	}

	@Then("^Verify recently played menu exist|$")
	public void verify_recently_played_menu_exist() {
		objMobileMyAccountPage.recentlyPlayedButtonDisplayed();
	}

	@Then("^Click on LiveChat$")
	public void click_on_LiveChat() {
		objMobileMyAccountPage.clickLiveChat();
	}

	@Then("^Verify device navigate user to containing url \"([^\"]*)\"$")
	public void verify_device_navigate_user_to_containing_url(String url) {
		objMobileMyAccountPage.verifyContainsUrl(url);
	}

	@Then("^Click menu option \"([^\"]*)\"$")
	public void click_menu_option(String arg1) {
		objMobileMyAccountPage.clickMenuOption(arg1);
	}

	@Then("^Verify \"([^\"]*)\" as header displayed$")
	public void verify_as_header_displayed(String arg1) {
		objMobileMyAccountPage.verifyMenuHeaderDisplayed(arg1);
	}

	@Then("^Click on back button$")
	public void click_on_back_button() {
		objMobileMyAccountPage.clickBackButton();
	}

	@Then("^Verify message count displayed$")
	public void verify_message_count_displayed() {
		objMobileMyAccountPage.verifyMessageCountDisplayed();
	}

	@Then("^Verify 'Live help' link on mobile$")
	public void verify_Live_help_link_on_mobile() {
		objMobileMyAccountPage.verifyLiveHelpDisplayed();
	}

	@Then("^Verify current password displayed$")
	public void verify_current_password_displayed() {
		objMobileMyAccountPage.verifyCurrentPasswordDisplayed();
	}

	@Then("^Verify new password displayed$")
	public void verify_new_password_displayed() {
		objMobileMyAccountPage.verifyNewPasswordDisplayed();
	}

	@Then("^Verify update button displayed$")
	public void verify_update_button_displayed() {
		objMobileMyAccountPage.verifyUpdateCTADisplayed();
	}

	@Then("^Enter current password \"([^\"]*)\"$")
	public void enter_current_password(String currentPassword) {
		objMobileMyAccountPage.enterCurrentPassword(currentPassword);
	}

	@Then("^Verify green line displayed for current password$")
	public void verify_green_line_displayed_for_current_password() {
		objMobileMyAccountPage.verifyGreenLineForCurrentPassword();
	}

	@Then("^Enter new password \"([^\"]*)\"$")
	public void enter_new_password(String newPassword) {
		objMobileMyAccountPage.enterNewPassword(newPassword);
	}

	@Then("^Verify registration page displays following password guidelines with Right tick$")
	public void verify_registration_page_displays_following_password_guidelines_with_Right_tick(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objMobileMyAccountPage
					.verifyPasswordGuildelineDisplayedWithRightTick(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^Verify update CTA is disabled$")
	public void verifyUpdateCTADisabled() {
		objMobileMyAccountPage.verifyUpdateCTADisabled();
	}

	@Then("^Verify update CTA is enabled$")
	public void verifyUpdateCTAEnabled() {
		objMobileMyAccountPage.verifyUpdateCTAEnabled();
	}

	@Then("^Verify title \"([^\"]*)\" of sub menu page$")
	public void verify_title_of_sub_menu_page(String arg1) {
		objMobileMyAccountPage.verifyTitleOfSubMenuPage(arg1);
	}

	@Then("^Verify link \"([^\"]*)\" of sub menu page$")
	public void verify_link_of_sub_menu_page(String arg1) {
		objMobileMyAccountPage.verifyLinkOfSubMenuPage(arg1);
	}

	@Then("^Verify available funds section of withdrawal sub menu page$")
	public void verify_available_funds_section_of_withdrawal_sub_menu_page() {
		objMobileMyAccountPage.verifyAvailableFundsSectionOfWithdrawal();
	}

	@Then("^Verify \"([^\"]*)\" field of sub menu page$")
	public void verify_field_of_sub_menu_page(String arg1) {
		objMobileMyAccountPage.verifyFieldOfSubMenuPage(arg1);
	}

	@Then("^Enter withdrawal amount \"([^\"]*)\"$")
	public void enter_withdrawal_amount(String arg1) {
		objMobileMyAccountPage.enterWithdrawalAmt(arg1);
	}

	@Then("^Verify helptext \"([^\"]*)\" displayed$")
	public void verify_helptext_displayed(String arg1) {
		objMobileMyAccountPage.helpTextDisplayed(arg1);
	}

	@Then("^Verify green line for withdrawal amount$")
	public void verify_green_line_for_withdrawal_amount() {
		objMobileMyAccountPage.verifyGreenLineForWithdrawalAmount();
	}

	@Then("^Enter withdrawal password \"([^\"]*)\"$")
	public void enter_withdrawal_password(String arg1) {
		objMobileMyAccountPage.enterWithdrawalPassword(arg1);
	}

	@Then("^Verify button having label as \"([^\"]*)\" is enabled$")
	public void verify_span_button_enabled(String arg1) {
		objMobileMyAccountPage.verifySpanButtonEnabled(arg1);
	}

	@Then("^click link \"([^\"]*)\"$")
	public void click_link(String link) {
		objMobileMyAccountPage.clickLink(link);
	}

	@Then("^Enter password \"([^\"]*)\" for take a break$")
	public void enter_password_for_take_a_break(String strPassword) {
		objMobileMyAccountPage.enterPasswordForTakeBreak(strPassword);
	}

	@Then("^Verify \"([^\"]*)\" reminder button is highlighted$")
	public void verify_reminder_button_is_highlighted(String arg1) {
		objMobileMyAccountPage.reminderButtonHighlighted(arg1);
	}

	@Then("^Enter bonus code \"([^\"]*)\"$")
	public void enter_bonus_code(String arg1) {
		objMobileMyAccountPage.enterBonusCode(arg1);
	}

	@Then("^click \"([^\"]*)\" clear button$")
	public void click_clear_button(String arg1) {
		objMobileMyAccountPage.clickClearButton(arg1);
	}

	@Then("^Verify \"([^\"]*)\" with text \"([^\"]*)\" displayed$")
	public void verify_with_text_displayed(String arg1, String arg2) {
		objMobileMyAccountPage.verifyTagWithTextDisplayed(arg1, arg2);
	}

	@Then("^Verify password field displayed for take a break page$")
	public void Verify_password_field_displayed_for_take_a_break_page() {
		objMobileMyAccountPage.verifyPasswdFieldForTakeABreak();
	}

	@Then("^Verify bold text \"([^\"]*)\" displayed$")
	public void Verify_bold_text_displayed(String strText) {
		objMobileMyAccountPage.verifyH5TextDisplayed(strText);
	}

	@Then("^Verify information text \"([^\"]*)\" displayed$")
	public void Verify_information_text_displayed(String strText) {
		objMobileMyAccountPage.verifyDivTextDisplayed(strText);
	}

	@Then("^Verify password field displayed for Self exclusion$")
	public void Verify_password_field_displayed_for_Self_exclusion() {
		objMobileMyAccountPage.verifyPasswdFieldDisplayedForSelfExclusion();
	}

	@Then("^Click on plus sign of reality check$")
	public void Click_on_plus_sign_of_reality_check() {
		objMobileMyAccountPage.clickPlusSignOfRealityCheck();
	}

	@Then("^Click on plus sign of self exclude$")
	public void Click_on_plus_sign_of_self_exclude() {
		objMobileMyAccountPage.clickPlusSignOfSelfExclude();
	}

	@Then("^Click on Deposit button of balance page$")
	public void Click_on_Deposit_button_of_balance_page() {
		objMobileMyAccountPage.clickDepositOfBalance();
	}
	
	@Then("^Verify transaction filter box displayed$")
	public void Verify_transaction_filter_box_displayed() {
		objMobileMyAccountPage.transactionFilterBoxDisplayed();
	}
	
	@Then("^Verify dates are populated with today date$")
	public void Verify_dates_are_populated_with_today_date() {
		objMobileMyAccountPage.datesPopulatedWithTodayDate();
	}
	
	@Then("^Verify transaction history box$")
	public void Verify_transaction_history_box() {
		objMobileMyAccountPage.transactionHistoryBoxDisplayed();
	}
	
	@Then("^Select \"([^\"]*)\" filter activity$")
	public void select_filter_activity(String strFilterActivity) {
		objMobileMyAccountPage.selectFilterActivity(strFilterActivity);
	}
	
	@Then("^Verify records are found of \"([^\"]*)\"$")
	public void verifyTransactionHistoryRecords(String activity) {
		objMobileMyAccountPage.verifyTransactionHistoryRecords(activity);
	}
	@Then("^Click button Filter$")
	public void Click_button_Filter() {
		objMobileMyAccountPage.clickFilterButton();
	}
	
	@Then("^Verify option \"([^\"]*)\" displayed$")
	public void Verify_option_displayed(String strOption) {
		objMobileMyAccountPage.optionDisplayed(strOption);
	}
	
	@Then("^Select SortBy \"([^\"]*)\"$")
	public void Select_SortBy(String strSortBy) {
		objMobileMyAccountPage.selectSortBy(strSortBy);
	}
	
	@Then("^Collapse one record$")
	public void collapse_one_record() {
		objMobileMyAccountPage.collapseRecord();
	}
	
	@Then("^Verify the collapsed record$")
	public void Verify_the_collapsed_record() {
		objMobileMyAccountPage.verifyCollapsedRecord();
	}
	
	@Then("^Open first message$")
	public void Open_first_message() {
		objMobileMyAccountPage.clickFirstMessage();
	}
	
	@Then("^Verify message in detail$")
	public void Verify_message_in_detail() {
		objMobileMyAccountPage.verifyMessageInDetail();
	}
	
	@Then("^Click delete button$")
	public void Click_delete_button() {
		objMobileMyAccountPage.clickMsgDeleteButton();
	}
	
	@Then("Verify delete icon displayed")
	public void verify_delete_icon_displayed() {
		objMobileMyAccountPage.verifyDeleteIconDisplayed();
	}
	
	@Then("Verify messages table displayed")
	public void verify_messages_table_displayed() {
		objMobileMyAccountPage.verifyMessagesTableDisplayed();
	}
	
	@Then("Read the message count")
	public void read_the_message_count() {
		objMobileMyAccountPage.setMessageCount();
	}
	
	@Then("Verify that message count is reduced")
	public void verify_that_message_count_is_reduced() {
		objMobileMyAccountPage.verifyMsgCountReduced();
	}
	
	@Then("^Enter limit \"([^\"]*)\"$")
	public void enter_limit(String strLimit) {
		objMobileMyAccountPage.enterLimit(strLimit);
	}
	
	@Then("Click button having text {string}")
	public void click_button_having_text(String strName) {
		objMobileMyAccountPage.clickButtonHavingGivenText(strName);
	}
	
	@Then("Reduce deposit limit by one")
	public void Reduce_deposit_limit_by_one() {
		objMobileMyAccountPage.reduceDepositLimitByOne();
	}
	
	@Then("Increase deposit limit by one")
	public void Increase_deposit_limit_by_one() {
		objMobileMyAccountPage.increaseDepositLimitByOne();
	}
	
	@Then("^Click deposit limit button \"([^\"]*)\"$")
	public void click_deposit_limit_button(String strName) {
		objMobileMyAccountPage.clickDepositLimitButton(strName);
	}
}
