Feature: My Account

@MobileOptimized
Scenario: Check whether system displays Documents required pop up when “KYC check not possible“ response received while registration
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id with suffix ".kycrejected@mailinator.com" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then click Enter address manually button
Then Enter random address line1
Then Enter random city
Then Enter random county
Then Enter postal code "AB35 5XB"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify button having label as "Upload Documents" displayed
Then Verify button having label as "Live Help" displayed


@MobileOptimized
Scenario: Check whether  system displays error when user tries to register user with details who is already suspended/ blocked
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter "testman012@mailinator.com" email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify message "An error has occurred and has been logged, please contact system administrator"

@MobileOptimized @AndroidReg
Scenario: Check whether digital user gets registered successfully and deposited on Click of Register button on entering all valid details on Page 1 and Page 2
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "AB30 1JB"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@MobileOptimized @AndroidReg
Scenario: Check whether digital user gets registered successfully on Click of Register button on entering all valid details on Page 1 and Page 2
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "AB30 1JB"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@AndroidReg
Scenario: Check whether user gets self excluded and loghed out from site when user clicks on Self Exclude and Log out cta from confirmation pop up
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam04" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "Yes"
Then click button having label as "6 Months"
Then Enter password "Password123" for take a break
Then click button having label as "Yes, I want to Self Exclude"
Then Verify text "Are you sure?" displayed
Then Verify button having label as "Self exclude & Log out" displayed
Then click button having label as "Cancel"

@MobileOptimized @AndroidReg
Scenario: Check whether system do not apply changes deposit limits when user increases limits
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam02" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Click deposit limit button "Weekly"
Then Increase deposit limit by one
Then click button having label as "Set Limit"
Then Verify message "After 24 hours you will be able to activate your new limit."
Then click button having label as "Close"

@MobileOptimized @AndroidReg
Scenario: Check whether system changes deposit limits immidiatly when user decrese limits
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testman007" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Click deposit limit button "Monthly"
Then Reduce deposit limit by one
Then click button having label as "Set Limit"
Then Verify message "Your limits have been updated successfully"
Then click button having label as "Close"

@MobileOptimized
Scenario: Check whether system displays error message when user enters  limit equal or smaller than next higher limits
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam02" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Verify button having label as "£" displayed 
Then Verify button having label as "Set Limit" displayed 
Then Verify button having label as "Monthly" displayed 
Then Verify button having label as "Weekly" displayed 
Then Verify button having label as "Daily" displayed 
Then Click deposit limit button "Monthly"
Then Click button having text "Reset limit"
Then Enter limit "400"
Then Verify error message "Your monthly limit must be greater than your weekly limit"
Then Click deposit limit button "Weekly"
Then Click button having text "Reset limit"
Then Enter limit "40"
Then Verify error message "Your weekly limit must be greater than your daily limit"
Then Click deposit limit button "Daily"
Then Click button having text "Reset limit"
Then Enter limit "1"
Then Verify error message "Please enter an amount greater than 5"

@MobileOptimized
Scenario: Check whether user able to set new relaity check by selecting any of the options from list  on My Account Responsible gaming - Reality Checks page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testmeccam02" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then click button having label as "3 mins"
Then click button having label as "Save changes"
Then Click close 'X' icon on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then Verify "3 mins" reminder button is highlighted

@MobileOptimized
Scenario: Check whether user able to update options selected for marketting preferences from my account 
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then Click menu option "Marketing Preferences"
Then Verify "Marketing Preferences" as header displayed
Then Click on "post" checkbox on mobile
Then Verify "post" checkbox is checked on mobile
Then Click on "post" checkbox on mobile
Then Verify "post" checkbox is unchecked on mobile
Then Click on "all" checkbox on mobile
Then contact preferences checkboxes are checked
Then Click on "all" checkbox on mobile
Then contact preferences checkboxes are unchecked
Then click button having label as "Update"
Then Verify text "Account details updated" displayed

@MobileOptimized
Scenario: Check whether system displays account details page contains users information on Account Details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile
Then Verify following my account menu options:
| Header |
| Change Password |
| Marketing Preferences |

@MobileOptimized @AndroidReg
Scenario: Check whether system updates message count from Title section, Message sub menu icon and My account icon from header when user read / delete unread message
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then Read the message count
Then Open first message
Then Click delete button
Then Verify that message count is reduced
Then Click on back button
Then Read the message count
Then Open first message
Then Verify that message count is reduced

@MobileOptimized @AndroidReg
Scenario: Check whether user abet o delete message using Delete button from Message overlay
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then Open first message
Then Verify message in detail
Then Click delete button

@MobileOptimized @AndroidReg
Scenario: Check whether system displays Body section for message single view

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then Verify delete icon displayed
Then Open first message
Then Verify message in detail
Then Verify delete icon displayed
And Verify close 'X' icon on mobile
Then Verify 'Live help' link on mobile

@MobileOptimized @AndroidReg
Scenario: Check whether system displays header and body section for Messages overlay
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile
Then Verify 'Live help' link on mobile
Then Verify delete icon displayed
Then Verify messages table displayed

@MobileOptimized
Scenario: Check whether system displays new drop down list when user selects Net Deposits from filter option
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Net deposits" filter activity
Then Verify option "Last 24 Hours" displayed
Then Verify option "6 Months" displayed
Then Verify option "Last Week" displayed
Then Verify option "All Time" displayed
Then Select SortBy "Last 30 Days"

@MobileOptimized
Scenario: Check whether system displays Collapsed and uncollapsed version for Transactions
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Deposits" filter activity
Then Click button Filter
Then Collapse one record
Then Verify the collapsed record

@AndroidReg
Scenario: Check whether system displays Transactions History according to option selected from list 
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Deposits" filter activity
Then Click button Filter
Then Verify records are found of "Deposit"
Then Select "Withdrawals" filter activity
Then Click button Filter
Then Verify records are found of "Withdrawal"
Then Select "Withdrawals" filter activity
Then Click button Filter
Then Select "Net deposits" filter activity
Then Select "Bonuses" filter activity
Then Click button Filter

@MobileOptimized
Scenario: Check whether system displays Header and Body section on My Account Transaction 
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile
Then Verify 'Live help' link on mobile
Then Verify transaction filter box displayed
Then Verify dates are populated with today date
Then Verify transaction history box

@MobileOptimized
Scenario: Check whether user navigate to respective section on click of links from My Account > Balance overlay
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Balance"
Then Verify "Balance" as header displayed
Then Verify link "deposit_limits" of sub menu page
Then Verify 'Live help' link on mobile
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile
Then Verify bold text "Playable Balance" displayed
Then Verify message "Cash"
Then Verify message "All game Bonuses"
Then Verify button having label as "Deposit" displayed
Then Verify link "BonusHistory" of sub menu page

@MobileOptimized
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paypal and click on Deposit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Click saved card "davidh"
Then Enter amount to deposit "5"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@MobileOptimized
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Credit Card and click on Deposit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Click saved card "3139"
Then Enter CVV "123"
Then Enter amount to deposit "5"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@MobileOptimized @AndroidReg
Scenario: Check whether system navigate user to safecharge frame when user accept TnC For PoPF 
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "E1 6AN"
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Click close 'X' icon on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Verify message "We hold your balance in a designated bank account so that, in the event of insolvency, sufficient funds are always available for you to withdraw at any time. This represents the medium level of protection, based on the categories provided by the UK Gambling Commission"
Then Verify button having label as "Next" displayed
Then Check Agree Checkbox on mobile
Then click button having label as "Next"
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@MobileOptimized @AndroidReg
Scenario: Check whether system displays success message when clicks on Withdraw button from Safecharge iFrame  when user do not have active bonuses for account
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
Then Enter withdrawal amount "10"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then Verify text "Are you sure" displayed
Then click button having label as "Continue"
Then Click withdraw
Then Verify withdraw is successful 

@MobileOptimized @AndroidReg
Scenario: Check whether system displays success message when clicks on Withdraw button from Safecharge iFrame when user have active bonuses for account
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
Then Enter withdrawal amount "10"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then Verify text "Are you sure" displayed
Then click button having label as "Continue"
Then Click withdraw
Then Verify withdraw is successful 

@MobileOptimized
Scenario: Check whether system displays correct amount under Cash, Reward and Bonus Amount section in uncollapsed state
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Verify Balance block in expanded state

#Mobile EMC-1031
@Android @AndroidReg @MobileOptimized
Scenario: Check whether system dsipalys all required sections on My Account Overlay page when accesssed
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Username "automecca2020" as header displayed
Then Verify Balance block in Collapsed state
Then Verify following my account menu options:
| Cashier |
| Message |
| Bonuses |
| Account Details |
| Responsible Gambling |
Then Verify recently played menu exist
And Verify close 'X' icon on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile

@Android
Scenario: Check whether system naviate user to repective pages when user clicks on Links / Menu availabel on My Account Overlay

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on LiveChat
Then Switch to child window on mobile
Then Verify device navigate user to containing url "https://rank.secure.force.com/chat/"
Then Switch to parent window on mobile
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click on back button
Then Verify message count displayed
Then Click on logout button on mobile
Then Verify header displays Not a member yet? Register here link 

#Mobile EMC-1032
@Android 
Scenario: Check whether system dispalys all required sections on My Account > Cashier Overlay page when accesssed

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Verify following my account menu options:
| Deposit |
| Withdrawal |
| Transaction History |
| Balance |
And Verify close 'X' icon on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile

@Android
Scenario: Check whether system naviate user to repective pages when user clicks on Links / Menu availabel on My Account > Cashier Overlay

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
And Verify close 'X' icon on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile
Then Click menu option "Deposit"
Then Verify "Deposit" as header displayed
Then Click on back button
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
Then Click on back button
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Click on back button
Then Click menu option "Balance"
Then Verify "Balance" as header displayed

#Mobile EMC-1100

@Android
Scenario: System displays Balance block in Collapsed state when user access My Account Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Verify Balance block in Collapsed state

@Android
Scenario: System displays Balance block in expanded state when user access My Account Page and click on + button from Balance section
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Verify Balance block in expanded state

@Android
Scenario: system collapse balance block when user clicks on any part of the TOP SECTION of the Balance Box
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Verify Balance block in expanded state
Then Click top section of balance box
Then Verify Balance block in Collapsed state

@Android
Scenario: Check whether no action trigger whn user click on Cash and Bonuses title or amount from Balance section
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Click on cash title
Then Verify Balance block in expanded state
Then Click on bonuses amount
Then Verify Balance block in expanded state

@Android @MobileOptimized
Scenario: Check whether system navigate user to respective page when user clicks on Detailed View CTA and Deposit CTA
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on plus sign
Then Click on Detailed view button
Then Verify user navigate to balance overlay page
Then Click on Deposit button of balance page
Then Verify user navigate to deposit overlay page

#EMC-1030
@Android
Scenario: Check whether system displays Header and body on Change Password page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then Click menu option "Change Password"
Then Verify "Change Password" as header displayed
Then Verify 'Live help' link on mobile
Then Verify current password displayed
Then Verify new password displayed
Then Verify update button displayed
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile


#EMC-1030
@Android
Scenario: Check whether system allow user to enter current password and new password in respective fields
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then Click menu option "Change Password"
Then Verify "Change Password" as header displayed
Then Enter current password "Password123"
Then Verify green line displayed for current password
Then Enter new password "Password123"
Then Verify registration page displays following password guidelines with Right tick
|Please use at least one capital letter|
|Please use at least one number|
|Please use at least one lower case|
|Minimum 8 characters|


#EMC-1030
@Android
Scenario: Check whether Update CTA is active when user enters Current passsword and New password in valid format
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then Click menu option "Change Password"
Then Verify "Change Password" as header displayed
Then Verify update CTA is disabled
Then Enter current password "Password123"
Then Enter new password "Password123"
Then Verify update CTA is enabled


#EMC-1034
@Android
Scenario: Check whether system dsiplays Header Body and Footer section on My Account Responsible Gambling section
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
And Verify close 'X' icon on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile
Then Verify following my account menu options:
|Header|
|Reality Check|
|Deposit Limits|
|Take a break|
|Self Exclude|
|GamStop|


#EMC-1034
@Android
Scenario: Check whether system Navigate to respective Pages when click on Links available on Responsible Gambling page

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then Click on back button
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Click on back button
Then Click menu option "Take a break"
Then Verify "Take a break" as header displayed
Then Click on back button
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then Click on back button
Then Click menu option "GamStop"
Then Verify "GamStop" as header displayed

#EMC-165
@Android
Scenario: Check whether system displays header and body for Withdraw screen 1
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then Verify title "Choose amount of withdraw" of sub menu page
Then Verify available funds section of withdrawal sub menu page
Then Verify "input-password" field of sub menu page
Then Verify "input-amount" field of sub menu page
Then click button having label as "Next"
Then Verify message "How long?"
Then Verify message "Please note:"

@Android
Scenario: Check whether system displays info text for Withdraw screen 1
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Verify message "How long? Withdrawals can take 1 to 3 workings days to reach you depending on the payment method used to withdraw."
Then Verify message "Please note: we will not charge for deposits or withdrawals however, some card providers may impose a charge."

#EMC-165
@Android
Scenario: Check whether system allow user to enter withdraw amount in amount field from Withdraw screen 1

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "2"
Then Verify helptext "Minimum of £10, unless your balance is less than £10" displayed
Then click button having label as "Next"
Then Enter withdrawal amount ""
Then Enter withdrawal amount "0"
Then click button having label as "Next"
Then Enter withdrawal amount "7"
Then click button having label as "Next"
Then Verify error message "Withdrawal amount invalid. You must enter an amount more than £10"

#EMC-165
@Android
Scenario: Check whether system allow withdraw amount less than 10 only when it is equal to user's available funds balance amount

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "9"
Then click button having label as "Next"
Then Verify error message "Withdrawal amount invalid"

#EMC-165
@Android
Scenario: Check whether Next button gets active when Amount and Password fields entered correctly

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "12"
Then Enter withdrawal password "Password123"
Then Verify button having label as "Next" is enabled


#EMC-165
@Android
Scenario: Check whether system navigate user to Withdraw Screen 2 when click on Next button from Screen 1

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "12"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then Verify "Withdrawal" as header displayed


#EMC-1083
@Android
Scenario: Check whether system displays Header and body on withdraw Screen 2

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoandroid09" on mobile
Then User enters password "Password321" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password321"
Then click button having label as "Next"
Then click button having label as "Continue"
Then Verify "Withdrawal" as header displayed
Then Click withdraw
Then Verify withdraw is successful

@Android
Scenario: Check whether system navigate user to respective pages from Withdraw screen 2 on click

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoandroid09" on mobile
Then User enters password "Password321" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then click button having label as "Continue"
Then Verify "Withdrawal" as header displayed
Then Click on back button
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then Verify "Withdrawal" as header displayed
Then Click on back button
Then Verify "Cashier" as header displayed
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then Verify "Withdrawal" as header displayed


#EMC-1084
@Android
Scenario: Check whether system navigate user to respective pages from Withdraw screen 2 on click

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then Verify "Withdrawal" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile

@Android
Scenario: Check whether system displays success message when clicks on Withdraw button from Safecharge iFrame

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Withdrawal"
Then Enter withdrawal amount "11"
Then Enter withdrawal password "Password123"
Then click button having label as "Next"
Then click button having label as "Continue"
Then Click withdraw
Then Verify withdraw is successful
Then Click close button from deposit successful popup

#EMC-64
@Android
Scenario: Check whether system displays Header , Body and Safecharge iFrame is on Deposit page for user have already made a FTD (First time deposit)

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then Verify link "deposit_limits" of sub menu page
Then Verify 'Live help' link on mobile


@Android
Scenario: Check whether system displays Header , Body on Deposit page for user have never made deposit

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "E1 6AN"
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Click close 'X' icon on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Verify message "We hold your balance in a designated bank account so that, in the event of insolvency, sufficient funds are always available for you to withdraw at any time. This represents the medium level of protection, based on the categories provided by the UK Gambling Commission"
Then Verify button having label as "Next" displayed

@Android @AndroidReg
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Credit Card and click on Depsit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Click saved card "3139"
Then Enter CVV "123"
Then Enter amount to deposit "5"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@Android @AndroidReg
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paypal and click on Depsit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Click saved card "davidh"
Then Enter amount to deposit "5"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup

@Android @AndroidReg
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paysafe and click on Depsit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Click menu option "Deposit"
Then Click saved card "paysafe"
Then Enter amount to deposit "5"
Then Click on Deposit button from Deposit page
Then Switch to child window on mobile
Then Enter paysafe account "0000000009903207"
Then Click paysafe agree
Then click button having label as "Pay"
Then Switch to parent window on mobile
Then Verify deposit is successful
Then Click close button from deposit successful popup

#EMC - 167
@Android
Scenario: Check whether system displays success message for deposit  when user enters all correct details for Paysafe and click on Depsit button
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile
Then Verify following my account menu options:
| Header |
| Change Password |
| Marketing Preferences |


#EMC - 172
@Android
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Take a break page

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then Verify "Take a break" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then Verify 'Live help' link on mobile
Then Verify button having label as "1 Day" displayed
Then Verify button having label as "2 Days" displayed
Then Verify button having label as "6 Weeks" displayed
Then click button having label as "1 Day"
Then Verify message "Locked until: "
Then Verify button having label as "Take A Break" displayed
Then Verify password field displayed for take a break page

@Android
Scenario: Check whether system displays Text box along with “Why to take a break“ Box + a collapse/uncollapse icon on My Account Responsible gaming - Take a break page

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then Verify "Take a break" as header displayed
Then Verify message "If you would like to take a temporary break from gambling, you can choose a break period from 1 day up to 6 weeks"
Then Click on plus sign of take a break page
Then Verify message "If you wish to take a break for a different length of time, please contact Customer Support and a member of our team will be able to help you. By choosing to take a break, you will not be able to access your account and you will be prevented from gambling until your break period has finished."
Then Verify message "You may extend your break period at any time by contacting our Customer Support Team."
Then Verify message "Your break will end automatically once the selected time period has passed and your account will be re-opened at this time."


#EMC-172
@Android
Scenario: Check whether system displays Take a break confirmation pop up when user selects any of the option for Take a break and Password on My Account Responsible gaming - Take a break page and click on Take a break CTA

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "tester2344" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then Verify "Take a break" as header displayed
Then click button having label as "1 Day"
Then Enter password "Password123" for take a break
Then click button having label as "Take A Break"
Then Verify message " you are choosing to start your break. Your account will be locked until "
Then Verify button having label as "Take a break & Log Out" displayed
Then Verify button having label as "Cancel" displayed


@Android @AndroidReg
Scenario: Check whether user gets logged out from Site and unabel to login back till break time completed on click of “Take a break & Log Out” CTA from Confirmation Pop up

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "tester2344" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then Verify "Take a break" as header displayed
Then click button having label as "1 Day"
Then Enter password "Password123" for take a break
Then click button having label as "Take A Break"
Then click button having label as "Take a break & Log Out"
Then Verify header displays Not a member yet? Register here link
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "tester2344" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify text "You have chosen to take a break from Mecca Bingo. You can login again from" displayed

#EMC-175
@Android
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Reality Checks page

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testman007" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then Verify 'Live help' link on mobile
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then Verify message "Set your reminder"
Then Verify button having label as "1 mins" displayed
Then Verify button having label as "1 hour" displayed
Then Click on plus sign of reality check
Then Verify button having label as "Save changes" displayed

@Android
Scenario: Check whether system displays Text box on My Account Responsible gaming - Reality Checks page

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "testman007" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then Verify message "Reality Checks are a helpful way of keeping track of the time you have spent playing our games."
Then Verify message "You can choose how often you would like to receive a Reality Check below. You will see a reminder each time you reach your chosen Reality Check time interval during your gaming session and you will have the option to continue with your game or stop playing."
Then Verify message "Your gaming session will begin when you place your first real money bet and will end when you log out"
Then Click on plus sign of reality check
Then Verify message "Some games will also offer you the facility to set a Reality Check within the game and your in-game Reality Check preferences will apply to the time you spend playing these games."
Then Verify message "You can change your Reality Check settings at any time by selecting a new interval below. Any changes to your Reality Check settings after your gaming session has started will take effect the next time you login to the site."

@Android
Scenario: Check whether user able to set new relaity check by selecting any of the options from list  on My Account Responsible gaming - Reality Checks page

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then click button having label as "1 mins"
Then click button having label as "Save changes"
Then Click close 'X' icon on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Reality Check"
Then Verify "Reality Check" as header displayed
Then Verify "1 mins" reminder button is highlighted

@Android @AndroidReg
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Gamstop page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "GamStop"
Then Verify "GamStop" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
And Verify message "If you are considering self-exclusion, you may wish to register with GAMSTOP."
And Verify message "GAMSTOP is a free service that enables you to self-exclude from all online gambling companies licensed in Great Britain."

@Android @AndroidReg
Scenario: Check whether system displays Text box on My Account Responsible gaming - Gamstop page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "GamStop"
Then Verify "GamStop" as header displayed
Then click link "http://www.gamstop.co.uk/"
Then Switch to child window on mobile
Then Verify device navigate user to "https://www.gamstop.co.uk/" url

#EMC-223
@Android
Scenario: Check whether user able to access my account from Game window
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify system loads game window for selected game in real mode
Then Click on my account of game
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode

@Android
Scenario: Check whether user able to continue game play after completing any actions from My Account Overlay
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "slots" menu
Then Click on Play Now button from Game
And Verify system loads game window for selected game in real mode
Then Click on my account of game
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Deposit"
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode
Then Click on my account of game
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode
Then Click on my account of game
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode


@Android
Scenario: Check whether user gets logged out from game window when user Take a break
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoseandroid" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Take a break"
Then Verify "Take a break" as header displayed
Then click button having label as "1 Day"
Then Enter password "Password123" for take a break
Then click button having label as "Take A Break"
Then click button having label as "Take a break & Log Out"
Then Verify header displays Not a member yet? Register here link


@Android
Scenario: Check whether  System displays Header and Body on My Account > Enter bonus code [Screen 1] page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Bonuses"
Then Verify "Bonuses" as header displayed
Then click button having label as "Enter bonus code"
Then Verify "Bonuses" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then Verify 'Live help' link on mobile
Then Verify "bonus-code" field of sub menu page
Then Verify button having label as "Submit" displayed
Then Click on back button
Then Verify "Bonuses" as header displayed
Then Click close 'X' icon on mobile
Then Verify balance section is displayed

@Android
Scenario: Check whether  System displays Submit CTA in active state when user enters Bonus code in Bonus code field
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Bonuses"
Then Verify "Bonuses" as header displayed
Then click button having label as "Enter bonus code"
Then Verify "Bonuses" as header displayed
Then Enter bonus code "Rtt"
Then Verify button having label as "Submit" is enabled

@Android
Scenario: Check whether  System Navigates to approprite pages on My Account > Bonuses
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Bonuses"
Then Verify "Bonuses" as header displayed
Then click button having label as "Enter bonus code"
Then Verify "Bonuses" as header displayed
Then Click on back button
Then Verify "Bonuses" as header displayed
Then click button having label as "Active bonuses"
Then Verify "Active bonuses" as header displayed
Then Click on back button
Then Verify "Bonuses" as header displayed
Then click button having label as "Bonus History"
Then Verify "Bonus History" as header displayed

@Android
Scenario: Check whether System displays Header and Body for My Account - Responsible gaming - Self Exclusion screen 1
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then Verify button having label as "Yes" displayed
Then Verify button having label as "No" displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile

@Android
Scenario: Check whether  System displays different options for My Account - Responsible gaming - Self Exclusion screen 2(NO)
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "No"
Then Verify button having label as "Reality Check" displayed
Then Verify button having label as "Deposit Limits" displayed
Then Verify button having label as "Take a Break" displayed

@Android @AndroidReg
Scenario: Check whether  System displays different options for My Account - Responsible gaming - Self Exclusion screen 2(Yes)
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoandroid02" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "Yes"
Then Verify "Self Exclude" as header displayed
Then Verify bold text "How long do you want to lock your account for?" displayed
Then Verify button having label as "6 Months" displayed
Then Verify button having label as "1 Year" displayed
Then Verify button having label as "2 Years" displayed
Then Verify button having label as "5 Years" displayed
Then click button having label as "6 Months"
Then Verify message "Locked until: "
Then Enter password "Password123" for take a break
Then Verify button having label as "Yes, I want to Self Exclude" displayed

@Android @AndroidReg
Scenario: Check whether  System displays Header and Body on My Account > Edit Details Page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then click button having label as "Edit"
Then Verify "Edit Details" as header displayed
Then Verify Address Line1 populated with "5 Queensway"
Then Verify City populated with "Leeds"
Then Verify County populated with "West Yorkshire"
Then Verify Postcode populated with "LS25 1AZ"
Then Verify "password" field of sub menu page
Then Verify button having label as "Update" displayed
Then Click on back button
Then Verify "Account Details" as header displayed

@Android
Scenario: Check whether  user able to edit details from My Account > Edit Details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then click button having label as "Edit"
Then Verify "Edit Details" as header displayed
Then click "1" clear button
Then Enter random email id on mobile
Then click "2" clear button
Then Enter address line1 "1 Owen Close"
Then Enter city "Fareham"
Then Enter county "Hampshire"
Then Enter postal code "HP21 9NU"
Then Enter paypal password "Password123"
Then click button having label as "Update"
Then Verify text "Account details updated" displayed


@Android
Scenario: Check whether user able select populated address for address fields from My Account > Edit Details Page 
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then click button having label as "Edit"
Then Verify "Edit Details" as header displayed
Then click button having label as "Clear"
Then click button having label as "Enter address manually"
Then Enter address line1 "1 Owen Close"
Then Enter city "Fareham"
Then Enter county "Hampshire"
Then Enter static postal code "PO16 7GZ"

@Android
Scenario: Check whether changed details gets saved for customer account on click of Update CTA
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then click button having label as "Edit"
Then Verify "Edit Details" as header displayed
Then click button having label as "Clear"
Then click button having label as "Enter address manually"
Then Enter address line1 "1 Owen Close"
Then Enter city "Fareham"
Then Enter county "Hampshire"
Then Enter postal code "HP21 9NU"
Then Enter paypal password "ssword123"
Then click button having label as "Update"
Then Verify error message "Forgotten your password? You can reset it"
Then Enter paypal password "Password123"
Then click button having label as "Update"
Then Verify text "Account details updated" displayed

@Android
Scenario: Check whether  System navigat eon respective pages on click of options for My Account - Responsible gaming - Self Exclusion screen 2(NO)
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "No"
Then Verify button having label as "Reality Check" displayed
Then Verify button having label as "Deposit Limits" displayed
Then Verify button having label as "Take a Break" displayed
Then Verify information text "Alternatively, please get in touch if you want to Self Exclude for different reasons on 0800 083 1990" displayed
Then click button having label as "Reality Check"
Then Verify "Reality Check" as header displayed
Then Click on back button
Then click button having label as "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Click on back button
Then click button having label as "Take a Break"
Then Verify "Take a break" as header displayed

@Android
Scenario: Check whether  System displays text box on Self Exclusion Screen 2(yes)
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "Yes"
Then Verify message "You can block yourself from playing with Mecca for a chosen period of time. This can only be reversed by written request after that period has ended."
Then Click on plus sign of self exclude
Then Verify message "If you believe you might have a problem with gambling, it is advisable to stop gambling altogether, or for a prolonged period. We would also recommend that you seek guidance from the support agencies listed on our keepitfun website."


@Android
Scenario: Check whether  System displays extended version when user select  one of the 4 options: 6 months/ 1 year/ 1 years/ 5 years from My Account - Responsible gaming - Self Exclusion screen 2(Yes)
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoandroid01" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "Yes"
Then click button having label as "6 Months"
Then Verify message "Locked until: "
Then Verify password field displayed for Self exclusion
Then Verify button having label as "Yes, I want to Self Exclude" displayed


@Android
Scenario: Check whether  System displays Self Exclude Confirmation pop up when user selects Self Exclude CTA from overly page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoandroid01" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "Yes"
Then click button having label as "6 Months"
Then Enter password "Password123" for take a break
Then click button having label as "Yes, I want to Self Exclude"
Then Verify text "Are you sure?" displayed
Then Verify button having label as "Self exclude & Log out" displayed
Then click button having label as "Cancel"

@Android
Scenario: Check whether system displays Top section and bottom section on Balance box section for My Account > Balance overlay

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Balance"
Then Verify "Balance" as header displayed
Then Verify link "deposit_limits" of sub menu page
Then Verify 'Live help' link on mobile
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile
Then Verify bold text "Playable Balance" displayed
Then Verify message "Cash"
Then Verify message "All game Bonuses"
Then Verify button having label as "Deposit" displayed
Then Verify link "BonusHistory" of sub menu page

@Android
Scenario: Check whether user navgate to respective section on click of links from My Account > Balance overlay
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Balance"
Then Verify "Balance" as header displayed
Then click button having label as "Deposit"
Then Verify "Deposit" as header displayed
Then Click on back button
Then click link "deposit_limits"
Then Verify "Deposit Limits" as header displayed
Then Click on back button
Then click link "BonusHistory"
Then Verify "Bonus History" as header displayed


@Android
Scenario: Check whether system displays Head and bosy for Deposit Limit page

Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoandroid03" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
And Verify close 'X' icon on mobile
And Verify back arrow '<' on mobile
Then Verify 'Live help' link on mobile
Then Verify message "We recommend that you set deposit limits to help you manage your spending"
Then Verify bold text "Current limit" displayed
Then Verify "Daily" reminder button is highlighted
Then Verify button having label as "Weekly" displayed
Then Verify button having label as "Monthly" displayed

@Android
Scenario: Check whether system displays Extended version for Deposi Limit page when no pending limit available

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "autoandroid03" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Verify button having label as "£" displayed 
Then Verify button having label as "Set Limit" displayed 
Then Verify button having label as "Monthly" displayed 
Then Verify button having label as "Weekly" displayed 
Then Verify button having label as "Daily" displayed 
