Feature: Registration

#Mobile EMC-59
@Android
Scenario: Check whether all marketing preference checkboxes (Email, SMS, Phone and Post) are checked when user select 'Select All' CTA from marketing preference section

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Click on "SelectAll" Button on mobile
Then contact preferences checkboxes are checked

@Android
Scenario: Check whether user able to select single marketing preference checkboxes (Email, SMS, Phone and Post) from marketing preference section

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Click on "post" checkbox on mobile
Then Verify "post" checkbox is checked on mobile

#Mobile EMC-863

@Android
Scenario: Check whether system displays green line below the username field when user enters username in valid format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Verify green line below username field displayed


@Android
Scenario: Check whether system displays error message when user enters the existing username in username field

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter existing email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify general error is displayed


#Mobile EMC-871

@Android
Scenario: Check whether Marketing preference checkboxes  are displayed in red color and displays err below fields when user do not select any of the option from Marketing preference section and click on Register button

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Click on "Register" Button on mobile
Then Verify red background color displayed for "email" checkbox on mobile
Then Verify red background color displayed for "sms" checkbox on mobile
Then Verify red background color displayed for "phone" checkbox on mobile
Then Verify red background color displayed for "post" checkbox on mobile
Then Verify red background color displayed for "not-receive" checkbox on mobile
Then Verify please select contact preference error displayed on mobile

#Mobile EMC-810

@Android
Scenario: Check whether error message displayed under respective fields when user clicks on next button on first step registration page

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Click on Next Button on mobile
Then Verify error message "You must enter a valid email address"
Then Verify error message "You must agree to the terms and conditions in order to continue"


@Android
Scenario: Check whether help text disappear and error message display when user  enters invalid email address on first step registration screen and leave field

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter invalid email id on mobile
Then Verify error message "You must enter a valid email address"
Then Verify message "Please enter a valid email address so we can verify your registration." not displayed


#Mobile EMC-61
@Android
Scenario: Check whether below fields are displayed on second step of registration journey:

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify registration page displays following options
|First name|
|Sur name|
|Title|
|Date of birth|

@Android
Scenario: Check whether user able to select any option from Title section

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Click on "Mr" title on mobile
Then Verify "Mr" title is selected

@Android
Scenario: Check whether green line is displayed when user enter the first name in correct format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter firstname "wer" on mobile
Then Verify green line displayed for firstname

@Android
Scenario: Check whether green line is displayed when user enter the surname in correct format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter surname "wer" on mobile
Then Verify green line displayed for surname

@Android
Scenario: Check whether green line is displayed when user enter the DOB in correct format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter DOB "29-08-1990" on mobile
Then Verify green line displayed for DOB


#Mobile EMC-62
@Android
Scenario: Check whether below fields are displayed on second step of registration journey:
-Username
-Password

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify registration page displays following options
|Username|
|Password|

@Android
Scenario: Check whether system displays green line below the username field when user enters username in valid format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter random username
Then Verify green line is displayed for username

@Android
Scenario: Check whether system displays green line below the password field when user enters the password in correct format

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter password "Password123"
Then Verify green line is displayed for password

@Android
Scenario: Check whether system displays grey line along with password guidance below the password field:
- Please use at least one capital letter
- Please use at least one number
- Please use at least one lower case
- Minimum 8 characters

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Enter password " "
Then Verify registration page displays following password guidelines
|Please use at least one capital letter|
|Please use at least one number|
|Please use at least one lower case|
|Minimum 8 characters|


#Mobile EMC-63
@Android
Scenario: Check whether Yes option is selected by default on first step registration page

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Verify button "Yes" selected


#Mobile EMC-63

@Android
Scenario: Check whether system displays Registration Page 2 when user enters valid email address n tick TnC check box and click on Next button

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed

@Android
Scenario: Check whether green line is displayed below email address field when user enter the email address in correct format
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Verify green line below username field displayed

@Android
Scenario: Check whether system displays error message and displays Login screen when user enters existing Email address and click on Next button
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter existing email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify general error is displayed

#Mobile EMC-1026
@Android @AndroidReg
Scenario: Check whether system displays Documents required pop up when “KYC check not possible“ response received while registration

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id with suffix ".kycrejected@mailinator.com" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then click Enter address manually button
Then Enter random address line1
Then Enter random city
Then Enter random county
Then Enter postal code "AB35 5XB"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify button having label as "Upload Documents" displayed
Then Verify button having label as "Live Help" displayed



@Android
Scenario: Check whether system open live chat iframe in a separate window when user clicks on Live Chat option from Documenst required pop up

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id with suffix ".kycrejected@mailinator.com" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "AB25 2QJ"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify button having label as "Upload Documents" displayed
Then Verify button having label as "Live Help" displayed
Then click button having label as "Live Help"
Then Switch to child window on mobile
Then Verify device navigate user to "https://rank.secure.force.com/chat?cid=VerificationMecca#/liveSupport" url


@Android
Scenario: Check whether system open Upload documents iframe in a separate window when user clicks on Live Chat option from Documenst required pop up

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id with suffix ".kycrejected@mailinator.com" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "AB16 5HW"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify button having label as "Upload Documents" displayed
Then Verify button having label as "Live Help" displayed
Then click button having label as "Upload Documents"
Then Switch to child window on mobile
Then Verify device navigate user to "https://rank.secure.force.com/chat?cid=VerificationMecca#/phoneEmail" url

@Android
Scenario: Check whether system do not display Dr and Mx fro title while regsitration
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify button having label as "Dr" not displayed


@Android
Scenario: Check whether  System displays Header and Footer for Registration Screen 1
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Verify text "Sign Up" displayed
Then Verify help text displayed
And Verify back arrow '<' on mobile
Then Click on back button
Then Verify agree checkbox is unchecked

#EMC-1400
@Android
Scenario: Check whether system allow user to enter Special Charcaters in Password field
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Pas@word123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "AB16 5HW"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed