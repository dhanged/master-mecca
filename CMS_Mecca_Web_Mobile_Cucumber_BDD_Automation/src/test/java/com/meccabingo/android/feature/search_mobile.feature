Feature: Search
@MobileOptimized @AndroidReg
Scenario: Check whether system displays Search results for Clubs for Valid Text and Include clubs in search result checkbox is ON 
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Mecc" in Search field from header on Mobile
Then Check checkbox of include clubs in search result
Then Verify system displays search results to user

@MobileOptimized
Scenario: Check whether system displays Search overlay with all required fields
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Chilli Con" in Search field from header on Mobile
Then Verify device displays search results to user
Then Click button having text "Clear"
Then Verify user able to view 'Quick Links'

@MobileOptimized @AndroidReg
Scenario: Check whether system displays Search results for Slots Games for Valid Text
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Chi" in Search field from header on Mobile
Then Verify system displays search results to user 

@MobileOptimized @AndroidReg
Scenario: Check whether system displays Search results for bingo Games for Valid Text
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Emoji" in Search field from header on Mobile
Then Verify system displays search results to user  

#EMC-217 : Search tile

@Android 
Scenario: Check whether system displays following options for search title:
-Image
-Name
-""Play Now"" CTA
-”Info"" CTA"

Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Chilli Con" in Search field from header on Mobile
Then Verify device displays search results to user
Then Verify device displays following options for search title:
|Header|
|Image|
|Name|
|Play Now CTA|
|Info CTA|



#EMC-30 : Header - Search functionality

@Android 
Scenario: Check whether search trigger when user enters three or more characters in search field

Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
Then User enters three characters in search filed on Mobile
Then Verify device displays search result section


#EMC-563 : Search functionality - Quick links/ what others are playing

@Android
Scenario: Check Whether user able to view "Quick Links" and "What others are playing" section in search overlay till user enters 3 or more characters in search field
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "" in Search field from header on Mobile
Then Verify user able to view 'Quick Links'
Then Search Game "Ex" in Search field from header on Mobile
Then Verify user able to view 'Quick Links'
Then Search Game "Extra " in Search field from header on Mobile
Then Verify device displays search result section

#EMC-212
@Android @AndroidReg
Scenario: Check whether system displays Game / bingo details Page when user clicks on I button from Search overlay
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Jumanji" in Search field from header on Mobile
Then Click on i button from searched Game
Then Verify game detail page open successfully

#EMC-238
@Android @AndroidReg
Scenario: Check whether user able Navigate back to Page from where registration Journey started without deposit after clicking on play button in non logged in mode

Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Extra Fruity" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP3 9TY"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
And Verify system loads game window for selected game in real mode

#EMC-237
@Android @AndroidReg
Scenario: Check whether system launch searched game after completing registration journey with deposit
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Extra Fruity" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "HP14 4DJ"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
And Verify system loads game window for selected game in real mode
