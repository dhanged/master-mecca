Feature: Clubs

@AndroidReg
Scenario: Check whether System displays confirmation message pop up when user clicks on “Favourite“ CTA from Club details page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Rochadale"
Then Select first club that appears in list
Then Click on more Info
Then Mark it as favorite
Then Verify text "Next time you click on the club section, Mecca Rochdale will appear as your homepage. Happy to go ahead and favourite it?" displayed

@AndroidReg
Scenario: Check whether System displays confirmation message pop up after successful login when user clicks on “Favourite“ CTA in logged out mode
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Rochadale"
Then Select first club that appears in list
Then Click on more Info
Then Mark it as favorite
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Verify text "Next time you click on the club section, Mecca Rochdale will appear as your homepage. Happy to go ahead and favourite it?" displayed
Then click button having label as "Yes"
Then Verify club marked as favorite

@AndroidReg
Scenario: Check whether System displays unfavourite“ the club message pop up when user clicks on “Favourite“ CTA from Club details page which is already added in favourities
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Rochadale"
Then Select first club that appears in list
Then Click on more Info
Then Unfavorite it
Then Verify text "This club will be removed from your favourites and will no longer appear as your club homepage. Want to carry on?" displayed
Then click button having label as "Yes"
Then Verify club favorite tag is removed

@AndroidReg
Scenario: Check whether User able to join favourite club using Join Club Cta available on club details page
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Oldham"
Then Select first club that appears in list
Then Click on more Info
Then Click link having text as "Join Club"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter postal code "HP16 0EE"
Then Enter join club email "automecca2020@mailinator.com"
Then Enter Home phone "023446789"
Then Enter random mobile number
Then Click on "SelectAll" Button on mobile
Then Check Agree Checkbox on mobile
Then click button having label as "Join your local club"
Then Verify user is joined club successfully