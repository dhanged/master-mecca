Feature: Registration - Membership


#Mobile EMC-45

@Android
Scenario: Check whether system displays “Membership number sign up” screen with “Card membership number”, “Date of Birth” fields, "Terms & Conditions" checkbox, "Next" CTA on click of “No (I have a membership card)” CTA from First step of Registration page

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Verify membership sign up screen

@Android
Scenario: Check whether system displays green boarder for “Card membership number” and DOB fields when user enters Membership number and DOB in correct format on “Membership number sign up” screen
 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Enter membership number "123456789"
Then Verify green line displayed for membership number
Then Enter DOB "29-08-1990" on mobile
Then Verify green line displayed for DOB


@Android
Scenario: Check whether Next Cta gets active when user enters membership number and DOB in correct format and selected “Terms & Conditions”  checkbox on “Membership number sign up” screen 
 
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Enter membership number "123456789"
Then Enter DOB "29-08-1990" on mobile
Then Check Agree Checkbox on mobile
Then Verify next button is enabled


#This week needs to completeWeek
@NewReg
Scenario: Check whether system displays Accoutn details and Registration Form for entered membership user when Number and DOB entered correctly
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Enter membership number "907277185"
Then Enter DOB "29-08-1990" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify message "Title"
Then Verify message "Name"
Then Verify message "Date of Birth"
Then Enter random username
Then Enter password "Password123"
Then Click on "SelectAll" Button on mobile


#Mobile EMC-801
@Android
Scenario: if membership user with membership details are not found with backend then  the system should display an error message below the “Card membership number” field as Unable to get customer details
 
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Click on No button
Then Enter membership number "907277153"
Then Enter DOB "29-08-1990" on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify message "Something’s not quite right. Contact"
