Feature: Footer


#Mobile EMC-22

@Android @AndroidReg
Scenario: Check whether system navigate user to respective URL when user clicks on any block

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on Facebook block from footer on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://m.facebook.com/MeccaBingo" url
Then Switch to parent window on mobile

Then Click on Instagram block from footer on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "https://www.instagram.com/"
Then Switch to parent window on mobile

Then Click on Twitter block from footer on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://mobile.twitter.com/MeccaBingo" url
Then Switch to parent window on mobile

Then Click on Youtube block from footer on Mobile 
Then Switch to child window on mobile
Then Verify device navigate user to "https://m.youtube.com/user/MeccaBingoClubs" url

#Mobile EMC-27

@Android
Scenario: Check whether system navigate to appropriate URL when user clicks on any Link

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Privacy Policy Link on Mobile
Then Switch to child window on mobile 
Then Verify device navigate user to "https://qa01-mecc-cms2.rankgrouptech.net/privacy-policy" url
Then Switch to parent window on mobile

Then Click on Terms and Conditions Link on Mobile
Then Switch to child window on mobile 
Then Verify device navigate user to "https://qa01-mecc-cms2.rankgrouptech.net/terms-and-conditions" url
Then Switch to parent window on mobile

Then Click on Affiliates Link on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://www.rankaffiliates.com/" url
Then Switch to parent window on mobile


Then Click on Mecca Club Terms Link on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://qa01-mecc-cms2.rankgrouptech.net/club-terms-and-conditions" url
Then Switch to parent window on mobile


Then Click on Play Online Casino Link on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://www.grosvenorcasinos.com/" url
Then Switch to parent window on mobile

Then Click on Mecca Blog Link on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "https://blog.meccabingo.com/"


#EMC-36 : Footer - GamCare, 18, GamStop, Gambling commision, Gambling control Logos

@Android  
Scenario: Check whether user able to view following logos at Footer section:
-GamCare
-18
-GamStop
-Gambling commision
-Gambling control
-IBAS
-ESSA

Given Invoke the mecca site on Mobile in LandScape Mode
Then Verify user able to view following logos at Footer in partners block on mobile:
|ESSA|
|IBAS|
|18|
|Gambling Control|
|GamCare|
|GamStop|
|Gambling Commission|


@Android @AndroidReg
Scenario: Check whether user able to navigate to configured URL when user clicks on any of the Logo

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on "ESSA" logo on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://www.essa.uk.com/" url
Then Switch to parent window on mobile

Then User clicks on "IBAS" logo on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://www.ibas-uk.com/" url
Then Switch to parent window on mobile

Then User clicks on "Gambling Control" logo on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://www.gamblingcontrol.org/" url
Then Switch to parent window on mobile

Then User clicks on "GamCare" logo on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://www.gamcare.org.uk/" url
Then Switch to parent window on mobile

Then User clicks on "GamStop" logo on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://www.gamstop.co.uk/" url
Then Switch to parent window on mobile

Then User clicks on "Gambling Commission" logo on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to "https://www.gamblingcommission.gov.uk/Home.aspx" url


#EMC-272 : Footer - Verisign Secured

@Android @AndroidReg
Scenario: Check whether user able to view Verisign Secured logo at Footer section
Given Invoke the mecca site on Mobile in Portrait Mode
Then Verify user should be able to view Verisign Secured logo at Footer section on Mobile

#EMC-23 : Footer - Trusted Partners - Payment methods

@Android 
Scenario: Check whether system displays following section for Trusted partners components:
-Title
-Description 
-Logos

Given Invoke the mecca site on Mobile in LandScape Mode
Then Verify device displays following section for payment providers components:
|Title|
|Description|
|Logos|

#EMC-10 : Footer - Rank group
@Android
Scenario: Check whether user navigate to Configured link on click of hyperlinked text

Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Alderney Gambling Control Commission link on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "https://www.gamblingcontrol.org"


Then Switch to parent window on mobile
Then User clicks on UK Gambling Commission link on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "https://www.gamblingcommission.gov.uk"

Then Switch to parent window on mobile
Then User click on BeGambleAware link on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "https://www.begambleaware.org"

Then Switch to parent window on mobile
Then User clicks on Rank Group link on Mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "https://www.rank.com"