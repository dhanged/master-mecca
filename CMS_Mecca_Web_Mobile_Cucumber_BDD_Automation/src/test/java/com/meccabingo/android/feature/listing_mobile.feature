Feature: Listing

#EMC-688
@Android
Scenario: Check whether system displays Carousel,Title,Tabs,Game panel block and Generic content block (Text)
Given Invoke the mecca site on Mobile in Portrait Mode
Then Verify below components
|HEADER|
|Carousel|
|Tabs|
|Bingo Lobby CTA|

@Android
Scenario: Check whether user able to select Tabs from Listsing pages
Given Invoke the mecca site on Mobile in LandScape Mode
Then Navigate through hamburger to "slot" menu


#EMC-1131
@Android
Scenario: Check whether system displays configured sections on bingo listing page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Verify below components
|HEADER|
|Carousel|
|Tabs|
|Bingo Lobby CTA|

@Android
Scenario: Check whether system loads respective games configured for Tabs selected on Bingo listing page 
Then Navigate through hamburger to "bingo" menu
Then user should remain on bingo container page