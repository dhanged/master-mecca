Feature: Registration - Digital

@AndroidReg
Scenario: Check whether  system displays "Your account is suspended. Please contact customer support. " error when user tries to register user with details who is already suspended/ blocked
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter join club email "testman012@mailinator.com"
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify error message "Your account is suspended. Please contact customer support."

@Android
Scenario: Check whether system displays correct validation when user selects Country field on Digital Registration Page 2

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Select country "Gibraltar"
Then Select country "United Kingdom"
Then click Enter address manually button


@Android
Scenario: Check whether system displays correct validation when user selects Country field on Digital Registration Page 2

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter postal code ""
Then Verify help text "Start typing your postcode to find your full address" displayed for postcode
Then Verify grey color displayed for postcode
Then Enter static postal code "PO167GZ"
Then Verify Address Line1 populated with "1 Owen Close"
Then Verify City populated with "Fareham"
Then Verify County populated with "Hampshire"
Then Verify Postcode populated with "PO16 7GZ"

@Android
Scenario: Check whether system displays correct validation when user selected Enter address manually option for entering Address on Digital Registration Page 2

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then click Enter address manually button
Then Verify postcode search button displayed
Then Enter address line1 "1 Owen Close"
Then Verify green line displayed for address line1
Then Enter city "Fareham"
Then Verify green line displayed for city
Then Enter county "Hampshire"
Then Verify green line displayed for county
Then Enter postal code "PO16 7GZ"
Then Verify green color displayed for postcode

@Android
Scenario: Check whether system displays correct validation for Mobiel Number field on Digital Registration Page 2
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter mobile number "07"
Then Verify grey line displayed for mobile number
Then Verify help text "Please enter your contact phone number" displayed for postcode
Then Enter mobile number "911123456"
Then Verify green line displayed for mobile number


@Android
Scenario: Check whether system displays correct validations for postcode when user enters incomplete or invalid postcode on Digital Registration page 2
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter postal code "PO1"
Then Verify error message "Too many results, please enter more characters"
Then Enter postal code "234123"
Then Verify error message "Too many results, please enter more characters"

@Android
Scenario: Check whether system displays correct validations for Address fields when user select enter address manually option on Digital Registration page 2

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then click Enter address manually button
Then Click on "Register" Button on mobile
Then Verify error message "Please enter a valid address"
Then Verify error message "Please enter a valid town or city"
Then Verify error message "Please enter your postcode"

@Android
Scenario: Check whether system displays correct validations for Mobiel number fields when entered incorrect or incomplete information on Digital Registration page 2

Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter mobile number "07911123456123456"
Then Click on "Register" Button on mobile
Then Verify error message "Please enter a valid phone number"

@Android @AndroidReg
Scenario: Check whether user able deposit after successful digital Registration
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "AB30 1JB"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup


@Android @AndroidReg
Scenario: Check whether user able deposit after successful digital Registration

Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "BS9 4RG"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed


@Android
Scenario: Check whether  System displays Header and Footer for Digital Registration Screen 2
Given Invoke the mecca site on Mobile in LandScape Mode
Then Click on "JoinNow" Button on mobile
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
And Verify back arrow '<' on mobile
Then Click on back button
Then Verify agree checkbox is unchecked
Then Verify Email address textbox is not empty