Feature: Blocks

#EMC-312
@Android
Scenario: Check whether in configured Three coulumns blcok section whether all 3 blocks are displaying one below other in Mobile Landscape mode
Given Invoke the mecca site on Mobile in Portrait Mode
Then Verify blocks contain one row
Then Verify all three blocks

#EMC-310
@Android
Scenario: Check whether each  generic content blocks are displayed one below another under battenberg component on Mecca Bingo page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Verify card is displayed in battenberg block

#Mobile EMC-1218
@Android 
Scenario: Check whether system allwo user to Add Anchor Links from CMS
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate to Auto tab of Slot games
Then Click on back to top button
Then Verify header displays join now button

@AndroidR
Scenario: Check whether system launch bingo lobby in separate window when user clicks on Join Now button from Bingo Tile  

Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"


@Android
Scenario: Check whether system displays Winners feed on Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Verify winners feed available with multiple boxes

@Android
Scenario: Check whether system displays back on top CTA on all pages irrespective of any anchor links settings
Given Invoke the mecca site on Mobile in LandScape Mode
Then scroll page till footer of the page
Then Verify back on top CTA is displayed
Then Click on back to top button

@Android
Scenario: Check whether system displays back on top CTA on all pages irrespective of any anchor links settings
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then scroll page till footer of the page
Then Verify back on top CTA is displayed
Then Click on back to top button
