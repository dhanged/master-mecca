Feature: Promotion

@Promo1
Scenario: Check whether system displays Learn more CTA, Claim CTA, T&Cs hyperlink on promotion tile when use rnot cliamed promotion
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "promotions" menu
Then click learn more
Then Navigate back to window on Mobile
Then Verify claim button displayed
Then click claim button
Then click button opt out
Then click button yes
Then Verify claim button displayed


@Promo1
Scenario: Check whether user able to cliam promotion using Claim Cta available on Promotion Tile
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "promotions" menu

@PromoNew
Scenario: Check whether system displays Claim CTA on Promotion details page when user not yet claimed promotion
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "promotions" menu
Then click learn more
Then Navigate back to window on Mobile
Then Verify claim button displayed