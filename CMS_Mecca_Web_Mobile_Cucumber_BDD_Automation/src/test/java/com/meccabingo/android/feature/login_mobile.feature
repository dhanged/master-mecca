Feature: Login

@Android  
Scenario: Check whether system displays Title, close “X“ icon, Fileds, Buttons, Links and Support area on Login page
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
And Verify device displays login Title on mobile
And Verify close 'X' icon on mobile
And Verify 'Username' field on mobile
And Verify 'Password' field on mobile
And Verify Toggle within password field on mobile
And Verify Remember me checkbox on mobile
And Verify 'Log In' CTA on mobile
And Verify 'Forgot username' link on mobile
And Verify 'Forgot password' link on mobile
And Verify 'New to Mecca Bingo?' text on mobile
And Verify 'Sign up' link on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile

@Android
Scenario: Portrait_Check whether Login button gets enabled when user enters Valid details in Username and password field
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "meccaauto2020" on mobile
And User enters password "Password123" on mobile
Then Verify Login button gets enabled on mobile

@Android 
Scenario: Portrait_Check whether system displays Title, back arrow, close “X“ icon, helper text, Fileds, Buttons, Links and Support area on Forgotten your password page
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User clicks on forgot password link on mobile
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile
And Verify text "Please supply the following account information and we'll send you an email with password reset instructions 3" on mobile
And Verify 'Username' field on mobile
And Verify 'Send reset instructions' CTA on mobile
And Verify 'Forgot username' link on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile

@Android 
Scenario: Portrait_Check whether system displays Success message when system sends email on Registered Email ID
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User clicks on forgot password link on mobile
Then Verify system displays Forgotten your password page with header as 'Forgotten your password' on mobile
Then User enters username "testmecca0503" on mobile
Then User clicks on 'Send reset instructions' on mobile
Then Verify Success Message displayed to user on mobile
Then Verify 'I didnot receive an email' link on mobile

@Android 
Scenario: Portrait_Check whether system displays Title, back arrow, close “X“ icon, helper text, Fileds, Buttons, Links and Support area on Forgotten your password page
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User clicks on forgot usename link on mobile
Then Verify system displays Forgotten your username page with header as 'Forgotten your username' on mobile
And Verify back arrow '<' on mobile
And Verify close 'X' icon on mobile
And Verify helper text "Please supply the following account information and we'll send you an email with password reset instructions 2" on mobile
And Verify 'Email' field on mobile
And Verify 'Send usename reminder' CTA on mobile
And Verify 'Forgot password' link on mobile
And Verify Help contact information on mobile
And Verify 'Live Chat' link on mobile

@Android 
Scenario: Portrait_Check whether system displays Success message when system sends username remainder email on Registered Email ID
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User clicks on forgot usename link on mobile
Then Verify system displays Forgotten your username page with header as 'Forgotten your username' on mobile
Then User enters email "testmecca0503@mailinator.com" on mobile
Then User clicks on 'Send username reminder' on mobile
Then Verify Success Message displayed to user on mobile
Then Verify 'I didnot receive an email' link on mobile

@MobileOptimized @Android @AndroidReg
Scenario: Login Success_Check whether when user login successfully to the site login overlay gets closed and display page from whether user started journey
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "meccaauto01" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile

@MobileOptimized @Android @AndroidReg
Scenario: Check whether system remember username for next login when user selected Remember me tick box while login to site
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "meccaauto01" on mobile
And User enters password "Password123" on mobile
Then User clicks on 'Remember Me' tick on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Click on my account
Then Click on logout button on mobile
Then User clicks on Login Button from header of the Page on mobile
Then Verify Username auto pupulated with "meccaauto01" on mobile

@MobileOptimized 
Scenario: Check whether system send reset instructions mail on Registered Email ID when user enters username in Username field and click on Send Reset instructions CTA from Forgotten your password Page
Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User clicks on forgot password link
Then User enters username "johnoliver"
Then User clicks on 'Send reset instructions'
Then Verify success message
Then Switch to child window
Then Verify system navigate user to "https://www.mailinator.com/" url
Then Enter email id "johnoliver@mailinator.com"
Then Click on go from search
Then Verify email in inbox
And Verify reset link inside mail

@MobileOptimized @AndroidReg
Scenario: Check whether the system displays a notification message when user tries to login with invalid details
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "meccaauto01123" on mobile
And User enters password "Password1" on mobile
Then User clicks on login button on mobile
Then Verify text "Your login details aren't quite right. Forgotten your password? You can reset it" displayed

@MobileOptimized @AndroidReg
Scenario: Check whether the system displays a notification message when user tries to login during “break set period” and does not allow user to login
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman015" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify text "You have chosen to take a break from Mecca Bingo. You can login again from" displayed

@MobileOptimized
Scenario: Check whether the system displays a notification message when Closed Status user tries to Login to the Site
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman013" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify text "User account closed" displayed

@MobileOptimized
Scenario: Check whether the system displays a notification message when Suspended Status user tries to Login to the Site
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman012" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify text "The user is suspended" displayed

@MobileOptimized @AndroidReg
Scenario: Check whether the system displays a notification message when Self Excluded Status user tries to Login to the Site
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman011" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify text "The user is self excluded" displayed

@MobileOptimized
Scenario: Login Failed 3rd Attempt_Check whether system displays hyperlinks for error message when user fails to login after 3rd attempt
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman010" on mobile
And User enters password "Password1234" on mobile
Then User clicks on login button on mobile
Then Click close 'X' icon on mobile
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman010" on mobile
And User enters password "Password1234" on mobile
Then User clicks on login button on mobile
Then Click close 'X' icon on mobile
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "testman010" on mobile
And User enters password "Password1234" on mobile
Then User clicks on login button on mobile
Then Verify text "Looks like you’re having some trouble signing in. Try" displayed
And Verify 'Forgot username' link on mobile
And Verify 'Forgot password' link on mobile