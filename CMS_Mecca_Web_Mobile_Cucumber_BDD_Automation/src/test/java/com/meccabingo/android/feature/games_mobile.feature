Feature: Games/Bingo container

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby from Bingo tile available on Homepage
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Click on Join Now button of first game of bingo section
Then Switch to child window on mobile
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby from Bingo tile available on Listing Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Switch to child window on mobile
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby from Bingo details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify game detail page open successfully

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby from Bingo Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Best" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from Bingo tile available on Homepage
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Switch to child window on mobile
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"


@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from bingo tile available on Listing Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Switch to child window on mobile
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from bingo details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify game detail page open successfully

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful login when accessing from Bingo Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Best" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful registration when accessing from bingo tile available on Homepage 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA11 0QG"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful registration when accessing from bingo tile available on Listing Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA2 7SX"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration when accesing from game details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA10 3DU"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to navigated to Page from where jouney started after successful registration when accessing game from Bingo Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Best" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA14 4LU"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click close 'X' icon on mobile
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful registration and deposit when accessing from bingo tile available on Homepage 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA11 0QG"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch Bingo lobby after successful registration and deposit  when accessing from bingo tile available on Listing Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA2 7SX"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration and deposit when accesing from game details Page
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Click button having text "Join Now"
Then Verify Login Page displayed on mobile
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA10 3DU"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

@MobileOptimized
Scenario: Check whether user able to launch and Play game after successful registration and deposit when accessing game from Bingo Search tile available under search results
Given Invoke the mecca site on Mobile in LandScape Mode
When User clicks on search filed on Mobile
Then Verify Search overlay opens on Mobile
When Search Game "Best" in Search field from header on Mobile
Then Click on Play Now button from searched Game
Then Click on 'Sign Up' link
Then Enter random email id on mobile
Then Check Agree Checkbox on mobile
Then Click on "Next" Button on mobile
Then Verify second page of registration displayed
Then Enter random username
Then Enter password "Password123"
Then Enter random firstname on mobile
Then Enter random surname on mobile
Then Enter random DOB on mobile
Then Enter random mobile number
Then Enter postal code "CA14 4LU"
Then Verify postcode search button displayed
Then Click on "email" checkbox on mobile
Then Click on "Register" Button on mobile
Then Verify "Deposit" as header displayed
Then Click payment method as "Card"
Then Enter Card number "4444499431371889"
Then Enter Expiry "12/22"
Then Enter CVV "123"
Then Enter amount to deposit "15"
Then Click on Deposit button from Deposit page
Then Verify deposit is successful
Then Click close button from deposit successful popup
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"

#EMC-11
@Android 
Scenario: Mobile_Portrait/Landscape_Check the actions on game tile when user clicks on below sections:-
-Image
-title of a hero tile
-click the "i" CTA
-click the top flag or jackpot flag

Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on image of the game
Then Verify Login Page displayed on mobile
Then Click close 'X' icon on mobile
Then Navigate through hamburger to "bingo" menu
Then Click on title of the game
Then user should remain on bingo container page
Then Navigate through hamburger to "bingo" menu
Then click on top flag of bingo game
Then Verify Login Page displayed on mobile

#EMC-100 this fails due to site cantbe reached as we use rank vpn on mobile
@Android
Scenario: Mobile_Portrait/Landscape_Check the actions on game hero tile when user clicks on below sections:-
- the image of the game hero tile
- the title of a hero tile
- the description of a hero tile
- the top flag or the bottom flag of a hero tile
- click "i" CTA of a hero tile
Given Invoke the mecca site on Mobile in Portrait Mode
Then User clicks on Login Button from header of the Page on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Navigate through hamburger to "bingo" menu
Then Click on description of the game
Then Click on title of the game
Then Click on info button of first game of bingo section
Then Verify game detail page open successfully
Then Navigate back to window on Mobile
Then Navigate through hamburger to "bingo" menu
Then Click on image of the game
Then Navigate back to window on Mobile
Then click on top flag of bingo game

#EMC-89
@Android
Scenario: Mobile_Portrait / Landscape_Check whether when horizontal scroll enabled Game tiles are displaying in one row and content is scrollable
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "slot" menu
Then Verify user can scroll slot game tiles horizontally

@Android
Scenario: Mobile_Portrait / Landscape_Check whether when vertical scroll enabled Game tiles are distributed in several rows and user able to scroll Up and Down to move through container 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then scroll page till footer of the page
Then Verify back on top CTA is displayed
Then Click on back to top button


#Bingo container
#EMC-88
@Android
Scenario: Mobile_Portrait/Landscape_Check the actions on bingo tile when user clicks on below sections:-
-Image
-the title of hero tile
-click the "i" CTA
-click the top flag or jackpot flag
-click the price/prize/type of bingo

Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on image of the game
Then Verify Login Page displayed on mobile
Then Click close 'X' icon on mobile
Then Click on title of the game
Then user should remain on bingo container page
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Verify game detail page open successfully
Then Navigate back to window on Mobile
Then Navigate through hamburger to "bingo" menu
Then Click on prize of the bingo tile
Then user should remain on bingo container page
 
#EMC-350
@Android
Scenario: Mobile_Portrait/Landscape_Check the actions on bingo hero tile when user clicks on below sections:-
- the image of the bingo hero tile
- the title of a hero tile
- the description of a hero tile
- the top flag or the bottom flag of a hero tile
- click "i" CTA of a hero tile 
Given Invoke the mecca site on Mobile in Portrait Mode
Then Navigate through hamburger to "bingo" menu
Then Click on image of the game
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify balance section is displayed
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"
Then Switch to parent window on mobile
Then Navigate through hamburger to "bingo" menu
Then Click on title of the game
Then user should remain on bingo container page
Then click on top flag of bingo game
Then Switch to child window on mobile
Then Verify device navigate user to containing url "bingo.meccabingo.com"
Then Switch to parent window on mobile
Then Navigate through hamburger to "bingo" menu
Then Click on info button of first game of bingo section
Then Navigate back to window on Mobile
Then Click on prize of the bingo tile
Then user should remain on bingo container page

#EMC-29
@Android
Scenario: Mobile_Portrait / Landscape_Check whether when horizontal scroll enabled Bingo tiles are displaying in one row and content is scrollable
Given Invoke the mecca site on Mobile in Portrait Mode
Then Verify user can scroll bingo game tiles horizontally

@Android 
Scenario: Check whether system launch bingo lobby in separate window when user clicks on Join Now button from Bingo Tile
Given Invoke the mecca site on Mobile in Portrait Mode
Then Click on Join Now button of first game of bingo section
Then Verify Login Page displayed on mobile
Then User enters username "automecca2020" on mobile
Then User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Switch to child window on mobile
Then Verify device navigate user to containing url "sta.bingo.meccabingo.com"
