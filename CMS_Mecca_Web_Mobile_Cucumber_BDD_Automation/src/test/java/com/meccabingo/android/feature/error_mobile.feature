Feature: Error

#Mobile EMC-1069

@Android
Scenario: Check whether system displays 404 error page to user when user tries to access page which is not configured

Given Invoke the mecca site on Mobile in Portrait Mode
When User clicks on Login Button from header of the Page on mobile
Then Verify Login Page displayed on mobile
Then User enters username "meccaauto01" on mobile
And User enters password "Password123" on mobile
Then User clicks on login button on mobile
Then Verify user is successfully logged in on mobile
Then Verify balance section is displayed
Then Navigate to wrong URL
Then Error displayed