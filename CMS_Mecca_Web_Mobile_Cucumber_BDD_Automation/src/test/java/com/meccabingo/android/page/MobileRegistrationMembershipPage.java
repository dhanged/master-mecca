package com.meccabingo.android.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobileRegistrationMembershipPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobileRegistrationMembershipPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void clickTagWithText(String strTag, String strText) {
		By locator = By.xpath("//" + strTag + "[contains(text(),'" + strText + "')]");
		logReporter.log("Tag " + strTag + " with text " + strText + " click  : > >", objMobileActions.click(locator));
	}

}
