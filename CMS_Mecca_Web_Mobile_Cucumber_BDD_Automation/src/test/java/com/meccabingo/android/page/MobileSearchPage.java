/**
 * 
 */
package com.meccabingo.android.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class MobileSearchPage {

	By search_loupe = By.xpath("//i[contains(@class,'search')]"); // button[contains(@class,'search-open-btn')]

	By search_overLay = By.xpath("//div[@class='field-container']");

	By search_TxtBox = By.xpath("//input[contains(@class,'search-input')]"); // input[@class='search-input']
	By search_clearButton = By.xpath("//button[contains(@class,'search-open-btn')]"); // button[@class='search-clear-btn']

	By searched_Section = By.xpath("//section[contains(@class,'search-results-items')]");
	By searched_Section_NoResult = By.xpath("//div[@class='search-no-results']");

	By searched_Container = By.xpath("//div[contains(@class,'search-results-items-container')]");

	By searchedGame_Img = By.xpath("//div[contains(@class,'search-results-item')]/div/img"); // div[@class='search-results-item']//img
	By searchedGame_Name = By.xpath("//div[contains(@class,'search-results-item-content')]/h3/span"); // div[@class='search-results-item-content']//h3//span
	By searchedGame_PlayNow_btn = By
			.xpath("(//section[contains(@class,'search-result')]//button[contains(@class,'play')])[1]"); // a[contains(text(),'Play
	// Now')]
	By searchedGame_Info = By.xpath("//*[contains(text(),'Play ')]/following-sibling::a[contains(@class,'info-sign')]");

	By search_WhatOthersArePlaying_Section = By
			.xpath("//section[contains(@class,'search-suggestions-what-others-play')]"); // section[@class='search-suggestions-what-others-play']
	By search_QuickLinks_Section = By.xpath("//section[contains(@class,'search-suggestions-quick-links')]"); // section[@class='search-suggestions-quick-links']

	private LogReporter logReporter;
	// private AppiumDriverProvider objAppiumDriverProvider;
	private MobileActions objMobileActions;

	public MobileSearchPage(LogReporter logReporter, AppiumDriverProvider appiumDriverProvider,
			MobileActions mobileActions) {
		this.logReporter = logReporter;
		// this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
	}

	public void clickSearchLoupe() {
		logReporter.log("click 'Search Loupe' > >", objMobileActions.click(search_loupe));
	}

	public void verifySearchOverLay() {
		logReporter.log("Check 'search OverLay' ", objMobileActions.checkElementDisplayed(search_overLay));

	}

	public void enterGameName(String gameName) {
		logReporter.log("Enter Value in search box' ", objMobileActions.setText(search_TxtBox, gameName));
	}

	public void verifySearchSection() {
		if (objMobileActions.checkElementDisplayed(searched_Section)) {
			logReporter.log("Check 'searched item list' ", objMobileActions.checkElementDisplayed(searched_Container));
		}

	}

	public void verifyGameDeatails(String text) {
		switch (text) {

		case "Image": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(searchedGame_Img));
			break;
		}

		case "Name": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(searchedGame_Name));
			break;
		}

		case "Play Now CTA": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(searchedGame_PlayNow_btn));
			break;
		}

		case "Info CTA": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(searchedGame_Info));
			break;
		}

		}

	}

	public void enterValuesInSearchBox(String string) {

		logReporter.log("Check 'searched blank list' ",
				objMobileActions.checkElementDisplayed(searched_Section_NoResult));
	}

	public void verifyResultSectionWithNoMatch() {
		logReporter.log("Check 'searched blank list' ",
				objMobileActions.checkElementDisplayed(searched_Section_NoResult));

	}

	public void clickPlayNowFromSearchedGame() {
		logReporter.log("Click play now ", objMobileActions.click(searchedGame_PlayNow_btn));
	}

	public void clickiBtnFromSearchedGame() {
		By locator = By.xpath("(//section[contains(@class,'search-result')]//a)[1]");
		logReporter.log("Click i button ", objMobileActions.click(locator));
	}

	public void quickLinksDisplayed() {
		By locator = By.xpath("//h2[contains(text(),'Quick ')]");
		logReporter.log("Check Quick links ", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void checkCheckboxOfIncludeClubs() {
		By locator = By.xpath("//div[contains(@class,'ip-checkbox')]/input");
		logReporter.log("click on checkbox", objMobileActions.click(locator));
	}
}
