package com.meccabingo.android.page;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

import io.cucumber.java.en.Then;

public class MobileMyAccountPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;
	
	private int messageCount;

	By plusSign = By.xpath("(//i[contains(@class,'icon-expand')])[2]");
	By pBal = By.xpath("//h5[text()='Playable Balance']");
	By amt = By.xpath("//h5[text()='Playable Balance']/span");
	By btnDetailed = By.xpath("//span[contains(text(),'Detailed')]");
	By btnDeposit = By.xpath("//span[contains(text(),'Detailed')]/following::button/span");
	By cash = By.xpath("//p[text()='Cash']");
	By bonusAmt = By.xpath("//p[text()='Bonuses']/span");

	// Change Password page locators
	By currentPasswd = By.id("old-password");
	By newPasswd = By.id("new-password");
	By updateCTA = By.xpath("//input[@id='new-password']/following::button[2]");
	By liveHelp = By.xpath("//a[contains(text(),'Live help')]");

	String greencolor = "rgba(0, 157, 122, 1)";// "#5ea85a";

	By withdrawAmt = By.id("input-amount");

	public MobileMyAccountPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void clickOnMyAccount() {
		By myAccount = By.xpath("//i[contains(@class,'my-account')]");
		objMobileActions.processMobileElement(myAccount);
		if (objMobileActions.checkElementDisplayedWithMidWait(myAccount))
			logReporter.log("Click back to top button> >", objMobileActions.click(myAccount));
	}

	public void verifyPlayableBalanceSectionInCollpasedStateDisplayed() {
		logReporter.log("Check Playable balance > >", objMobileActions.checkElementDisplayed(pBal));
		logReporter.log("Check amount > >", objMobileActions.checkElementDisplayed(amt));
		logReporter.log("Check plus sign > >", objMobileActions.checkElementDisplayed(plusSign));
	}

	public void clickOnPlusSign() {
		if (objMobileActions.checkElementDisplayedWithMidWait(plusSign)) {
			objMobileActions.androidScrollToElement(plusSign);
			logReporter.log("Click plus sign> >", objMobileActions.click(plusSign));
			
		}
	}

	public void clickOnPlusSignOfTakeABreakPage() {
		By locator = By.xpath("(//i[contains(@class,'expand')])[3]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click plus sign of take break> >", objMobileActions.click(locator));
	}

	public void verifyPlayableBalanceSectionInExpandedStateDisplayed() {
		By minusSign = By.xpath("//i[contains(@class,'icon-collapse opened')]");
		By cash = By.xpath("//p[text()='Cash']");
		By cashAmt = By.xpath("//p[text()='Cash']/span");
		By bonus = By.xpath("//p[text()='Bonuses']");

		logReporter.log("Check Playable balance > >", objMobileActions.checkElementDisplayed(pBal));
		logReporter.log("Check amount > >", objMobileActions.checkElementDisplayed(amt));
		logReporter.log("Check minus sign > >", objMobileActions.checkElementDisplayed(minusSign));

		logReporter.log("Check Cash > >", objMobileActions.checkElementDisplayed(cash));
		logReporter.log("Check Cash amount > >", objMobileActions.checkElementDisplayed(cashAmt));
		logReporter.log("Check Bonuses > >", objMobileActions.checkElementDisplayed(bonus));
		logReporter.log("Check Bonuses amount > >", objMobileActions.checkElementDisplayed(bonusAmt));

		logReporter.log("Check Detailed > >", objMobileActions.checkElementDisplayed(btnDetailed));
		logReporter.log("Check Deposit > >", objMobileActions.checkElementDisplayed(btnDeposit));

	}

	public void clickOnTopSectionOfBalanceBlock() {
		By balTopSection = By.xpath("//div[contains(@class,'collapsible-title flex')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(balTopSection))
			logReporter.log("Click top section of balance block> >", objMobileActions.click(balTopSection));
	}

	public void clickOnCash() {
		if (objMobileActions.checkElementDisplayedWithMidWait(cash))
			logReporter.log("Click back to top button> >", objMobileActions.click(cash));
	}

	public void clickOnBonusAmt() {
		if (objMobileActions.checkElementDisplayedWithMidWait(bonusAmt))
			logReporter.log("Click bonus amount> >", objMobileActions.click(bonusAmt));
	}

	public void clickDeposit() {
		if (objMobileActions.checkElementDisplayedWithMidWait(btnDeposit))
			logReporter.log("Click Deposit> >", objMobileActions.click(btnDeposit));
	}

	public void clickDetailedView() {
		if (objMobileActions.checkElementDisplayedWithMidWait(btnDetailed))
			logReporter.log("Click Detailed View> >", objMobileActions.click(btnDetailed));
	}

	public void verifyBalanceOverlayPage() {
		By locator = By.xpath("//h4[contains(text(),'Balance')]");
		logReporter.log("Check balance overlay page> >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyDepositOverlayPage() {
		By locator = By.xpath("//h4[contains(text(),'Deposit')]");
		logReporter.log("Check Deposit overlay page> >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyMenuOptions(String strMenuName) {
		By locator = By.xpath("//a[contains(@heading,'" + strMenuName + "')]");
		logReporter.log("Check menu : " + strMenuName + "> >",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void usernameDisplayed(String strUName) {
		strUName = strUName.toUpperCase();
		By locator = By.xpath("//h4[text()='" + strUName + "']");
		logReporter.log("Check username in header : " + strUName + "> >",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void logoutButtonDisplayed() {
		By locator = By.xpath("//div[contains(@class,'logout')]");
		logReporter.log("Check logout> >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void recentlyPlayedButtonDisplayed() {
		By locator = By.xpath("//h4[contains(text(),'Recently Play')]");
		logReporter.log("Check Recently Played> >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyMessageCountDisplayed() {
		By locator = By.xpath("//i[contains(@class,'messages')]/div");
		logReporter.log("Check Message count> >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void clickBackButton() {
		By locator = By.xpath("//a[contains(@class,'icon-arrow-left')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			objMobileActions.click(locator);
	}

	public void clickMenuOption(String strMenuName) {
		By locator = By.xpath("//a[contains(@heading,'" + strMenuName + "')]");
		objMobileActions.processMobileElement(locator);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click back button> >", objMobileActions.click(locator));
	}

	public void verifyMenuHeaderDisplayed(String strMenuName) {
		By locator = By.xpath("//h4[text()='" + strMenuName + "']");
		objMobileActions.processMobileElement(locator);
		logReporter.log("Check menuname in header : " + strMenuName + "> >",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyContainsUrl(String url) {
		String currentURL = objAppiumDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("verify URL > >" + url, currentURL.contains(url));
	}

	public void clickLiveChat() {
		By locator = By.xpath("//i[contains(@class,'live-chat')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click LiveChat > >", objMobileActions.click(locator));
	}

	public void verifyCurrentPasswordDisplayed() {
		logReporter.log("Check current password : > >",
				objMobileActions.checkElementDisplayedWithMidWait(currentPasswd));
	}

	public void verifyNewPasswordDisplayed() {
		logReporter.log("Check new password : > >", objMobileActions.checkElementDisplayedWithMidWait(newPasswd));
	}

	public void verifyUpdateCTADisplayed() {
		logReporter.log("Check update button : > >", objMobileActions.checkElementDisplayedWithMidWait(updateCTA));
	}

	public void verifyLiveHelpDisplayed() {
		logReporter.log("Check Live help : > >", objMobileActions.checkElementDisplayedWithMidWait(liveHelp));
	}

	public void enterCurrentPassword(String cPass) {
		logReporter.log("enter current password  : > >", objMobileActions.setText(currentPasswd, cPass));
	}

	public void enterNewPassword(String newPass) {
		logReporter.log("enter new password  : > >", objMobileActions.setText(newPasswd, newPass));
	}

	public void verifyGreenLineForCurrentPassword() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(currentPasswd, "border-bottom-color", greencolor));
	}

	public void verifyPasswordGuildelineDisplayedWithRightTick(String strGuideline) {
		By locator = By.xpath("//li[text()='" + strGuideline + "']/span[contains(@class,'success')]");
		logReporter.log("Password guideline with right tick '" + strGuideline + "' displayed >>",
				objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyUpdateCTAEnabled() {
		if (objMobileActions.getAttribute(updateCTA, "disabled") == null)
			logReporter.log("updateCTA enabled", true);
		else
			logReporter.log("updateCTA enabled", false);
	}

	public void verifyUpdateCTADisabled() {
		if (objMobileActions.getAttribute(updateCTA, "disabled") != null)
			logReporter.log("updateCTA disabled", true);
		else
			logReporter.log("updateCTA disabled", false);
	}

	public void verifyTitleOfSubMenuPage(String strTitle) {
		By locator = By.xpath("//h3[contains(text(),'" + strTitle + "')]");
		logReporter.log("title : " + strTitle, objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyLinkOfSubMenuPage(String strLink) {
		By locator = By.xpath("//a[contains(@href,'" + strLink + "')]");
		logReporter.log("Link : " + strLink, objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyFieldOfSubMenuPage(String strIDOfField) {
		By locator = By.id(strIDOfField);
		logReporter.log("Field of sub menu page : " + strIDOfField,
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyAvailableFundsSectionOfWithdrawal() {
		By fundsText = By.xpath("//p[contains(text(),'funds of:')]");
		By amount = By.xpath("//p[contains(text(),'funds of:')]/span");

		logReporter.log("check From available funds of: ",
				objMobileActions.checkElementDisplayedWithMidWait(fundsText));
		logReporter.log("check Amount : ", objMobileActions.checkElementDisplayedWithMidWait(amount));
	}

	public void enterWithdrawalAmt(String strAmt) {
		objMobileActions.invokeActionOnMobileLocator(withdrawAmt, "clearText");
		logReporter.log("enter withdrawal amount  : > >", objMobileActions.setText(withdrawAmt, strAmt));
	}

	public void helpTextDisplayed(String strHelpText) {
		By locator = By
				.xpath("//div[contains(@class,'inputHintDiv show')]/span[contains(text(),'" + strHelpText + "')]");
		logReporter.log("Check Help text", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyGreenLineForWithdrawalAmount() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(withdrawAmt, "border-bottom-color", greencolor));
	}

	public void enterWithdrawalPassword(String passwd) {
		By locator = By.id("input-password");
		logReporter.log("enter withdrawal amount  : > >", objMobileActions.setText(locator, passwd));
	}

	public void verifySpanButtonEnabled(String spanName) {
		By locator = By.xpath("//span[text()='" + spanName + "']/ancestor::button");
		if (objMobileActions.getAttribute(locator, "disabled") == null)
			logReporter.log("span button enabled > >", true);
		else
			logReporter.log("span button enabled > >", false);
	}

	public void clickLink(String linkName) {
		By locator = By.xpath("//a[contains(@href,'" + linkName + "')]");
		logReporter.log("Click Link : " + linkName, objMobileActions.click(locator));
	}

	public void enterPasswordForTakeBreak(String strPassword) {
		By locator = By.xpath("//input[@placeholder='Password']");
		logReporter.log("enter take break password  : > >", objMobileActions.setText(locator, strPassword));
	}

	public void reminderButtonHighlighted(String strBtn) {
		By locator = By.xpath("//span[contains(text(),'" + strBtn + "')]/ancestor::button");
		objMobileActions.processMobileElement(locator);
		String strValue = objMobileActions.getAttribute(locator, "class");
		if (strValue.contains("yellow"))
			logReporter.log(strBtn + " button is hightlighted  : > >", true);
		else
			logReporter.log(strBtn + " button is hightlighted  : > >", false);

	}

	public void enterBonusCode(String strBonusCode) {
		By locator = By.id("bonus-code");
		logReporter.log("enter bonus code  : > >", objMobileActions.setText(locator, strBonusCode));
	}

	public void clickClearButton(String strOrder) {
		By locator = By.xpath("(//button[contains(text(),'Clear')])[" + strOrder + "]");
		logReporter.log("click clear button  : > >", objMobileActions.click(locator));
	}

	public void verifyTagWithTextDisplayed(String strTag, String strText) {
		By locator = By.xpath("//" + strTag + "[contains(text(),'" + strText + "')]");
		logReporter.log("Tag " + strTag + " with text " + strText + " displayed  : > >",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyPasswdFieldForTakeABreak() {
		By locator = By.xpath("//div[contains(@class,'field-row ip-password')]");
		logReporter.log("password field displayed for take a break >>",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyH5TextDisplayed(String strText) {
		By locator = By.xpath("//h5[contains(text(),'" + strText + "')]");
		logReporter.log("Text displayed >>", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyDivTextDisplayed(String strText) {
		By locator = By.xpath("//div[contains(text(),'" + strText + "')]");
		logReporter.log("Text displayed >>", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyPasswdFieldDisplayedForSelfExclusion() {
		By locator = By.xpath("//div[contains(@class,'field-row ip-password')]");
		logReporter.log("Password field displayed for self exclusion >>",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void clickPlusSignOfRealityCheck() {
		By locator = By.xpath("//h5[text()='Tell me more']//following::i");
		logReporter.log("click plus sign  : > >", objMobileActions.click(locator));

	}

	public void clickPlusSignOfSelfExclude() {
		By locator = By.xpath("//h5[text()='Why Self Exclude?']//following::i");
		logReporter.log("click plus sign  : > >", objMobileActions.click(locator));
	}

	public void clickDepositOfBalance() {
		By locator = By.xpath("//span[text()='Deposit']");
		if(objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click deposit  : > >", objMobileActions.click(locator));
	}
	
	public void transactionFilterBoxDisplayed() {
		By locator = By.xpath("//div[contains(@class,'ip-select')]");
		logReporter.log("Transaction filter box displayed >>",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void datesPopulatedWithTodayDate() {
		By locator = By.xpath("(//div[contains(@class,'date-picker')]//input)[2]");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		LocalDateTime now = LocalDateTime.now();  
		System.out.println(dtf.format(now));  
		String todayDate = dtf.format(now);
		String actualDate = objMobileActions.getAttribute(locator, "value");
		if(actualDate.equals(todayDate))
			logReporter.log("Dates populated with today's date", true);
		else
			logReporter.log("Dates populated with today's date", false);
	}
	
	public void transactionHistoryBoxDisplayed() {
		By locator = By.xpath("//table[@class='my-account-table']");
		logReporter.log("Transaction History box displayed >>",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void selectFilterActivity(String strFilterActivity) {
		By locator = By.xpath("//option[text()='"+strFilterActivity+"']");
		logReporter.log("click filter activity  : > >", objMobileActions.click(locator));
	}
	
	public void verifyTransactionHistoryRecords(String activity) {
		boolean flag=true;
		List  rows = objAppiumDriverProvider.getAppiumDriver().findElements(By.xpath("//table[@class='my-account-table']/tbody/tr")); 
        System.out.println("No of rows are : " + rows.size());
		int totalNoOfRows = rows.size();
		for(int i=1;i<totalNoOfRows;i++) {
			String rowText= objAppiumDriverProvider.getAppiumDriver().findElement(By.xpath("//table[@class='my-account-table']/tbody/tr["+i+"]/td[2]")).getText();
			System.out.println("row text = " + rowText);
			if(rowText.contains(activity)==false) {
				flag=false;
				break;
			}
				
		}
		
		if(flag)
			logReporter.log("Matching records found", true);
		else
			logReporter.log("Matching records found", false);
	}
	
	public void clickFilterButton() {
		By locator = By.xpath("//button[text()='Filter']");
		logReporter.log("click filter button  : > >", objMobileActions.click(locator));
		objMobileActions.checkElementDisplayedWithMidWait(By.xpath("//table[@class='my-account-table']"));
	}
	
	public void optionDisplayed(String strOption) {
		By locator = By.xpath("//option[text()='"+strOption+"']");
		logReporter.log("option displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void selectSortBy(String strSortBy) {
		By locator = By.xpath("//option[text()='"+strSortBy+"']");
		logReporter.log("click sort by  : > >", objMobileActions.click(locator));
	}
	
	public void collapseRecord() {
		By locator = By.xpath("(//table[@class='my-account-table']//i[contains(@class,'plus')])[1]");
		logReporter.log("Collapse first record", objMobileActions.click(locator));
	}
	
	public void verifyCollapsedRecord() {
		By locator1 = By.xpath("//td[contains(text(),'Reference:')]");
		logReporter.log("Reference displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator1));
		By locator2 = By.xpath("//td[contains(text(),'Wallet:')]");
		logReporter.log("Wallet displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator2));
		By locator3 = By.xpath("//td[contains(text(),'Balance')]");
		logReporter.log("Balance displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator3));
	}
	
	public void verifyDeleteIconDisplayed() {
		By locator = By.xpath("//i[contains(@class,'delete')]");
		logReporter.log("delete icon displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void verifyMessagesTableDisplayed() {
		By locator = By.xpath("//section[contains(@class,'my-account-messages')]");
		logReporter.log("Messages table displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}
	
	public void clickFirstMessage() {
		By locator = By.xpath("(//section[contains(@class,'my-account-messages')]//a[contains(@class,'messages')])[1]");
		logReporter.log("click first message  : > >", objMobileActions.click(locator));
	}
	public void verifyMessageInDetail() {
		By locator1 = By.xpath("//i[contains(@class,'icon-back')]");
		logReporter.log("Message back button displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator1));
		By locator2 = By.xpath("//i[contains(@class,'icon-next')]");
		logReporter.log("Message next button displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator2));
	}
	
	public void clickMsgDeleteButton() {
		By locator = By.xpath("//i[contains(@class,'icon-delete')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click message delete button  : > >", objMobileActions.clickUsingJS(locator));
	}
	
	public int getMessageCount() {
		return messageCount;
	}
    
    public void setMessageCount() {
    	By locator = By.xpath("//div[@class='slideout-overlay-heading']/h4[contains(text(),'Messages')]");
    	String headerText = objMobileActions.getText(locator);
    	int start = headerText.indexOf("(");
    	messageCount = Integer.parseInt(headerText.substring(start + 1, headerText.length()-1));
    	System.out.println("message count: " + messageCount);
    }
    
    public void verifyMsgCountReduced() {
    	By locator = By.xpath("//div[@class='slideout-overlay-heading']/h4");
    	String headerText = objMobileActions.getText(locator);
    	int start = headerText.indexOf("(");
    	int actualMsgCount = Integer.parseInt(headerText.substring(start + 1, headerText.length()-1));
    	System.out.println("actual msg count : " + actualMsgCount);
    	if(actualMsgCount < messageCount)
    		logReporter.log("Message count is reduced >",true);
    	else
    		logReporter.log("Message count is reduced >",false);
    }
    
    public void enterLimit(String strLimit) {
    	By locator = By.id("deposit-limits");
    	objMobileActions.invokeActionOnMobileLocator(locator, "clearText");
		logReporter.log("enter limit", objMobileActions.setText(locator, strLimit));
    }
    
    public void clickButtonHavingGivenText(String strName) {
    	By locator = By.xpath("//button[text()='"+strName+"']");
    	logReporter.log("click limit button", objMobileActions.click(locator));
    }
    
    public void reduceDepositLimitByOne() {
    	By locator = By.id("deposit-limits");
    	String actualLimit = objMobileActions.getAttribute(locator, "value");
    	int decreasedLimit = Integer.parseInt(actualLimit) -1;
    	clickButtonHavingGivenText("Reset limit");
    	enterLimit(String.valueOf(decreasedLimit));
    }
    
    public void increaseDepositLimitByOne() {
    	By locator = By.id("deposit-limits");
    	String actualLimit = objMobileActions.getAttribute(locator, "value");
    	int increasedLimit = Integer.parseInt(actualLimit) +1;
    	clickButtonHavingGivenText("Reset limit");
    	enterLimit(String.valueOf(increasedLimit));
    }
    
    public void clickDepositLimitButton(String strName) {
    	By locator = By.xpath("(//span[text()='"+strName+"'])[2]");
    	logReporter.log("click limit button", objMobileActions.click(locator));
    }
    
}
