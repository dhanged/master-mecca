package com.meccabingo.android.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobileHeaderPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobileHeaderPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void verifyBalanceSectionDisplayed() {
		By balanceSection = By.xpath("//apollo-balance-block");
		objMobileActions.processMobileElement(balanceSection);
		objMobileActions.checkElementEnabled(balanceSection);
		logReporter.log("verify 'balance section' > >",
				objMobileActions.checkElementDisplayedWithMidWait(balanceSection));
	}

	public void verifyBalaneAsTitleDisplayed() {
		By balanceLabel = By.xpath("//span[contains(@class,'balance')]");
		logReporter.log("verify title as'Balance' > >",
				objMobileActions.checkElementDisplayedWithMidWait(balanceLabel));
	}

	public void verifyAmountUnderBalanceSectionDisplayed() {
		By amountLabel = By.xpath("//apollo-balance-block/strong");
		logReporter.log("verify amount > >", objMobileActions.checkElementDisplayedWithMidWait(amountLabel));
	}

	public void hideBalance() {
		By balanceHidden = By.xpath("//apollo-balance-block[@hidden]");
		By balance = By.xpath("//label[@for='balance']");
		if (!objMobileActions.checkElementExists(balanceHidden))
			logReporter.log("Click balance > >", objMobileActions.click(balance));
	}

	public void clickBalanceTitle() {
		By balanceTitle = By.xpath("//span[contains(@class,'balance')]");
		logReporter.log("click balance title > >", objMobileActions.click(balanceTitle));
	}

	public void clickFiveDots() {
		By fiveDots = By.xpath("//apollo-balance-block/strong");
		logReporter.log("click five dots > >", objMobileActions.click(fiveDots));
	}

	public void verifyOptions(String strOptions) {
		By locator = null;
		switch (strOptions) {
		case "Hamburger menu":
			locator = By.xpath("//div[contains(@class,'hamburger')]");
			break;
		case "Mecca logo":
			locator = By.xpath("//a[contains(@class,'logo flex')]");
			break;
		case "Deposit CTA":
			locator = By.xpath("//button[contains(@class,'deposit')]");
			break;
		case "My account CTA":
			locator = By.xpath("//a[contains(@class,'myaccount')]");
			break;
		case "no of unread messages":
			locator = By.xpath("//a[contains(@class,'myaccount')]/span[contains(@class,'messages')]");
			break;
		}

		logReporter.log("verify " + strOptions + " > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}
}
