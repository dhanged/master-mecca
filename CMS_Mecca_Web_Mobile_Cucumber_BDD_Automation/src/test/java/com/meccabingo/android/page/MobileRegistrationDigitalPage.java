package com.meccabingo.android.page;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobileRegistrationDigitalPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	By postCode = By.xpath("//input[@placeholder='Postcode']");
	By city = By.id("input-town-city");
	By county = By.id("input-county");
	By addressLine1 = By.id("input-address-line1");
	By mobileNumber = By.id("input-mobile");

	String greycolor = "rgba(94, 204, 243, 1)";

	public MobileRegistrationDigitalPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void verifyGreyColorDisplayedForPostcode() {
		logReporter.log("Check grey color > >",
				objMobileActions.checkCssValue(postCode, "border-bottom-color", "rgba(94, 204, 243, 1)"));
	}

	public void verifyHelpTextDisplayedForPostcode(String helpText) {
		By locator = By.xpath("//div[contains(@class,'inputHintDiv show')]/span[contains(text(),'" + helpText + "')]");
		logReporter.log("Check Help text", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyAddressLine1Populated(String str) {
		String actualText = objMobileActions.getAttribute(addressLine1, "value");
		logReporter.log("Check Address Line1", true, actualText.equals(str));
	}

	public void verifyCityPopulated(String str) {
		String actualText = objMobileActions.getAttribute(city, "value");
		logReporter.log("Check City", true, actualText.equals(str));
	}

	public void verifyCountyPopulated(String str) {

		String actualText = objMobileActions.getAttribute(county, "value");
		logReporter.log("Check County", true, actualText.equals(str));
	}

	public void verifyPostcodePopulated(String str) {
		String actualText = objMobileActions.getAttribute(postCode, "value");
		logReporter.log("Check Postcode", true, actualText.equals(str));
	}

	public void clickEnterAddressManually() {
		By locator = By.xpath("//span[text()='Enter address manually']");
		logReporter.log("Click Enter Address manually", true, objMobileActions.click(locator));
	}

	public void verifyPostcodeSearchDisplayed() {
		By locator = By.xpath("//span[text()='Postcode search']");
		logReporter.log("Check postcode search", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void enterAddressLine1(String str) {
		objMobileActions.invokeActionOnMobileLocator(addressLine1, "clearText");
		logReporter.log("enter address line1", objMobileActions.setText(addressLine1, str));

	}

	public void enterRandomAddressLine1() {
		String strRandomText = RandomStringUtils.random(10, true, false);
		logReporter.log("enter random address line1", objMobileActions.setText(addressLine1, strRandomText));
	}

	public void enterCity(String str) {
		logReporter.log("enter city", objMobileActions.setText(city, str));
	}

	public void enterRandomCity() {
		String strRandomText = RandomStringUtils.random(10, true, false);
		logReporter.log("enter random city", objMobileActions.setText(city, strRandomText));
	}

	public void enterCounty(String str) {
		objMobileActions.invokeActionOnMobileLocator(county, "clearText");
		logReporter.log("enter county", objMobileActions.setText(county, str));
	}

	public void enterRandomCounty() {
		String strRandomText = RandomStringUtils.random(10, true, false);
		logReporter.log("enter random county", objMobileActions.setText(county, strRandomText));
	}

	public void greenLineDisplayedForAddressLine1() {
		logReporter.log("Check color addressline1 > >",
				objMobileActions.checkCssValue(addressLine1, "border-bottom-color", "rgba(0, 157, 122, 1)"));

	}

	public void greenLineDisplayedForCity() {
		logReporter.log("Check color city > >",
				objMobileActions.checkCssValue(city, "border-bottom-color", "rgba(0, 157, 122, 1)"));

	}

	public void greenLineDisplayedForCounty() {
		logReporter.log("Check color county > >",
				objMobileActions.checkCssValue(county, "border-bottom-color", "rgba(0, 157, 122, 1)"));

	}

	public void greenLineDisplayedForPostcode() {
		logReporter.log("Check color postCode > >",
				objMobileActions.checkCssValue(postCode, "border-bottom-color", "rgba(0, 157, 122, 1)"));
	}

	public void greyLineDisplayedForMobileNumber() {
		logReporter.log("Check grey color mobile number > >",
				objMobileActions.checkCssValue(mobileNumber, "border-bottom-color", "rgba(94, 204, 243, 1)"));
	}

	public void greenLineDisplayedForMobileNumber() {
		logReporter.log("Check green color mobile number > >",
				objMobileActions.checkCssValue(mobileNumber, "border-bottom-color", "rgba(0, 157, 122, 1)"));
	}

	public void verifyCheckboxIsUnchecked(String strCheckbox) {
		By locator = By.id(strCheckbox);
		if (objMobileActions.getAttribute(locator, "value").equals("false"))
			logReporter.log("check checkbox", true, true);
		else
			logReporter.log("check checkbox", true, false);
	}

	public void verifyTextBoxIsNotEmpty(String strTextBox) {
		By locator = By.xpath(strTextBox);
		if (objMobileActions.getAttribute(locator, "value").equals(""))
			logReporter.log("check textbox", true, false);
		else
			logReporter.log("check textbox", true, true);
	}

	public void verifyEmailTextBoxIsNotEmpty() {
		By locator = By.xpath("//input[@placeholder='Email address']");
		if (objMobileActions.getAttribute(locator, "value").equals(""))
			logReporter.log("check textbox", true, false);
		else
			logReporter.log("check textbox", true, true);
	}
}
