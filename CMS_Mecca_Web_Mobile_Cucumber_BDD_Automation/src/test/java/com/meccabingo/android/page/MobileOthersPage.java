package com.meccabingo.android.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobileOthersPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobileOthersPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void verifyCookieBannerDisplayed() {
		By cookieBanner = By.xpath(
				"//p[contains(text(),'Meccabingo.com utilises cookies to give you the best experience possible. By continuing to use the site, you are agreeing to our')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(cookieBanner))
			logReporter.log("cookie banner displayed> >", objMobileActions.click(cookieBanner));
	}

	public void verifyContinueButtonDisplayed() {
		By continueButton = By.xpath("//button[text()='Continue']");
		if (objMobileActions.checkElementDisplayedWithMidWait(continueButton))
			logReporter.log("continue button displayed> >", objMobileActions.click(continueButton));
	}
}
