/**
 * 
 */
package com.meccabingo.android.page;

import org.openqa.selenium.By;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class MobileHomePage {

	By logoBingoHeader = By.xpath("//*[contains(@id,'header')]/a/picture");

	By footer_SocialMediaBlock = By.xpath("//div[contains(@class,'footer-social')]");
	By footer_UsefulLinkBlock = By.xpath("//ul[@class='useful-links']");
	By footer_PartnersLogoSection = By.xpath("//ul[@class='partners']");
	By footer_privacyAndSecurityBlock = By.xpath("//div[@class='footer-row'] ");
	By footer_PaymentProvidersBlock = By.xpath("//div[@class='footer-payments']");
	By loginButton = By.xpath("//button[text()='Login']");
	By myAccounticon = By.xpath("//i[@class='my-account']");

	private LogReporter logReporter;
	private AppiumDriverProvider objAppiumDriverProvider;
	private MobileActions objMobileActions;

	public MobileHomePage(LogReporter logReporter, AppiumDriverProvider appiumDriverProvider,
			MobileActions mobileActions) {
		this.logReporter = logReporter;
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
	}

	public void verifyDeviceIsLandscape() {
		boolean result = false;
		if (ScreenOrientation.LANDSCAPE.equals(objAppiumDriverProvider.getAppiumDriver().getOrientation()))
			result = true;
		logReporter.log("Check Device is in LandScape mode > >", result);
	}

	public void verifyHeaderLogo() {
		logReporter.log("check logo on homepage > >", objMobileActions.checkElementDisplayed(logoBingoHeader));
	}
	
	public void clickLetsPlay() {
		By locator = By.xpath("//a[contains(text(),'Let')]");
		if(objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click lets play > >", objMobileActions.click(locator));
	}

	public void clickCookieContinueButton() {
		//this is to click on Continue button of cookies
		By btnContinue = By.xpath("//button[text()='Continue']");
		if(objMobileActions.checkElementDisplayedWithMidWait(btnContinue))
			logReporter.log("click continue button > >", objMobileActions.click(btnContinue));
	}
	
	public void scrollToSocialMediaBlock() {
		objMobileActions.androidScrollToElement(footer_SocialMediaBlock);
	}

	public void scrollToUsefulLinkBlock() {

		objMobileActions.androidScrollToElement(footer_UsefulLinkBlock);

	}

	public void scrollToLogoSection() {
		objMobileActions.androidScrollToElement(footer_PartnersLogoSection);

	}

	public void scrollToprivacyAndSecurityBlock() {
		objMobileActions.androidScrollToElement(footer_privacyAndSecurityBlock);

	}

	public void scrollToPaymentProvidersBlock() {
		objMobileActions.androidScrollToElement(footer_PaymentProvidersBlock);

	}

	public void clickOnLogInButton() {
		logReporter.log("click on 'LogIn button' > >", objMobileActions.click(loginButton));

	}

	public void verifyRegisterNowLink() {
		By registernowlink = By.xpath("//a[contains(text(),'Not a member')]");
		logReporter.log("Verify join now button", objMobileActions.checkElementDisplayed(registernowlink));
	}

	public void verifyHeaderJoinNowbtn() {
		By joinnowbtn = By.xpath("//a[contains(text(),'Join Now')]");
		logReporter.log("Verify join now button", objMobileActions.checkElementDisplayed(joinnowbtn));
	}

	public void verifybattenberg() {
		By battenberg = By.xpath("(//apollo-card-block)[1]");
		logReporter.log("verify batternberg block", objMobileActions.checkElementDisplayed(battenberg));
	}

	public void verifyBlockInOneRow() {
		By onerowblock = By.xpath("//section[contains(@class,'three-columns')]");
		objMobileActions.processMobileElement(onerowblock);
		logReporter.log("Verify block", objMobileActions.checkElementDisplayed(onerowblock));
	}

	public void verifyThreeBlock() {
		By blockone = By.xpath("//section[contains(@class,'three-columns')]//h4[contains(text(),'Help')]");// By.xpath("//section[4]/apollo-card-block[1]");
		By blocktwo = By.xpath("//section[contains(@class,'three-columns')]//h4[contains(text(),'Contact')]");// By.xpath("//section[4]/apollo-card-block[2]");
		By blockthree = By.xpath("//section[contains(@class,'three-columns')]//h4[contains(text(),'Community')]");// By.xpath("//section[4]/apollo-card-block[3]");
		logReporter.log("Verify block one", objMobileActions.checkElementDisplayed(blockone));
		logReporter.log("Verify block two", objMobileActions.checkElementDisplayed(blocktwo));
		logReporter.log("Verify block three", objMobileActions.checkElementDisplayed(blockthree));

	}

	public void clickOnHamburgerMenu() {
		By hamburgermenu = By.xpath("//div[contains(@class,'hamburger')]");
		logReporter.log("click on 'Hamburger Menu' > >", objMobileActions.click(hamburgermenu));
	}

	public void verifyHamburgerMenu() {
		By hamburgermenuheader = By.xpath("//*[contains(@class,'hamburger')]");
		logReporter.log("verify 'Hamburger Menu' > >", objMobileActions.checkElementDisplayed(hamburgermenuheader));
	}

	public void verifyClosedHamburgerMenu() {
		By hamburgermenuheader = By.xpath("//*[contains(@class,'hamburger')]");
		if (!objMobileActions.checkElementDisplayed(hamburgermenuheader))
			logReporter.log("verify 'Hamburger Menu' > >", true);
	}

	public void clickOnSearchIcon() {
		By search_loupe = By.xpath("//button[contains(@class,'search-open-btn')]");
		logReporter.log("click 'Search Loupe' > >", objMobileActions.click(search_loupe));
	}

	public void verifySearchMenu() {
		By search_loupe = By.xpath("//button[contains(@class,'search-open-btn')]");
		logReporter.log("click 'Search Loupe' > >", objMobileActions.checkElementDisplayed(search_loupe));
	}

	public void verifyClosedSearchMenu() {
		By searchmenu = By.xpath("//input[contains(@class,'search-input')]");
		if (!objMobileActions.checkElementDisplayed(searchmenu))
			logReporter.log("click 'Search Loupe' > >", true);
	}

	public void checkSurveyAndCloseItForPortrait() {
		By survey = By.xpath("//div[contains(@class,'sv__survey')]");
		By btnPlus = By.xpath("//div[contains(@class,'_plus-button')]");
		By btnClose = By.xpath("//button[contains(@class,'sv__btn-close')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(survey)) {
			if (objMobileActions.checkElementDisplayed(btnPlus))
				objMobileActions.click(btnPlus);
			if (objMobileActions.checkElementDisplayedWithMidWait(btnClose))
				objMobileActions.click(btnClose);
		}
	}

	public void checkSurveyAndCloseItForLandScape() {
		By survey = By.xpath("//div[contains(@class,'sv__survey')]");
		By btnClose = By.xpath("//button[contains(@class,'sv__btn-close')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(survey)) {
			if (objMobileActions.checkElementDisplayedWithMidWait(btnClose))
				objMobileActions.click(btnClose);
		}
	}
}
