package com.meccabingo.android.page;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobileErrorPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobileErrorPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void navigateToWrongURL() throws MalformedURLException {
		String strCurrentURL = objAppiumDriverProvider.getAppiumDriver().getCurrentUrl();
		strCurrentURL = strCurrentURL.replace("#login", "temp");
		// strCurrentURL = strCurrentURL + "/wrong/";
		System.out.println("Wrong URL  " + strCurrentURL);
		// URL url=new URL(strCurrentURL);

		objAppiumDriverProvider.getAppiumDriver().get("https://qa01-mecc-cms2.rankgrouptech.net/404");
		System.out.println("after URL  " + objAppiumDriverProvider.getAppiumDriver().getCurrentUrl());
	}

	public void errorDisplayed() {
		By error = By.xpath("//h1[contains(text(),'404 Error')]");
		logReporter.log("Check Error 404 > >", objMobileActions.checkElementDisplayedWithMidWait(error));
	}
}
