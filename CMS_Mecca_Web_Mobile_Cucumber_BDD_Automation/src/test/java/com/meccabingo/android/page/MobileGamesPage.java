package com.meccabingo.android.page;

import org.openqa.selenium.By;
import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;

public class MobileGamesPage {

	private MobileActions mobileActions;
	private LogReporter logReporter;
	private WaitMethods objWaitMethods;

	public MobileGamesPage(MobileActions mobileActions, WaitMethods waitMethods, LogReporter logReporter) {
		this.mobileActions = mobileActions;
		this.logReporter = logReporter;
		this.objWaitMethods = waitMethods;
	}

	// Normal Tile
	public void clickOnibtn(String nameofthegame) {
		By ibtn = By.xpath("//p[contains(text(),'" + nameofthegame
				+ "')]/ancestor::apollo-slot-tile/div[contains(@class,'game-panel-image')]/div[contains(@class,'game-panel-info')]/i");
		logReporter.log("click on 'i' > >", mobileActions.click(ibtn));
	}

	public void verifyBackroungimageInGame() {
		By bagroundimage = By.xpath("//*[contains(@id,'1073148485177582543')]"); // *[contains(@id,'Desktop')]/body/canvas
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(bagroundimage));
	}

	public void clickOnTitleoftheGame(String nameofthegame) {
		By titleofthegame = By.xpath("//p[contains(text(),'" + nameofthegame + "')]");
		if (!mobileActions.click(titleofthegame)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnImageOfTile(String nameofthegame) {
		By image = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/img");
		logReporter.log("click on 'image' > >", mobileActions.click(image));

	}

	public void verifyGameDetailPage() {
		By gameInfoBox = By.xpath("//section[contains(@class,'game-info-detail')]");
		logReporter.log("click on 'Game info page> >", mobileActions.checkElementDisplayed(gameInfoBox));
	}

	public void clickOnPrizeofTile() {
		By prizebtn = By.xpath("//p[contains(text(),'Turbo Gold')]/parent::div/div/div[1]");
		if (!mobileActions.click(prizebtn)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnDescriptionOfTile(String nameofthegame) {
		By description = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/p[2]");
		if (!mobileActions.click(description)) {
			logReporter.log("Check element", true);
		}
	}

	// Hero tile
	public void clickOnibtnOfHeroTile(String nameofthegame) {
		By ibtn = By.xpath("//p[contains(text(),'" + nameofthegame
				+ "')]/ancestor::apollo-slot-hero-tile/div[contains(@class,'game-panel-image')]/div[contains(@class,'game-panel-info')]/i");
		logReporter.log("click on 'i' > >", mobileActions.click(ibtn));
	}

	public void clickOnImageOfHerotile(String nameofthegame) {
		By image = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/img");
		if (!mobileActions.click(image)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnTitleOfHerotile(String nameofthegame) {
		By title = By.xpath("//p[contains(text(),'" + nameofthegame + "')]");
		if (!mobileActions.click(title)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnDescriptionOfHerotile(String nameofthegame) {
		By description = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/p[2]");
		if (!mobileActions.click(description)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnPrizeofHerotile() {
		By prizebtn = By.xpath("//p[contains(text(),'Turbo Gold')]/parent::div/div/div[1]");
		if (!mobileActions.click(prizebtn)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickAutoTabOfBingoGames() {

		By bingoGamesAutoTab = By.xpath("(//span[text()='Auto'])[1]");
		// mobileActions.androidScrollToElement(bingoGamesAutoTab);
		// objWaitMethods.waitForElementNotStale(mobileActions.processMobileElement(bingoGamesAutoTab),
		// 30);
		By selectObj = By.xpath("(//div[contains(@class,'ip-apollo-select-input')])[1]");
		mobileActions.click(selectObj);
		if (mobileActions.checkElementDisplayedWithMidWait(bingoGamesAutoTab))
			logReporter.log("Click Auto tab of bingo games >> ", mobileActions.click(bingoGamesAutoTab));
	}

	public void clickHeroTileImage() {
		By heroTileGameImage = By.xpath("(//apollo-bingo-tile//div[contains(@class,'image')])[1]");
		mobileActions.androidScrollToElement(heroTileGameImage);
		logReporter.log("Click hero tile image of bingo games >> ", mobileActions.click(heroTileGameImage));
	}

	public void navigateToHomePage() {
		By meccaLogo = By.xpath("(//img[contains(@src,'logo-mecca')])[1]");
		mobileActions.click(meccaLogo);
	}

	public void clickTitleOfBingoGame() {
		By titleOfBingoGame = By.xpath("(//apollo-bingo-tile//p[contains(@class,'title')])[1]");
		// mobileActions.androidScrollToElement(titleOfBingoGame);
		mobileActions.click(titleOfBingoGame);
	}

	public void bingoContainerSectionDisplayed() {
		By bingoContainerSection = By.xpath("//apollo-filter-heading[@title='Bingo']");
		logReporter.log("bingo container displayed > >", mobileActions.checkElementDisplayed(bingoContainerSection));
	}

	public void clickOniButtonOfBingoGame() {
		By iButtonOfBingoGame = By.xpath("//apollo-bingo-hero-tile//button[contains(@class,'game-panel-info')]");
		mobileActions.click(iButtonOfBingoGame);
	}

	public void clickTopFlagOfBingoGame() {
		By topFlagOfBingoGame = By.xpath("(//apollo-bingo-tile//div[contains(@class,'pot-flag')])[1]");
		mobileActions.androidScrollToElement(topFlagOfBingoGame);
		mobileActions.click(topFlagOfBingoGame);
	}

	public void scrollSlotGamesHorizantally() {
		By startElement = By.xpath("(//apollo-slot-tile)[1]");
		By endElement = By.xpath("(//apollo-slot-tile)[5]");

		mobileActions.androidScrollToElement(endElement);
		logReporter.log("game 5 displayed > >", mobileActions.checkElementDisplayed(endElement), true);

		mobileActions.androidScrollToElement(startElement);
		logReporter.log("game 1 displayed > >", mobileActions.checkElementDisplayed(startElement), true);

	}

	public void scrollBingoGamesHorizantally() {
		By startElement = By.xpath("(//apollo-bingo-tile)[1]");
		By endElement = By.xpath("(//apollo-bingo-tile)[3]");

		mobileActions.androidScrollToElement(endElement);
		logReporter.log("game 3 displayed > >", mobileActions.checkElementDisplayed(endElement), true);

		mobileActions.androidScrollToElement(startElement);
		logReporter.log("game 1 displayed > >", mobileActions.checkElementDisplayed(startElement), true);
	}

	public void clickOnDescriptionOfGame() {
		By desc = By.xpath("(//apollo-bingo-tile//div[contains(@class,'description')])[1]");
		mobileActions.androidScrollToElement(desc);
		mobileActions.click(desc);
	}

	public void clickOnPrize() {
		By prize = By.xpath("(//apollo-bingo-tile//div[contains(@class,'prize')])[1]");
		mobileActions.androidScrollToElement(prize);
		mobileActions.click(prize);
	}

	public void clickPreBuyOfFirstBingoGame() {
		By locator = By.xpath("(//button[contains(@class,'-pink')])[3]");
		mobileActions.processMobileElement(locator);
		mobileActions.androidScrollToElement(locator);
		if (mobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click Pre Buy> >", mobileActions.click(locator));
	}
}
