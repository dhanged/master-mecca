/**
 * 
 */
package com.meccabingo.android.page;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class MobileFooterPage {

	By linkTwitter = By.xpath("//a[contains(@rel,'Twitter')]"); // a[@title='Twitter']
	By linkFacebook = By.xpath("//a[@rel='Facebook']//img");
	By linkYouTube = By.xpath("//a[@rel='YouTube']");
	By linkInstagram = By.xpath("//a[@rel='Instagram']");

	By txtTwitter = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'Twitter')]"); // p[@class='footer-social-text']/span[contains(text(),'Twitter')]
	By txtFacebook = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'Facebook')]");
	By txtYouTube = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'YouTube')]");
	By txtInstagram = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'Instagram')]");

	By imageTwitter = By.xpath("//img[@alt='Twitter']");
	By imageFacebook = By.xpath("//img[@alt='Facebook']");
	By imageYouTube = By.xpath("//img[@alt='YouTube']");
	By imageInstagram = By.xpath("//img[@alt='Instagram']");

	By descriptionTwitter = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'Twitter')]//preceding-sibling::span"); // p[@class='footer-social-text']/span[contains(text(),'Twitter')]//preceding-sibling::span
	By descriptionFacebook = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'Facebook')]//preceding-sibling::span");
	By descriptionYouTube = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'YouTube')]//preceding-sibling::span");
	By descriptionInstagram = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'Instagram')]//preceding-sibling::span");

	By linkPrivacyPolicy = By.xpath("//a[contains(text(),'Privacy Policy')]"); // a[contains(text(),'Privacy Policy')]
	By linkTermsAOndConditions = By.xpath("//a[contains(text(),'Terms and Conditions')]");
	By linkOurMeccaPromise = By.xpath("//a[contains(text(),'Our Mecca Promise')]");
	By linkAffiliates = By.xpath("//a[contains(text(),'Affiliates')]");
	By linkMeccaClubTerms = By.xpath("//a[contains(text(),'Mecca Club Terms')]");
	By linkPlayOnlineCasino = By.xpath("//a[contains(text(),'Play Online Casino')]");
	By linkMeccaBlog = By.xpath("//a[contains(text(),'Mecca Blog')]");

	By logo_Essa = By.xpath("//a[@title='ESSA']//img");
	By logo_Ibsa = By.xpath("//a[@title='IBAS']//img");
	By logo_18 = By.xpath("//img[@alt='Over 18s only']");
	By logo_GamblingControl = By.xpath("//a[@title='Gambling Control']//img");
	By logo_GamCare = By.xpath("//a[@title='GamCare']//img");
	By logo_GamStop = By.xpath("//a[@title='GamStop']//img");
	By logo_GamblingCommission = By.xpath("//a[@title='Gambling Commission']//img");

	By paymentTitleBlock = By.xpath("//section[contains(@class,'footer-payments')]"); // div[@class='footer-payments-header']
	By paymentDescriptionBlock = By.xpath("//section[contains(@class,'footer-payments')]//preceding-sibling::h5"); // div[@class='footer-payments-subheader']
	By paymentLogosBlock = By.xpath("//section[contains(@class,'footer-payments')]//preceding-sibling::ul"); // ul[@class='footer-payments-list']

	By link_Alderney_Gambling_Control_Commission = By
			.xpath("//a[contains(text(),'Alderney Gambling Control Commission')]");
	By link_UK_Gambling_Commission = By.xpath("//a[contains(@title,'Gambling Commission')]");
	By link_BeGambleAware = By.xpath("//a[contains(@href,'begambleaware')]");
	By link_Rank_Group = By.xpath("//a[contains(text(),'Rank Group')]");

	private LogReporter logReporter;
	private AppiumDriverProvider objAppiumDriverProvider;
	private MobileActions objMobileActions;

	public MobileFooterPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {

		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void switchToChild() {
		objMobileActions.switchToChildWindow();
	}

	public void switchToParent() {
		objMobileActions.switchToParentWindow();
	}

	public void verifyUrl(String url) {
		logReporter.log("verify URL > >" + url, objAppiumDriverProvider.getAppiumDriver().getCurrentUrl(), url);
	}

	public void clickOnFacebook() {
		logReporter.log("click 'Facebook' > >", objMobileActions.click(linkFacebook));
		// logReporter.log("click 'Facebook' >
		// >",objMobileActions.clickUsingJS(linkFacebook));
		// logReporter.log("click 'Facebook' > >", true,
		// objMobileActions.clickUsingJS(linkFacebook));

	}

	public void clickOnInstagram() {
		logReporter.log("click 'Instagram' > >", objMobileActions.click(linkInstagram));
		// logReporter.log("click 'Instagram' >
		// >",objMobileActions.clickUsingJS(linkInstagram));
	}

	public void clickOnTwitter() {
		logReporter.log("click 'Twitter' > >", objMobileActions.click(linkTwitter));
	}

	public void clickOnYoutube() {

		logReporter.log("click 'Youtube' > >", objMobileActions.click(linkYouTube));
	}

	public void clickOnPrivacyPolicyLink() {
		if(objMobileActions.checkElementDisplayed(linkPrivacyPolicy))
		logReporter.log("click 'Privacy Policy Link' > >", objMobileActions.click(linkPrivacyPolicy));

	}

	public void clickOnTermsAndConditionsLink() {
		logReporter.log("click 'Terms And Conditions Link' > >", objMobileActions.click(linkTermsAOndConditions));
	}

	public void clickOnOurMeccaPromiseLink() {

		logReporter.log("click 'Our Mecca Promise Link' > >", objMobileActions.click(linkOurMeccaPromise));
	}

	public void clickOnAffiliatesLink() {
		logReporter.log("click 'Affiliates Link' > >", objMobileActions.click(linkAffiliates));
	}

	public void clickOnMeccaClubTermsLink() {
		logReporter.log("click 'Club Terms Link' > >", objMobileActions.click(linkMeccaClubTerms));
	}

	public void clickOnPlayOnlineCasinoLink() {
		logReporter.log("click 'Privacy Policy Link' > >", objMobileActions.click(linkPlayOnlineCasino));
	}

	public void clickOnMeccaBlogLink() {
		logReporter.log("click 'Play Online Casino Link' > >", objMobileActions.click(linkMeccaBlog));
	}

	public void verifyPartnersLogoDetails(String text) {
		switch (text) {
		case "ESSA": {
			objMobileActions.androidScrollToElement(logo_Essa);
			logReporter.log("Check ESSA Logo > >", objMobileActions.checkElementDisplayed(logo_Essa));
			break;
		}

		case "IBAS": {
			logReporter.log("Check IBAS logo > >", objMobileActions.checkElementDisplayed(logo_Ibsa));
			break;
		}

		case "18": {
			logReporter.log("Check '18' logo > >", objMobileActions.checkElementDisplayed(logo_18));
			break;
		}

		case "Gambling Control": {
			logReporter.log("Check Gambling Control logo > >",
					objMobileActions.checkElementDisplayed(logo_GamblingControl));
			break;
		}

		case "GamCare": {
			logReporter.log("Check GamCare logo > >", objMobileActions.checkElementDisplayed(logo_GamCare));
			break;
		}
		case "GamStop": {
			logReporter.log("Check GamStop Logo > >", objMobileActions.checkElementDisplayed(logo_GamStop));
			break;
		}
		case "Gambling Commission": {
			logReporter.log("Check Gambling Commision logo > >",
					objMobileActions.checkElementDisplayed(logo_GamblingCommission));
			break;
		}
		}

	}

	public void clickOnPartnersLogo(String option) {

		switch (option) {
		case "ESSA": {
			if(objMobileActions.checkElementDisplayed(logo_Essa))
			logReporter.log("Click ESSA Logo > >", objMobileActions.click(logo_Essa));
			break;
		}

		case "IBAS": {
			logReporter.log("Click IBAS logo > >", objMobileActions.click(logo_Ibsa));
			break;
		}

		case "18": {
			logReporter.log("Click 18 logo > >", objMobileActions.click(logo_18));
			break;
		}

		case "Gambling Control": {
			logReporter.log("Click Gambling Control logo > >", objMobileActions.click(logo_GamblingControl));
			break;
		}

		case "GamCare": {
			logReporter.log("Click GameCare logo> >", objMobileActions.click(logo_GamCare));
			break;
		}
		case "GamStop": {
			logReporter.log("Click GamStop logo> >", objMobileActions.click(logo_GamStop));
			break;
		}
		case "Gambling Commission": {
			logReporter.log("Click Gambling Commission logo> >", objMobileActions.click(logo_GamblingCommission));
			break;
		}
		}

	}

	public void verifyVeriSignLogo() {
		By logo_VeriSignSecured = By.xpath("//img[contains(@title,'Verisign')]");
		logReporter.log("Check VeriSign Secured Logo > >",
				objMobileActions.checkElementDisplayed(logo_VeriSignSecured));

	}

	public void verifyPaymentProvidersBlockInFooter(String text) {
		switch (text) {

		case "Title": {
			logReporter.log("Check Title block in payment providers > >",
					objMobileActions.checkElementDisplayed(paymentTitleBlock));
			break;
		}

		case "Description": {
			logReporter.log("Check Description block in payment providers > >",
					objMobileActions.checkElementDisplayed(paymentDescriptionBlock));
			break;
		}

		case "Logos": {
			logReporter.log("Check logo block in payment providers > >",
					objMobileActions.checkElementDisplayed(paymentLogosBlock));
			break;
		}
		}

	}

	public void click_Alderney_Gambling_Control_Commission_link() {

		logReporter.log("click 'Aderney Gambling Control Commision Link' > >",
				objMobileActions.click(link_Alderney_Gambling_Control_Commission));
	}

	public void click_UK_Gambling_Commission_link() {

		logReporter.log("click 'UK Gambling Commission Link' > >", objMobileActions.click(link_UK_Gambling_Commission));
	}

	public void click_BeGambleAware_link() {
		logReporter.log("click 'BeGambleAware Link' > >", objMobileActions.click(link_BeGambleAware));
	}

	public void click_Rank_Group_link() {
		logReporter.log("click 'Rank_Group Link' > >", objMobileActions.click(link_Rank_Group));

	}

	public void closeCurrentBrowser() {
		objAppiumDriverProvider.getAppiumDriver().close();
		logReporter.log("close browser window > >", true);
	}
}
