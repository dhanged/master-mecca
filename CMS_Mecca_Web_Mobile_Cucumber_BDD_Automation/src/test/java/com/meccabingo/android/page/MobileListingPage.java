package com.meccabingo.android.page;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;

public class MobileListingPage {
	MobileActions objMobileActions;
	private AppiumDriverProvider objAppiumDriverProvider;
	private LogReporter logReporter;

	public MobileListingPage(AppiumDriverProvider appiumDriverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objAppiumDriverProvider = appiumDriverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void navigateToWrongURL() throws MalformedURLException {
		String strCurrentURL = objAppiumDriverProvider.getAppiumDriver().getCurrentUrl();
		strCurrentURL = strCurrentURL.replace("#login", "temp");
		// strCurrentURL = strCurrentURL + "/wrong/";
		System.out.println("Wrong URL  " + strCurrentURL);
		// URL url=new URL(strCurrentURL);

		objAppiumDriverProvider.getAppiumDriver().get("https://np01-mecca-cms.rankgrouptech.net/404");
		System.out.println("after URL  " + objAppiumDriverProvider.getAppiumDriver().getCurrentUrl());
	}

	public void verifyComponents(String options) {
		switch (options) {

		case "Carousel":
			By locator1 = By.xpath("//*[@id='promoCarousel']");
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(locator1));
			break;

		case "Tabs":
			By locator2 = By.xpath("//div[contains(@class,'select-games')]");
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(locator2));
			break;

		case "Bingo Lobby CTA":
			By locator3 = By.xpath("//button[text()='Play Bingo Now']");
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(locator3));
			break;
		}
	}

	public void clickBingoTabFromHeader() {
		By locator = By.xpath("//section[contains(@class,'help-links')]//a[1]");
		logReporter.log("click bingo tab from header", objMobileActions.click(locator));
	}
}
