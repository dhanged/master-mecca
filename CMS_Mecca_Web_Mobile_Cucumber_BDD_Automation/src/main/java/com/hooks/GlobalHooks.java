package com.hooks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.generic.StepBase;

import com.generic.cucumberReporting.CustomFormatter;
import com.generic.utils.Configuration;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

/**
 * @ScriptName : Hooks
 * @Description : This class contains global hooks required for scenarios
 * @Author : Harhvardhan Yadav, Namrata Donikar (Expleo)
 * 
 */
public class GlobalHooks {
	private StepBase objStepBase;
	private Configuration objConfiguration;

	public GlobalHooks(StepBase stepBase, Configuration configuration) {
		this.objStepBase = stepBase;
		this.objConfiguration = configuration;
	}


	@Before
	public void applyHook(Scenario scenario) {
		System.out.println("Executing Cucumber Feature :- " + CustomFormatter.getCurrentFeatureName());
		System.out.println("Tags Applied :- " + scenario.getSourceTagNames());
		System.out.println("Scenario name :- " + scenario.getName());
		
		objConfiguration.loadConfigProperties();
		System.err.println("---Initializing environment---");
		if (objConfiguration.getConfig("mobile.android").equalsIgnoreCase("true")) {
			objStepBase.initializeAndroidEnvironment(scenario);

		} else if (objConfiguration.getConfig("mobile.ios").equalsIgnoreCase("true")) {
			objStepBase.initializeIOSEnvironment();
		} else {
			objStepBase.initializeEnvironment(scenario);
		}
	}

	@After
	public void removeHook(Scenario scenario) {
		if (objConfiguration.getConfig("mobile.android").equalsIgnoreCase("true")) {
			objStepBase.tearDownAppiumEnvironmentAndroid(scenario);
		} else if (objConfiguration.getConfig("mobile.ios").equalsIgnoreCase("true")) {
			objStepBase.tearDownAppiumEnvironmentIOS(scenario);
		} else
			objStepBase.tearDownEnvironment(scenario);
	}

}
