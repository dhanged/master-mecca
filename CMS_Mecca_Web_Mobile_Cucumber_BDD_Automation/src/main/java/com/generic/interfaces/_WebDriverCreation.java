package com.generic.interfaces;

import org.openqa.selenium.WebDriver;

import com.generic.utils.Configuration;

/**
 * WebDriver Creation
 * @author Harshvardhan Yadav(Expleo)
 */
public interface _WebDriverCreation{
	abstract WebDriver setWebDriver(Configuration objConfigurationo) throws Exception;
}
