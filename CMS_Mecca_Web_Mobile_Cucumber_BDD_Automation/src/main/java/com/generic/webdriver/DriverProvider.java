package com.generic.webdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.generic.utils.Configuration;
/**
 * used to initiate selenium webdriver for automation  
 * @author Harshvardhan Yadav(SQS)
 * @modified 	  :Harhvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class DriverProvider extends WebDriverFactory 
{
	// Local variables
 	private WebDriver webDriver;
	private WebDriverWait webDriverWait;
	private Configuration objConfiguration;
 
	public DriverProvider(Configuration configuration){
 		this.objConfiguration = configuration;  
	}

	/**
	 *  initialize selenium webdriver for automation
	 */
	public void initialize(){
		try
		{
			//objNetworkTraffic.startProxyServer();
	 	 	webDriver = this.setWebDriver(objConfiguration.getConfigProperties());
	 	 	webDriver.manage().deleteAllCookies();
			webDriver.manage().window().maximize();
			webDriver.manage().timeouts().implicitlyWait(Integer.parseInt(objConfiguration.getConfig("driver.implicitlyWait")), TimeUnit.SECONDS);
			webDriver.manage().timeouts().pageLoadTimeout(Integer.parseInt(objConfiguration.getConfig("driver.pageLoadTimeout")), TimeUnit.SECONDS);
			webDriverWait = new WebDriverWait(webDriver, Integer.parseInt(objConfiguration.getConfig("driver.WebDriverWait").trim()));
			//objNetworkTraffic.getProxyServer().enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
			//objNetworkTraffic.getProxyServer().newHar("FORD_AWS");
	 	}
		catch(Exception exception){
			exception.printStackTrace();
		}
	}

	/**
	 *  initialize selenium webdriver for automation
	 */
	public void tearDown(){
		try
		{
			//objNetworkTraffic.stopProxyServer();
	 		webDriver.quit();
		}
		catch(Exception exception){
			exception.printStackTrace();
		}
	}
	/**
	 * @return - webdriver instance
	 */
	public WebDriver getWebDriver(){
		return webDriver;
	}

	/**
	 * @return - generic webdriver wait instance
	 */
	public WebDriverWait getWebDriverWait(){
		return webDriverWait;
	}


}
