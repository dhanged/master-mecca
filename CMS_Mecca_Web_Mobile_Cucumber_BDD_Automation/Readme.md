# Mecca bingo automation
# Installation
1.  Java 1.8
2.  Eclipse Oxygen
3.  NodeJs
4.  Appium
5.  ADB
6.  Cisco any connect
7.	Android SDK

# setup environment variables
	JAVA_HOME=C:\Program Files\RedHat\java-1.8.0-openjdk-1.8.0.212-3
	ADB=<user's location>\Android\platform-tools
	ANDROID_HOME=<user's location>\AppData\Local\Android\sdk
	
# Import project as maven project from eclipse
	First download/update latest copy of the mecca automation project from bitbucket 
	1. Open Eclipse
	2. File -> click Import
	3. Select Maven
	4. Select Existing maven Projects
	5. Select root directory as ' ..\CMS_Mecca_Web_Mobile_Cucumber_BDD_Automation'
	6. Click Next
	Project is successfully imported
	
# Setup in eclipse
	1. Right click on project 
	2. Click Maven - > Installation - you will see build success message in console
	3. Again Right click on project
	3. Click Maven - > test - you will see build success message in console
	
	maven setup is completed successfully

# Run configurations for Android.
	1. open config.properties 
	2. set web.browser=chrome
	3. mobile.android=true
	4. Open AndroidRuuner.java and set tags ='@AndroidReg'
	
	* connect android phone through datacable to pc.
	a. Enable developer's options from android settings
	b. Enable USB debugging option from developer's options 
	
	* Run Configurations in eclipse
		1. Right click on Project
		2. Click New under Maven Build 
		3. Click Run Configurations
		4. On Run configurations dialog box 
		5. Populate Name:'Mac_Run'
		6. Set Goals: test
		7. Profiles as Android
		8. Click Apply
		for execution select saved run configuration it will execute the project.
		
		Similarly create configuration for 'Android_Rerun' to execute failed test cases again. For this you need to select profile
		as 'Android_Rerun'

# Run configurations for Desktop(Windows).
	1. open config.properties 
	2. set web.browser=chrome
	3. mobile.android=false
	4. mobile.ios=false
	5. Open AndroidRuuner.java and set tags ='@DesktopRegression'
	
	* Run Configurations in eclipse
		1. Right click on Project
		2. Click New under Maven Build 
		3. Click Run Configurations
		4. On Run configurations dialog box 
		5. Populate Name:'Desktop_Run'
		6. Set Goals: test
		7. Profiles as Android
		8. Click Apply
		for execution select saved run configuration it will execute the project.
		
		Similarly create configuration for 'Desktop_Rerun' to execute failed test cases again. For this you need to select profile
		as 'Desktop_Rerun'
	
# Run configurations for Mac. Below steps you need to perform on Mac machine.
	1. open config.properties 
	2. set web.browser=safari
	3. mobile.android=false
	4. mobile.ios=false
	5. Open MacRuuner.java and set tags ='@MACRegression'
	
	* Run Configurations in eclipse
		1. Right click on Project
		2. Click New under Maven Build 
		3. Click Run Configurations
		4. On Run configurations dialog box 
		5. Populate Name:'Mac_Run'
		6. Set Goals: test
		7. Profiles as Mac
		8. Click Apply
		for execution select saved run configuration it will execute the project.

# Installation for iOS.
	First install below components in mac os
	a. Java
	b. NodeJs
	
	1. Follow the steps to install appium v1.17.1
		$ npm install -g appium v1.17.1
	2. Install carthage
		$ brew install carthage
	3. Install webdriver
		$ npm install wd
	4. Install appium doctor
		$ npm install -g appium-doctor
	5. To install ios-webkit-debug-proxy run the below command.
		$ brew install ios-webkit-debug-proxy
	6. Configure Appium to run the app on iOS Simulator
		WebDriverAgent is a WebDriver server implementation for iOS that can be used to remote control iOS devices.
		We need to add an account to xCode (you can use your Apple Id or create new).
		For that go to xCode — Preferences — Accounts:
	7. We need to add Signing Certificate to this account:
		Click on Download Manual Profiles
		Click on Manage Certificates — Plus icon — Apple Development. Once it is done — you will see a new certificate added to the list.
	8. To make sure our certificate is placed — go to Keychain Access — Keys and check if this certificate is present.
	9. Open WebDriverAgent project in xCode.
		To find it please use the following path : /usr/local/lib/node_modules/appium/node_modules/appium-webdriveragent/WebDriverAgent.xcodeproj
		(NOTE: If you do not see /urs folder — please use shortkeys “Shift”+”Command”+”.” to display hidden files in your Macintosh HD root)
	10. Build the webdriveragent project by providing the necessary parameters of xcode project
	
	for more details go through the below URL.
	https://www.swtestacademy.com/how-to-install-appium-on-mac/                                                                                https://github.com/isonic1/appium-workshop/blob/master/Appium%20Mac%20Installation%20Instructions.md
	


		
# Run configurations for iOS.
	1. open config.properties 
	2. set web.browser=chrome
	3. mobile.android=false
	4. mobile.ios=true
	5. Open iOSRuuner.java and set tags ='@iOSRegression'
	6. Enter the details of iPad in src\main\resources\mobileResources\devices\iOS\iPad_Air.properties as shown below.
		device.udid=<udid of your device>
		device.name=<udid of your device>
		device.platformName=ios
		device.platformVersion=<OS version> e.g. 13.1
		device.appium.ip=<ip address on which command line appium server is running>
		device.appium.port=<port number on which command line server is running>
		device.browser=safari
	
	7. start the command line appium server
	
	* Run Configurations in eclipse
		1. Right click on Project
		2. Click New under Maven Build 
		3. Click Run Configurations
		4. On Run configurations dialog box 
		5. Populate Name:'iOS_Run'
		6. Set Goals: test
		7. Profiles as iOS
		8. Click Apply
		for execution select saved run configuration it will execute the project.

	
# Test execution reports
	Test execution report is generated at below location:
	For Android:
		.\target\Cucumber_Report\Android
	For Desktop:
		.\target\Cucumber_Report\Desktop
	For Mac:
		.\target\Cucumber_Report\Mac
	For iOS:
		.\target\Cucumber_Report\iOS
		
	Similarly you can find for corresponding Rerun folder for failed test cases.
	
	in that folder open the file 'cucumber-html-reports\overview-features.html'
	